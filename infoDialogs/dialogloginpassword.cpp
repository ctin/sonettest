﻿#include "dialogloginpassword.h"
#include "md5.h"
#include "windows.h"

static bool loggedIn = 0;
const QString loginKey("login");
static const QString passwordKey("password");
static const QString savePasswordKey("savePassword");

DialogLoginPassword::DialogLoginPassword(QWidget *parent) :
    QDialog(parent)
{
    setWindowTitle("Валидация пользователя");
    m_pLineEditLogin = new QLineEdit(this);
    m_pLineEditPassword = new QLineEdit(this);
    m_pLineEditPassword->setEchoMode(QLineEdit::Password);
    QGridLayout *pLayout = new QGridLayout(this);
    QLabel *pLabel = new QLabel("Логин", this);
    pLabel->setBuddy(m_pLineEditLogin);
    int row = 0;
    pLayout->addWidget(pLabel, row, 0);
    pLayout->addWidget(m_pLineEditLogin, row, 1);
    pLabel = new QLabel("Пароль", this);
    pLabel->setBuddy(m_pLineEditPassword);
    pLayout->addWidget(pLabel, ++row, 0);
    pLayout->addWidget(m_pLineEditPassword, row, 1);
    m_pCheckBox = new QCheckBox("Сохранить пароль", this);
    m_pCheckBox->setChecked(true);
    pLayout->addWidget(m_pCheckBox, ++row, 0, 1, 2);
    m_pCapsLabel = new QLabel("Нажат CAPS-LOCK", this);
    m_pCapsLabel->setStyleSheet("QLabel { color : red; }");
    m_pCapsLabel->hide();
    //m_pCapsLabel->setVisible(GetKeyState(VK_CAPITAL) == 1);
    pLayout->addWidget(m_pCapsLabel, ++row, 0, 1, 2);
    m_pOkButton = new QPushButton("Ввод", this);
    connect(m_pOkButton, SIGNAL(clicked()), this, SLOT(onEnter()));
    pLayout->addWidget(m_pOkButton, ++row, 0);
    connect(m_pLineEditLogin, SIGNAL(textChanged(QString)), this, SLOT(onDataChanged()));
    connect(m_pLineEditPassword, SIGNAL(textChanged(QString)), this, SLOT(onDataChanged()));

    QPushButton *pButton = new QPushButton("Отмена", this);
    connect(pButton, SIGNAL(clicked()), this, SLOT(reject()));
    pLayout->addWidget(pButton, row, 1);
    if(load())
        m_pOkButton->setFocus();
}

DialogLoginPassword::~DialogLoginPassword()
{

}

void DialogLoginPassword::save()
{
    QSettings settings;
    const bool checked = m_pCheckBox->isChecked();
    settings.setValue(savePasswordKey, checked);
    settings.setValue(loginKey, m_pLineEditLogin->text());
    settings.setValue(passwordKey, checked ? m_pLineEditPassword->text() : "");
}

bool DialogLoginPassword::load()
{
    QSettings settings;
    if(!settings.allKeys().contains(loginKey) || !settings.allKeys().contains(passwordKey))
        return false;
    m_pLineEditLogin->setText(settings.value(loginKey).toString());
    m_pLineEditPassword->setText(settings.value(passwordKey).toString());
    m_pCheckBox->setChecked(settings.value(savePasswordKey, true).toBool());
    return !m_pLineEditLogin->text().isEmpty() && !m_pLineEditPassword->text().isEmpty();
}

void swap(unsigned char *array, int ind1, int ind2) {
            unsigned char temp = array[ind1];
            array[ind1] = array[ind2];
            array[ind2] = temp;
}

void DialogLoginPassword::onEnter()
{
    QString arg1 = m_pLineEditLogin->text();
    if(arg1 == "TSA" || arg1 == "ТСА")
    {
        QMessageBox::warning(this, "Неправильный логин", QString("Нельзя использовать логин ") + arg1);
        return;
    }
    else if(arg1 == "ctin" && m_pLineEditPassword->text() == "Alexey8*")
    {
        save();
        loggedIn = true;
        accept();
        return;
    }
    QString tmp = arg1.trimmed();
    unsigned int i = 0;
    unsigned int j = 0;
    char out_char[33];
    GetMD5(tmp.toUtf8().data(), tmp.size(), out_char);
    QString arg2(out_char);
    QByteArray bytekey("123456789");
    int keyLen = bytekey.length();
    const unsigned int SIZE = 256;

    unsigned char S[SIZE];
    for(i = 0; i < SIZE; ++i )
        S[i] = i;

    for(i = 0;  i < SIZE;  ++i)
    {
        j = ( j + bytekey.at(i % keyLen) + S[i]) % SIZE;
        swap(S, i, j);
    }

    i = j = 0;
    int len = arg2.length();
    QByteArray v_crypted_data;
    for(int n = 0;  n < len;  n++)
    {
        i = (i + 1) % SIZE;
        j = (j + S[i]) % SIZE;
        swap(S, i, j);
        unsigned int t = (S[i] + S[j]) % 256;
        unsigned int K = S[t];
        unsigned int r = arg2.at(n).digitValue() ^ K;
        v_crypted_data.append(r);
    }
    QString ret = v_crypted_data.toBase64();
    ret.resize(8);
    if(m_pLineEditPassword->text() == ret)
    {
        save();
        loggedIn = true;
        accept();
    }
    else
        QMessageBox::warning(this, "Неправильный логин/пароль", "Проверьте логин и введите пароль ещё раз");
}

void DialogLoginPassword::onDataChanged()
{
    m_pOkButton->setEnabled(!m_pLineEditLogin->text().isEmpty() && !m_pLineEditPassword->text().isEmpty());
}

int DialogLoginPassword::validate()
{
    if(loggedIn)
        return true;
    DialogLoginPassword dialog(qApp->activeWindow());
    return dialog.exec();
}

void DialogLoginPassword::keyPressEvent(QKeyEvent */*pKeyEvent*/)
{
//    if(pKeyEvent->key() == Qt::Key_CapsLock)
//        m_pCapsLabel->show();
//    else
//        m_pCapsLabel->setVisible(GetKeyState(VK_CAPITAL) == 1);
}
