﻿#include "dialogcomsettings.h"

#include "serialport.h"
#include "sonetdevice.h"

DialogCOMSettings::DialogCOMSettings(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
    setLayout(gridLayout);
    gridLayout->setMargin(10);
    QRegExp rxlen("(\\d+)");
    int pos = rxlen.indexIn(g_defaultPort);
    if(pos > -1)
    {
        spinBox_portNum->setValue(rxlen.cap().toInt());
    }
    else
    {
        spinBox_portNum->setValue(1);
        g_defaultPort = 1;
    }

    pos = rxlen.indexIn(g_defaultPortICSU);
    if(pos > -1)
    {
        spinBox_portNumICSU->setValue(rxlen.cap().toInt());
    }
    else
    {
        spinBox_portNumICSU->setValue(1);
        g_defaultPortICSU = 1;
    }
    comboBox_speeds->addItems(SerialPort::createSpeedListStr());
    comboBox_parity->addItems(SerialPort::createParityList());
    comboBox_flow->addItems(SerialPort::createFlowList());

    checkBox_autoSearch->setChecked(g_autoSearchEnabled);
    doubleSpinBox_stopBits->setValue(g_defaultStopBits);
    spinBox_dataBits->setValue(g_defaultDataBits);
    comboBox_parity->setCurrentIndex(g_defaultParityIndex);
    comboBox_speeds->setCurrentIndex(g_defaultRateIndex);
    comboBox_flow->setCurrentIndex(g_defaultFlowIndex);

    connect(spinBox_portNum, SIGNAL(valueChanged(int)), this, SLOT(setdefaultPort()));
    connect(comboBox_parity, SIGNAL(activated(int)), this, SLOT(setdefaultParityIndex()));
    connect(comboBox_speeds, SIGNAL(activated(int)), this, SLOT(setdefaultRateIndex()));
    connect(checkBox_autoSearch, SIGNAL(toggled(bool)), this, SLOT(setautoSearchEnabled()));
    connect(doubleSpinBox_stopBits, SIGNAL(valueChanged(double)), this, SLOT(setdefaultStopBits()));
    connect(spinBox_dataBits, SIGNAL(valueChanged(int)), this, SLOT(setdefaultDataBits()));
    connect(comboBox_flow, SIGNAL(activated(int)), this, SLOT(setdefaultFlowIndex()));
    connect(spinBox_additionalWaitTime, SIGNAL(valueChanged(int)), this, SLOT(setadditionalWaitTime()));
    connect(spinBox_portNumICSU, SIGNAL(valueChanged(int)), this, SLOT(setdefaultPortICSU()));
}

void DialogCOMSettings::setdefaultPort()
{
    g_defaultPort = QString("COM%1").arg(spinBox_portNum->value());
}

void DialogCOMSettings::setdefaultPortICSU()
{
    g_defaultPortICSU = QString("COM%1").arg(spinBox_portNumICSU->value());
}

void DialogCOMSettings::setdefaultParityIndex()
{
    g_defaultParityIndex = comboBox_parity->currentIndex();
}

void DialogCOMSettings::setdefaultRateIndex()
{
    g_defaultRateIndex = comboBox_speeds->currentIndex();
}

void DialogCOMSettings::setautoSearchEnabled()
{
    g_autoSearchEnabled = checkBox_autoSearch->isChecked();
}

void DialogCOMSettings::setdefaultStopBits()
{
    g_defaultStopBits = doubleSpinBox_stopBits->value();
}

void DialogCOMSettings::setdefaultDataBits()
{
    g_defaultDataBits = spinBox_dataBits->value();
}

void DialogCOMSettings::setdefaultFlowIndex()
{
    g_defaultFlowIndex = comboBox_flow->currentIndex();
}

void DialogCOMSettings::setadditionalWaitTime()
{
    g_additionalWaitTime = spinBox_additionalWaitTime->value();
}
