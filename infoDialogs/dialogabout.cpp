﻿#include "dialogabout.h"
#include "ui_dialogabout.h"
#include <QDate>
#include <QPushButton>

DialogAbout::DialogAbout(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAbout)
{
    ui->setupUi(this);
    QString preferencesLink = QString("<a href=\"http://www.ezan.ac.ru/\">%1</a>").arg(tr("http://www.ezan.ac.ru/"));
    setModal(true);
    ui->label_7->setText(preferencesLink);
    ui->label_7->setTextInteractionFlags(Qt::LinksAccessibleByKeyboard | Qt::LinksAccessibleByMouse);
    ui->label_7->setOpenExternalLinks(1);
    ui->label_7->openExternalLinks();
    ui->label_kyni->setText(QString("КУНИ.501614.001-%1").arg(VERSION));
    ui->label_version->setText(QString("Версия программы: <b>%1</b>").arg(VERSION));
    ui->label_7->setWhatsThis("Нажмите чтоб перейти на сайт завода-изготовителя.");
    ui->gridLayout->setMargin(10);
    QDate date = QLocale(QLocale::C).toDate(QString(__DATE__).simplified(), QLatin1String("MMM d yyyy"));
    QString result = "Дата изготовления: <b>" + date.toString("d MMM yyyy") + "</b>";
    ui->label_date->setText(result);
    ui->label_time->setText("Время изготовления: <b>"__TIME__"</b>");
    setLayout(ui->gridLayout);
    QPushButton *pButton = new QPushButton("Закрыть", this);
    connect(pButton, SIGNAL(clicked()), this, SLOT(accept()));
    ui->gridLayout->addWidget(pButton, ui->gridLayout->rowCount(), 0, 1, ui->gridLayout->columnCount(), Qt::AlignCenter);
} 
 
DialogAbout::~DialogAbout() 
{
    delete ui; 
}
