﻿#ifndef DIALOGLOGINPASSWORD_H
#define DIALOGLOGINPASSWORD_H

#include <QtWidgets>
#include <QtGui>
#include <QtCore>

extern const QString loginKey;

class DialogLoginPassword : public QDialog
{
    Q_OBJECT
    
public:
    explicit DialogLoginPassword(QWidget *parent = 0);
    ~DialogLoginPassword();
    static int validate();
private:
    QLineEdit *m_pLineEditLogin, *m_pLineEditPassword;
    QPushButton *m_pOkButton;
    QCheckBox *m_pCheckBox;
    QLabel *m_pCapsLabel;
    void save();
    bool load();
private slots:
    void onEnter();
    void onDataChanged();
protected:
    void keyPressEvent(QKeyEvent *pKeyEvent);
};

#endif // DIALOGLOGINPASSWORD_H
