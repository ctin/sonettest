#ifndef DIALOGMODULESDATAFORREPORT_H
#define DIALOGMODULESDATAFORREPORT_H

#include <QDialog>
#include "dialogreportfactory.h"
class DialogModulesDataForReport : public QDialog
{
    Q_OBJECT
public:
    explicit DialogModulesDataForReport(DeviceWithData *pDevice, QMap<int, ModuleData> *pModuleData, QWidget *parent = 0);
    ~DialogModulesDataForReport();
    QMap<int, ModuleData> *m_pModuleData;
    DeviceWithData *m_pDeviceWithData;
    QVector<QSpinBox*>  m_spinBoxesNum;
    QVector<QComboBox*> m_comboboxesTypes;
    QVector<QLineEdit*> m_lineeditsNames;
    QVector<QCheckBox*> m_checkBoxesExtView;
    QVector<QCheckBox*> m_checkBoxesTested;
signals:
    
public slots:
    void loadData();
    void saveData();
};

#endif // DIALOGMODULESDATAFORREPORT_H
