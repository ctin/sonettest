﻿#include "dialogmodbuslinereport.h"
#include "macro.h"
#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtPrintSupport/QtPrintSupport>

void ModbusLineData::clear()
{
    m_nodesErr.clear(); m_nodesFoundedRes.clear(); m_nodesFoundedNoRes.clear();
}

DialogMODBUSLineReport::DialogMODBUSLineReport(ModbusLineData *pModbusLineData, QWidget *parent) :
    QDialog(parent), m_pModbusLineData(pModbusLineData)
{
    setupUi(this);
    pushButton_print->setIcon(QIcon(":/content/printer.ico"));
    textEdit->setFontFamily("Arial");
    setWindowTitle(QString("Создание отчета о проверке линии связи"));
    setLayout(gridLayout);
    lineEdit_name->setText(m_pModbusLineData->m_name);
    QString text;
    foreach(int num, m_pModbusLineData->m_nodesRes)
        text.append(QString::number(num) + " ");
    lineEdit_numRes->setText(text);
    text.clear();
    foreach(int num, m_pModbusLineData->m_nodesNoRes)
        text.append(QString::number(num) + " ");
    lineEdit_numNoRes->setText(text);

    connect(lineEdit_FIO,       SIGNAL(textChanged(QString)), this, SLOT(refreshDocument()));
    connect(lineEdit_name,      SIGNAL(textChanged(QString)), this, SLOT(refreshDocument()));
    connect(lineEdit_numNoRes,  SIGNAL(textChanged(QString)), this, SLOT(refreshDocument()));
    connect(lineEdit_numRes,    SIGNAL(textChanged(QString)), this, SLOT(refreshDocument()));
    connect(this,               SIGNAL(htmlTextChanged(QString)), this, SLOT(refreshTextEdit(QString)));

    refreshDocument();
}

void DialogMODBUSLineReport::refreshDocument()
{//93-88
    int pos = 0;
    QRegExp rx("\\d+");
    m_pModbusLineData->m_nodesRes.clear();
    QString text = lineEdit_numRes->text();
    while ((pos = rx.indexIn(text, pos)) != -1) {
        m_pModbusLineData->m_nodesRes.append(rx.cap().toInt());
        pos += rx.matchedLength();
    }
    pos = 0;
    m_pModbusLineData->m_nodesNoRes.clear();
    text = lineEdit_numNoRes->text();
    while ((pos = rx.indexIn(text, pos)) != -1) {
        m_pModbusLineData->m_nodesNoRes.append(rx.cap().toInt());
        pos += rx.matchedLength();
    }
    QString currentHtmlText;
    bool isValid = false;
    currentHtmlText.append("<HTML><HEAD>\n");
    currentHtmlText.append("<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=Windows-1251\">\n");
    currentHtmlText.append("<META NAME=\"Language\" CONTENT=\"ru\">\n");
    currentHtmlText.append("</HEAD>\n");
    currentHtmlText.append("<BODY>\n");
    currentHtmlText.append("<center>\n");

    currentHtmlText.append(lineEdit_name->text() + QString("<p>\n"));
    currentHtmlText.append("<p>\n");
    currentHtmlText.append("</center>\n");

    currentHtmlText.append(QString("<LI> Узлы с резервом: <U>&nbsp;&nbsp;"));
    foreach(int num, m_pModbusLineData->m_nodesRes)
        currentHtmlText.append(QString::number(num) + "&nbsp;");
    currentHtmlText.append("&nbsp;&nbsp;</U>");
    currentHtmlText.append(QString("<LI> Узлы без резерва: <U>&nbsp;&nbsp;"));
    foreach(int num, m_pModbusLineData->m_nodesNoRes)
        currentHtmlText.append(QString::number(num) + "&nbsp;");
    currentHtmlText.append("</U><p>\n");
    if(!m_pModbusLineData->m_nodesErr.isEmpty())
    {
        currentHtmlText.append(QString("<LI> Узлы, ответившие ошибкой: <U>&nbsp;&nbsp;"));
        foreach(int num, m_pModbusLineData->m_nodesErr)
            currentHtmlText.append(QString::number(num) + "&nbsp;");
        currentHtmlText.append("&nbsp;&nbsp;</U><p>\n</OL>\n<p>\n");
    }

    int notFoundedRes = 0;
    foreach(int num, m_pModbusLineData->m_nodesRes)
        if(!m_pModbusLineData->m_nodesFoundedRes.contains(num))
            notFoundedRes++;
    if(notFoundedRes)
    {
        currentHtmlText.append(QString("<LI> Узлов с резервом не обнаружено: %1<p>\n <U>&nbsp;&nbsp;").arg(notFoundedRes));
        foreach(int num, m_pModbusLineData->m_nodesRes)
            if(!m_pModbusLineData->m_nodesFoundedRes.contains(num))
                currentHtmlText.append(QString::number(num) + "&nbsp;");
        currentHtmlText.append("&nbsp;&nbsp;</U><p>\n</OL>\n<p>\n");
    }
    int notFoundedNoRes = 0;
    foreach(int num, m_pModbusLineData->m_nodesNoRes)
        if(!m_pModbusLineData->m_nodesFoundedNoRes.contains(num))
            notFoundedNoRes++;
    if(notFoundedNoRes)
    {
        currentHtmlText.append(QString("<LI> Узлов без резерва не обнаружено: %1<p>\n <U>&nbsp;&nbsp;").arg(notFoundedNoRes));
        foreach(int num, m_pModbusLineData->m_nodesNoRes)
            if(!m_pModbusLineData->m_nodesFoundedNoRes.contains(num))
                currentHtmlText.append(QString::number(num) + "&nbsp;");
        currentHtmlText.append("&nbsp;&nbsp;</U><p>\n</OL>\n<p>\n");
    }


    int overFoundedRes = 0;
    foreach(int num, m_pModbusLineData->m_nodesFoundedRes)
        if(!m_pModbusLineData->m_nodesRes.contains(num))
            overFoundedRes++;
    if(overFoundedRes)
    {
        currentHtmlText.append(QString("<LI> Лишних узлов с резервом обнаружено: %1<p>\n <U>&nbsp;&nbsp;").arg(overFoundedRes));
        foreach(int num, m_pModbusLineData->m_nodesFoundedRes)
            if(!m_pModbusLineData->m_nodesRes.contains(num))
                currentHtmlText.append(QString::number(num) + "&nbsp;");
        currentHtmlText.append("&nbsp;&nbsp;</U><p>\n</OL>\n<p>\n");
    }
    int overFoundedNoRes = 0;
    foreach(int num, m_pModbusLineData->m_nodesFoundedNoRes)
        if(!m_pModbusLineData->m_nodesNoRes.contains(num))
            overFoundedNoRes++;
    if(overFoundedNoRes)
    {
        currentHtmlText.append(QString("<LI> Лишних узлов без резерва обнаружено: %1<p>\n <U>&nbsp;&nbsp;").arg(overFoundedNoRes));
        foreach(int num, m_pModbusLineData->m_nodesFoundedNoRes)
            if(!m_pModbusLineData->m_nodesNoRes.contains(num))
                currentHtmlText.append(QString::number(num) + "&nbsp;");
        currentHtmlText.append("&nbsp;&nbsp;</U><p>\n</OL>\n<p>\n");
    }
    isValid = m_pModbusLineData->m_nodesErr.isEmpty() && !notFoundedRes && !notFoundedNoRes
            && !overFoundedRes && !overFoundedNoRes;
    QDateTime currentDateTime = QDateTime::currentDateTime();
    static const QStringList monthes(QStringList() << "Января"
                                     << "Февраля"
                                     << "Марта"
                                     << "Апреля"
                                     << "Мая"
                                     << "Июня"
                                     << "Июля"
                                     << "Августа"
                                     << "Сентября"
                                     << "Октября"
                                     << "Ноября"
                                     << "Декабря");

    currentHtmlText.append(QString("Дата поверки: <U>&#171;&nbsp;%1&nbsp;&#187;&nbsp;%2&nbsp;&nbsp;%3&nbsp;г</U>\n")
                    .arg(currentDateTime.date().day())
                    .arg(monthes.at(currentDateTime.date().month() - 1))
                    .arg(currentDateTime.date().year()));

    currentHtmlText.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n");
    currentHtmlText.append(QString("Результат поверки: <U>&nbsp;&nbsp;<b>%1</b>&nbsp;&nbsp;</U><br><p>\n")
                    .arg(isValid ? "всё ОК" : "есть ошибки"));

    currentHtmlText.append("<TABLE CELLPADDING=0 CELLSPACING=0>\n");
    currentHtmlText.append("<TR>\n");
    currentHtmlText.append("<TD>Поверку провел </TD>\n");
    currentHtmlText.append("<TD>&nbsp;&nbsp;&nbsp;</TD>\n");
    currentHtmlText.append("<TD ALIGN=CENTER VALIGN=BOTTOM><U>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</U></TD>\n");
    currentHtmlText.append("<TD>&nbsp;&nbsp;&nbsp;</TD>\n");
    QString fio = lineEdit_FIO->text();
    if(fio.isEmpty())
        fio = "<U>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</U>";
    else
    {
        fio.prepend("<U>&nbsp;&nbsp;&nbsp;");
        fio.append("&nbsp;&nbsp;&nbsp;</U>");
    }
    currentHtmlText.append(QString("<TD ALIGN=CENTER VALIGN=BOTTOM>%1</TD>\n").arg(fio));
    currentHtmlText.append("</TR>\n");
    currentHtmlText.append("<TR>\n");
    currentHtmlText.append("<TD></TD>\n");
    currentHtmlText.append("<TD></TD>\n");
    currentHtmlText.append("<TD ALIGN=CENTER VALIGN=TOP><FONT SIZE=-4>Подпись</FONT></TD>\n");
    currentHtmlText.append("<TD></TD>\n");
    currentHtmlText.append("<TD ALIGN=CENTER VALIGN=TOP><FONT SIZE=-4>Расшифровка подписи</FONT></TD>\n");
    currentHtmlText.append("</TR>\n");
    currentHtmlText.append("</TABLE>\n");
    currentHtmlText.append("</BODY>\n");
    sethtmlText(currentHtmlText);
}

void DialogMODBUSLineReport::on_pushButton_toHTML_clicked()
{
    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Сохранить файл"), "", tr("html (*.html)"));
    if(saveFileName.isEmpty())
        return;
    QFile file(saveFileName);
    if(!file.open(QIODevice::WriteOnly))
        return;
    QTextStream out(&file);
    out.setCodec("CP1251");
    out << htmlText();
    file.close();
}

void DialogMODBUSLineReport::refreshTextEdit(QString txt)
{
    textEdit->setHtml(txt);
    textEdit->toHtml();
}

void DialogMODBUSLineReport::on_pushButton_toPDF_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Сохранить файл в PDF"), "", tr("pdf (*.pdf)"));
        if (!fileName.isEmpty())
        {
            if (QFileInfo(fileName).suffix().isEmpty())
                fileName.append(".pdf");
            QPrinter printer(QPrinter::ScreenResolution);
            printer.setPaperSize(QPrinter::A4);
            printer.setOutputFormat(QPrinter::PdfFormat);
            printer.setOutputFileName(fileName);
            printer.setFullPage(true);
            printer.setOrientation(QPrinter::Landscape);
            textEdit->document()->setPageSize(QSize(printer.width(), printer.height()));
            textEdit->document()->print(&printer);
        }
}

void DialogMODBUSLineReport::on_pushButton_print_clicked()
{
     QPrinter printer(QPrinter::ScreenResolution);
     printer.setOrientation(QPrinter::Landscape);
     printer.setPaperSize(QPrinter::A4);

     QPrintDialog dialog(&printer, this);
     dialog.setWindowTitle(tr("Печать документа"));
     if (textEdit->textCursor().hasSelection())
         dialog.addEnabledOption(QAbstractPrintDialog::PrintSelection);
     if (dialog.exec() != QDialog::Accepted)
         return;
     textEdit->document()->setPageSize(QSize(printer.width(), printer.height()));
     textEdit->print(&printer);
}

PROPERTY_CPP(QString, DialogMODBUSLineReport, htmlText)

