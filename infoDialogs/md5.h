﻿#include <windef.h>
#include <stdio.h>
#include <string>

#ifdef __alpha  
typedef unsigned int UINT4;  
#else  
typedef unsigned long int UINT4;  
#endif

#define MD5_INIT_STATE_0 0x65432109
#define MD5_INIT_STATE_1 0xefcdab75
#define MD5_INIT_STATE_2 0x52bacdfe
#define MD5_INIT_STATE_3 0x12345678

void MD5Init(void);  
void MD5Update(unsigned char *bug, unsigned int len);  
void MD5Final(char* cReturnStr);  
void Transform(UINT4 *buf, UINT4 *in);  
void GetMD5(const char* pBuf, UINT nLength,char* cReturnStr);
std::string MD5(std::string in);
