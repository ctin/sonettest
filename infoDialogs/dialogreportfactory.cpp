﻿#include <QtCore>
#include <QObject>
#include "dialogreportfactory.h"
#include "macro.h"
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>
#include "dialogmodulesdataforreport.h"
#include <QSettings>

PROPERTY_CPP(QString, DialogReportFactory, htmlText)

static const QString allModules("ReportallModules");
static const QString controllerTypeKey("ReportControllerType");
static const QString controllerKYNItypeKey("ReportControllerKYNItype");

static const QString temperatureKey("ReportTemperature");
static const QString humidityKey("ReportHumidity");
static const QString pressureKey("ReportPressure");
static const QString calibrNameKey("ReportCalibrName");
static const QString calibrNumKey("ReportCalibrNum");
static const QString fioKey("ReportFIO");

ModuleData::ModuleData()
{
    moduleNum = 0;
    collected = false;
    extView = false;
    tested = false;
}

DialogReportFactory::DialogReportFactory(const bool input, DeviceWithData *pDevice, const int currentSlot, const ResultController *pResults, QMap<int, QString> selectedCalibrators, const VerifyType type, QWidget *parent)
    : QDialog(parent), m_pResults(pResults), m_pDevice(pDevice), m_selectedCalibrators(selectedCalibrators), m_currentSlot(currentSlot), m_type(type), m_input(input)
{
    setupUi(this);
    setWindowTitle(QString("Создание отчета ") + QString(m_type == InputOTK || m_type == OutputOTK ? "по ТУ" : "ЦЛИТ"));
    setLayout(gridLayout);
    textEdit->setFontFamily("Arial");
    pushButton_print->setIcon(QIcon(":/content/printer.ico"));

    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        QVector<qreal> points = g_parameterStorage.getPoints(getID(slot));
        int pointsCount = points.size();
        ModuleData moduleData;
        if(g_parameterStorage.getCalibrators(getID(slot)).contains(m_selectedCalibrators.value(slot)))
            moduleData.calibratorType = m_selectedCalibrators[slot];
        moduleData.moduleNum = slot;
        for(int chan = 0;  chan < m_pResults->at(slot).size() && !moduleData.collected ;  chan++)
            moduleData.collected = m_pResults->at(slot).at(chan).size() == pointsCount;
        m_moduleData.insert(slot, moduleData);
    }

    loadData();
    connect(checkBox_allModules,    SIGNAL(toggled(bool)), this, SLOT(refreshDocument()));
    connect(lineEdit_reportNum,     SIGNAL(textChanged(QString)), this, SLOT(refreshDocument()));
    connect(spinBox_slotNum,        SIGNAL(valueChanged(int)), this, SLOT(refreshDocument()));
    connect(comboBox_controllerType,    SIGNAL(currentIndexChanged(int)), this, SLOT(refreshDocument()));
    connect(lineEdit_controllerKYNItype,SIGNAL(textChanged(QString)), this, SLOT(refreshDocument()));
    connect(lineEdit_controllerNum,  SIGNAL(textChanged(QString)), this, SLOT(refreshDocument()));
    connect(spinBox_temperature,    SIGNAL(valueChanged(int)), this, SLOT(refreshDocument()));
    connect(spinBox_humidity,       SIGNAL(valueChanged(int)), this, SLOT(refreshDocument()));
    connect(spinBox_pressure,       SIGNAL(valueChanged(int)), this, SLOT(refreshDocument()));
    connect(comboBox_voltage,           SIGNAL(currentIndexChanged(int)), this, SLOT(refreshDocument()));
    connect(comboBox_calibrType,        SIGNAL(currentIndexChanged(int)), this, SLOT(refreshDocument()));
    connect(lineEdit_calibratorNum,  SIGNAL(textChanged(QString)), this, SLOT(refreshDocument()));
    connect(lineEdit_FIO,           SIGNAL(textChanged(QString)), this, SLOT(refreshDocument()));
    connect(checkBox_extView,        SIGNAL(toggled(bool)), this, SLOT(refreshDocument()));
    connect(checkBox_tested,        SIGNAL(toggled(bool)), this, SLOT(refreshDocument()));
    //connect(spinBox_KYNI_num,       SIGNAL(valueChanged(int)), this, SLOT(refreshDocument()));
    connect(this,               SIGNAL(htmlTextChanged(QString)), this, SLOT(refreshTextEdit(QString)));
    readSettings();
    prepareWidgets();
    refreshDocument();
}

DialogReportFactory::~DialogReportFactory()
{
    saveData();
    writeSettings();
}

int DialogReportFactory::getID(int slot)
{
    return m_pDevice->deviceIDs().at(slot);
}

void DialogReportFactory::saveData()
{
    QSettings settings;
    settings.setValue(allModules, checkBox_allModules->isChecked());
    settings.setValue(controllerTypeKey, comboBox_controllerType->currentIndex());
    settings.setValue(controllerKYNItypeKey, lineEdit_controllerKYNItype->text());
    settings.setValue(temperatureKey, spinBox_temperature->value());
    settings.setValue(humidityKey, spinBox_humidity->value());
    settings.setValue(pressureKey, spinBox_pressure->value());
    settings.setValue(calibrNameKey, comboBox_calibrType->currentText());
    settings.setValue(calibrNumKey, lineEdit_calibratorNum->text());
    settings.setValue(fioKey, lineEdit_FIO->text());
}

void DialogReportFactory::loadData()
{
    QSettings settings;
    checkBox_allModules->setChecked(settings.value(allModules, 0).toBool());
    comboBox_controllerType->setCurrentIndex(settings.value(controllerTypeKey, 1).toInt());
    lineEdit_controllerKYNItype->setText(settings.value(controllerKYNItypeKey).toString());
    spinBox_temperature->setValue(settings.value(temperatureKey, 20).toInt());
    spinBox_humidity->setValue(settings.value(humidityKey, 75).toInt());
    spinBox_pressure->setValue(settings.value(pressureKey, 760).toInt());
    lineEdit_FIO->setText(settings.value(fioKey).toString());
    QString calibrName = settings.value(calibrNameKey).toString();
    if(!calibrName.isEmpty())
    {
        int index = comboBox_calibrType->findText(calibrName);
        if(index >= 0)
        {
            comboBox_calibrType->setCurrentIndex(index);
            lineEdit_calibratorNum->setText(settings.value(calibrNumKey).toString());

        }
    }
}

void DialogReportFactory::prepareWidgets()
{
    label_calibrType->setText(QString("Тип ") + QString(m_input ? "калибратора" : "измерителя"));
    label_calibratorNum->setText(QString("Номер ") + QString(m_input ? "калибратора" : "измерителя"));
    on_comboBox_calibrType_activated(0);
    switch (m_type)
    {
    case InputOTK:
    case OutputOTK:
        break;
    case InputCLIT:
    case OutputCLIT:
        spinBox_temperature->setMinimum(15);
        spinBox_temperature->setMaximum(25);
        label_voltage->hide();
        comboBox_voltage->hide();
        break;
    }
    pushButton_showModulesParam->setVisible(checkBox_allModules->isChecked());

    comboBox_calibrType->addItems(g_parameterStorage.getCalibrators(getID(m_currentSlot)));
    int index = comboBox_calibrType->findText(m_selectedCalibrators[m_currentSlot]);
    if(index >= 0)
        comboBox_calibrType->setCurrentIndex(index);
    else if(!comboBox_calibrType->count())
    {
        comboBox_calibrType->setEnabled(false);
        comboBox_calibrType->setItemText(0, "отсутствуют");
    }
    else if(comboBox_calibrType->count() == 2)
        comboBox_calibrType->setCurrentIndex(1);
    //lineEdit_controllerNum->setText(QString::number(controllerNum));
    on_comboBox_calibrType_activated(comboBox_calibrType->currentIndex());
    on_checkBox_allModules_clicked(checkBox_allModules->isChecked());
}


void DialogReportFactory::appendModuleAmplitude(int slot, QString *pCurrentHtmlText)
{
    int id = m_pDevice->deviceIDs().at(slot);
    pCurrentHtmlText->append(QString("Диапазон измерения входного сигнала: <U>&nbsp;%1&#247;%2&nbsp;%3</U> Допускаемая абсолютная погрешность: <U>&nbsp;±%4&nbsp;%3</U>\n")
                    .arg(g_parameterStorage.realMin(id), 6, 'f', 3)
                    .arg(g_parameterStorage.realMax(id), 6, 'f', 3)
                    .arg(g_parameterStorage.getUnit(id))
                    .arg(getCurrentAdjustment(slot), 6, 'f', 3));
}

double DialogReportFactory::getCurrentAdjustment(int slot)
{
    int id = m_pDevice->deviceIDs().at(slot);
    double currentAdjustment = (float)0.01*(g_parameterStorage.realMax(id) - g_parameterStorage.realMin(id));
    if(m_type == InputOTK || m_type == OutputOTK)
    {
        currentAdjustment *= g_parameterStorage.getPercentOTK(id) +
                ((spinBox_temperature->value() > 20 ? 1 : -1) * (spinBox_temperature->value() - 20)
                 * g_parameterStorage.getAdjustment(id)) / 10.0 + (comboBox_voltage->currentIndex() != 0) * g_parameterStorage.getAdjustment(id); //на каждые 10 градусов прибавляется m_adjustment;

    }
    else
        currentAdjustment *= g_parameterStorage.getPercentCLIT(id);

    return currentAdjustment;
}

void DialogReportFactory::createTitleController(QString *pCurrentHtmlText)
{
    pCurrentHtmlText->append("<center>\n");
    pCurrentHtmlText->append(QString("поверки "));
    if(!checkBox_allModules->isChecked())
        pCurrentHtmlText->append(QString("модуля: тип <U>%1</U>   № <U>&nbsp;&nbsp;&nbsp;%2&nbsp;&nbsp;&nbsp;</U> ")
                                 .arg(g_parameterStorage.getKYNI(getID(m_currentSlot)))
                                .arg(spinBox_slotNum->value(), 4, 10, QChar('0')));

    pCurrentHtmlText->append("контроллера <U>&nbsp;&nbsp;");

    QString controllerName;
    switch(comboBox_controllerType->currentIndex())
    {
    case 0: controllerName = ""; break;
    case 1: controllerName = QString("СН-1 «Сонет» КУНИ.466945.%1")
                .arg(lineEdit_controllerKYNItype->text()); break;
    }

    int size = controllerName.size();
    for(int i = 0;  i < (int)(70 - size) / 2;  i++)
        pCurrentHtmlText->append("&nbsp;");
    pCurrentHtmlText->append(controllerName);
    for(int i = 0;  i < (int)(70 - size) / 2;  i++)
        pCurrentHtmlText->append("&nbsp;");

    pCurrentHtmlText->append("&nbsp;&nbsp;</U> № <U>");
    size = lineEdit_controllerNum->text().size();
    for(int i = 0;  i < (int)(10 - size) / 2;  i++)
        pCurrentHtmlText->append("&nbsp;");
    pCurrentHtmlText->append(lineEdit_controllerNum->text());
    for(int i = 0;  i < (int)(10 - size) / 2;  i++)
        pCurrentHtmlText->append("&nbsp;");
//            &nbsp;&nbsp;&nbsp;%1&nbsp;&nbsp;&nbsp;")
//                           .arg(lineEdit_controllerNum->text()));
    pCurrentHtmlText->append("</U><p>\n");
    if(!checkBox_allModules->isChecked())
        appendModuleAmplitude(m_currentSlot, pCurrentHtmlText);
    pCurrentHtmlText->append("</center>\n");
}

void DialogReportFactory::insertCalibrData(QString calibrType, QString calibrNum, QString *pCurrentHtmlText)
{
    if(calibrType.toLower() == "внешний")
        return;
    pCurrentHtmlText->append("<DL>");
    {
        QString fileName = ":/sonetData/" + calibrType + "_" + (m_input ? "output" : "input"); //если была поверка ввода - используется калибратор как вывод.
        QFile calibrFile(fileName);
        if(calibrFile.open(QIODevice::ReadOnly))
        {
            pCurrentHtmlText->append(QString(calibrFile.readAll()).arg(calibrNum));
            calibrFile.close();
        }
        else
            qWarning() << "файл не найден:" << calibrType;
    }
    pCurrentHtmlText->append("</DL>");
}

void DialogReportFactory::appendModuleAppearance(int slot, QString *pCurrentHtmlText)
{
    pCurrentHtmlText->append("<OL>\n");
    bool extView    = checkBox_allModules->isChecked() && slot >= 0 ? m_moduleData.value(slot).extView : checkBox_extView->isChecked();
    bool tested     = checkBox_allModules->isChecked() && slot >= 0 ? m_moduleData.value(slot).tested  : checkBox_tested->isChecked();
    pCurrentHtmlText->append(QString("<LI> Внешний осмотр <U>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</U>\n").arg(extView ? "Соотв." : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
    pCurrentHtmlText->append(QString("<LI> Опробование <U>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</U>\n").arg(tested ? "Соотв." : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
    pCurrentHtmlText->append("<LI> Определение основной абсолютной погрешности.\n");
    pCurrentHtmlText->append("</OL>\n");
}

void DialogReportFactory::appendTableForModule(int slot, QString *pCurrentHtmlText)
{
    int id = m_pDevice->deviceIDs().at(slot);
    pCurrentHtmlText->append("<TABLE WIDTH=100% ALIGN=CENTER VALIGN=CENTER BORDER=2 BORDERCOLOR=BLACK >\n");
    pCurrentHtmlText->append("<TR>\n");
    pCurrentHtmlText->append("<TD ROWSPAN=3 ALIGN=CENTER>№ канал</TD>\n");

    QVector<qreal> points = g_parameterStorage.getPoints(id);
    int pointsCount = points.size();
    pCurrentHtmlText->append(QString("<TD COLSPAN=%1 ALIGN=CENTER>Проверяемые точки диапазона</TD>\n</TR>\n")
                             .arg(pointsCount * 2));

    pCurrentHtmlText->append("<TR>\n");

    int pos = 0;
    for(QVector<qreal>::ConstIterator pointIter  = points.constBegin();  pointIter != points.constEnd();  pointIter++)
        pCurrentHtmlText->append(QString("<TD COLSPAN=2 ALIGN=CENTER>X<SUB>%1</SUB>=%2&nbsp;%3</TD>\n")
                                 .arg(++pos)
                                 .arg(*pointIter, 6, 'f', 3)
                                 .arg(g_parameterStorage.getUnit(id)));
    pCurrentHtmlText->append("</TR>\n");
    pCurrentHtmlText->append("<TR>\n");

    double currentAdjustment = getCurrentAdjustment(slot);

    for(QVector<qreal>::ConstIterator pointIter  = points.constBegin();  pointIter != points.constEnd();  pointIter++)
    {
        pCurrentHtmlText->append(QString("<TD ALIGN=CENTER>Min=%1</TD>\n")
                                 .arg(*pointIter - currentAdjustment + 0.0001, 6, 'f', 3));
        pCurrentHtmlText->append(QString("<TD ALIGN=CENTER>Max=%1</TD>\n")
                                 .arg(*pointIter + currentAdjustment - 0.0001, 6, 'f', 3));
    }

    pCurrentHtmlText->append("</TR>\n");

    QString colorForCeil = " bgcolor=\"#fbf0f0\"";
    for(int chan = 0;  chan < m_pResults->at(slot).size();  chan++)
    {
        if(m_pResults->at(slot).at(chan).size() != pointsCount)
            continue;
        pCurrentHtmlText->append("<TR>\n");
        pCurrentHtmlText->append(QString("<TD ALIGN=CENTER >%1</TD>\n")
                                 .arg(chan + 1));
        for(QVector<qreal>::ConstIterator pointIter  = points.constBegin();  pointIter != points.constEnd();  pointIter++)
        {
            qreal min = m_pResults->at(slot).at(chan).value(*pointIter).first,
                    max = m_pResults->at(slot).at(chan).value(*pointIter).second;
            pCurrentHtmlText->append(QString("<TD ALIGN=CENTER%1>%2</TD>\n")
                                   .arg(min >= *pointIter - currentAdjustment + 0.0001 && min <= *pointIter + currentAdjustment - 0.0001 ? "" : colorForCeil)
                                   .arg(min, 6, 'f', 3));	// ceil
            pCurrentHtmlText->append(QString("<TD ALIGN=CENTER%1>%2</TD>\n")
                                   .arg(max <= *pointIter + currentAdjustment - 0.0001 && max >= *pointIter - currentAdjustment + 0.0001 ? "" : colorForCeil)
                                   .arg(max, 6, 'f', 3)); // floor
        }
        pCurrentHtmlText->append("</TR>\n");
    }

    pCurrentHtmlText->append("\n");
    pCurrentHtmlText->append("</TABLE>\n");
}

bool DialogReportFactory::appendModuleInfoAll(int pos, int slot, QString *pCurrentHtmlText)
{
    int id = m_pDevice->deviceIDs().at(slot);
    bool calibrEnabled = checkBox_allModules->isChecked() ? m_moduleData.value(slot).calibratorType.length()
                                                          : comboBox_calibrType->currentIndex();

    int moduleMum = checkBox_allModules->isChecked() ? m_moduleData.value(slot).moduleNum : spinBox_slotNum->value();
    if(checkBox_allModules->isChecked())
    {
        QString letter = "A";
        switch(pos)
        {
        case 0: letter = "A"; break;
        case 1: letter = "Б"; break;
        case 2: letter = "В"; break;
        case 3: letter = "Г"; break;
        case 4: letter = "Д"; break;
        case 5: letter = "Е"; break;
        case 6: letter = "Ж"; break;
        case 7: letter = "З"; break;
        case 8: letter = "И"; break;
        case 9: letter = "К"; break;
        case 10: letter = "Л"; break;
        case 11: letter = "М"; break;
        case 12: letter = "Н"; break;
        case 13: letter = "О"; break;
        case 14: letter = "П"; break;
        case 15: letter = "Р"; break;
        default: letter = "ошибка"; break;
        }
        if(letter != "ошибка")
            letter.append(')');

        pCurrentHtmlText->append(QString("%1 модуля: тип <U>%2</U>   № <U>&nbsp;&nbsp;&nbsp;%3&nbsp;&nbsp;&nbsp;</U> ")
                                 .arg(letter)
                                .arg(g_parameterStorage.getKYNI(id))
                                .arg(moduleMum, 4, 10, QChar('0')));
        pCurrentHtmlText->append("<p>");
        appendModuleAmplitude(slot, pCurrentHtmlText);
    }
    if(checkBox_allModules->isChecked() && calibrEnabled)
    {
        QString type = checkBox_allModules->isChecked() ? m_moduleData.value(slot).calibratorType
                                                        : comboBox_calibrType->currentText();
        QString name = checkBox_allModules->isChecked() ? m_moduleData.value(slot).calibratorNum
                                                        : lineEdit_calibratorNum->text();
        insertCalibrData(type, name, pCurrentHtmlText);
        pCurrentHtmlText->append("<p>");
    }
    //appendModuleAppearance(slot, pCurrentHtmlText);

    appendTableForModule(slot, pCurrentHtmlText);

    bool moduleIsValid = true;
    QVector<qreal> points = g_parameterStorage.getPoints(m_pDevice->deviceIDs().at(slot));
    int pointsCount = points.size();
    double currentAdjustment = getCurrentAdjustment(slot);
    for(int chan = 0;  chan < m_pResults->at(slot).size() && moduleIsValid;  chan++)
    {
        if(m_pResults->at(slot).at(chan).size() != pointsCount)
            continue;
        for(QVector<qreal>::ConstIterator pointIter  = points.constBegin();  pointIter != points.constEnd();  pointIter++)
            moduleIsValid &=
                    m_pResults->at(slot).at(chan).value(*pointIter).first >= *pointIter - currentAdjustment + 0.0001
                    && m_pResults->at(slot).at(chan).value(*pointIter).second <= *pointIter + currentAdjustment - 0.0001;
    }

    if(!moduleIsValid)
        pCurrentHtmlText->append("<p align=\"right\">*Подсвечены ячейки, содержимое которых выходит за рамки допустимых значений</p>");
    else
        pCurrentHtmlText->append("<p>");

    return moduleIsValid;
}

void DialogReportFactory::refreshDocument()
{
    QString currentHtmlText;
    currentHtmlText.append("<HTML><HEAD>\n");
    currentHtmlText.append("<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=Windows-1251\">\n");
    currentHtmlText.append("<META NAME=\"Language\" CONTENT=\"ru\">\n");
    currentHtmlText.append("</HEAD>\n");
    currentHtmlText.append("<BODY>\n");
    currentHtmlText.append(QString("<center>ПРОТОКОЛ  № <U>&nbsp;&nbsp;&nbsp;&nbsp;%1&nbsp;&nbsp;&nbsp;&nbsp;</U></center>\n")
                           .arg(lineEdit_reportNum->text()));
    currentHtmlText.append("<p>\n");

    createTitleController(&currentHtmlText);
    currentHtmlText.append("<p>\n");
    if(!checkBox_allModules->isChecked() && comboBox_calibrType->currentIndex())
    {
        QString type = comboBox_calibrType->currentText();
        QString name = lineEdit_calibratorNum->text();
        insertCalibrData(type, name, &currentHtmlText);
        currentHtmlText.append("<p>");
    }
    currentHtmlText.append(QString("Температура окружающей среды (20&#177;5)&deg;C<U>&nbsp;%1&nbsp;</U>").arg((double)spinBox_temperature->value(), 4, 'f', 1));
    currentHtmlText.append(QString("&nbsp;&nbsp;&nbsp;Относительная влажность воздуха от 30 до 80%<U>&nbsp;%1&nbsp;</U><br>\n").arg((double)spinBox_humidity->value(), 4, 'f', 1));
    currentHtmlText.append(QString("Атмосферное давление от 630 до 795 мм.рт.ст.<U>&nbsp;%1&nbsp;</U><p>\n").arg((double)spinBox_pressure->value(), 4, 'f', 1));

    appendModuleAppearance(-1, &currentHtmlText);
    bool controllerIsValid = true;
    int validslots = 0;
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        if(m_moduleData.contains(slot) && m_moduleData.value(slot).collected)
            validslots++;

    }
    int pos = 0;
    if(checkBox_allModules->isChecked())
        for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
        {
            bool enabled = m_moduleData.contains(slot) && m_moduleData.value(slot).collected;
            if(enabled)
            {
                controllerIsValid = appendModuleInfoAll(pos, slot, &currentHtmlText);
                if(pos != validslots -1)
                {
                    currentHtmlText.append(QString("<p align=\"left\">Страница №%1 из %2</p>").arg(++pos).arg(validslots));
                   // currentHtmlText.append(QString(" Страница №%1 из %2\n <p>").arg(slot + 1).arg(validslots));
                    currentHtmlText.append("<br clear=\"all\" style=\"page-break-before:always\"/>\n");
                }
            }
        }
    else
        controllerIsValid = appendModuleInfoAll(pos, m_currentSlot, &currentHtmlText);

    currentHtmlText.append("\n");
    QDateTime currentDateTime = QDateTime::currentDateTime();

    static const QStringList monthes(QStringList() << "Января"
                                     << "Февраля"
                                     << "Марта"
                                      << "Апреля"
                                     << "Мая"
                                     << "Июня"
                                     << "Июля"
                                     << "Августа"
                                     << "Сентября"
                                     << "Октября"
                                     << "Ноября"
                                     << "Декабря");

    currentHtmlText.append(QString("Дата поверки: <U>&#171;&nbsp;%1&nbsp;&#187;&nbsp;%2&nbsp;&nbsp;%3&nbsp;г</U>\n")
                    .arg(currentDateTime.date().day())
                    .arg(monthes.at(currentDateTime.date().month() - 1))
                    .arg(currentDateTime.date().year()));

    currentHtmlText.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n");
    currentHtmlText.append(QString("Результат поверки: <U>&nbsp;&nbsp;%1&nbsp;&nbsp;</U><br><p>\n")
                    .arg(controllerIsValid ? "годен" : "брак"));

    currentHtmlText.append("<TABLE CELLPADDING=0 CELLSPACING=0>\n");
    currentHtmlText.append("<TR>\n");
    currentHtmlText.append("<TD>Поверку провел </TD>\n");
    currentHtmlText.append("<TD>&nbsp;&nbsp;&nbsp;</TD>\n");
    currentHtmlText.append("<TD ALIGN=CENTER VALIGN=BOTTOM><U>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</U></TD>\n");
    currentHtmlText.append("<TD>&nbsp;&nbsp;&nbsp;</TD>\n");
    QString fio = lineEdit_FIO->text();
    if(fio.isEmpty())
        fio = "<U>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</U>";
    else
    {
        fio.prepend("<U>&nbsp;&nbsp;&nbsp;");
        fio.append("&nbsp;&nbsp;&nbsp;</U>");
    }
    currentHtmlText.append(QString("<TD ALIGN=CENTER VALIGN=BOTTOM>%1</TD>\n").arg(fio));
    currentHtmlText.append("</TR>\n");
    currentHtmlText.append("<TR>\n");
    currentHtmlText.append("<TD></TD>\n");
    currentHtmlText.append("<TD></TD>\n");
    currentHtmlText.append("<TD ALIGN=CENTER VALIGN=TOP><FONT SIZE=-4>Подпись</FONT></TD>\n");
    currentHtmlText.append("<TD></TD>\n");
    currentHtmlText.append("<TD ALIGN=CENTER VALIGN=TOP><FONT SIZE=-4>Расшифровка подписи</FONT></TD>\n");
    currentHtmlText.append("</TR>\n");
    currentHtmlText.append("</TABLE>\n");
    if(checkBox_allModules->isChecked())
        currentHtmlText.append(QString("Страница №%1 из %1").arg(validslots));
    currentHtmlText.append("</BODY>\n");
    sethtmlText(currentHtmlText);

}

void DialogReportFactory::on_pushButton_toHTML_clicked()
{
    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Сохранить файл"), "", tr("html (*.html)"));
    if(saveFileName.isEmpty())
        return;
    QFile file(saveFileName);
    if(!file.open(QIODevice::WriteOnly))
        return;
    QTextStream out(&file);
    out.setCodec("CP1251");
    out << htmlText();
    file.close();
}

void DialogReportFactory::refreshTextEdit(QString txt)
{
    textEdit->setHtml(txt);
    textEdit->toHtml();
}

void DialogReportFactory::on_pushButton_toPDF_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Сохранить файл в PDF"), "", tr("pdf (*.pdf)"));
        if (!fileName.isEmpty())
        {
            if (QFileInfo(fileName).suffix().isEmpty())
                fileName.append(".pdf");
            QPrinter printer(QPrinter::ScreenResolution);
            printer.setPaperSize(QPrinter::A4);
            printer.setOutputFormat(QPrinter::PdfFormat);
            printer.setOutputFileName(fileName);
            printer.setFullPage(true);
            printer.setOrientation(QPrinter::Landscape);
            textEdit->document()->setPageSize(QSize(printer.width(), printer.height()));
            textEdit->document()->print(&printer);
        }
}

void DialogReportFactory::on_pushButton_print_clicked()
{
     QPrinter printer(QPrinter::ScreenResolution);
     printer.setOrientation(QPrinter::Landscape);
     printer.setPaperSize(QPrinter::A4);
     QPrintDialog dialog(&printer, this);
     dialog.setWindowTitle(tr("Печать документа"));
     if (textEdit->textCursor().hasSelection())
         dialog.addEnabledOption(QAbstractPrintDialog::PrintSelection);
     if (dialog.exec() != QDialog::Accepted)
         return;
     textEdit->document()->setPageSize(QSize(printer.width(), printer.height()));
     textEdit->print(&printer);
}

void DialogReportFactory::on_comboBox_calibrType_activated(int index)
{
    label_calibrType->setEnabled(index);
    lineEdit_calibratorNum->setEnabled(index);
    lineEdit_calibratorNum->setText(index ? "" : QString("Укажите ") + QString(m_input ? "калибратор" : "измеритель"));
}

void DialogReportFactory::on_comboBox_controllerType_currentIndexChanged(int index)
{
    Q_UNUSED(index);

    label_controllerKYNItype->setVisible(index);
    lineEdit_controllerKYNItype->setVisible(index);
}

void DialogReportFactory::on_checkBox_allModules_clicked(bool checked)
{
    pushButton_showModulesParam->setVisible(checked);
    label_slotNum->setVisible(!checked);
    spinBox_slotNum->setVisible(!checked);
    label_calibrType->setVisible(!checked);
    comboBox_calibrType->setVisible(!checked);
    label_calibratorNum->setVisible(!checked);
    lineEdit_calibratorNum->setVisible(!checked);
    //checkBox_extView->setVisible(!checked);
    //checkBox_tested->setVisible(!checked);
}

void DialogReportFactory::on_pushButton_showModulesParam_clicked()
{
    {
        DialogModulesDataForReport dialog(m_pDevice, &m_moduleData, this);
        dialog.exec();
    }
    refreshDocument();
}

void DialogReportFactory::writeSettings()
{
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("DialogReportFactory");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
}

void DialogReportFactory::readSettings()
{
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("DialogReportFactory");
    resize(settings.value("size", QSize(800, 600)).toSize());
    move(settings.value("pos", QPoint(0, 0)).toPoint());
    settings.endGroup();
}
