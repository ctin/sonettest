﻿#ifndef DIALOGREPORTFACTORY_H
#define DIALOGREPORTFACTORY_H

#include "ui_dialogreportfactory.h"
#include "dialogverifymodule.h"
#include "objects.h"
#include "devicewithdata.h"

struct ModuleData 
{
    ModuleData();
    QString calibratorType;
    QString calibratorNum;
    int moduleNum;
    bool collected;
    bool extView, tested;
};

class DialogReportFactory : public QDialog, private Ui::DialogReportFactory
{
    Q_OBJECT
    Q_PROPERTY(QString htmlText READ htmlText WRITE sethtmlText NOTIFY htmlTextChanged)
public:
    explicit DialogReportFactory(const bool input, DeviceWithData *pDevice, const int slot, const ResultController *pResults, QMap<int, QString> selectedCalibrators, const VerifyType type, QWidget *parent = 0);
    ~DialogReportFactory();
    const ResultController *m_pResults;
    DeviceWithData *m_pDevice;
    QMap<int, QString> m_selectedCalibrators;

    QMap<int, ModuleData> m_moduleData;
private slots:
    void refreshDocument();
    void on_pushButton_toHTML_clicked();
    void on_pushButton_toPDF_clicked();
    void on_pushButton_print_clicked();

    void on_comboBox_calibrType_activated(int index);
    void refreshTextEdit(QString txt);
    void on_comboBox_controllerType_currentIndexChanged(int index);

    void on_checkBox_allModules_clicked(bool checked);

    void on_pushButton_showModulesParam_clicked();

private:
    int m_currentSlot;
    VerifyType m_type;
    bool m_input;
    void saveData();
    void loadData();

    int getID(int slot);
    //properties
public:
    QString htmlText() const;
    void prepareWidgets();
    void createTitleController(QString *pCurrentHtmlText);
    void insertCalibrData(QString calibrType, QString calibrNum, QString *pCurrentHtmlText);
    void appendModuleAmplitude(int slot, QString *pCurrentHtmlText);
    double getCurrentAdjustment(int slot);
    void appendModuleAppearance(int slot, QString *pCurrentHtmlText);
    void appendTableForModule(int slot, QString *pCurrentHtmlText);
    bool appendModuleInfoAll(int pos, int slot, QString *pCurrentHtmlText);
public slots:
    void sethtmlText(const QString htmlText);
private slots:
    void readSettings();
    void writeSettings();
signals:
    void htmlTextChanged(QString htmlText);
private:
    QString m_htmlText;
};

#endif // DIALOGREPORTFACTORY_H
