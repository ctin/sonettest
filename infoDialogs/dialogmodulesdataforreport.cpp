#include "dialogmodulesdataforreport.h"

static const QString calibrTypeKey("ReportControllerCalibrName");
static const QString calibrNameKey("ReportControllerCalibrNum");
static const QString extView("ReportControllerExtView");
static const QString tested("ReportControllerTested");

DialogModulesDataForReport::DialogModulesDataForReport(DeviceWithData *pDevice, QMap<int, ModuleData> *pModuleData, QWidget *parent) :
    QDialog(parent), m_pModuleData(pModuleData), m_pDeviceWithData(pDevice)
{
    setModal(true);
    setWindowTitle("Ввод данных о модулях");
    QGridLayout *pLayout = new QGridLayout(this);
    int row = 0;
    pLayout->addWidget(new QLabel("Позиция"), row, 0);
    pLayout->addWidget(new QLabel("Номер"), row, 1);
    pLayout->addWidget(new QLabel("Тип калибратора"), row, 2);
    pLayout->addWidget(new QLabel("Номер калибратора"), row, 3);
    //pLayout->addWidget(new QLabel("Внешний осмотр"), row, 4);
    //pLayout->addWidget(new QLabel("Опробование"), row, 5);
    row++;
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        QSpinBox *pSpinBox = new QSpinBox(this);
        QComboBox *pCombobox = new QComboBox(this);
        QLineEdit *pLineEdit = new QLineEdit(this);
        QCheckBox *pCheckBoxExtView = new QCheckBox("соотв.", this);
        QCheckBox *pCheckBoxTested = new QCheckBox("соотв", this);

        m_spinBoxesNum.append(pSpinBox);
        m_comboboxesTypes.append(pCombobox);
        m_lineeditsNames.append(pLineEdit);
        m_checkBoxesExtView.append(pCheckBoxExtView);
        m_checkBoxesTested.append(pCheckBoxTested);

        bool enabled = m_pModuleData->contains(slot) && m_pModuleData->value(slot).collected;
        pSpinBox->setEnabled(enabled);
        pCombobox->setEnabled(enabled);
        pLineEdit->setEnabled(enabled);
        pCheckBoxExtView->setEnabled(enabled);
        pCheckBoxTested->setEnabled(enabled);

        pLayout->addWidget(new QLabel(QString("Слот №%1").arg(slot + 1)), row, 0);
        pLayout->addWidget(pSpinBox, row, 1);
        pLayout->addWidget(pCombobox, row, 2);
        pLayout->addWidget(pLineEdit, row, 3);
        //pLayout->addWidget(pCheckBoxExtView, row, 4);
        //pLayout->addWidget(pCheckBoxTested, row, 5);
        if(!enabled)
            pLayout->addWidget(new QLabel("нет данных"), row, 6);

        if(enabled)
        {
            pCheckBoxExtView->setChecked(m_pModuleData->value(slot).extView);
            pCheckBoxTested->setChecked(m_pModuleData->value(slot).tested);
            pLineEdit->setText(m_pModuleData->value(slot).calibratorNum);
            pCombobox->addItem("Выбрать");
            pCombobox->addItems(g_parameterStorage.getCalibrators(m_pDeviceWithData->deviceIDs().at(slot)));
            int index = pCombobox->findText(m_pModuleData->value(slot).calibratorType);
            if(index >= 0)
                pCombobox->setCurrentIndex(index);
            else if(!pCombobox->count())
            {
                pCombobox->setEnabled(false);
                pCombobox->setItemText(0, "отсутствуют");
            }
            else if(pCombobox->count() == 2)
                pCombobox->setCurrentIndex(1);
        }
        row++;
    }

    loadData();
}

DialogModulesDataForReport::~DialogModulesDataForReport()
{
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        bool enabled = m_pModuleData->contains(slot) && m_pModuleData->value(slot).collected;
        if(!enabled)
            continue;
        ModuleData data = m_pModuleData->value(slot);
        if(m_comboboxesTypes.at(slot)->currentIndex())
        {
            data.calibratorType = m_comboboxesTypes.at(slot)->currentText();
            data.calibratorNum  = m_lineeditsNames.at(slot)->text();
        }
        data.moduleNum      = m_spinBoxesNum.at(slot)->value();
        data.extView        = m_checkBoxesExtView.at(slot)->isChecked();
        data.tested         = m_checkBoxesTested.at(slot)->isChecked();
        m_pModuleData->insert(slot, data);
    }
    saveData();
}

void DialogModulesDataForReport::saveData()
{
    QSettings settings;
    QStringList strListTypes, strListNames;
    QVariantList extViewLst, testedLst;
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        if(m_comboboxesTypes.at(slot)->currentIndex())
        {
            strListTypes << m_comboboxesTypes.at(slot)->currentText();
            strListNames << m_lineeditsNames.at(slot)->text();
        }
        extViewLst << m_checkBoxesExtView.at(slot)->isChecked();
        testedLst << m_checkBoxesTested.at(slot)->isChecked();
    }
    settings.setValue(calibrTypeKey, strListTypes);
    settings.setValue(calibrNameKey, strListNames);
    settings.setValue(extView, extViewLst);
    settings.setValue(tested, testedLst);
}

void DialogModulesDataForReport::loadData()
{
    QSettings settings;
    QStringList strListTypes = settings.value(calibrTypeKey).toStringList();
    QStringList strListNames = settings.value(calibrNameKey).toStringList();
    QVariantList extViewLst = settings.value(extView).toList();
    QVariantList testedLst = settings.value(tested).toList();
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        ModuleData data = m_pModuleData->value(slot);
        if(data.moduleNum != 0)
            m_spinBoxesNum.at(slot)->setValue(data.moduleNum);
        QString calibrType;
        if(!data.calibratorType.isEmpty())
            calibrType = data.calibratorType;
        else if(strListTypes.size() > slot)
            calibrType = strListTypes.at(slot);

        int index = m_comboboxesTypes.at(slot)->findText(calibrType);
        if(index >= 0)
        {
            m_comboboxesTypes.at(slot)->setCurrentIndex(index);
            QString calibrNum;
            if(!data.calibratorNum.isEmpty())
                calibrNum = data.calibratorNum;
            else if(strListNames.size() > slot)
                calibrNum = strListNames.at(slot);
            m_lineeditsNames.at(slot)->setText(calibrNum);
        }
        if(extViewLst.size() > slot)
            m_checkBoxesExtView.at(slot)->setChecked(extViewLst.at(slot).toBool());
        else
            m_checkBoxesExtView.at(slot)->setChecked(data.extView);
        if(testedLst.size() > slot)
            m_checkBoxesTested.at(slot)->setChecked(testedLst.at(slot).toBool());
        else
            m_checkBoxesTested.at(slot)->setChecked(data.tested);
    }
}


