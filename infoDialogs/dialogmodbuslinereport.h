﻿#ifndef DIALOGMODBUSLINEREPORT_H
#define DIALOGMODBUSLINEREPORT_H

#include "ui_dialogmodbuslinereport.h"

struct ModbusLineData
{
    QString m_name;
    QList<int> m_nodesRes, m_nodesNoRes, m_nodesErr, m_nodesFoundedRes, m_nodesFoundedNoRes;
    void clear();
};

class DialogMODBUSLineReport : public QDialog, private Ui::DialogMODBUSLineReport
{
    Q_OBJECT
    Q_PROPERTY(QString htmlText READ htmlText WRITE sethtmlText NOTIFY htmlTextChanged)

public:
    explicit DialogMODBUSLineReport(ModbusLineData *pModbusLineData, QWidget *parent = 0);
    ModbusLineData *m_pModbusLineData;
public slots:
    void refreshDocument();
    void on_pushButton_toHTML_clicked();
    void on_pushButton_toPDF_clicked();
    void on_pushButton_print_clicked();
    void refreshTextEdit(QString);
    //properties
public:
    QString htmlText() const;
public slots:
    void sethtmlText(const QString htmlText);
signals:
    void htmlTextChanged(QString htmlText);
private:
    QString m_htmlText;
};

#endif // DIALOGMODBUSLINEREPORT_H
