﻿#ifndef DIALOGAUTOVERIFYPREFERENCES_H
#define DIALOGAUTOVERIFYPREFERENCES_H

#include "ui_dialogautoverifypreferences.h"

class DialogAutoVerifyPreferences : public QDialog, public Ui::DialogAutoVerifyPreferences
{
    Q_OBJECT
    Q_PROPERTY(QString unit READ unit WRITE setunit NOTIFY unitChanged)
    Q_PROPERTY(qreal range READ range WRITE setrange NOTIFY rangeChanged)
    Q_PROPERTY(qreal percent READ percent WRITE setpercent NOTIFY percentChanged)
public:
    explicit DialogAutoVerifyPreferences(QWidget *parent = 0);
private slots:
    void onUnitOrRangeChanged();
    void onRangeOrPercentChanged();
    void onSliderAcuracyValueChanged(int position);
    void onDoubleSpinboxValueChanged(double value);

public:
QString unit() const;
qreal range() const;
qreal percent() const;

public slots:
void setunit(QString arg);
void setrange(qreal arg);
void setpercent(qreal arg);
signals:
void unitChanged(QString arg);
void rangeChanged(qreal arg);
void percentChanged(qreal arg);
private:
QString m_unit;
qreal m_range;
qreal m_percent;
};

#endif // DIALOGAUTOVERIFYPREFERENCES_H
