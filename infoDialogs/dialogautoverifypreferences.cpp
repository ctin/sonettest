﻿#include "dialogautoverifypreferences.h"
#include "macro.h"

DialogAutoVerifyPreferences::DialogAutoVerifyPreferences(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
    setLayout(gridLayout_preferences);
    gridLayout_preferences->setMargin(5);

    setrange(0);
    setpercent(0);
    setunit("NaN");
    doubleSpinBox_accuracy->setValue(dial_accuracy->value());
    spinBox_timeToConfirm->setValue(dial_timeToConfirm->value());
    connect(this, SIGNAL(unitChanged(QString)), this, SLOT(onUnitOrRangeChanged()));
    connect(this, SIGNAL(rangeChanged(qreal)), this, SLOT(onUnitOrRangeChanged()));
    connect(dial_accuracy, SIGNAL(sliderMoved(int)), this, SLOT(onSliderAcuracyValueChanged(int)));
    connect(doubleSpinBox_accuracy, SIGNAL(valueChanged(double)), this, SLOT(onDoubleSpinboxValueChanged(double)));
    connect(this, SIGNAL(rangeChanged(qreal)), this, SLOT(onRangeOrPercentChanged()));
    connect(this, SIGNAL(percentChanged(qreal)), this, SLOT(onRangeOrPercentChanged()));

}

void DialogAutoVerifyPreferences::onRangeOrPercentChanged()
{
    doubleSpinBox_accuracy->setMaximum(percent());
}

void DialogAutoVerifyPreferences::onUnitOrRangeChanged()
{
    label_range->setText(QString("Соотв. %1 %2").arg((doubleSpinBox_accuracy->value() * range()) / 100, 5).arg(unit()));
}

PROPERTY_CPP(QString, DialogAutoVerifyPreferences, unit)
PROPERTY_CPP(qreal, DialogAutoVerifyPreferences, range)
PROPERTY_CPP(qreal, DialogAutoVerifyPreferences, percent)

void DialogAutoVerifyPreferences::onSliderAcuracyValueChanged(int position)
{
    qreal val = (percent() * position) / 10;
    doubleSpinBox_accuracy->setValue(val);
    onUnitOrRangeChanged();
}

void DialogAutoVerifyPreferences::onDoubleSpinboxValueChanged(double value)
{
    if(!percent())
        return;
    value += 0.0000001;
    int val = (value * 10) / percent();
    dial_accuracy->setValue(val);
    onUnitOrRangeChanged();
}
