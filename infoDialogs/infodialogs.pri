INCLUDEPATH  += $$PWD
DEPENDPATH   += $$PWD

#QMAKE_CXXFLAGS_DEBUG += -pg
#QMAKE_LFLAGS_DEBUG += -pg

HEADERS += \
    $$PWD/dialogabout.h \
    infoDialogs/dialogcomsettings.h \
    infoDialogs/md5.h \
    infoDialogs/dialogloginpassword.h \
    infoDialogs/dialogautoverifypreferences.h \
    infoDialogs/dialogreportfactory.h \
    infoDialogs/dialogmodbuslinereport.h \
    infoDialogs/dialogmodulesdataforreport.h

SOURCES += \
    $$PWD/dialogabout.cpp \
    infoDialogs/dialogcomsettings.cpp \
    infoDialogs/md5.cpp \
    infoDialogs/dialogloginpassword.cpp \
    infoDialogs/dialogautoverifypreferences.cpp \
    infoDialogs/dialogreportfactory.cpp \
    infoDialogs/dialogmodbuslinereport.cpp \
    infoDialogs/dialogmodulesdataforreport.cpp

FORMS += \
    $$PWD/dialogabout.ui \
    infoDialogs/dialogCOMSettings.ui \
    infoDialogs/dialogautoverifypreferences.ui \
    infoDialogs/dialogreportfactory.ui \
    infoDialogs/dialogmodbuslinereport.ui
