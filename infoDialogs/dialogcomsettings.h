﻿#ifndef DIALOGCOMSETTINGS_H
#define DIALOGCOMSETTINGS_H

#include "ui_dialogCOMSettings.h"

class DialogCOMSettings : public QDialog, public Ui::DialogCOMSettings
{
    Q_OBJECT
    
public:
    explicit DialogCOMSettings(QWidget *parent = 0);
private slots:
    void setautoSearchEnabled();
    void setdefaultStopBits();
    void setdefaultDataBits();
    void setdefaultFlowIndex();
    void setdefaultParityIndex();
    void setdefaultRateIndex();
    void setdefaultPort();
    void setdefaultPortICSU();
    void setadditionalWaitTime();
};

#endif // DIALOGCOMSETTINGS_H
