#ifndef PERIPHERAL_H
#define PERIPHERAL_H

#include <QStringList>
#include <QApplication>
#include <QMessageBox>
#include <QTimer>

#define ANTILOCKTIME    60 //sec
class AntiLockTimer : public QTimer
{
    Q_OBJECT
public:
    AntiLockTimer(QObject *parent = 0);
    void startAntiLockTimer(int timeLimit = ANTILOCKTIME);
    void stopAntiLockTimer();
    volatile int m_deathCounter;
public slots:
    void onTimeOut();

private:
    volatile bool m_isRunning;

};

QStringList getAllPorts();
void addAvailablePort(const QString port);

class Application : public QApplication
{
    Q_OBJECT
public:
    Application(int &argc, char *argv[]);
public slots:
    bool showErrorMessage(const QString &title, const QString &text, const QString &details);
    void beforeQuit();
protected:
    QMessageBox *m_pErrorBox;
    bool winEventFilter (MSG *message, long *result);
    QTimer *m_refreshPortsTimer;
};

void customMessageHandler(QtMsgType type, const char *msg);
bool startProcess();

#endif // PERIPHERAL_H
