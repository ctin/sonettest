﻿#include "objects.h"
#include <QtCore>
#include "macro.h"

#define DEFAULT_SPEED   7
#define DEFAULT_NUM     44

AntiLockTimer *g_pAntiLockTimer = new AntiLockTimer();

void Sleepy::msleep(unsigned long msec)
{
    return QThread::msleep(msec);
}

void Sleepy::usleep(unsigned long usec)
{
    return QThread::usleep(usec);
}

int ByteArray::at(int i) const
{
    if(size() <= i)
    {
        qWarning() << "ByteArray::at(" << i << "): not anouth data!!!";
        return 0x00;
    }
    return QByteArray::at(i) & 0xff;
}

ParameterStorage g_parameterStorage;
QThreadStorage <ParameterStorage> g_threadStorage;

ModuleDataItem::ModuleDataItem()
{
    for(int i = 0;  i < 8;  i++)
    {
        deviceEEPROMValues.append(unreadedValue());
        userEEPROMValues.append(unitializedValue());
    }
    deviceValue = 0xffff;
    userValue = unitializedValue();
    needRefreshAfterSet = 0;
    dontUnset = false;
}

bool ModuleDataItem::requestToSetChannelValue() const
{
    return userValue != unitializedValue() && deviceValue != userValue;
}

bool ModuleDataItem::requestToSetEEPROMValue(int index) const
{
    if(index < 0 ||deviceEEPROMValues.size() <= index || userEEPROMValues.size() <= index)
        return false;
    return userEEPROMValues.at(index) != unitializedValue() && deviceEEPROMValues.at(index) != userEEPROMValues.at(index);
}

void ModuleDataItem::unsetValues()
{
    if(dontUnset)
        return;
    for(QList<unsigned short>::Iterator iter = deviceEEPROMValues.begin();  iter != deviceEEPROMValues.end();  iter++)
        *iter = unreadedValue();
    for(QList<unsigned long>::Iterator iter = userEEPROMValues.begin();  iter != userEEPROMValues.end();  iter++)
        *iter = unitializedValue();
    deviceValue = 0;
}

bool ModuleDataItem::isEmpty() const
{
    for(QList<unsigned short>::ConstIterator iter = deviceEEPROMValues.constBegin();  iter != deviceEEPROMValues.constEnd();  iter++)
        if(*iter != 0xffff)
            return false;
    return true;
}

bool ModuleDataItem::isValid() const
{
    for(QList<unsigned short>::ConstIterator iter = deviceEEPROMValues.constBegin();  iter != deviceEEPROMValues.constEnd();  ++iter)
        if(*iter == unreadedValue() || *iter == 0xffff || *iter == 0)
            return false;
    if(deviceEEPROMValues.size() > 1)
    {
        int tmpValue = *deviceEEPROMValues.constBegin();
        for(QList<unsigned short>::ConstIterator iter = ++deviceEEPROMValues.constBegin();  iter != deviceEEPROMValues.constEnd();  ++iter)
            if(tmpValue == *iter)
                return false;
    }
    return true;
}

ModuleDataModel::ModuleDataModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_roleNames = roleNames();
    m_roleNames.insert(DeviceValueRole, "deviceValue");
    m_roleNames.insert(UserValueRole, "userValue");
    m_roleNames.insert(DeviceEEPROMValuesRole, "deviceEEPROMValues");
    m_roleNames.insert(UserEEPROMValuesRole, "userEEPROMValues");
    m_roleNames.insert(RequestToSetChannelValueRole, "requestToSetChannelValue");
    m_roleNames.insert(RequestToSetEERPOMValueRole, "requestToSetEEPROMValue");
    m_roleNames.insert(IsValidRole, "isValid");
    m_roleNames.insert(IsEmptyRole, "isEmpty");
    m_roleNames.insert(DontUnsetRole, "dontUnset");
    m_roleNames.insert(RequestToSetCalibrDateRole, "requestToSetCalibrDate");
    m_roleNames.insert(RequestToSetVerifyDateRole, "requestToSetVerifyDate");
    m_roleNames.insert(RequestToSetCalibrFullNameRole, "requestToSetCalibrFullName");
    m_roleNames.insert(RequestToSetVerifyFullNameRole, "requestToSetVerifyFullName");
    m_roleNames.insert(ContainsDataFullNameRole, "containsDataFullName");
    m_roleNames.insert(DeviceCalibrDateRole, "deviceCalibrDate");
    m_roleNames.insert(DeviceVerifyDateRole, "deviceVerifyDate");
    m_roleNames.insert(DeviceCalibrFullNameRole, "deviceCalibrFullName");
    m_roleNames.insert(DeviceVerifyFullNameRole, "deviceVerifyFullName");
    m_roleNames.insert(UserCalibrDateRole, "userCalibrDate");
    m_roleNames.insert(UserVerifyDateRole, "userVerifyDate");
    m_roleNames.insert(UserCalibrFullNameRole, "userCalibrFullName");
    m_roleNames.insert(UserVerifyFullNameRole, "userVerifyFullName");

    containsDateFullName = ModuleDataItem::unitializedValue();
}

QHash<int, QByteArray> ModuleDataModel::roleNames() const
{
    return m_roleNames;
}

ModuleDataModel::~ModuleDataModel()
{
    m_moduleDataItems.clear();
}

bool ModuleDataModel::requestToSetCalibrDate() const
{
    return !userCalibrDate.isNull() && userCalibrDate.isValid() && userCalibrDate != deviceCalibrDate;
}

bool ModuleDataModel::requestToSetVerifyDate() const
{
    return !userVerifyDate.isNull() && userVerifyDate.isValid() && userVerifyDate != deviceVerifyDate;
}

bool ModuleDataModel::requestToSetCalibrFullName() const
{
    return !userCalibrFullName.isEmpty() && userCalibrFullName != deviceCalibrFullName;
}

bool ModuleDataModel::requestToSetVerifyFullName() const
{
    return !userVerifyFullName.isEmpty() && userVerifyFullName != deviceVerifyFullName;
}

int ModuleDataModel::length() const
{
    return m_moduleDataItems.size();
}

bool ModuleDataModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if(!count)
        return false;
    beginInsertRows(parent, row, row + count - 1);
    {
        for(int i = 0;  i < count;  i++)
            m_moduleDataItems.insert(row + i, ModuleDataItem());
    }
    endInsertRows();
    return true;
}

bool ModuleDataModel::removeRows(int row, int count, const QModelIndex &parent)
{

    if(!rowCount() || rowCount() < (row + count))
        return false;
    beginRemoveRows(parent, row, row + count - 1);
    {
        m_moduleDataItems.clear();
    }
    endRemoveRows();
    return true;
}


QVariant ModuleDataModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if(row > (m_moduleDataItems.size() - 1))
        return QVariant();
    if(!index.isValid() && role <= DontUnsetRole)
        return QVariant();
    const ModuleDataItem &moduleDataItem = row >= 0 && row < m_moduleDataItems.size() ? m_moduleDataItems.at(index.row())
                                                                                      : ModuleDataItem();


    switch(role)
    {
    case DeviceValueRole:   return QVariant::fromValue(moduleDataItem.deviceValue);
    case UserValueRole:     return QVariant::fromValue(moduleDataItem.userValue);
    case DeviceEEPROMValuesRole:
    {
        QVariantList varlist;
        for(QList<unsigned short>::ConstIterator iter = moduleDataItem.deviceEEPROMValues.begin();  iter != moduleDataItem.deviceEEPROMValues.end();  iter++)
                varlist.append((int)*iter);
        return QVariant::fromValue(varlist);
    }
    case UserEEPROMValuesRole:
    {
        QVariantList varlist;
        for(QList<unsigned long>::ConstIterator iter = moduleDataItem.userEEPROMValues.begin();  iter != moduleDataItem.userEEPROMValues.end();  iter++)
                varlist.append((int)*iter);
        return QVariant::fromValue(varlist);
    }
    case RequestToSetChannelValueRole:          return QVariant::fromValue(moduleDataItem.requestToSetChannelValue());
    case RequestToSetEERPOMValueRole:              return QVariant::fromValue(moduleDataItem.requestToSetEEPROMValue(index.row()));
    case IsValidRole:       return QVariant::fromValue(moduleDataItem.isValid());
    case IsEmptyRole:       return QVariant::fromValue(moduleDataItem.isEmpty());
    case DontUnsetRole:     return QVariant::fromValue(moduleDataItem.dontUnset);
    case RequestToSetCalibrDateRole:    return QVariant::fromValue(requestToSetCalibrDate());
    case RequestToSetVerifyDateRole:    return QVariant::fromValue(requestToSetVerifyDate());
    case RequestToSetCalibrFullNameRole:     return QVariant::fromValue(requestToSetCalibrFullName());
    case RequestToSetVerifyFullNameRole:    return QVariant::fromValue(requestToSetVerifyFullName());
    case ContainsDataFullNameRole:   return QVariant::fromValue(containsDateFullName);

    case DeviceCalibrDateRole:    return QVariant::fromValue(deviceCalibrDate);
    case DeviceVerifyDateRole:    return QVariant::fromValue(deviceVerifyDate);
    case DeviceCalibrFullNameRole:    return QVariant::fromValue(deviceCalibrFullName);
    case DeviceVerifyFullNameRole:    return QVariant::fromValue(deviceVerifyFullName);
    case UserCalibrDateRole:    return QVariant::fromValue(userCalibrDate);
    case UserVerifyDateRole:    return QVariant::fromValue(userVerifyDate);
    case UserCalibrFullNameRole:    return QVariant::fromValue(userCalibrFullName);
    case UserVerifyFullNameRole:    return QVariant::fromValue(userVerifyFullName);
    default:                    return QVariant();
    }
}

void ModuleDataModel::__setData(const int pos, const QVariant &value, const int role)
{
    Q_ASSERT(pos >= 0);
    //Q_ASSERT(pos < m_moduleDataItems.size());
    if(pos >= m_moduleDataItems.size())
        return;
    ModuleDataItem moduleDataItem = getItem(pos);
    switch(role)
    {
    case DeviceValueRole:
    {
        moduleDataItem.deviceValue = value.toUInt();
    }
    break;
    case UserValueRole:
    {
        moduleDataItem.userValue = value.toUInt();
    }
    break;
    case DeviceEEPROMValuesRole:
    {
        if(value.canConvert<QList<unsigned short> >())
            moduleDataItem.deviceEEPROMValues = value.value<QList<unsigned short> >();
        else if(value.canConvert<QList<unsigned long> >())
        {
            QList<unsigned long> source = value.value<QList<unsigned long> >();
            QList<unsigned short> target;
            for(QList<unsigned long>::const_iterator iter = source.constBegin();  iter != source.constEnd();  iter++)
                target.append(*iter);
            moduleDataItem.deviceEEPROMValues = target;
        }
        Q_ASSERT(moduleDataItem.deviceEEPROMValues.size() <= moduleDataItem.userEEPROMValues.size());
    }
    break;
    case UserEEPROMValuesRole:
    {
        moduleDataItem.needRefreshAfterSet = true;
        if(value.canConvert<QList<unsigned long> >())
            moduleDataItem.userEEPROMValues = value.value<QList<unsigned long> >();
        else if(value.canConvert<QList<unsigned short> >())
        {
            QList<unsigned short> source = value.value<QList<unsigned short> >();
            QList<unsigned long> target;
            for(QList<unsigned short>::const_iterator iter = source.constBegin();  iter != source.constEnd();  iter++)
                target.append(*iter);
            moduleDataItem.userEEPROMValues = target;
        }
    }
    break;
    case DontUnsetRole:
    {
        moduleDataItem.dontUnset = value.value<bool>();
    }
    break;
    case ContainsDataFullNameRole:
    {
        moduleDataItem.needRefreshAfterSet = true;
        containsDateFullName = value.value<int>();
    }
    break;
    case DeviceCalibrDateRole:
    {
        deviceCalibrDate = value.value<QDate>();
    }
    break;
    case DeviceVerifyDateRole:
    {
        deviceVerifyDate = value.value<QDate>();
    }
    break;
    case DeviceCalibrFullNameRole:
    {
        deviceCalibrFullName = value.value<QByteArray>();
    }
    break;
    case DeviceVerifyFullNameRole:
    {
        deviceVerifyFullName = value.value<QByteArray>();
    }
    break;
    case UserCalibrDateRole:
    {
        moduleDataItem.needRefreshAfterSet = true;
        userCalibrDate = value.value<QDate>();
    }
    break;
    case UserVerifyDateRole:
    {
        moduleDataItem.needRefreshAfterSet = true;
        userVerifyDate = value.value<QDate>();
    }
    break;
    case UserCalibrFullNameRole:
    {
        moduleDataItem.needRefreshAfterSet = true;
        userCalibrFullName = value.value<QByteArray>();
    }
    break;
    case UserVerifyFullNameRole:
    {
        moduleDataItem.needRefreshAfterSet = true;
        userVerifyFullName = value.value<QByteArray>();
    }

    }  
    //!if(role <= DontUnsetRole)
        m_moduleDataItems.replace(pos, moduleDataItem);
    QVector <int> roles;
    roles.append(role);
    emit dataChanged(index(pos), index(pos), roles);
}

void ModuleDataModel::unsetValues_s()
{
    {
        for(QList<ModuleDataItem>::Iterator iter = m_moduleDataItems.begin();  iter != m_moduleDataItems.end();  iter++)
            (*iter).unsetValues();
    }
    deviceCalibrDate = QDate();
    deviceVerifyDate = QDate();
    deviceCalibrFullName.clear();
    deviceVerifyFullName.clear();
    emit dataChanged(index(0), index(rowCount() - 1));
}

const ModuleDataItem& ModuleDataModel::getItem(int channel)
{
    Q_ASSERT(channel >= 0 && channel < m_moduleDataItems.size());
    return m_moduleDataItems.at(channel);
}

bool ModuleDataModel::setData(const int first, const int last, const QVariantList &values, const int role)
{
    if(last <= first || values.size() < last - first)
        return false;
    for(int i = first;  i < last;  i++)
    {
        __setData(i, values.at(i), role);
    }
    return true;
}

bool ModuleDataModel::setData(const int first, const int last, const QVariant &value, const int role)
{
    if(last <= first)
        return false;
    for(int i = first;  i < last;  i++)
    {
        __setData(i, value, role);
    }
    return true;
}

bool ModuleDataModel::setData(const QModelIndex &index, const QVariant &value, const int role)
{
    if(!index.isValid() || index.row() > m_moduleDataItems.size() - 1)
        return false;
    __setData(index.row(), value, role);
    return true;
}

bool ModuleDataModel::setData(const int row, const QVariant &value, const int role)
{
    if(row < 0 || row > m_moduleDataItems.size() - 1)
        return false;
    __setData(row, value, role);
    return true;
}

Qt::ItemFlags ModuleDataModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

int ModuleDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_moduleDataItems.size();
}

void ModuleDataModel::refreshModuleData(int channelsCount)
{
    removeRows(0, rowCount());
    if(!IS_CHANNEL_VALID(channelsCount)) // != 0xff)
    {
        deviceCalibrDate = QDate();
        deviceVerifyDate = QDate();
        deviceCalibrFullName.clear();
        deviceVerifyFullName.clear();
        return;
    }
    insertRows(0, channelsCount, QModelIndex());
}

DeviceParameters::DeviceParameters()
{
    m_ID = 0xff;
    m_min = m_max = errorValue();
    m_dchan = 0;
    m_percentOTK = errorValue();
    m_percentCLIT = errorValue();
    m_minMetrol = m_maxMetrol = errorValue();
    m_resistor = errorValue();
}

DeviceParameters::~DeviceParameters()
{

}

bool DeviceParameters::isDataValid(QVariant data)
{
    if(data.isNull() || !data.isValid())
        return false;
    if(data.canConvert(QVariant::String))
        return !data.toString().isEmpty() && data.toString().toDouble() != DeviceParameters::errorValue();
    else
        return data.toReal() != DeviceParameters::errorValue();
}

void DeviceParameters::setPoints(const QString &string)
{
    if(string.isEmpty())
        return;
    m_points.clear();
    QStringList list = string.split(" ", QString::SkipEmptyParts);
    foreach(const QString &str, list)
        m_points.push_back(str.toDouble());
}

QString DeviceParameters::getPoints() const
{
    QStringList stringList;
    foreach(qreal val, m_points)
        stringList << QString("%1").arg(val);
    return stringList.join(" ");
}

void DeviceParameters::setStringData(const QString &string)
{
    if(string.isEmpty())
        return;
    QRegExp regexp("\\s");
    QStringList stringList = string.split(regexp, QString::SkipEmptyParts);
    int size = stringList.size();
    if(size)
        m_name = stringList.at(0);
    if(size > 1)
        m_KYNI = stringList.at(1);
    if(size > 2)
    {
        int pos = 2;
        while(pos < size)
            m_comments += stringList.at(pos++) + " ";
    }
}

qreal DeviceParameters::realMax() const
{
    if(m_maxMetrol != errorValue())
        return m_maxMetrol;
    else
        return m_max;
    return m_max;
}

qreal DeviceParameters::realMin() const
{
    if(m_minMetrol != errorValue())
        return m_minMetrol;
    else
        return m_min;
    return m_min;
}

qreal DeviceParameters::displayMax() const
{
    return m_max;
}

qreal DeviceParameters::displayMin() const
{
    return m_min;
}

QString DeviceParameters::getStringData() const
{
    QString result;
    if(!m_name.isEmpty())
        result = m_name;
    if(!m_KYNI.isEmpty())
        result += " " + m_KYNI;
    if(!m_comments.isEmpty())
        result += " " + m_comments;
    return result;
}

QString DeviceParameters::getCalibratorsString() const
{
    return m_calibrators.join(",");
}

void DeviceParameters::setCalibrators(const QString string)
{
    if(string.isEmpty())
        return;
    m_calibrators.clear();
    m_calibrators = string.split(" ", QString::SkipEmptyParts);
}

bool DeviceParameters::operator == (const DeviceParameters conf) const
{
    return m_ID == conf.m_ID &&
            m_min == conf.m_min &&
            m_max == conf.m_max &&
            m_dchan == conf.m_dchan &&
            m_percentCLIT == conf.m_percentCLIT &&
            m_percentOTK == conf.m_percentOTK &&
            m_minMetrol == conf.m_minMetrol &&
            m_maxMetrol == conf.m_maxMetrol &&
            m_resistor == conf.m_resistor &&
            m_firmware == conf.m_firmware;
}

bool DeviceParameters::operator != (const DeviceParameters conf) const
{
    return m_ID != conf.m_ID ||
            m_min != conf.m_min ||
            m_max != conf.m_max ||
            m_dchan != conf.m_dchan ||
            m_percentCLIT != conf.m_percentCLIT ||
            m_percentOTK != conf.m_percentOTK ||
            m_minMetrol != conf.m_minMetrol ||
            m_maxMetrol != conf.m_maxMetrol ||
            m_resistor != conf.m_resistor ||
            m_firmware != conf.m_firmware;
}


ParameterStorage::ParameterStorage(QObject *parent)
    : QObject(parent)
{
    setParameterMap(g_parameterStorage.getParameterMap());
}

bool ParameterStorage::containsID(const int ID) const
{
    return m_parameterMap.contains(ID);
}

const DeviceParameters ParameterStorage::getParameters(const int ID) const
{

    return m_parameterMap.value(ID);
}


void ParameterStorage::setParameters(const int ID, const DeviceParameters &deviceParameters)
{
    {
        m_parameterMap.insert(ID, deviceParameters);
    }
    emit parametersChanged();
}

int ParameterStorage::getID(const int ID) const
{

    return m_parameterMap.value(ID).m_ID;
}

qreal ParameterStorage::realMax(const int ID) const
{

    return m_parameterMap.value(ID).realMax();
}

qreal ParameterStorage::realMin(const int ID) const
{

    return m_parameterMap.value(ID).realMin();
}

qreal ParameterStorage::displayMax(const int ID) const
{

    return m_parameterMap.value(ID).displayMax();
}

qreal ParameterStorage::displayMin(const int ID) const
{

    return m_parameterMap.value(ID).displayMin();
}

QString ParameterStorage::getUnit(const int ID) const
{

    return m_parameterMap.value(ID).m_unit;
}

QVector <qreal> ParameterStorage::getPoints(const int ID) const
{

    return m_parameterMap.value(ID).m_points;
}

QString ParameterStorage::getPointsStr(const int ID) const
{

    return m_parameterMap.value(ID).getPoints();
}

QString ParameterStorage::getName(const int ID) const
{

    return m_parameterMap.value(ID).m_name;
}

QString ParameterStorage::getKYNI(const int ID) const
{

    return m_parameterMap.value(ID).m_KYNI;
}

QString ParameterStorage::getComments(const int ID) const
{

    return m_parameterMap.value(ID).m_comments;
}

bool ParameterStorage::getDchan(const int ID) const
{
    bool res = m_parameterMap.value(ID).m_dchan;
    return DeviceParameters::isDataValid(res) && res;
}

qreal ParameterStorage::getPercentCLIT(const int ID) const
{

    return m_parameterMap.value(ID).m_percentCLIT;
}

qreal ParameterStorage::getPercentOTK(const int ID) const
{

    return m_parameterMap.value(ID).m_percentOTK;
}

qreal ParameterStorage::getAdjustment(const int ID) const
{

    return m_parameterMap.value(ID).m_adjustment;
}

qreal ParameterStorage::getResistor(const int ID) const
{

    return m_parameterMap.value(ID).m_resistor;
}

QString ParameterStorage::getStringData(const int ID) const
{

    return m_parameterMap.value(ID).getStringData();
}

QString ParameterStorage::getFirmware(const int ID) const
{

    return m_parameterMap.value(ID).m_firmware;
}

QStringList ParameterStorage::getCalibrators(const int ID) const
{

    return m_parameterMap.value(ID).m_calibrators;
}

void ParameterStorage::setParameterMap(const ParameterMap &parameterMap)
{
    m_parameterMap = parameterMap;
}

const ParameterMap& ParameterStorage::getParameterMap(void) const
{
    return m_parameterMap;
}

SonetConfig::SonetConfig(int rateIndexTmp, int deviceNumTmp, int autoSearchTmp, QString portTmp)
    : rateIndex(rateIndexTmp),
      deviceNum(deviceNumTmp),
      autoSearch(autoSearchTmp),
      port(portTmp)
{
}

SonetConfig::SonetConfig(const SonetConfig &conf)
    : rateIndex(conf.rateIndex),
      deviceNum(conf.deviceNum),
      autoSearch(conf.autoSearch),
      port(conf.port)
{
}

QDataStream &operator << (QDataStream &out, const DeviceParameters& param)
{
    out << param.m_ID;
    out << param.m_min;
    out << param.m_max;
    out << param.m_unit;
    out << param.m_dchan;
    out << param.m_percentCLIT;
    out << param.m_percentOTK;
    out << param.m_adjustment;
    out << param.m_minMetrol;
    out << param.m_maxMetrol;
    out << param.m_resistor;
    out << param.getPoints();
    out << param.getStringData();
    out << param.getCalibratorsString();
    out << param.m_firmware;
    return out;
}
QDataStream &operator >> (QDataStream &in, DeviceParameters& param)
{
    in >> param.m_ID;
    in >> param.m_min;
    in >> param.m_max;
    in >> param.m_unit;
    in >> param.m_dchan;
    in >> param.m_percentCLIT;
    in >> param.m_percentOTK;
    in >> param.m_adjustment;
    in >> param.m_minMetrol;
    in >> param.m_maxMetrol;
    in >> param.m_resistor;
    QString text;
    in >> text;
    param.setPoints(text);
    in >> text;
    param.setStringData(text);
    in >> text;
    param.setCalibrators(text);
    in >> param.m_firmware;
    return in;
}

QDataStream &operator << ( QDataStream &out, const SonetConfig& config)
{
    out << config.rateIndex;
    out << config.deviceNum;
    out << config.port;
    out << config.rateIndex;
    return out;
}

QDataStream &operator >> ( QDataStream &in, SonetConfig& config)
{
    in >> config.rateIndex;
    in >> config.deviceNum;
    in >> config.port;
    in >> config.rateIndex;
    return in;
}
