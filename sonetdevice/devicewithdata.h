﻿#ifndef DEVICEFACEDATA_H
#define DEVICEFACEDATA_H

#include <QtCore>
#include <QtDeclarative>
#include "commands.h"
#include "sonetdevice.h"
#include "moduleqmladapter.h"

class DeviceList;
class DeviceWithData;
typedef bool (DeviceWithData::*RequestFunc)();
//класс отвчает за привязку прибора к нарисованным модулям
class DeviceWithData : public SonetDevice
{
    Q_OBJECT
    Q_PROPERTY(QList<int> deviceIDs READ deviceIDs WRITE setdeviceIDs NOTIFY deviceIDsChanged)
    Q_PROPERTY(QList<int> types READ types WRITE settypes NOTIFY typesChanged)
    Q_PROPERTY(QList<int> channelsCount READ channelsCount WRITE setchannelsCount NOTIFY channelsCountChanged)
    Q_PROPERTY(QList<int> canSurveyModules READ canSurveyModules WRITE setcanSurveyModules NOTIFY canSurveyModulesChanged)
    Q_PROPERTY(QList<int> hasPower READ hasPower WRITE sethasPower NOTIFY hasPowerChanged)

    Q_PROPERTY(int componentID READ componentID WRITE setcomponentID NOTIFY componentIDChanged)
    Q_PROPERTY(bool deleteMe READ deleteMe WRITE setdeleteMe NOTIFY deleteMeChanged)
    Q_PROPERTY(bool isSonet READ isSonet WRITE setisSonet NOTIFY isSonetChanged)
public:
    DeviceWithData(DeviceList *pDeviceList = 0, int rateIndex = 7, int deviceNum = 44, bool autoSearch = true, QString port = QString(), QObject *parent = 0);
    ~DeviceWithData();
    volatile bool m_needSetEEPROMValues, m_underTests, m_writingEEPROM;

    Q_INVOKABLE ModuleDataModel *getDataModel(int slot);
    Q_INVOKABLE void setEEPROMUserValue(const int slot, const int channel, const int pos, const QVariant &value);
    Q_INVOKABLE void setModelData(const int slot, const int channel, const QVariant &value, const QByteArray &roleValue);
    Q_INVOKABLE void setModelData(const int slot, const QVariant &value, const QByteArray &role);
    Q_INVOKABLE QVariant getModelData(const int slot, const int channel, const QByteArray &role);

    bool find();
    bool sendChangedData();
    bool sendChangedEEPROMData();
    bool readEEPROMData();
    bool readModules();
    bool getProcessorModuleData();
    bool refreshConsist();
    void addSingleUseFunction(QByteArray req);
    void collectControllerDataWrapper();

    Q_INVOKABLE bool isOldModule(int slot);
    Q_INVOKABLE bool input(int slot);
    Q_INVOKABLE bool valid(int slot);
    Q_INVOKABLE bool analog(int slot);
    Q_INVOKABLE void addReset();
    Q_INVOKABLE void setMaxToAll(int slot);
    Q_INVOKABLE void setMaxToAll();
    Q_INVOKABLE void setMinToAll(int slot);
    Q_INVOKABLE void setMinToAll();

    bool __onError(QString message = QString()); //межпоточный обработчик ошибок
    void emitAllGUISignals();

public slots:
    bool eeprom_set(unsigned char module, unsigned char addr, unsigned char value);
    bool eeprom_get(unsigned char module, unsigned char addr, unsigned char *result);
    bool request(QByteArray req, ByteArray *ans = 0); //должен использоваться генератор функций из commands.h, req как значение запроса.
    void createBaseSurveyFunctions();
    void createCalibrSurveyFunctions();
    void __addSingleUseFunction(QByteArray req);
    void __setDeviceStruct(QList<int> types, QList<int> chCounts, QList<int> deviceIDs);
private:
    void collectControllerData();
    QList <RequestFunc> m_commandList;
    QList <QByteArray> m_singleUseCommands;
    QMutex m_commandListMutex;
    QList<ModuleDataModel*> m_moduleDataModel;
    bool checkSPIEcho(unsigned char slot, unsigned char command);
    bool checkModuleContainsCalibrDate(unsigned char slot, bool *ok = 0);
private:
    DeviceList *m_pDeviceList;
    bool m_needRefreshValue;
private slots:
    void onDeviceParametersChanged();
    void onDeviceAviableChanged();
    //property
public:
    QList<int> deviceIDs() const;
    QList<int> types() const;
    QList<int> channelsCount() const;
    QList<int> canSurveyModules() const;
    QList<int> hasPower() const;
    int componentID() const;
    bool deleteMe() const;
    bool isSonet() const;

public slots:
    void setdeviceIDs(const QList<int> deviceIDs);
    void settypes(const QList<int> types);
    void setchannelsCount(const QList<int> channelsCount);
    void setcanSurveyModules(const QList<int> canSurveyModules);
    void sethasPower(const QList<int> hasPower);
    void setcanSurveyModule(const int slot, const int canSurveyModule);
    void setcomponentID(const int componentID);
    void setdeleteMe(bool arg);
    void setisSonet(bool arg);
protected slots:
    void refreshIsSonet();
signals:
    void deviceIDsChanged(QList<int> deviceIDs);
    void typesChanged(QList<int> types);
    void channelsCountChanged(QList<int> channelsCount);
    void canSurveyModulesChanged(QList<int> canSurveyModules);
    void hasPowerChanged(QList<int> hasPower);
    void componentIDChanged(int componentID);
    void deleteMeChanged(bool arg);
    void isSonetChanged(bool arg);

private:
    QList<int> m_deviceIDs;
    QList<int> m_types;
    QList<int> m_channelsCount;
    QList<int> m_canSurveyModules;
    QList<int> m_hasPower;
    int m_componentID;
    bool m_deleteMe;
    bool m_isSonet;

private slots:
    void refreshList(const QList<int> lastchannelCounts, const QList<int> channelsCount);
private:
    void refreshModuleEEPROMChannelCounts(const int slot);

};
Q_DECLARE_METATYPE(DeviceWithData*)

#endif // DEVICEFACEDATA_H
