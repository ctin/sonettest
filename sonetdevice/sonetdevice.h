﻿#ifndef SONETDEVICE_H
#define SONETDEVICE_H

#include "modbusdevice.h"
#include "objects.h"

//класс декорирует стандартный MODBUS прибор деталями контроллера "Сонет"
class SonetDevice : public ModbusDevice
{
    Q_OBJECT
    Q_PROPERTY(int bprState READ bprState WRITE setbprState NOTIFY bprStateChanged)
    Q_PROPERTY(bool turnedOn READ turnedOn WRITE setturnedOn NOTIFY turnedOnChanged RESET unsetturnedOn)
    Q_PROPERTY(qreal temperature READ temperature WRITE settemperature NOTIFY temperatureChanged)
    Q_PROPERTY(int frontPanelX10 READ frontPanelX10 WRITE setfrontPanelX10 NOTIFY frontPanelX10Changed)
    Q_PROPERTY(int frontPanelX1 READ frontPanelX1 WRITE setfrontPanelX1 NOTIFY frontPanelX1Changed)
    Q_PROPERTY(int frontPanelSpeed READ frontPanelSpeed WRITE setfrontPanelSpeed NOTIFY frontPanelSpeedChanged)
public:
    explicit SonetDevice(QObject *parent = 0);
    bool MODBUS_command(QByteArray req, ByteArray *ans = 0); //"выше" используется генератор функций, req - команда.
    void emitAllGUISignals();

public slots:
    bool find();
private:

private slots:
    void createInfo();
    void onTurnedOnChanged(bool turnedOn);

    //properties
public:
    int bprState() const;
    bool turnedOn() const;
    qreal temperature() const;
    int frontPanelX10() const;
    int frontPanelX1() const;
    int frontPanelSpeed() const;
    QString information() const;
public slots:
    void setbprState(const int bprState);
    void setturnedOn(const bool turnedOn);
    void settemperature(const qreal temperature);
    void setfrontPanelX10(const int frontPanelX10);
    void setfrontPanelX1(const int frontPanelX1);
    void setfrontPanelSpeed(const int frontPanelSpeed);
    void unsetturnedOn();
signals:
    void bprStateChanged(int bprState);
    void turnedOnChanged(bool turnedOn);
    void temperatureChanged(qreal temperature);
    void frontPanelX10Changed(int frontPanelX10);
    void frontPanelX1Changed(int frontPanelX1);
    void frontPanelSpeedChanged(int frontPanelSpeed);
private:
    int m_bprState;
    bool m_turnedOn;
    qreal m_temperature;
    int m_frontPanelX10;
    int m_frontPanelX1;
    int m_frontPanelSpeed;
};

#endif // SONETDEVICE_H
