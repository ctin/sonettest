﻿#ifndef MODULEQMLADAPTER_H
#define MODULEQMLADAPTER_H

#include <QtDeclarative>
#include <QtCore>
#include "objects.h"



class ModuleQMLAdapter : public QDeclarativeItem
{
    Q_OBJECT
    Q_PROPERTY(int slot READ slot WRITE setslot NOTIFY slotChanged)
    Q_PROPERTY(int deviceID READ deviceID WRITE setdeviceID NOTIFY deviceIDChanged)
    Q_PROPERTY(int type READ type WRITE settype NOTIFY typeChanged)
    Q_PROPERTY(int channelsCount READ channelsCount WRITE setchannelsCount NOTIFY channelsCountChanged)
    Q_PROPERTY(bool analog READ analog NOTIFY analogChanged)
    Q_PROPERTY(bool output READ output NOTIFY outputChanged)
    Q_PROPERTY(bool input READ input NOTIFY inputChanged)
    Q_PROPERTY(bool inverse READ inverse NOTIFY inverseChanged)
    Q_PROPERTY(bool valid READ valid NOTIFY validChanged)
    Q_PROPERTY(qreal realMin READ realMin NOTIFY realMinChanged)
    Q_PROPERTY(qreal realMax READ realMax NOTIFY realMaxChanged)
    Q_PROPERTY(qreal displayMin READ displayMin NOTIFY displayMinChanged)
    Q_PROPERTY(qreal displayMax READ displayMax NOTIFY displayMaxChanged)
    Q_PROPERTY(QString unit READ unit NOTIFY unitChanged)
    Q_PROPERTY(QString rangeInfo READ rangeInfo NOTIFY rangeInfoChanged) //объединяет 5 строк до.
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(QString KYNI READ KYNI NOTIFY KYNIChanged)
    Q_PROPERTY(QString comments READ comments NOTIFY commentsChanged)
    Q_PROPERTY(int dchan READ dchan NOTIFY dchanChanged)
    Q_PROPERTY(QString firmware READ firmware NOTIFY firmwareChanged)

public:
    explicit ModuleQMLAdapter(QDeclarativeItem *parent = 0);
    Q_INVOKABLE bool isDataValid(QVariant data) const;//return == errorValue()
    Q_INVOKABLE QString toReal(int code) const;
    Q_INVOKABLE QString toHex(int code) const;
    Q_INVOKABLE QString toRealDiagModule(int code) const;
    int slot() const;
    int deviceID() const;
    int type() const;
    int channelsCount() const;
    bool analog() const;
    bool output() const;
    bool input() const;
    bool inverse() const;
    bool valid() const;
    qreal realMin() const;
    qreal realMax() const;
    qreal displayMin() const;
    qreal displayMax() const;
    QString unit() const;
    QString rangeInfo() const;
    QString name() const;
    QString KYNI() const;
    QString comments() const;
    int dchan() const;
    QString firmware() const;

public slots:
    void notifyParam();
    void setslot(const int val);
    void setdeviceID(const int deviceID);
    void settype(const int type);
    void setchannelsCount(const int channelsCount);

signals:
    void slotChanged(int slot);
    void realMinChanged(qreal realMin);
    void realMaxChanged(qreal realMax);
    void displayMinChanged(qreal displayMin);
    void displayMaxChanged(qreal displayMax);
    void unitChanged(QString unit);
    void rangeInfoChanged(QString rangeInfo);
    void nameChanged(QString name);
    void KYNIChanged(QString KYNI);
    void commentsChanged(QString comments);
    void dchanChanged(int dchan);
    void deviceIDChanged(int deviceID);
    void typeChanged(int type);
    void channelsCountChanged(int channelsCount);
    void analogChanged(bool analog);
    void outputChanged(bool output);
    void inputChanged(bool input);
    void inverseChanged(bool inverse);
    void validChanged(bool valid);
    void firmwareChanged(QString firmware);
private:
    int m_slot;
    int m_deviceID;
    int m_type;
    int m_channelsCount;
};

#endif // MODULEQMLADAPTER_H
