﻿#include "sonetqmladapter.h"

ProcModuleQMLAdapter::ProcModuleQMLAdapter(QDeclarativeItem *parent) :
    QDeclarativeItem(parent)
{
}

DeviceConfig ProcModuleQMLAdapter::getConfig() const
{
    return m_config;
}

int ProcModuleQMLAdapter::togglerState() const
{
    return m_togglerState;
}

int ProcModuleQMLAdapter::dial10Value() const
{
    return m_config.getNumTen();
}

int ProcModuleQMLAdapter::dial1Value() const
{
    return m_config.getNumUnit();
}

int ProcModuleQMLAdapter::dialSpeedValue() const
{
    return m_config.getSpeed();
}

int ProcModuleQMLAdapter::modbusInput() const
{
    return m_config.getModbusType();
}

void ProcModuleQMLAdapter::setTogglerState(int val)
{
    if(m_togglerState == val)
        return;
    m_togglerState = val;
    emit togglerStateChanged(val);
}

void ProcModuleQMLAdapter::setDial10Value(int val)
{
    if(m_config.getNumTen() == val)
        return;
    m_config.setNum(m_config.getModbusType() * 100 + val * 10 + m_config.getNumUnit());
    emit dial10ValueChanged(val);
}

void ProcModuleQMLAdapter::setDial1Value(int val)
{
    if(m_config.getNumUnit() == val)
        return;
    m_config.setNum((m_config.getNum() / 10) * 10 + val);
    emit dial1ValueChanged(val);
}

void ProcModuleQMLAdapter::setDialSpeedValue(int val)
{
    if(m_config.getSpeed() == val)
        return;
    m_config.setSpeed(val);
    emit dialSpeedValueChanged(val);
}

void ProcModuleQMLAdapter::setModbusInput(int val)
{
    if(m_config.getModbusType() == val)
        return;
    m_config.setNum(m_config.getNum() % 100 + val * 100);//TODO: сделать что-нибудь чтоб можно было использовать для модуля диагностики
    emit modbusInputChanged(val);
}

ModuleQMLAdapter::ModuleQMLAdapter(const int num, DeviceData *pData, QDeclarativeItem *parent) : QDeclarativeItem(parent), m_slotNum(num)
{
    m_data.setConnections(pData);
}

int ModuleQMLAdapter::ID() const
{
    return m_data.getConsist().getModuleConsist(m_slotNum).getID();
}

int ModuleQMLAdapter::min() const
{
    return m_data.getParameters()[ID()].m_min;
}

int ModuleQMLAdapter::max() const
{
    return m_data.getParameters()[ID()].m_max;
}

QString ModuleQMLAdapter::unit() const
{
    return m_data.getParameters()[ID()].m_unit;
}

QString ModuleQMLAdapter::name() const
{
    return m_data.getParameters()[ID()].m_name;
}

QString ModuleQMLAdapter::KYNI() const
{
    return m_data.getParameters()[ID()].m_KYNI;
}

QString ModuleQMLAdapter::comments() const
{
    return m_data.getParameters()[ID()].m_comments;
}

int ModuleQMLAdapter::dchan() const
{
    return m_data.getParameters()[ID()].m_dchan;
}
