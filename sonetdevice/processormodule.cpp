﻿#include "processormodule.h"

using namespace SonetWidgets;

ProcessorModule::ProcessorModule(QWidget *parent)
    :   QWidget(parent)
{
    createControls();
    createLayouts();
    setConfig(DeviceConfig());
    setStyleSheet("QWidget{background: rgb(238, 238, 229);}");
    toggled();

    connect(m_pToggler, SIGNAL(clicked()), SLOT(toggled()));
}

void ProcessorModule::deviceAviable()
{
    m_pGreenDiodWork->setOn();
    m_pRedDiodFailure->setOff();
}

void ProcessorModule::deviceNotFound()
{
    m_pGreenDiodWork->setOff();
    m_pRedDiodFailure->setOn();
}

void ProcessorModule::bytesRecieved()
{
    m_pModbusSocket1->isOn() ? m_pGreenDiodRx1->setOn() : m_pGreenDiodRx2->setOn();
}

void ProcessorModule::bytesWritten()
{
    m_pModbusSocket1->isOn() ? m_pGreenDiodTx1->setOn() : m_pGreenDiodTx2->setOn();

}

void ProcessorModule::toggled()
{
    if(m_pToggler->isOn())
    {
        m_pGreenDiodWork->setWait();
        m_pRedDiodFailure->setWait();
        connect(m_pDialSpeed, SIGNAL(valueChanged(int)), SLOT(changeConfig()));
        connect(m_pDialX10, SIGNAL(valueChanged(int)), SLOT(changeConfig()));
        connect(m_pDialX1, SIGNAL(valueChanged(int)), SLOT(changeConfig()));
        connect(m_pModbusSocket1, SIGNAL(clicked()), SLOT(modbus1Clicked()));
        connect(m_pModbusSocket2, SIGNAL(clicked()), SLOT(modbus2Clicked()));
        connect(m_pDialSpeed, SIGNAL(valueChanged(int)), m_pGreenDiodWork, SLOT(setWait()));
        connect(m_pDialX10, SIGNAL(valueChanged(int)), m_pGreenDiodWork, SLOT(setWait()));
        connect(m_pDialX1, SIGNAL(valueChanged(int)), m_pGreenDiodWork, SLOT(setWait()));
        connect(m_pModbusSocket1, SIGNAL(clicked()), m_pGreenDiodWork, SLOT(setWait()));
        connect(m_pModbusSocket2, SIGNAL(clicked()), m_pGreenDiodWork, SLOT(setWait()));
        connect(m_pDialSpeed, SIGNAL(valueChanged(int)), m_pRedDiodFailure, SLOT(setWait()));
        connect(m_pDialX10, SIGNAL(valueChanged(int)), m_pRedDiodFailure, SLOT(setWait()));
        connect(m_pDialX1, SIGNAL(valueChanged(int)), m_pRedDiodFailure, SLOT(setWait()));
        connect(m_pModbusSocket1, SIGNAL(clicked()), m_pRedDiodFailure, SLOT(setWait()));
        connect(m_pModbusSocket2, SIGNAL(clicked()), m_pRedDiodFailure, SLOT(setWait()));
    }
    else
    {
        m_pGreenDiodWork->setOff();
        m_pRedDiodFailure->setOff();
        disconnect(m_pDialSpeed, SIGNAL(valueChanged(int)), this, SLOT(changeConfig()));
        disconnect(m_pDialX10, SIGNAL(valueChanged(int)), this, SLOT(changeConfig()));
        disconnect(m_pDialX1, SIGNAL(valueChanged(int)), this, SLOT(changeConfig()));
        disconnect(m_pModbusSocket1, SIGNAL(clicked()), this, SLOT(modbus1Clicked()));
        disconnect(m_pModbusSocket2, SIGNAL(clicked()), this, SLOT(modbus2Clicked()));
        disconnect(m_pDialSpeed, SIGNAL(valueChanged(int)), m_pGreenDiodWork, SLOT(setWait()));
        disconnect(m_pDialX10, SIGNAL(valueChanged(int)), m_pGreenDiodWork, SLOT(setWait()));
        disconnect(m_pDialX1, SIGNAL(valueChanged(int)), m_pGreenDiodWork, SLOT(setWait()));
        disconnect(m_pModbusSocket1, SIGNAL(clicked()), m_pGreenDiodWork, SLOT(setWait()));
        disconnect(m_pModbusSocket2, SIGNAL(clicked()), m_pGreenDiodWork, SLOT(setWait()));
        disconnect(m_pDialSpeed, SIGNAL(valueChanged(int)), m_pRedDiodFailure, SLOT(setWait()));
        disconnect(m_pDialX10, SIGNAL(valueChanged(int)), m_pRedDiodFailure, SLOT(setWait()));
        disconnect(m_pDialX1, SIGNAL(valueChanged(int)), m_pRedDiodFailure, SLOT(setWait()));
        disconnect(m_pModbusSocket1, SIGNAL(clicked()), m_pRedDiodFailure, SLOT(setWait()));
        disconnect(m_pModbusSocket2, SIGNAL(clicked()), m_pRedDiodFailure, SLOT(setWait()));
    }
}

void ProcessorModule::setConfig(const DeviceConfig config)
{
    m_config = config;
    m_pDialSpeed->setValue(m_config.getSpeed());
    m_pDialX10->setValue(m_config.getNumTen());
    m_pDialX1->setValue(m_config.getNumUnit());
    m_config.getModbusType() ? modbus2Clicked() : modbus1Clicked();
}

DeviceConfig ProcessorModule::getConfig() const
{
    return DeviceConfig(m_pDialSpeed->getValue(), m_pModbusSocket2->isOn() * 100 + m_pDialX10->getValue() * 10 + m_pDialX1->getValue());
}

void ProcessorModule::changeConfig()
{
    m_config = getConfig();
    emit configChanged(getConfig());
}

void ProcessorModule::modbus1Clicked()
{
    static QFont bold, normal;
    bold.setBold(true);
    normal.setBold(false);
    m_pTextModbus1->setFont(bold);
    m_pTextModbus2->setFont(normal);
    m_pTextModbus1->adjustSize();
    m_pTextModbus2->adjustSize();
    m_pModbusSocket1->setOn();
    m_pModbusSocket2->setOff();
    changeConfig();
}

void ProcessorModule::modbus2Clicked()
{
    static QFont bold, normal;
    bold.setBold(true);
    normal.setBold(false);
    m_pTextModbus2->setFont(bold);
    m_pTextModbus1->setFont(normal);
    m_pModbusSocket1->setOff();
    m_pModbusSocket2->setOn();
    m_pTextModbus1->adjustSize();
    m_pTextModbus2->adjustSize();
    changeConfig();
}

void ProcessorModule::createControls()
{
    m_pGreenDiodRx1     = new GreenDiod(100, this);
    m_pGreenDiodRx2     = new GreenDiod(100, this);
    m_pGreenDiodTx1     = new GreenDiod(100, this);
    m_pGreenDiodTx2     = new GreenDiod(100, this);
    m_pGreenDiodWork    = new GreenDiod(1500, this);
    m_pGreenDiodWork->setWait();
    m_pRedDiodFailure   = new RedDiod(1500, this);
    m_pRedDiodFailure->setWait();
    m_pDialX1           = new Dial(this);
    m_pDialX10          = new Dial(this);
    m_pDialSpeed        = new Dial(this);
    m_pToggler          = new Toggler(this);
    m_pModbusSocket1    = new ModbusSocket(this);
    m_pModbusSocket2    = new ModbusSocket(this);

    m_pTextModbus1      = new QLabel("Modbus 1", this);
    m_pTextModbus2      = new QLabel("Modbus 2", this);

    m_pPortList         = new QComboBox(this);
    m_pPortList->addItem("Автоматически");
    for(int i = 0; i < 10;  i++)
        m_pPortList->addItem(QString::number(i));
}

void ProcessorModule::createLayouts()
{
    QLabel *pLabel = new QLabel("Tx", this);
    pLabel->move(10, 1);
    pLabel = new QLabel("Rx", this);
    pLabel->move(10, 22);
    m_pGreenDiodTx1->move(35, 4);
    m_pGreenDiodRx1->move(35, 26);
    m_pTextModbus1->move(1, 47);
    m_pModbusSocket1->move(10, 68);
    pLabel = new QLabel("Tx", this);
    pLabel->move(10, 171);
    pLabel = new QLabel("Rx", this);
    pLabel->move(10, 192);
    m_pGreenDiodTx2->move(35, 174);
    m_pGreenDiodRx2->move(35, 195);
    m_pTextModbus2->move(1, 215);
    m_pModbusSocket2->move(10, 235);


    pLabel = new QLabel("Работа", this);
    pLabel->move(102, 1);
    pLabel = new QLabel("Авария", this);
    pLabel->move(102, 22);
    pLabel = new QLabel("Пуск", this);
    pLabel->move(102, 47);
    pLabel = new QLabel("Стоп", this);
    pLabel->move(102, 67);
    m_pGreenDiodWork->move(171, 4);
    m_pRedDiodFailure->move(171, 26);
    m_pToggler->move(170, 57);

    pLabel = new QLabel("X10", this);
    pLabel->move(102, 116);
    pLabel = new QLabel("X1", this);
    pLabel->move(102, 193);

    m_pDialX10->move(135, 95);
    m_pDialX1->move(135, 170);
    VerticalText *pVerticalText = new VerticalText("адрес", this);
    pVerticalText->move(188, 150);
    m_pDialSpeed->move(135, 250);
    pVerticalText = new VerticalText("скорость", this);
    pVerticalText->move(188, 248);
    pLabel = new QLabel("COM:", this);
    pLabel->move(1, 333);
    m_pPortList->move(55, 331);
    setFixedSize(215, 375);
}

void ProcessorModule::createTabs()
{
    setTabOrder(m_pDialX10, m_pDialX1);
    setTabOrder(m_pDialX1, m_pDialSpeed);
    setTabOrder(m_pDialSpeed, m_pDialX10);
}
