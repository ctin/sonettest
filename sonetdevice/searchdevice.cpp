﻿#include "searchdevice.h"
#include "commands.h"

SearchDevice::SearchDevice(QObject *parent) :
    SonetDevice(parent)
{
    setrunning(false);
    setdeviceNum(0);
    setfounded(0);
    setautoSearch(false);
    setpacketComplexity(0);
    setreadTotalTimeoutConstant(0);
    m_requestToStart = false;
    m_needToCheck = false;
    m_scanMode = true;
}

SearchDevice::~SearchDevice()
{
    stop();
}

void SearchDevice::__processCurrentNumber(const int num, const QString &port)
{
    ByteArray ans;
    unsetLastError();
    setdeviceNum(num);
    if(find()
            && ((!m_needToCheck)
                || (MODBUS_command(MODBUS_get_fullConfig(getCommandNamePtr()))
                    && MODBUS_command(MODBUS_get_fullConfig(getCommandNamePtr()), &ans) && ans.size() > 3)))
    {
        int bprState = !m_needToCheck || (ans.at(2) >= 100) ? -1 : ans.at(0);
        emit deviceFounded(bprState, rateIndex(), num, true, port);
        setfounded(founded() + 1);
        return;
    }
    if(!m_answer.isEmpty())
        emit deviceFailed(rateIndex(), num, true, port, lastError());
}

void SearchDevice::__scanPort(const QString &port)
{
    if(!running())
        return;
    if(m_modbus1)
    {
        for(int num = 0;  num < 101 && running() && isOpen();  num++)
            __processCurrentNumber(num, port);
    }
    if(m_modbus2)
    {
        for(int num = 101;  num < 200 && running() && isOpen();  num++)
            __processCurrentNumber(num, port);
    }
    if(m_modbus2 || m_modbus2)
        __processCurrentNumber(200, port);
    if(m_modbus1)
    {
        for(int num = 201;  num < 210 && running() && isOpen();  num++)
            __processCurrentNumber(num, port);
    }
    if(m_modbus2)
    {
        for(int num = 210;  num < 220 && running() && isOpen();  num++)
            __processCurrentNumber(num, port);
    }
    setdeviceNum(0);
}

void SearchDevice::findAllProcess()
{
    m_collecting = true;
    setfounded(0);
    setautoSearch(false);
    setrunning(true);//здесь running - показатель процесса поиска.
    setdataBits(g_defaultDataBits);
    setstopBits(g_defaultStopBits);
    setflowIndex(g_defaultFlowIndex);
    setparityIndex(g_defaultParityIndex);
    if(!g_autoSearchEnabled && port().isEmpty())
        setport(g_defaultPort);
    if(g_autoSearchEnabled && port().isEmpty())
    {
        for(int num = 0xff;  num > 0;  num--) //0xff - portsCount
        {
            QString portName(QString("COM%1").arg(num));
            setport(portName);
            PortOpener opener(this);
            if(!isOpen())
            {
                if(!hardwareError().isEmpty())
                {
                    for(int i = 0;  i < 100 && running();  i++)
                        Sleepy::msleep(3000 / 100);
                    unsetHardwareError();
                }
            }
            else
            {
                addAvailablePort(portName);
                __scanPort(portName);
            }
        }
        unsetPort();
    }
    else if(!port().isEmpty())
    {
        PortOpener opener(this);
        if(!isOpen())
        {
            for(int i = 0;  i < 100 && running();  i++)
                Sleepy::msleep(3000 / 100);
            unsetHardwareError();
        }
        else
            __scanPort(port());
    }
    setdeviceNum(0);
    setrunning(false);
    m_requestToStart = false;
    m_collecting = false;
    emit finished();
}

void SearchDevice::findAllSlow(int modbus1, int modbus2)
{
    setwaitTime(floor(11 * 14 * 1.0 * 1000 * 3 / realRate()) + 11 + 120 + g_additionalWaitTime);
    m_modbus1 = modbus1; m_modbus2 = modbus2;
    m_needToCheck = true;
    m_requestToStart = true;
}

void SearchDevice::findAllFast()
{
    setwaitTime(floor(11 * 14 * 1.0 * 1000 * 3 / realRate()) + 11 + 20 + g_additionalWaitTime);
    m_modbus1 = true; m_modbus2 = true;
    m_needToCheck = false;
    m_requestToStart = true;
}

void SearchDevice::stop()
{
    setrunning(false);
    m_future.waitForFinished();
}

PROPERTY_CPP(int, SearchDevice, founded)
