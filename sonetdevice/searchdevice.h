﻿#ifndef SEARCHDEVICE_H
#define SEARCHDEVICE_H

#include "sonetdevice.h"

class SearchDevice : public SonetDevice
{
    Q_OBJECT
    Q_PROPERTY(int founded READ founded WRITE setfounded NOTIFY foundedChanged)
public:
    explicit SearchDevice(QObject *parent = 0);
    ~SearchDevice();
    volatile bool m_requestToStart, m_needToCheck;
public slots:
    void stop();
    void findAllSlow(int modbus1, int modbus2);
    void findAllFast();
private:
    QFuture<void> m_future;
    void __scanPort(const QString &port);
    void __processCurrentNumber(const int number, const QString &port);
signals:
    void deviceFounded(int bprState, int rateIndex, int deviceNum, int autoSearch, QString port);
    void deviceFailed(int rateIndex, int deviceNum, int autoSearch, QString port, QString lastError);
    void finished();
public:
    void findAllProcess();
private:
    int m_modbus1, m_modbus2;
public:
    int founded() const;
public slots:
    void setfounded(int arg);
signals:
    void foundedChanged(int arg);
private:
    int m_founded;
};

#endif // SEARCHDEVICE_H
