﻿#include "moduleqmladapter.h"
#include "macro.h"


ModuleQMLAdapter::ModuleQMLAdapter(QDeclarativeItem *parent) : QDeclarativeItem(parent)
{
    m_deviceID = 0xff;
    m_type = 0xff;
    m_channelsCount = 0xff;
    connect(this, SIGNAL(deviceIDChanged(int)), this, SLOT(notifyParam()), Qt::QueuedConnection);
}

bool ModuleQMLAdapter::isDataValid(QVariant data) const
{
    return DeviceParameters::isDataValid(data);
}

QString ModuleQMLAdapter::toReal(int code) const
{
    return QString::number(code * (displayMax() - displayMin()) / 0xffff + displayMin(), 'f').leftJustified(7, '0', true);
}

QString ModuleQMLAdapter::toRealDiagModule(int code) const
{
    qreal result = 24.0 * code / 0xf8;
    if(result < 15)
        return " < 15";
    return QString::number(result, 'f').leftJustified(7, '0', true);
}

QString ModuleQMLAdapter::toHex(int code) const
{
    return QString::number(code, 16).rightJustified(4, '0');
}

void ModuleQMLAdapter::notifyParam()
{
    emit realMinChanged(realMin());
    emit realMaxChanged(realMax());
    emit displayMinChanged(displayMin());
    emit displayMaxChanged(displayMax());
    emit unitChanged(unit());
    emit rangeInfoChanged(rangeInfo());
    emit nameChanged(name());
    emit KYNIChanged(KYNI());
    emit commentsChanged(comments());
    emit dchanChanged(dchan());
    emit firmwareChanged(firmware());
}

int ModuleQMLAdapter::slot() const
{
    return m_slot;
}

void ModuleQMLAdapter::setslot(const int val)
{
    if(m_slot != val)
    {
        m_slot = val;
        emit validChanged(valid());
        emit slotChanged(val);
    }
}

void ModuleQMLAdapter::setdeviceID(const int deviceID)
{
    if(m_deviceID == deviceID)
        return;
    m_deviceID = deviceID;
    emit validChanged(valid());
    emit deviceIDChanged(deviceID);
}

int ModuleQMLAdapter::deviceID() const
{
    return m_deviceID;
}

void ModuleQMLAdapter::setchannelsCount(const int channelsCount)
{
    if(m_channelsCount == channelsCount)
        return;
    m_channelsCount = channelsCount;
    emit validChanged(valid());
    emit rangeInfoChanged(rangeInfo());
    emit channelsCountChanged(this->channelsCount());
}

int ModuleQMLAdapter::channelsCount() const
{
    if(m_channelsCount == 0xff)
        return m_channelsCount;
    bool hasDchan = g_parameterStorage.getDchan(deviceID());
    return m_channelsCount - hasDchan;
}

void ModuleQMLAdapter::settype(const int type)
{
    if(m_type == type)
        return;
    m_type = type;
    emit validChanged(valid());
    emit analogChanged(analog());
    emit outputChanged(output());
    emit inputChanged(input());
    emit inverseChanged(inverse());
    emit typeChanged(type);
    emit rangeInfoChanged(rangeInfo());
}

int ModuleQMLAdapter::type() const
{
    return m_type;
}

bool ModuleQMLAdapter::analog() const
{
    return type() & 0x40;
}

bool ModuleQMLAdapter::output() const
{
    return type() & 0x20;
}

bool ModuleQMLAdapter::input() const
{
    return !(type() & 0x20);
}

bool ModuleQMLAdapter::inverse() const
{
    return type() & 0x10;
}

bool ModuleQMLAdapter::valid() const
{
    return slot() >= 0 && slot() < SLOTS_COUNT && deviceID() != 0xff && type() != 0xff && IS_CHANNEL_VALID(channelsCount());
}

qreal ModuleQMLAdapter::realMin() const
{
    return g_parameterStorage.realMin(deviceID());
}

qreal ModuleQMLAdapter::realMax() const
{
    return g_parameterStorage.realMax(deviceID());
}

qreal ModuleQMLAdapter::displayMin() const
{
    return g_parameterStorage.displayMin(deviceID());
}

qreal ModuleQMLAdapter::displayMax() const
{
    return g_parameterStorage.displayMax(deviceID());
}

QString ModuleQMLAdapter::unit() const
{
    return g_parameterStorage.getUnit(deviceID());
}

QString ModuleQMLAdapter::rangeInfo() const
{
    if(!valid())
        return "ошибка";
    QString result;
    if(analog())
    {
        if(isDataValid(realMin()))
            result += QString("%1").arg(realMin());
        else if(isDataValid(displayMin()))
            result += QString::number(displayMin());
        else
            result += "Na";
        result += "÷";
        if(isDataValid(realMax()))
            result += QString("%1").arg(realMax());
        else if(isDataValid(displayMax()))
            result += QString::number(displayMax());
        else
            result += "Na";
        result += " " + (isDataValid(unit()) ?  unit() : "Na");
    }
    else if(comments().contains("реле"))
    {
        result += "Реле";
    }
    else
    {
        if(isDataValid(realMax()))
            result += QString("%1").arg(realMax());
        else if(isDataValid(displayMax()))
            result += QString::number(displayMax());
        else
            result += "Na";
        result += " " + (isDataValid(unit()) ?  unit() : "Na");
    }
    return result;
}

QString ModuleQMLAdapter::name() const
{
    return g_parameterStorage.getName(deviceID());
}

QString ModuleQMLAdapter::KYNI() const
{
    return g_parameterStorage.getKYNI(deviceID());
}

QString ModuleQMLAdapter::comments() const
{
    return g_parameterStorage.getComments(deviceID());
}

int ModuleQMLAdapter::dchan() const
{
    bool hasDchan = g_parameterStorage.getDchan(deviceID());
    return hasDchan;
}

QString ModuleQMLAdapter::firmware() const
{
    return g_parameterStorage.getFirmware(deviceID());
}
