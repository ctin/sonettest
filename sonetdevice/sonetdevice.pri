
INCLUDEPATH  += $$PWD
DEPENDPATH   += $$PWD

#QMAKE_CXXFLAGS_DEBUG += -pg
#QMAKE_LFLAGS_DEBUG += -pg

SOURCES +=  \
    $$PWD/objects.cpp \
    $$PWD/sonetdevice.cpp \
    $$PWD/moduleqmladapter.cpp \
    $$PWD/devicewithdata.cpp \
    sonetdevice/searchdevice.cpp
HEADERS  +=  \
    $$PWD/objects.h \
    $$PWD/sonetdevice.h \
    $$PWD/moduleqmladapter.h \
    $$PWD/devicewithdata.h \
    sonetdevice/searchdevice.h
