﻿#ifndef OBJECTS_H
#define OBJECTS_H

#define SLOTS_COUNT     8
#include <QtCore>
#include <QtGui>
#include <QtDeclarative>
#include "peripheral.h"

#define IS_CHANNEL_VALID(channelsCount) ((channelsCount) <= 32)

#define IS_MODULE_NEED_REFLASH(id) ((id) >= 51 && (id) <= 54)

extern AntiLockTimer *g_pAntiLockTimer;

enum MODULE_TYPE {
    SONET_MODULE,
    DIAGNOSTIC_MODULE
};

class ByteArray : public QByteArray
{
public:
    int at(int i) const;
};

Q_DECLARE_METATYPE(ByteArray)

class Sleepy : public QThread
{
public:
    static void msleep(unsigned long msec);
    static void usleep(unsigned long usec);
};

struct ModuleDataItem
{
    ModuleDataItem();
    void unsetValues();
    static unsigned short unreadedValue() {return 0;}
    static unsigned long unitializedValue() {return 0x10000;}
    bool isEmpty() const;
    bool isValid() const;
    bool requestToSetChannelValue() const;
    bool requestToSetEEPROMValue(int index) const;
    bool needRefreshAfterSet; //для внутреннего использования. Проверка данных после выставления их в EEPROM прибора.
    QList<unsigned short> deviceEEPROMValues;
    QList<unsigned long> userEEPROMValues;
    unsigned short deviceValue;
    unsigned long userValue;
    bool dontUnset;
};

Q_DECLARE_METATYPE(QList<unsigned short>)

class ModuleDataModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum ModuleDataRoles
    {
        DeviceValueRole = Qt::UserRole + 1,
        UserValueRole,
        DeviceEEPROMValuesRole,
        UserEEPROMValuesRole,
        RequestToSetChannelValueRole,
        RequestToSetEERPOMValueRole,
        IsValidRole,
        IsEmptyRole,
        DontUnsetRole,
        RequestToSetCalibrDateRole,
        RequestToSetVerifyDateRole,
        RequestToSetCalibrFullNameRole,
        RequestToSetVerifyFullNameRole,
        ContainsDataFullNameRole,
        DeviceCalibrDateRole,
        DeviceVerifyDateRole,
        DeviceCalibrFullNameRole,
        DeviceVerifyFullNameRole,
        UserCalibrDateRole,
        UserVerifyDateRole,
        UserCalibrFullNameRole,
        UserVerifyFullNameRole
    };

    explicit ModuleDataModel(QObject *parent = 0);
    QHash<int, QByteArray> roleNames() const;
    ~ModuleDataModel();
    int length() const;
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    bool setData(const int index, const QVariant &value, const int role);
    bool setData(const QModelIndex &index, const QVariant &value, const int role);
    bool setData(const int first, const int last, const QVariantList &values, const int role);
    bool setData(const int first, const int last, const QVariant &value, const int role);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    const ModuleDataItem &getItem(int channel);

    bool requestToSetCalibrDate() const;
    bool requestToSetVerifyDate() const;
    bool requestToSetCalibrFullName() const;
    bool requestToSetVerifyFullName() const;
private:
    unsigned long containsDateFullName;
    QDate userCalibrDate, userVerifyDate;
    QDate deviceCalibrDate, deviceVerifyDate;
    QByteArray userCalibrFullName, userVerifyFullName;
    QByteArray deviceCalibrFullName, deviceVerifyFullName;
    QHash<int, QByteArray> m_roleNames;
public slots:
    void refreshModuleData(int channelsCount);
    void unsetValues_s();
public slots:
    void __setData(const int index, const QVariant &value, const int role);

private:
    QList<ModuleDataItem> m_moduleDataItems;
    Q_DISABLE_COPY(ModuleDataModel)
};

Q_DECLARE_METATYPE(ModuleDataModel*)

struct DeviceParameters
{
public:
    DeviceParameters();
    ~DeviceParameters();
    uchar m_ID;
    /*ситуация: модуль может показывать чуток больше чем то значение, для которого он предназанчен.
     *Для калибровки и пр. этого учитывать не стоит - только для отображения.
     *Отсюда displayVar - для отображения и realVar - для расчетов*/

    qreal realMax() const;
    qreal realMin() const;
    qreal displayMax() const;
    qreal displayMin() const;
    QString m_unit;
    QVector <qreal> m_points;
    QString m_name, m_KYNI, m_comments, m_firmware;
    qreal m_dchan;
    qreal m_percentCLIT;
    qreal m_percentOTK;
    qreal m_adjustment;
    qreal m_resistor;
    QString getPoints() const;
    QString getStringData() const;
    bool operator == (const DeviceParameters conf) const;
    bool operator != (const DeviceParameters conf) const;
    static qreal errorValue() {return -9999;}
    static bool isDataValid(QVariant data);//return == errorValue()
//private:
    void setPoints(const QString &string);
    void setStringData(const QString &string);
    qreal m_min, m_max;
    qreal m_minMetrol, m_maxMetrol;
    QStringList m_calibrators;
    QString getCalibratorsString() const;
    void setCalibrators(const QString string);
};

typedef QMap <uchar, DeviceParameters > ParameterMap;
typedef QMap <QString, QString> CalibratorMap;

class ParameterStorage : public QObject
{
    Q_OBJECT
public:
    bool containsID(const int ID) const;
    ParameterStorage(QObject *parent = 0);
    const DeviceParameters getParameters(const int ID) const;
    int getID(const int ID) const;
    qreal realMax(const int ID) const;
    qreal realMin(const int ID) const;
    qreal displayMax(const int ID) const;
    qreal displayMin(const int ID) const;
    QString getUnit(const int ID) const;
    QVector <qreal> getPoints(const int ID) const;
    QString getPointsStr(const int ID) const;
    QString getName(const int ID) const;
    QString getKYNI(const int ID) const;
    QString getComments(const int ID) const;
    bool getDchan(const int ID) const;
    qreal getPercentOTK(const int ID) const;
    qreal getPercentCLIT(const int ID) const;
    qreal getAdjustment(const int ID) const;
    qreal getResistor(const int ID) const;
    QString getStringData(const int ID) const;
    QString getFirmware(const int ID) const;
    QStringList getCalibrators(const int ID) const;
    void setParameters(const int ID, const DeviceParameters &deviceParameters);
    void setParameterMap(const ParameterMap &parameterMap); //для загрузки из файла
    const ParameterMap& getParameterMap(void) const;
signals:
    void parametersChanged();
private:
    ParameterMap m_parameterMap;
    mutable QMutex m_mutex;
};

struct SonetConfig
{
    SonetConfig(int rateIndexTmp = 7, int deviceNumTmp = 44, int autoSearchTmp = true, QString portTmp = QString());
    SonetConfig(const SonetConfig &conf);

    int rateIndex;
    int deviceNum;
    int autoSearch;
    QString port;
};

extern ParameterStorage g_parameterStorage;
//extern QThreadStorage <ParameterStorage> g_threadStorage;

Q_DECLARE_METATYPE(DeviceParameters)
Q_DECLARE_METATYPE(ParameterMap)
Q_DECLARE_METATYPE(SonetConfig)

extern QDataStream &operator << ( QDataStream &out, const DeviceParameters& );
extern QDataStream &operator >> ( QDataStream &in, DeviceParameters& );

extern QDataStream &operator << ( QDataStream &out, const SonetConfig& );
extern QDataStream &operator >> ( QDataStream &in, SonetConfig& );

//extern QDataStream &operator<<( QDataStream &out, const QMap <uchar, DeviceParameters >& );
//extern QDataStream &operator>>( QDataStream &in, QMap <uchar, DeviceParameters >& );




#endif // OBJECTS_H
