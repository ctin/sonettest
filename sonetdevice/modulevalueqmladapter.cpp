﻿#include "modulevalueqmladapter.h"

ModuleValueQMLAdapter::ModuleValueQMLAdapter(QDeclarativeItem *parent) :
    QDeclarativeItem(parent)
{
}

long ModuleValueQMLAdapter::value() const
{
    return m_value;
}

void ModuleValueQMLAdapter::setValue(const long value)
{
    m_newValue = value;
}

void ModuleValueQMLAdapter::setValueFromDevice(const long value)
{
    if(m_value == value)
        return;
    m_value = value;
    emit valueChanged(value);
}
