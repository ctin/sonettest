﻿#include "devicewithdata.h"
#include "commands.h"
#include "macro.h"
#include "peripheral.h"
#include "devicelist.h"
#include "peripheral.h"

static const int EEPROM_DATA_COUNT_SONET = 2, EEPROM_DATA_COUNT_DIAG = 8;

DeviceWithData::DeviceWithData(DeviceList *pDeviceList, int rateIndex, int deviceNum, bool autoSearch, QString port, QObject *parent)
    : SonetDevice(parent), m_pDeviceList(pDeviceList)
{
    Q_ASSERT(m_pDeviceList);
    if(rateIndex)
        setrateIndex(rateIndex);
    else
        setrateIndex(g_defaultRateIndex);
    setdeviceNum(deviceNum);
    setautoSearch(autoSearch);
    if(!port.isEmpty())
        setport(port);
    else
        setport(g_defaultPort);
    QStringList avPorts = availablePorts();
    for(int i = 0;  i < avPorts.size();  i++)
        if(port == avPorts.at(i))
        {
            m_portIndex = i;
            break;
        }

    setcomponentID(-1);
    setturnedOn(true);

    for(int i = 0;  i < SLOTS_COUNT;  i++)
    {
        m_moduleDataModel.append(new ModuleDataModel(this));
        m_deviceIDs.append(0xff);
        m_types.append(0xff);
        m_channelsCount.append(0xff);
        m_hasPower.append(0);
        m_canSurveyModules.append(1);
    }
    m_writingEEPROM = false;
    m_needSetEEPROMValues = false;
    m_underTests = false;
    createBaseSurveyFunctions();
    setdeleteMe(false);
    refreshIsSonet();
    connect(this, SIGNAL(deviceNumChanged(int)), this, SLOT(onDeviceParametersChanged()));
    connect(this, SIGNAL(rateIndexChanged(int)), this, SLOT(onDeviceParametersChanged()));
    connect(this, SIGNAL(deviceNumChanged(int)), this, SLOT(refreshIsSonet()));
    connect(this, SIGNAL(deviceIDsChanged(QList<int>)), this, SLOT(refreshIsSonet()));
    //connect(this, SIGNAL(deviceAviableChanged(bool)), this, SLOT(onDeviceAviableChanged()));
}

void DeviceWithData::emitAllGUISignals()
{
    SonetDevice::emitAllGUISignals();
    m_needRefreshValue = false;
}

DeviceWithData::~DeviceWithData()
{
    setrunning(false);
    while(m_collecting)
        Sleepy::usleep(100);
}
#define IS_SONET(num, id)  ((num) < 200 || ((num) == 200 && (id) < 100))

void DeviceWithData::refreshIsSonet()
{
    Q_ASSERT(!deviceIDs().isEmpty());
    int num = deviceNum(), id = deviceIDs().at(0);
    setisSonet(IS_SONET(num, id));
}

void DeviceWithData::setisSonet(bool arg)
{
    if(m_isSonet == arg)
        return;
    m_isSonet = arg;
    emit isSonetChanged(arg);
}

bool DeviceWithData::isSonet() const
{
    Q_ASSERT(!deviceIDs().isEmpty());
    int num = deviceNum(), id = deviceIDs().at(0);
    return IS_SONET(num, id);
}

void DeviceWithData::onDeviceParametersChanged()
{
    unsetPort();
    unsetDeviceAviable();
}

void DeviceWithData::onDeviceAviableChanged()
{
    if(deviceAviable())
        return;
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        int chCount = channelsCount().at(slot);
        if(!IS_CHANNEL_VALID(chCount))
            continue;
        getDataModel(slot)->unsetValues_s();
    }
}

bool DeviceWithData::__onError(QString message)
{
    bool last = !lastError().isEmpty();
    bool hard = !hardwareError().isEmpty();
    bool aviabled = deviceAviable();
    if(!last && !hard && aviabled)
        return true;
    QString title, text;
    if(message.isEmpty())
        message = "Продолжить?";
    if(last && !hard)
    {
        if(lastError().contains("Нет ответа от устройства"))
        {
            title = "Нет ответа от устройства. " + message;
            text = "";
        }
        else
        {
            title = "Ошибка ответа от устройства! " + message;
            text = "Не критичная для работы, рекомендуется повторить отправку и если не помогло - искать причину ошибки.";
        }
    }
    else if(!last && hard)
    {
        title = "Ошибка COM порта!" + message;
        text = "Проблема на уровне операционной системы, реже интерфейса или переходника. Рекомендуется локализовать ошибку перед продолжением работы.";
    }
    else if(last && hard)
    {
        title = "Ошибка COM порта и ошибка ответа от устройства. " + message;
        text = "Проблема на уровне операционной системы, реже интерфейса или переходника, приведшая к ошибке MODBUS. Рекомендуется локализовать ошибку порта перед продолжением работы.";
    }
    else
    {
        title = "Потеряна связь с устройством. " + message;
        text = "Прибор не ответил за тестовый запрос, связь потеряна.";
    }
    bool retval;
    QMetaObject::invokeMethod(static_cast<Application*>(qApp),
                                 "showErrorMessage",
                                 Qt::BlockingQueuedConnection,
                                 Q_RETURN_ARG(bool, retval),
                                 Q_ARG(QString, title),
                                 Q_ARG(QString, text),
                              Q_ARG(QString, commandName() + "\n" + pack()));
    return retval;
}

void DeviceWithData::createBaseSurveyFunctions()
{
    QMutexLocker lock(&m_commandListMutex);
    m_commandList.clear();
    m_commandList.append(&DeviceWithData::refreshConsist);
    m_commandList.append(&DeviceWithData::getProcessorModuleData);
    m_commandList.append(&DeviceWithData::sendChangedData);
    m_commandList.append(&DeviceWithData::readModules);
    if(valid(0) && deviceIDs().at(0) >= 111)
        m_commandList.append(&DeviceWithData::readEEPROMData);

}

void DeviceWithData::createCalibrSurveyFunctions()
{
    QMutexLocker lock(&m_commandListMutex);
    m_commandList.clear();
    m_commandList.append(&DeviceWithData::refreshConsist);
    m_commandList.append(&DeviceWithData::sendChangedData);
    m_commandList.append(&DeviceWithData::readModules);
    m_commandList.append(&DeviceWithData::sendChangedEEPROMData);
    m_commandList.append(&DeviceWithData::readEEPROMData);
}

void DeviceWithData::collectControllerDataWrapper()
{
    if(!turnedOn() || !deviceNum())
        return;

    m_collecting = true;
    setisOpen(true);
    collectControllerData();
    setisOpen(false);
    m_collecting = false;
}

void DeviceWithData::collectControllerData()
{
    QList<QByteArray> singleUseCommands;
    QList<RequestFunc> commands;
    {
        QMutexLocker lock(&m_commandListMutex);
        singleUseCommands = m_singleUseCommands;
        m_singleUseCommands.clear();
        commands = m_commandList;
    }
    if(!find())
        return;
    if(!singleUseCommands.isEmpty())
        for(QList<QByteArray>::Iterator dataIter = singleUseCommands.begin();
            dataIter != singleUseCommands.end() && running();
            dataIter++ )
        {
            if(!request(*dataIter))
                break;
        }
    for(QList<RequestFunc>::ConstIterator commandIter = commands.constBegin();
        commandIter != commands.constEnd() && running();
        commandIter++)
        if(!(this->*(*commandIter))())
            return;
}

bool DeviceWithData::find()
{
    if(deviceAviable())
        return true;
    if(m_pDeviceList->isDublicate(this))
    {
        setlastError("Такой контроллер уже есть в системе");
        m_portIndex++;
        setdeviceAviable(false);
        return false;
    }
    else
    {
        int lastWaitTime = waitTime();
        setwaitTime(floor(11 * 14 * 1.0 * 1000 * 3 / realRate()) + 11 + 200 + g_additionalWaitTime);
        bool k = SonetDevice::find();
        setwaitTime(lastWaitTime);
        return k;
    }
}

ModuleDataModel* DeviceWithData::getDataModel(int slot)
{
    Q_ASSERT(slot >= 0 && slot < SLOTS_COUNT);
    return m_moduleDataModel.at(slot);
}

void DeviceWithData::setModelData(const int slot, const int channel, const QVariant &value, const QByteArray &roleValue)
{
    Q_ASSERT(slot >= 0 && slot < SLOTS_COUNT);
    ModuleDataModel*    pModuleDataModel = getDataModel(slot);
    for(QHash<int, QByteArray>::ConstIterator iter = pModuleDataModel->roleNames().constBegin();
        iter != pModuleDataModel->roleNames().constEnd();
        iter++)
        if(iter.value() == roleValue)
        {
            pModuleDataModel->setData(pModuleDataModel->index(channel), value, iter.key());
            m_pDeviceList->setnextCollectedIndex(componentID());
            return;
        }
}

void DeviceWithData::setEEPROMUserValue(const int slot, const int channel, const int pos, const QVariant &value)
{
    if(value.toUInt() == ModuleDataItem::unitializedValue())
        return;
    Q_ASSERT(slot >= 0 && slot < SLOTS_COUNT);
    ModuleDataModel*    pModuleDataModel = getDataModel(slot);
    Q_ASSERT(channel >= 0);
    Q_ASSERT(channel < pModuleDataModel->length());
    QList<unsigned long> eepromValues = pModuleDataModel->getItem(channel).userEEPROMValues;
    eepromValues.replace(pos, value.toInt());
    pModuleDataModel->setData(channel, QVariant::fromValue(eepromValues), ModuleDataModel::UserEEPROMValuesRole);
    m_pDeviceList->setnextCollectedIndex(componentID());
}

void DeviceWithData::setModelData(const int slot, const QVariant &value, const QByteArray &roleValue)
{
    Q_ASSERT(slot >= 0 && slot < SLOTS_COUNT);
    ModuleDataModel*    pModuleDataModel = getDataModel(slot);
    for(QHash<int, QByteArray>::ConstIterator iter = pModuleDataModel->roleNames().constBegin();
        iter != pModuleDataModel->roleNames().constEnd();
        iter++)
        if(iter.value() == roleValue)
        {
            pModuleDataModel->setData(0, pModuleDataModel->rowCount(), value, iter.key());
            m_pDeviceList->setnextCollectedIndex(componentID());
            return;
        }
}

QVariant DeviceWithData::getModelData(const int slot, const int channel, const QByteArray &roleValue)
{
    Q_ASSERT(slot >= 0 && slot < SLOTS_COUNT);
    ModuleDataModel*    pModuleDataModel = getDataModel(slot);
    for(QHash<int, QByteArray>::ConstIterator iter = pModuleDataModel->roleNames().constBegin();
        iter != pModuleDataModel->roleNames().constEnd();
        iter++)
        if(iter.value() == roleValue) {
            QVariant result = pModuleDataModel->data(pModuleDataModel->index(channel), iter.key());
            return result;
        }

    return QVariant();
}

void DeviceWithData::addSingleUseFunction(QByteArray req)
{
    QMutexLocker lock(&m_commandListMutex);
    __addSingleUseFunction(req);
}

void DeviceWithData::__addSingleUseFunction(QByteArray req)
{
    m_singleUseCommands.append(req);
}

bool DeviceWithData::deleteMe() const
{
    return m_deleteMe;
}

void DeviceWithData::setdeleteMe(bool arg)
{
    if (m_deleteMe != arg)
    {
        m_deleteMe = arg;
        emit deleteMeChanged(arg);
        //if(m_deleteMe && !m_collecting)
            //deleteLater();
    }
}

QList<int> DeviceWithData::deviceIDs() const
{
    return m_deviceIDs;
}

void DeviceWithData::setdeviceIDs(const QList<int> deviceIDs)
{
    if(m_deviceIDs != deviceIDs && deviceIDs.size() >= SLOTS_COUNT)
    {
        m_deviceIDs = deviceIDs;
        emit deviceIDsChanged(deviceIDs);
    }
}

QList<int> DeviceWithData::types() const
{
    return m_types;
}

void DeviceWithData::settypes(const QList<int> types)
{
    if(m_needRefreshValue || (m_types != types && types.size() >= SLOTS_COUNT))
    {
        m_types = types;
        emit typesChanged(types);
    }
}

QList<int> DeviceWithData::channelsCount() const
{
    return m_channelsCount;
}

void DeviceWithData::setchannelsCount(const QList<int> channelsCount)
{
    if(m_needRefreshValue || (m_channelsCount != channelsCount && channelsCount.size() >= SLOTS_COUNT))
    {
        const QList<int> lastchannelCounts = this->channelsCount();
        {
            m_channelsCount = channelsCount;
        }
        refreshList(lastchannelCounts, channelsCount);
        emit channelsCountChanged(channelsCount);
    }

}

PROPERTY_CPP(int, DeviceWithData, componentID)
PROPERTY_CPP(QList<int>, DeviceWithData, canSurveyModules)
PROPERTY_CPP(QList<int>, DeviceWithData, hasPower)

void DeviceWithData::setcanSurveyModule(const int slot, const int canSurveyModule)
{
    m_canSurveyModules.replace(slot, canSurveyModule);
    emit canSurveyModulesChanged(canSurveyModules());
}

bool DeviceWithData::isOldModule(int slot)
{
    Q_ASSERT(slot >= 0 && slot < SLOTS_COUNT);
    return deviceIDs().at(slot) < 60 && !IS_MODULE_NEED_REFLASH(deviceIDs().at(slot));
}

void DeviceWithData::addReset()
{
    addSingleUseFunction(MODBUS_reset(1, getCommandNamePtr()));
    m_pDeviceList->setnextCollectedIndex(componentID());
}

void DeviceWithData::setMaxToAll(int slot)
{
    if(valid(slot))
    {
        if(!input(slot))
        {
            if(!analog(slot))
                addSingleUseFunction(MODBUS_set_diskret_channels(slot, 0xffff, getCommandNamePtr()));
            else
                setModelData(slot, 0xffff, "userValue"); //ну выставит он максимум. Считывать это кто будет?
        }
        m_pDeviceList->setnextCollectedIndex(componentID());
    }
}

void DeviceWithData::setMaxToAll()
{
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
        if(valid(slot))
            if(!input(slot))
            {
                if(!analog(slot))
                    addSingleUseFunction(MODBUS_set_diskret_channels(slot, 0xffff, getCommandNamePtr()));
                else
                    setModelData(slot, 0xffff, "userValue"); //ну выставит он максимум. Считывать это кто будет?
            }
    m_pDeviceList->setnextCollectedIndex(componentID());
}

void DeviceWithData::setMinToAll(int slot)
{
    if(valid(slot))
        if(!input(slot))
        {
            if(!input(slot))
            {
                if(!analog(slot))
                    addSingleUseFunction(MODBUS_set_diskret_channels(slot, 0, getCommandNamePtr()));
                else
                    setModelData(slot, 0, "userValue"); //ну выставит он максимум. Считывать это кто будет?
            }
            m_pDeviceList->setnextCollectedIndex(componentID());
        }
}

void DeviceWithData::setMinToAll()
{
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
        if(valid(slot))
            if(!input(slot))
            {
                if(!analog(slot))
                    addSingleUseFunction(MODBUS_set_diskret_channels(slot, 0, getCommandNamePtr()));
                else
                    setModelData(slot, 0, "userValue"); //ну выставит он максимум. Считывать это кто будет?
            }
    m_pDeviceList->setnextCollectedIndex(componentID());
}
void DeviceWithData::refreshModuleEEPROMChannelCounts(const int slot)
{
    Q_ASSERT(slot >= 0);
    Q_ASSERT(slot < SLOTS_COUNT);
    getDataModel(slot)->refreshModuleData(channelsCount().at(slot));
    if(deviceIDs().at(slot) == 40)
        getDataModel(slot)->setData(0, channelsCount().at(slot), 0);
}

void DeviceWithData::refreshList(const QList<int> lastchannelCounts, const QList<int> channelsCount)
{
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        if(lastchannelCounts.at(slot) != channelsCount.at(slot))
        {
            refreshModuleEEPROMChannelCounts(slot);
            if(valid(slot) && deviceIDs().at(slot) == 40)
            {
                ModuleDataModel *pModel = getDataModel(slot);
                Q_ASSERT(pModel);
                pModel->setData(0, channelsCount.at(slot), 0, ModuleDataModel::UserValueRole);
            }
        }
    }
}

bool DeviceWithData::getProcessorModuleData()
{
    ByteArray ans;
    if(request(MODBUS_get_frontPanel(getCommandNamePtr()), &ans))
    {
        if(ans.size() < 3)
            setlastError("Ошибка данных при считывании переключателей");
        else
        {
            QMetaObject::invokeMethod(this, "setfrontPanelX10", Qt::QueuedConnection, Q_ARG(int, ans.at(2) & 0xff));
            QMetaObject::invokeMethod(this, "setfrontPanelX1", Qt::QueuedConnection, Q_ARG(int, ans.at(1) & 0xff));
            QMetaObject::invokeMethod(this, "setfrontPanelSpeed", Qt::QueuedConnection, Q_ARG(int, ans.at(0) & 0xff));
        }
    }
    else
        return false;

    return isOpen();
}

bool DeviceWithData::input(int slot)
{
    Q_ASSERT(slot >= 0 && slot < SLOTS_COUNT);
    return !(types().at(slot) & 0x20);
}

bool DeviceWithData::valid(int slot)
{
    Q_ASSERT(slot >= 0 && slot < SLOTS_COUNT);
    return types().at(slot) != 0xff && deviceIDs().at(slot) != 0xff && IS_CHANNEL_VALID(channelsCount().at(slot));
}

bool DeviceWithData::analog(int slot)
{
    Q_ASSERT(slot >= 0 && slot < SLOTS_COUNT);
    return types().at(slot) & 0x40;
}

class ValueUnchecker
{
public:
    ValueUnchecker(volatile bool *pValue) : m_pValue(pValue)
    {
        if(m_pValue)
            *m_pValue = true;
    }
    ~ValueUnchecker()
    {
        if(m_pValue)
            *m_pValue = false;
    }
private:
    volatile bool *m_pValue;
};

bool DeviceWithData::sendChangedEEPROMData()
{
    ValueUnchecker valueUnchecker(&m_writingEEPROM);
    setpacketComplexity(0);
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)//поиск новый модулей аналогового вывода.
    {
        if(!canSurveyModules().at(slot))
            continue;
        if(!valid(slot) || !analog(slot) || !input(slot))
            continue;
        int chCount = channelsCount().at(slot);
        if(g_parameterStorage.getDchan(deviceIDs().at(slot)))
            chCount--;
        if(!IS_CHANNEL_VALID(chCount))
            continue;
        if(!isSonet() && chCount > 2)
            chCount = 2;
        ModuleDataModel *pModel = getDataModel(slot);
        Q_ASSERT(pModel);
        for(int chan = 0;  chan < chCount;  chan++)
        {
            bool wasRequested = false;
            const ModuleDataItem &item = pModel->getItem(chan);
            QList<unsigned short> deviceEEPROMValues = item.deviceEEPROMValues;
            QList<unsigned long> userEEPROMValues = item.userEEPROMValues;
            if(isSonet())
            {
                for(int i = 0;  i < EEPROM_DATA_COUNT_SONET;  i++)
                    if(item.requestToSetEEPROMValue(i))
                    {
                        wasRequested = true;
                        if(isOldModule(slot))//старые модули - у которых ID до 60
                        {
                            if(!eeprom_set(slot, 1 + i * 2 +(chan << 2), item.userEEPROMValues.at(i) & 0xff)
                                || !eeprom_set(slot, 0 + i * 2 +(chan << 2), (item.userEEPROMValues.at(i) >> 8) & 0xff))
                                return false;
                        }
                        else if(!request(MODBUS_set_SPI(slot, (i ? SPI_FULL_WR : SPI_ZERO_WR)|chan, (ushort)item.userEEPROMValues.at(i), getCommandNamePtr())))
                            return false;
                        deviceEEPROMValues.replace(i, item.userEEPROMValues.at(i));
                        userEEPROMValues.replace(i, ModuleDataItem::unitializedValue());
                        Sleepy::msleep(WRITE_EEPROM_DELAY);
                    }
            }
            else
            {
                for(int i = 0;  i < EEPROM_DATA_COUNT_DIAG / 2;  i++)
                {
                    int seg = i * 2;
                    if(item.requestToSetEEPROMValue(seg)
                        || (item.requestToSetEEPROMValue(seg + 1)))
                    {
                        wasRequested = true;
                        ushort val1 = item.requestToSetEEPROMValue(seg) ? item.userEEPROMValues.at(seg) : item.deviceEEPROMValues.at(seg);
                        ushort val2 = item.requestToSetEEPROMValue(seg + 1) ? item.userEEPROMValues.at(seg + 1) : item.deviceEEPROMValues.at(seg + 1);

                        if(!request(MODBUS_set_SPI(0, DIAG_CODE + i + (chan << 2),
                                                  val1,
                                                  val2,
                                                  getCommandNamePtr())))
                            return false;
                        deviceEEPROMValues.replace(seg, val1);
                        deviceEEPROMValues.replace(seg + 1, val2);
                        userEEPROMValues.replace(seg, ModuleDataItem::unitializedValue());
                        userEEPROMValues.replace(seg + 1,ModuleDataItem::unitializedValue());
                        Sleepy::msleep(100);
                    }
                }
            }
            if(wasRequested)
            {
                pModel->setData(chan, QVariant::fromValue(userEEPROMValues), ModuleDataModel::UserEEPROMValuesRole);
                pModel->setData(chan, QVariant::fromValue(deviceEEPROMValues), ModuleDataModel::DeviceEEPROMValuesRole);

            }
        }

        if(pModel->data(QModelIndex(), ModuleDataModel::ContainsDataFullNameRole) == true)
        {
            if(pModel->requestToSetCalibrDate())
            {
                QDate date = pModel->data(QModelIndex(), ModuleDataModel::UserCalibrDateRole).toDate();
                unsigned short first = date.year();
                unsigned short second = date.month() << 8 | date.day();
                if(!request(MODBUS_set_SPI(slot, CALIBR_DATE_WR, first, getCommandNamePtr())))
                    return false;
                Sleepy::msleep(WRITE_EEPROM_DELAY);
                if(!request(MODBUS_set_SPI(slot, CALIBR_DATE_WR | 1, second, getCommandNamePtr())))
                    return false;
                Sleepy::msleep(WRITE_EEPROM_DELAY);
                pModel->setData(0, QDate(), ModuleDataModel::UserCalibrDateRole);
                pModel->setData(0, date, ModuleDataModel::DeviceCalibrDateRole);
            }
            if(pModel->requestToSetVerifyDate())
            {
                QDate date = pModel->data(QModelIndex(), ModuleDataModel::UserVerifyDateRole).toDate();
                if(!request(MODBUS_set_SPI(slot, VERIFY_DATE_WR, date.year(), getCommandNamePtr())))
                    return false;
                Sleepy::msleep(WRITE_EEPROM_DELAY);
                if(!request(MODBUS_set_SPI(slot, VERIFY_DATE_WR | 1, date.month() << 8 | date.day(), getCommandNamePtr())))
                    return false;
                Sleepy::msleep(WRITE_EEPROM_DELAY);
                pModel->setData(0, QDate(), ModuleDataModel::UserVerifyDateRole);
                pModel->setData(0, date, ModuleDataModel::DeviceVerifyDateRole);
            }
            if(pModel->requestToSetCalibrFullName())
            {
                QByteArray calibrFullName = pModel->data(QModelIndex(), ModuleDataModel::UserCalibrFullNameRole).toByteArray();
                if(!calibrFullName.endsWith('\0'))
                    calibrFullName.append('\0');
                int size = calibrFullName.size();
                if(size % 2)
                {
                    calibrFullName.append('\0');
                    size++; //добиваем до четного количества байт. по два же записываем.
                }
                QApplication::setOverrideCursor(Qt::WaitCursor);
                for(int i = 0;  i < size / 2;  i++)
                {
                    int pos = i * 2;
                    uchar byte1 = calibrFullName.at(pos);
                    uchar byte2 = calibrFullName.at(pos + 1);
                    unsigned short val = byte1 << 8 | byte2;
                    if(!request(MODBUS_set_SPI(slot, CALIBR_FULL_NAME_WR, val, getCommandNamePtr())))
                        return false;
                    Sleepy::msleep(WRITE_EEPROM_DELAY);
                }
                QApplication::restoreOverrideCursor();
                pModel->setData(0, QByteArray(), ModuleDataModel::UserCalibrFullNameRole);
                pModel->setData(0, calibrFullName, ModuleDataModel::DeviceCalibrFullNameRole);
            }
            if(pModel->requestToSetVerifyFullName())
            {
                QByteArray verifyFullName = pModel->data(QModelIndex(), ModuleDataModel::UserVerifyFullNameRole).toByteArray();
                if(!verifyFullName.endsWith('\0'))
                    verifyFullName.append('\0');
                int size = verifyFullName.size();
                for(int i = 0;  i < size - 1;  i+= 2)
                {
                    uchar byte1 = verifyFullName.at(i);
                    uchar byte2 = verifyFullName.at(i + 1);
                    unsigned short val = byte1 << 8 | byte2;
                    if(!request(MODBUS_set_SPI(slot, VERIFY_FULL_NAME_WR, val, getCommandNamePtr())))
                        return false;
                    Sleepy::msleep(WRITE_EEPROM_DELAY);
                }
                if(size)
                    if(!request(MODBUS_set_SPI(slot, VERIFY_FULL_NAME_WR, '\0', getCommandNamePtr())))
                        return false;
                Sleepy::msleep(WRITE_EEPROM_DELAY);
                pModel->setData(0, QByteArray(), ModuleDataModel::UserVerifyFullNameRole);
                pModel->setData(0, verifyFullName, ModuleDataModel::DeviceVerifyFullNameRole);
            }
        }
    }
    m_needSetEEPROMValues = false;
    return true;
}

bool DeviceWithData::sendChangedData()
{
    setpacketComplexity(0);
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)//поиск новый модулей аналогового вывода.
    {
        if(!canSurveyModules().at(slot))
            continue;
        if(!valid(slot) || input(slot))
            continue;
        int chCount = channelsCount().at(slot);
        if(!IS_CHANNEL_VALID(chCount))
            continue;
        ModuleDataModel *pModel = getDataModel(slot);
        Q_ASSERT(pModel);
        Q_ASSERT(chCount == pModel->length());
        for(int chan = 0;  chan < chCount;  chan++)
        {
            const ModuleDataItem &item = pModel->getItem(chan);
            if(!item.requestToSetChannelValue())
                continue;
            unsigned short value = item.userValue;
            if(!analog(slot))
            {
                if(!request(MODBUS_set_diskret_channel_N(slot, chan, value, getCommandNamePtr())))
                    return false;
            }
            else if(!request(MODBUS_set_analog_channel(slot, chan, value, getCommandNamePtr())))
                return false;
            pModel->setData(chan, QVariant::fromValue(ModuleDataItem::unitializedValue()), ModuleDataModel::UserValueRole);
            pModel->setData(chan, value, ModuleDataModel::DeviceValueRole);
        }
    }
    return true;
}

bool DeviceWithData::eeprom_set(unsigned char module,unsigned char addr,unsigned char value)
{
    bool k = true;
    if(k)
        k = request(MODBUS_direct_write(module, 0x3d, 0xa6, getCommandNamePtr()));
    if(k)
        k = request(MODBUS_direct_write(module, 0x3f, addr, getCommandNamePtr()));
    if(k)
        k = request(MODBUS_direct_write(module, 0x3e, value, getCommandNamePtr()));
    if(k)
        k = request(MODBUS_direct_write(module, 0x3d, 0xa3, getCommandNamePtr()));
    return k;
}

bool DeviceWithData::eeprom_get(unsigned char module, unsigned char addr, unsigned char *result)
{
    ByteArray ans;
    bool k = true;
    if(k)
        k = request(MODBUS_direct_write(module, 0x3f, addr, getCommandNamePtr()));
    if(k)
        k = request(MODBUS_direct_write(module, 0x3d, 0xa1, getCommandNamePtr()));
    if(k)
        k = request(MODBUS_direct_read(module, 0x3c, 1, getCommandNamePtr()), &ans);
    if(k && ans.size() >= 2)
        *result = ans.at(1);
    return k;
}

bool DeviceWithData::checkSPIEcho(unsigned char slot, unsigned char command)
{
    ByteArray ans;
    bool k = request(MODBUS_get_SPI(slot, command, 1, getCommandNamePtr()), &ans);
    if(k && ans.size() >= 2)
    {
        unsigned short val = (ans.at(0) << 8) | ans.at(1);
        if(val == (command | (slot << 8)))
        {
            return true;
        }
    }
    return false;
}

bool DeviceWithData::checkModuleContainsCalibrDate(unsigned char slot, bool *ok)
{
    bool tmpOk;
    if(!ok)
        ok = &tmpOk;
    *ok = true;
    ByteArray ans;
    bool containsDataFullName = true;
    unsigned short lastDate = 0;
    if(*ok)
    {
        *ok = request(MODBUS_get_SPI(slot, SPI_ZERO, 1, getCommandNamePtr()), &ans) //для особо старых модулей переводим указатель данных в область кода нуля
        && request(MODBUS_get_SPI(slot, CALIBR_DATE, 1, getCommandNamePtr()), &ans) //выдаст либо дату, либо значение канала, либо 3 и 4 байт массива с кадами нуля.
        && ans.size() == 2;
        if(*ok)
        {
            lastDate = (ans.at(1) << 8) | ans.at(0);
            *ok = request(MODBUS_set_SPI(slot, CALIBR_DATE_WR, lastDate ^ 0xabcd, getCommandNamePtr())); //модуль не испортит.
            Sleepy::msleep(WRITE_EEPROM_DELAY);
        }
    }
    if(*ok)
    {
        *ok = request(MODBUS_get_SPI(slot, SPI_ZERO, 1, getCommandNamePtr()), &ans) //для особо старых модулей переводим указатель данных в область кода нуля
        && request(MODBUS_get_SPI(slot, CALIBR_DATE, 1, getCommandNamePtr()), &ans) //выдаст либо дату, либо значение канала, либо 3 и 4 байт массива с кадами нуля.
        && ans.size() == 2;
        if(*ok)
        {
            unsigned short tmp = (ans.at(1) << 8) | ans.at(0);
            containsDataFullName = (tmp == (lastDate ^ 0xabcd));
            if(containsDataFullName)
            {
                *ok = request(MODBUS_set_SPI(slot, CALIBR_DATE_WR, lastDate, getCommandNamePtr())); //ой... возвращаем дату на место
                Sleepy::msleep(WRITE_EEPROM_DELAY);
            }

        }
    }
    return containsDataFullName;
}

bool DeviceWithData::readEEPROMData()
{
    setpacketComplexity(1);
    for(int slot = 0; slot < SLOTS_COUNT;  slot++)
    {
        if(!canSurveyModules().at(slot))
            continue;
        ByteArray ans;
        if(!valid(slot) || !analog(slot) || !input(slot))
            continue;
        if(!isSonet() && deviceIDs().at(0) < 111)
            continue;
        int chCount = channelsCount().at(slot);
        if(!IS_CHANNEL_VALID(chCount))
            continue;
        if(g_parameterStorage.getDchan(deviceIDs().at(slot)))
            chCount--;
        if(!isSonet() && chCount > 2)
            chCount = 2; // в МД реально с eeprom работают только первые два канала.
        ModuleDataModel *pModel = getDataModel(slot);
        Q_ASSERT(pModel);
        if(pModel->data(QModelIndex(), ModuleDataModel::ContainsDataFullNameRole).toUInt() == ModuleDataItem::unitializedValue() && isSonet()) //сейчас мы узнаем, знаком ли  модуль с датой вцелом
        {
            bool ok;
            if(checkSPIEcho(slot, SPI_ZERO))
            {
                pModel->setData(0, false, ModuleDataModel::ContainsDataFullNameRole);
            }
            else
            {
                bool result = checkModuleContainsCalibrDate(slot, &ok);
                if(ok)
                {
                    pModel->setData(0, result, ModuleDataModel::ContainsDataFullNameRole);
                }
            }
        }
        if(pModel->data(QModelIndex(), ModuleDataModel::ContainsDataFullNameRole) == true)
        {
            int year = 0;
            bool k = request(MODBUS_get_SPI(slot, CALIBR_DATE, 1, getCommandNamePtr()), &ans);
            if(k && ans.size() >= 2)
            {
                year = ((ushort)(ans.at(1))) << 8 | ans.at(0);
            }
            if(k)
                k = request(MODBUS_get_SPI(slot, CALIBR_DATE | 1, 1, getCommandNamePtr()), &ans);
            if(k && ans.size() >= 2)
            {
                QDate date = (year & 0xff00) < 0x5500 ? QDate(year, ans.at(1), ans.at(0)) : QDate();
                pModel->setData(0, date, ModuleDataModel::DeviceCalibrDateRole);
            }

            if(k)
                k = request(MODBUS_get_SPI(slot, VERIFY_DATE, 1, getCommandNamePtr()), &ans);
            if(k && ans.size() >= 2)
            {
                year = ((ushort)(ans.at(1))) << 8 | ans.at(0);
            }
            if(k)
                k = request(MODBUS_get_SPI(slot, VERIFY_DATE | 1, 1, getCommandNamePtr()), &ans);
            if(k && ans.size() >= 2)
            {
                QDate date = (year & 0xff00) < 0x5500 ? QDate(year, ans.at(1), ans.at(0)) : QDate();
                pModel->setData(0, date, ModuleDataModel::DeviceVerifyDateRole);
            }
            QByteArray text;
            unsigned short nameLen = 0;
            if(k)
                k = request(MODBUS_get_SPI(slot, CALIBR_FULL_NAME | 1, 1, getCommandNamePtr()), &ans);
            if(k && ans.size() >= 2)
            {
                nameLen = ((ushort)(ans.at(1))) << 8 | ans.at(0);
                if(nameLen > FULL_NAME_MAX_LEN)
                    nameLen = 0;
            }
            int counter = nameLen;
            while((k = request(MODBUS_get_SPI(slot, CALIBR_FULL_NAME, 1, getCommandNamePtr()), &ans)) && ans.size() >= 2 && counter--)
            {
                if(ans.at(0) == 0xff && ans.at(1) == 0xff)
                {
                    text.clear(); //битые даные
                    break;
                }
                ans.append(ans.at(0)).remove(0, 1);
                text.append(ans);
                if(ans.at(0) == '\0' || ans.at(1) == '\0')
                    break;
            }
            pModel->setData(0, text, ModuleDataModel::DeviceCalibrFullNameRole);
            text.clear();
            if(k)
                k = request(MODBUS_get_SPI(slot, VERIFY_FULL_NAME | 1, 1, getCommandNamePtr()), &ans);
            if(k && ans.size() >= 2)
            {
                nameLen = ((ushort)(ans.at(1))) << 8 | ans.at(0);
                if(nameLen > FULL_NAME_MAX_LEN)
                    nameLen = 0;
            }
            counter = nameLen;
            while((k = request(MODBUS_get_SPI(slot, VERIFY_FULL_NAME, 1, getCommandNamePtr()), &ans)) && ans.size() >= 2 && counter--)
            {
                if(ans.at(0) == 0xff && ans.at(1) == 0xff)
                {
                    text.clear(); //битые даные
                    break;
                }
                if(ans.at(0) == '\0' || ans.at(1) == '\0')
                    break;
                ans.append(ans.at(0)).remove(0, 1);
                text.append(ans);
            }
            pModel->setData(0, text, ModuleDataModel::DeviceVerifyFullNameRole);
        }
        for(int chan = 0;  chan < chCount;  chan++)
        {
            QList<unsigned short> values;
            if(isSonet())
            {
                unsigned short value;
                for(int i = 0;  i < EEPROM_DATA_COUNT_SONET;  i++)
                {
                    if(isOldModule(slot)) //старые модули - у которых ID до 60
                    {
                        if(!eeprom_get(slot, 0 + i * 2 +(chan << 2), ((unsigned char *) & value) + 1) ||
                            !eeprom_get(slot, 1 + i * 2 + (chan << 2), ((unsigned char *) & value)))
                            return false;
                    }
                    else
                    {
                        if(request(MODBUS_get_SPI(slot, (i ? SPI_FULL : SPI_ZERO) | chan, 1, getCommandNamePtr()), &ans) && ans.size() >= 2)
                            value = (uchar)ans.at(1) << 8  | (uchar)ans.at(0);
                        else
                            return false;
                    }
                    values.push_back(value);
                }
            }
            else
            {
                for(int i = 0;  i < EEPROM_DATA_COUNT_DIAG / 2;  i++)
                {

                    //int seg = i * 2;
                    /*if(item.deviceEEPROMValues.at(seg) == ModuleDataItem::unreadedValue()
                        || item.userEEPROMValues.at(seg) == ModuleDataItem::unreadedValue()
                        || item.deviceEEPROMValues.at(seg + 1) == ModuleDataItem::unreadedValue()
                        || item.userEEPROMValues.at(seg + 1) == ModuleDataItem::unreadedValue()
                        || item.needRefreshAfterSet)*/
                    //теперь считываем EEPROM постоянно, потому что значения точек для обоих каналов общие. FAIL.
                    {
                        if(request(MODBUS_get_SPI(0, DIAG_CODE + i + (chan << 2), 2, getCommandNamePtr()), &ans), ans.size() >= 4)
                        {
                            values.push_back((unsigned short)((ushort)ans.at(1) << 8  | (uchar)ans.at(0)));
                            values.push_back((unsigned short)((ushort)ans.at(3) << 8  | (uchar)ans.at(2)));
                        }
                        else
                            return false;
                    }
                }
            }
            pModel->setData(chan, QVariant::fromValue(values), ModuleDataModel::DeviceEEPROMValuesRole);
        }
    }
    return true;
}

bool DeviceWithData::readModules()
{
    ByteArray ans;
    setpacketComplexity(1);
    Q_ASSERT(channelsCount().size() >= SLOTS_COUNT);
    if(request(MODBUS_get_powers(getCommandNamePtr()), &ans) && !ans.isEmpty())
    {
        bool changed = false;
        for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
        {
            bool val = ans.at(0) & (1 << slot);
            if(m_hasPower.at(slot) != val)
            {
                changed = true;
                m_hasPower.replace(slot, val);
            }
        }
        if(changed)
            emit hasPowerChanged(hasPower());
    }
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        if(!canSurveyModules().at(slot))
            continue;
        if(!valid(slot))
            continue;
        int chCount = channelsCount().at(slot);
        if(!IS_CHANNEL_VALID(chCount))
            continue;
        ModuleDataModel *pModel = getDataModel(slot);
        Q_ASSERT(pModel);
        QVariantList varlist;
        if(analog(slot) && deviceIDs().at(slot) != 40) //модуль с ID 40 Не умеет выдавать данные
        {
            if(request(MODBUS_get_analog_channels(slot, getCommandNamePtr()), &ans) && (ans.size() >= (2 + chCount * 2)))
                for(int chan = 0;  chan < chCount;  chan++)
                    varlist.append((unsigned short)((ushort)(ans.at(2 + chan * 2) << 8 )| (uchar)ans.at(3 + chan * 2)));
            else
                return false;
        }
        if(!analog(slot))
        {
            if(request(MODBUS_get_diskret_channels_N(slot, chCount, getCommandNamePtr()), &ans) && (ans.size() >= (1 + (chCount - 1) / 8 + 1)))
                for(int chan = 0;  chan < chCount;  chan++)
                {
                    int index = !isSonet() && slot < 4 ? chan + (chan < 4 ? chan + 1 : chan - 8) : chan;
                    varlist.append((unsigned short)((ushort)((ans.at(index / 8 + 1) & (1 << index % 8)) >> index % 8)));
                }

            else
                return false;
        }
        pModel->setData(0, chCount, varlist, ModuleDataModel::DeviceValueRole);
    }
    return true;
}

bool DeviceWithData::refreshConsist()
{
    setpacketComplexity(1);
    ByteArray ans;
    QList<int> currentDeviceIDs, types, channelsCount;

    if(request(MODBUS_get_fullConfig(getCommandNamePtr()), &ans) && ans.size() >= 1 + SLOTS_COUNT * 3)
    {
        setbprState(ans.at(0) & 0xff);
        ans.remove(0, 1);
        for(int i = 0;  i < SLOTS_COUNT * 3;  i++)
            switch(i % 3)
            {
            case 0: types.append(ans.at(i) & 0xff);             break;
            case 1: currentDeviceIDs.append(ans.at(i) & 0xff);               break;
            case 2: channelsCount.append(ans.at(i) & 0xff);    break;
            }
        for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
            if(currentDeviceIDs.at(slot) != deviceIDs().at(slot))
            {
                m_needRefreshValue = true;
            }
        __setDeviceStruct(types, channelsCount, currentDeviceIDs);
        return true;
    }
    else
        return false;
}

void DeviceWithData::__setDeviceStruct(QList<int> types, QList<int> chCounts, QList<int> deviceIDs)
{
    setdeviceIDs(deviceIDs);
    settypes(types);
    setchannelsCount(chCounts);
    m_needRefreshValue = false;
}

bool DeviceWithData::request(QByteArray req, ByteArray *ans)
{
    if(!turnedOn() || !isOpen())
        return false;
    bool k = SonetDevice::MODBUS_command(req, ans);
    if(!k)
    {
        if(m_underTests && isSonet())
        {
            if(__onError())
                return request(req, ans);
            else
                setturnedOn(false);
        }
    }
    return k && isOpen();
}

