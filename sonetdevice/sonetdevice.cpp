﻿#include    "sonetdevice.h"
#include    "commands.h"
#include    "macro.h"
#include    "objects.h"

class PacketDecor//оформляет пакет и отвечает за задержку modbus
{
public:
    PacketDecor(ModbusDevice *pDevice)
        : m_pDevice(pDevice) { }
    ~PacketDecor()
    {
        Sleepy::usleep(ceil(4 * 1000000 / m_pDevice->realRate()));
        m_pDevice->createPack();
    }
    ModbusDevice *m_pDevice;
};

SonetDevice::SonetDevice(QObject *parent)
    : ModbusDevice(parent)
{
    setstopBits(g_defaultStopBits);
    setdataBits(g_defaultDataBits);
    setflowIndex(g_defaultFlowIndex);
    setparityIndex(g_defaultParityIndex);
    setrateIndex(g_defaultRateIndex);
    setbprState(0);
    setturnedOn(true);
    settemperature(0);
    setfrontPanelX10(0);
    setfrontPanelX1(0);
    setfrontPanelSpeed(0);
    connect(this, SIGNAL(deviceNumChanged(int)), this, SLOT(createInfo()));
    connect(this, SIGNAL(runningChanged(bool)), this, SLOT(createInfo()));
    connect(this, SIGNAL(deviceAviableChanged(bool)), this, SLOT(createInfo()));
    connect(this, SIGNAL(hardwareErrorChanged(QString)), this, SLOT(createInfo()));
    connect(this, SIGNAL(turnedOnChanged(bool)), this, SLOT(createInfo()));
    connect(this, SIGNAL(lastErrorChanged(QString)), this, SLOT(createInfo()));
    createInfo();
}

void SonetDevice::emitAllGUISignals()
{
    ModbusDevice::emitAllGUISignals();
}

PROPERTY_CPP(int, SonetDevice, bprState)
PROPERTY_CPP(bool, SonetDevice, turnedOn)
PROPERTY_CPP(qreal, SonetDevice, temperature)
PROPERTY_CPP(int, SonetDevice, frontPanelX10)
PROPERTY_CPP(int, SonetDevice, frontPanelX1)
PROPERTY_CPP(int, SonetDevice, frontPanelSpeed)

void SonetDevice::unsetturnedOn()
{
    setturnedOn(true);
}

void SonetDevice::onTurnedOnChanged(bool /*turnedOn*/)
{
    if(!turnedOn())
    {
        unsetLastError();
        unsetHardwareError();
    }
}

bool SonetDevice::find()
{
    QByteArray req = MODBUS_get_version(getCommandNamePtr());
    bool result = findCurrentDevice(req);
    if(result)
    {
        if(m_answer.right(4).left(2) != m_request.right(4).left(2))
            return true;
        else
            setlastError("Обнаружено эхо.");
    }
    return false;
}

bool SonetDevice::MODBUS_command(QByteArray req, ByteArray *ans)
{
    if(!isOpen()|| !turnedOn() || !running())
        return false;
    PacketDecor packetDecor(this); // спит после завершения команды.
    if(sendCommand(req, ans))
    {
        if(ans)
        {
            ans->remove(0, commandNum() == 0x08 ? 4 : 2); //command
            ans->remove(ans->size() - 2, 2); //crc
        }
        return true;
    }
    return false;
}

void SonetDevice::createInfo()
{
    if(!running() || !deviceNum())
        setinfo("опрос не происходит");
    else if(!hardwareError().isEmpty())
        setinfo("аппаратная ошибка!");
    else if(!turnedOn())
        setinfo("опрос остановлен");
    else if(!deviceAviable())
        setinfo("поиск прибора");
    else if(!lastError().isEmpty())
        setinfo("ошибка MODBUS");
    else
        setinfo("идет обмен данными");
}
