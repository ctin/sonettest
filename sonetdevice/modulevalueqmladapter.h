﻿#ifndef MODULEVALUEQMLADAPTER_H
#define MODULEVALUEQMLADAPTER_H

#include <QDeclarativeItem>

class ModuleValueQMLAdapter : public QDeclarativeItem
{
    Q_OBJECT
    Q_PROPERTY(long value READ value WRITE setValue NOTIFY valueChanged)
public:
    explicit ModuleValueQMLAdapter(QDeclarativeItem *parent = 0);
    long value() const;
public slots:
    void setValue(const long value);
    void setValueFromDevice(const long value);

signals:
    void valueChanged(long);
private:
    long m_value, m_newValue;
};

#endif // MODULEVALUEQMLADAPTER_H
