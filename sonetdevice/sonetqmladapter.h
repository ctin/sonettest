﻿#ifndef SONETQMLADAPTER_H
#define SONETQMLADAPTER_H

#include <QObject>
#include <QtDeclarative>
#include <QtCore>
#include "objects.h"
class ProcModuleQMLAdapter : public QDeclarativeItem
{
    Q_OBJECT
    Q_PROPERTY(int dial10Value READ dial10Value WRITE setDial10Value NOTIFY dial10ValueChanged)
    Q_PROPERTY(int dial1Value READ dial1Value WRITE setDial1Value NOTIFY dial1ValueChanged)
    Q_PROPERTY(int dialSpeedValue READ dialSpeedValue WRITE setDialSpeedValue NOTIFY dialSpeedValueChanged)
    Q_PROPERTY(int modbusInput READ modbusInput WRITE setModbusInput NOTIFY modbusInputChanged)
    Q_PROPERTY(int togglerState READ togglerState WRITE setTogglerState NOTIFY togglerStateChanged)

public:
    explicit ProcModuleQMLAdapter(QDeclarativeItem *parent = 0);
    DeviceConfig getConfig() const;
    int dial10Value() const;
    int dial1Value() const;
    int dialSpeedValue() const;
    int modbusInput() const;
    int togglerState() const;
public slots:
    void setDial10Value(int);
    void setDial1Value(int);
    void setDialSpeedValue(int);
    void setModbusInput(int);
    void setTogglerState(int);
signals:
    void dial10ValueChanged(int);
    void dial1ValueChanged(int);
    void dialSpeedValueChanged(int);
    void modbusInputChanged(int);
    void togglerStateChanged(int);

    void deviceOk();
    void deviceBreakdown();
    void rx();
    void tx();
private:
    DeviceConfig m_config;
    bool m_togglerState;
};

class ModuleQMLAdapter : public QDeclarativeItem
{
    Q_OBJECT
    Q_PROPERTY(int ID READ ID)
    Q_PROPERTY(qreal min READ min)
    Q_PROPERTY(qreal max READ max)
    Q_PROPERTY(QString unit READ unit)
    Q_PROPERTY(QString name READ name)
    Q_PROPERTY(QString KYNI READ KYNI)
    Q_PROPERTY(QString comments READ comments)
    Q_PROPERTY(int dchan READ dchan)

public:
    explicit ModuleQMLAdapter(const int num = 0, DeviceData *pData = 0, QDeclarativeItem *parent = 0);
    DeviceData m_data;
    int ID() const;
    int min() const;
    int max() const;
    QString unit() const;
    QString name() const;
    QString KYNI() const;
    QString comments() const;
    int dchan() const;

private:
    int m_slotNum;
};

#endif // SONETQMLADAPTER_H
