﻿#include "mainwindow.h"
#include "dialogabout.h"
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);
#ifdef FAKE_SONET
    setWindowTitle("Sonet Test Fake mode");
#endif
    QAction *pActAbout = new QAction("О программе", menubar);
    menubar->addAction(pActAbout);
    connect(pActAbout, SIGNAL(triggered()), this, SLOT(about_triggered()));
}

void MainWindow::onDeviceListLengthChanged(int length)
{
    Q_UNUSED(length);
    const bool enabled = true;//length;
    menu_calibr->setEnabled(enabled);
    menu_diag_calibr->setEnabled(enabled);
    menu_EEPROM_calibr->setEnabled(enabled);
    //menu_others->setEnabled(enabled);
    action_temperature->setEnabled(enabled);
    action_replyTime->setEnabled(enabled);
    action_readModules->setEnabled(enabled);
    action_readDataViaSPI->setEnabled(enabled);
    //menu_tests->setEnabled(enabled);
    action_controllerInfo->setEnabled(enabled);
    action_checkSwitches->setEnabled(enabled);
    action_allChannelsOverview->setEnabled(enabled);
    action_switchReserve->setEnabled(enabled);
    action_powerCheck->setEnabled(enabled);
    action_timeToSetAnalogChannel->setEnabled(enabled);
    action_diagRegister->setEnabled(enabled);
    action_watchAnalogInput->setEnabled(enabled);
    action_watchAnalogOutput->setEnabled(enabled);
    action_watchDiscreteInput->setEnabled(enabled);
    action_watchDiscreteOutput->setEnabled(enabled);
    action_diagnosticModule->setEnabled(enabled);
    menu_verify->setEnabled(enabled);
}

void MainWindow::about_triggered()
{
    static DialogAbout *pDialogAbout = new DialogAbout(this);
    pDialogAbout->show();
}

void MainWindow::writeSettings()
{
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("MainWindow");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.setValue("windowState", (int)windowState());
    settings.endGroup();
}

void MainWindow::readSettings()
{
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("MainWindow");

    Qt::WindowState state = (Qt::WindowState)(settings.value("windowState", (int)Qt::WindowNoState).toInt());
    if(state != Qt::WindowMinimized)
        setWindowState(state);
    if(state == Qt::WindowNoState || state == Qt::WindowActive)
    {
        resize(settings.value("size", QSize(800, 600)).toSize());
        move(settings.value("pos", QPoint(0, 0)).toPoint());
    }
    settings.endGroup();
}

void MainWindow::closeEvent(QCloseEvent *pEvent)
{
    writeSettings();
    QMainWindow::closeEvent(pEvent);
}

