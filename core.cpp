﻿#include "core.h"
#include "dialogcalibr.h"
#include "dialogcalibrdiagmodule.h"
#include "dialogverifyInput.h"
#include "dialogverifyoutput.h"
#include "dialogcontrollerconsist.h"
#include "dialogcheckswitches.h"
#include "dialogallchannels.h"
#include "dialogdiagnosticmodule.h"
#include "dialogcommtest.h"
#include "dialogfullpower.h"
#include "dialogtimetosetallanalogchannels.h"
#include "dialogregistersprocmodule.h"
#include "dialogwatchtypes.h"
#include "dialogcheckreserve.h"
#include "dialogtemperature.h"
#include "dialogreplytime.h"
#include "dialogreadmodules.h"
#include "dialogreadmodulesspi.h"
#include "dialogshowerrorlist.h"
#include "dialogmodbusline.h"
#include "styleplugin.h"

Core *Core::self = 0;
#define CTIN 1
Core::Core(QObject *parent) :
    QObject(parent)
{
    Q_ASSERT_X(!self, "Core", "there should be only one core object");
    self = this;

    m_pSonetData = new SonetData(this);
    loadData();
    m_pMainWindow = new MainWindow();
    m_pCentralView = new QDeclarativeView(m_pMainWindow);
    m_pMainWindow->setCentralWidget(m_pCentralView);
    m_pCentralView->setResizeMode(QDeclarativeView::SizeRootObjectToView);
    StylePlugin::initializeCurrentEngine(m_pCentralView->engine());
    m_pCentralView->setMinimumWidth(480);
   // m_pCentralView->setWindowFlags(Qt::FramelessWindowHint);
    m_pDeviceList = new DeviceList(this);
    connect(m_pDeviceList, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentChanged(int)));
    m_pCentralView->rootContext()->setContextProperty("deviceList", QVariant::fromValue(m_pDeviceList));
    m_pCentralView->rootContext()->setContextProperty("core", QVariant::fromValue(this));
#ifdef FAKE_SONET
    m_pCentralView->rootContext()->setContextProperty("fake", QVariant::fromValue(true));
#else
    m_pCentralView->rootContext()->setContextProperty("fake", QVariant::fromValue(false));
#endif
    m_pCentralView->setSource(QUrl(QString::fromUtf8("qrc:qml/main.qml")));
   // disconnect(qApp, SIGNAL(lastWindowClosed()));
    //connect(qApp, SIGNAL(lastWindowClosed()), m_pDeviceList, SLOT(clearToDelete()));
    connect(qApp, SIGNAL(lastWindowClosed()), this, SIGNAL(lastWindowClosed()));
    connect(m_pDeviceList, SIGNAL(lengthChanged(int)), m_pMainWindow, SLOT(onDeviceListLengthChanged(int)));
    connect(m_pSonetData, SIGNAL(dataLoaded()), this, SIGNAL(dataLoaded()));
    connectActions();

    m_pMainWindow->onDeviceListLengthChanged(0);

    QTimer *pRefreshGUITimer = new QTimer(this);
    connect(pRefreshGUITimer, SIGNAL(timeout()), this, SLOT(emitAllGUISignals()));
    pRefreshGUITimer->start(20);
    readGlobalSettings();

}

Core::~Core()
{
}

void Core::emitAllGUISignals()
{
    m_pDeviceList->emitAllGUISignals();
}

void Core::showComSettings()
{
    DialogCOMSettings dialogCOMSettings(m_pMainWindow);
    connect(dialogCOMSettings.spinBox_portNum, SIGNAL(valueChanged(int)), m_pDeviceList, SLOT(setPortForAll(int)));
    connect(dialogCOMSettings.comboBox_parity, SIGNAL(activated(int)), m_pDeviceList, SLOT(setParityIndexForAll(int)));
    connect(dialogCOMSettings.comboBox_speeds, SIGNAL(activated(int)), m_pDeviceList, SLOT(setRateIndexForAll(int)));
    connect(dialogCOMSettings.doubleSpinBox_stopBits, SIGNAL(valueChanged(double)), m_pDeviceList, SLOT(setStopBitsForAll(double)));
    connect(dialogCOMSettings.spinBox_dataBits, SIGNAL(valueChanged(int)), m_pDeviceList, SLOT(setDataBitsForAll(int)));
    connect(dialogCOMSettings.comboBox_flow, SIGNAL(activated(int)), m_pDeviceList, SLOT(setFlowIndexForAll(int)));
    dialogCOMSettings.exec();
}

void Core::begin()
{
    m_pDeviceList->loadList();
    m_pDeviceList->settargetDeviceIndex(DeviceList::All);
    m_pMainWindow->readSettings();
    m_pMainWindow->show();
    m_pDeviceList->start();
}

void Core::startTest(QString id, int deviceCurrentID, int slot)
{
    if(!m_pDeviceList->length())
    {
        setBaseAdress();
        Sleepy::msleep(1000);
        //QMessageBox::warning(m_pMainWindow, "Нет опрашиваемых контроллеров", "Добавьте хотя бы один контроллер!");
        //return;
    }
    if(id.isEmpty())
        return;

    DeviceWithData *pDevice = m_pDeviceList->getDevice(deviceCurrentID < 0 ? m_pDeviceList->currentIndex()
                                                                           : deviceCurrentID);
    TestWindows::ParentWindow *pWindow = createTest(pDevice, id);
    if(!pWindow)
        return;
    m_pDeviceList->getSearchDevice()->stop();
    m_pDeviceList->setcurrentIndex(pDevice->componentID());
    pWindow->setdeviceCurrentID(pDevice->componentID());
    pWindow->ondeviceCurrentIDChanged();
    pWindow->setslot(slot);
    pWindow->onSlotChanged();
    if(pWindow->m_isOnline && !pDevice->deviceAviable())
    {
        QMessageBox::warning(qApp->activeWindow(), "Ошибка!", "По данному подключению нет связи. Установите связь и повторите попытку.");
        delete pWindow;
        return;
    }
    if(pWindow->beforeShow())
    {
        pWindow->exec();
    }
    delete pWindow;
}

void Core::startCommTest()
{
    TestWindows::DialogCommTest window(m_pDeviceList, m_pMainWindow);
    if(window.beforeShow())
        window.exec();

}

void Core::startMODBUSLine()
{
    TestWindows::DialogMODBUSLine pWindow(m_pDeviceList, m_pMainWindow);
    if(pWindow.beforeShow())
        pWindow.exec();

}

TestWindows::ParentWindow* Core::createTest(DeviceWithData *pDevice, const QString &id)
{
    if(id.toLower().contains("калибровка"))
    {
        if(pDevice->isSonet())
            return new TestWindows::DialogCalibr(m_pDeviceList, m_pMainWindow);
        else
            return new TestWindows::DialogCalibrDiagModule(m_pDeviceList, m_pMainWindow);
    }
    else if(id.toLower().contains("поверка"))
    {
        if(id.toLower().contains("ввод"))
            return new TestWindows::DialogVerifyInput(id.toLower().contains("цлит") ? InputCLIT : InputOTK, m_pDeviceList, m_pMainWindow);
        else
            return new TestWindows::DialogVerifyOutput(id.toLower().contains("цлит") ? OutputCLIT : OutputOTK, m_pDeviceList, m_pMainWindow);
    }
    else if(id == m_pMainWindow->action_controllerInfo->text())
        return new TestWindows::DialogControllerConsist(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_checkSwitches->text())
        return new TestWindows::DialogCheckSwitches(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_allChannelsOverview->text())
        return new TestWindows::DialogAllChannels(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_powerCheck->text())
        return new TestWindows::DialogFullPower(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_timeToSetAnalogChannel->text())
        return new TestWindows::DialogTimeToSetAllAnalogChannels(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_diagRegister->text())
        return new TestWindows::DialogRegistersProcModule(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_watchAnalogOutput->text())
        return new TestWindows::DialogAllChannelsWithFilter(m_pDeviceList, "AO", m_pMainWindow);
    else if(id == m_pMainWindow->action_watchAnalogInput->text())
        return new TestWindows::DialogAllChannelsWithFilter(m_pDeviceList, "AI", m_pMainWindow);
    else if(id == m_pMainWindow->action_watchDiscreteInput->text())
        return new TestWindows::DialogAllChannelsWithFilter(m_pDeviceList, "DI", m_pMainWindow);
    else if(id == m_pMainWindow->action_watchDiscreteOutput->text())
        return new TestWindows::DialogAllChannelsWithFilter(m_pDeviceList, "DO", m_pMainWindow);
    else if(id == m_pMainWindow->action_switchReserve->text())
        return new TestWindows::DialogCheckReserve(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_temperature->text())
        return new TestWindows::DialogTemperature(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_replyTime->text())
        return new TestWindows::DialogReplyTime(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_readModules->text() || id == "Чтение регистров модуля")
        return new TestWindows::DialogReadModules(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_readDataViaSPI->text())
        return new TestWindows::DialogReadModulesSpi(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_diagnosticModule->text())
        return new TestWindows::DialogDiagnosticModule(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_moduleDiag_autoCalibr->text())
        return new TestWindows::DialogCalibrDiagModule(m_pDeviceList, m_pMainWindow);
    else if(id == m_pMainWindow->action_errorLog->text())
        return new TestWindows::DialogShowErrorList(m_pDeviceList, m_pMainWindow);
    else
        return 0;
}

void Core::loadData()
{
//    QString location = QDesktopServices::storageLocation ( QDesktopServices::DocumentsLocation ) + "\\SonetTest\\analog05.ini";
//    if(QFile::exists(location))
//    {
//        m_pSonetData->loadDataFromIni(location);
//        return;
//    }
    m_pSonetData->loadDataFromIni(":/sonetData/analog05.ini");
}

#define PROCESS_ACTION(action) connect(m_pMainWindow->action, SIGNAL(triggered()), pSignalMapper, SLOT(map())); \
    pSignalMapper->setMapping(m_pMainWindow->action, m_pMainWindow->action->text());

void Core::connectActions()
{
    connect(m_pMainWindow->action_fileOpen, SIGNAL(triggered()), m_pSonetData, SLOT(loadDataFromIni()));
    connect(m_pMainWindow->action_fileSave, SIGNAL(triggered()), m_pSonetData, SLOT(saveDataToIni()));

    connect(m_pMainWindow->action_commTest, SIGNAL(triggered()), this, SLOT(startCommTest()));
    connect(m_pMainWindow->action_MODBUSline, SIGNAL(triggered()), this, SLOT(startMODBUSLine()));
    QSignalMapper *pSignalMapper = new QSignalMapper(this);

    PROCESS_ACTION(action_controllerInfo)
    PROCESS_ACTION(action_checkSwitches)
    PROCESS_ACTION(action_allChannelsOverview)
    PROCESS_ACTION(action_powerCheck)
    PROCESS_ACTION(action_timeToSetAnalogChannel)
    PROCESS_ACTION(action_diagRegister)
    PROCESS_ACTION(action_watchAnalogOutput)
    PROCESS_ACTION(action_watchAnalogInput)
    PROCESS_ACTION(action_watchDiscreteInput)
    PROCESS_ACTION(action_watchDiscreteOutput)
    PROCESS_ACTION(action_switchReserve)
    PROCESS_ACTION(action_temperature)
    PROCESS_ACTION(action_replyTime)
    PROCESS_ACTION(action_readModules)
    PROCESS_ACTION(action_readDataViaSPI)
    PROCESS_ACTION(action_diagnosticModule)
    PROCESS_ACTION(action_errorLog)

    connect(m_pMainWindow->action_verifyInputCLIT, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_verifyInputCLIT, "Поверка ввода ЦЛИТ");

    connect(m_pMainWindow->action_verifyInputTY, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_verifyInputTY, "Поверка ввода по ТУ");

    connect(m_pMainWindow->action_verifyOutputCLIT, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_verifyOutputCLIT, "Поверка вывода ЦЛИТ");

    connect(m_pMainWindow->action_verifyOutputTY, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_verifyOutputTY, "Поверка вывода по ТУ");

    connect(m_pMainWindow->action_autocalibr, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_autocalibr, "Калибровка");

    connect(m_pMainWindow->action_calibr_SPI_auto, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_calibr_SPI_auto, "Калибровка");

    connect(m_pMainWindow->action_calibr_SPI_manual, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_calibr_SPI_manual, "Калибровка");

    connect(m_pMainWindow->action_EEPROM_autocalibr, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_EEPROM_autocalibr, "Калибровка");

    connect(m_pMainWindow->action_EEPROM_manual, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_EEPROM_manual, "Калибровка");

    connect(m_pMainWindow->action_moduleDiag_autoCalibr, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_moduleDiag_autoCalibr, "Калибровка модуля диагностики");

    connect(m_pMainWindow->action_moduleDiag_manualCalibr, SIGNAL(triggered()), pSignalMapper, SLOT(map()));
    pSignalMapper->setMapping(m_pMainWindow->action_moduleDiag_manualCalibr, "Калибровка модуля диагностики");

    connect(pSignalMapper, SIGNAL(mapped(QString)), this, SLOT(startTest(QString)));

    connect(m_pMainWindow->action_COM_settings, SIGNAL(triggered()), this, SLOT(showComSettings()));

    connect(m_pMainWindow->action_writePackets, SIGNAL(triggered()), this, SLOT(writePackets()));
    connect(m_pMainWindow->action_baseAddr, SIGNAL(triggered()), this, SLOT(setBaseAdress()));
    connect(m_pMainWindow->action_reserveAddr, SIGNAL(triggered()), this, SLOT(setReserveAdress()));
    connect(m_pMainWindow->action_useBase, SIGNAL(triggered()), this, SLOT(useBase()));
    connect(m_pMainWindow->action_useReserve, SIGNAL(triggered()), this, SLOT(useReserve()));}

void Core::writePackets()
{
    g_writePackets = m_pMainWindow->action_writePackets->isChecked();
}

void Core::setBaseAdress()
{
    DeviceWithData *pDevice;
    if(!m_pDeviceList->length())
    {
        m_pDeviceList->addNew();
        pDevice = m_pDeviceList->getDevice(0);
    }
    pDevice = m_pDeviceList->getDevice(0);
    int value = pDevice->deviceNum();
    value = QInputDialog::getInt(0, "Введите номер основного контроллера", "Введите адрес <b>основного</b> контроллера от 0 до 219", value, 0, 219);
    pDevice->setdeviceNum(value);
}

void Core::setReserveAdress()
{
    if(!m_pDeviceList->length())
    {
        QMessageBox::information(qApp->activeWindow(), "Укажите сначала основной контроллер", "Не указан <b>основной</b> контроллер!<br>В следующем окне введите номер <b>основного</b> контроллера.");
        setBaseAdress();
    }
    if(m_pDeviceList->length() < 2)
        m_pDeviceList->addNew();

    DeviceWithData *pDevice = m_pDeviceList->getDevice(1);
    int value = pDevice->deviceNum();
    value = QInputDialog::getInt(0, "Введите номер резервного контроллера", "Введите адрес <b>резервного</b> контроллера от 0 до 219", value, 0, 219);
    pDevice->setdeviceNum(value);
}

void Core::onCurrentChanged(int currentIndex)
{
    m_pMainWindow->action_useBase->setChecked(currentIndex == 0 && m_pDeviceList->length());
    m_pMainWindow->action_useReserve->setChecked(currentIndex == 1);
}

void Core::useBase()
{
    if(!m_pDeviceList->length())
    {
        QMessageBox::information(qApp->activeWindow(), "Укажите сначала основной контроллер", "Не указан <b>основной</b> контроллер!<br>В следующем окне введите номер <b>основного</b> контроллера.");
        setBaseAdress();
    }
    Q_ASSERT(m_pDeviceList->getDevice(0));
    m_pDeviceList->setcurrentIndex(0);
}

void Core::useReserve()
{
    if(!m_pDeviceList->length())
    {
        QMessageBox::information(qApp->activeWindow(), "Укажите сначала основной контроллер", "Не указан <b>основной</b> контроллер!<br>В следующем окне введите номер <b>основного</b> контроллера.");
        setBaseAdress();
    }
    if(m_pDeviceList->length() < 2)
    {
        QMessageBox::information(qApp->activeWindow(), "Укажите сначала резервный контроллер", "Не указан <b>резервный</b> контроллер!<br>В следующем окне введите номер <b>резервного</b> контроллера.");
        setReserveAdress();
    }
    Q_ASSERT(m_pDeviceList->getDevice(1));
    m_pDeviceList->setcurrentIndex(1);
}
