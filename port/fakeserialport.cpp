#include "fakeserialport.h"
void CRC16(unsigned char *puchMsg,unsigned char usDataLen,
            unsigned char *HiByte,unsigned char *LoByte);
const char* errMessage_ERROR_FILE_NOT_FOUND = "Нет такого порта";
#define	MODULE_NOT_EXIST	0x80
#define	MODULE_IS_ANALOG	0x40
#define	MODULE_FOR_OUTPUT	0x20
#define	MODULE_IS_INVERT	0x10

#define	ERR_NO_SUCH_FUNCTION		0x01
#define	ERR_NO_SUCH_MODULE		0x02
#define	ERR_WRONG_BYTES_COUNT		0x03
#define	ERR_BROKEN_MODULE		0x04
#define	ERR_ENHANCED_REG		0x05
#define	ERR_SLOT_IS_DISCRET		0x11
#define	ERR_SLOT_IS_ANALOG		0x12
#define	ERR_SLOT_FOR_WRITE		0x13
#define	ERR_SLOT_FOR_READ		0x14
#define ERR_CONFIG_CHANGED		0x15
#define	ERR_UNREAL_CHANNEL		0x16
#define	ERR_WRONG_FORMAT		0x17
#define	ERR_NO_SUCH_SUBFUNCTION		0x20
#define	ERR_EEPROM_TIMEOUT_DATA_ERROR	0x31
#define	ERR_ENHANCED_REG_USAGE		0x40
#define	ERR_GLOBAL_ERROR		0x50

FakeSerialPort::FakeSerialPort(QObject *parent) :
    QObject(parent)
{
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        Module module;
        if(rand() % 5 == 0)
        {
            module.id = 0xff;
            module.type = 0xff;
            module.chCount = 0xff;
        }
        else
        {
        module.type = ((rand() & 1) << 5) | ((rand() & 1) << 6);
        module.chCount = ((rand() & 1) + 1) * (module.type & 0x40 ? 4 : 8);
        if(module.type & (1 << 6))
            module.id = rand() % 7 + 60;
        else
            module.id = rand() % 67;
        }
        for(int i = 0;  i < 16;  i++)
            module.values[i] = 0;
        m_modules[slot] = module;
    }
    m_frontPanelSpeed = 0;
    m_bprState = 0;
    m_temperature = 0;
    m_frontPanelX10 = 0;
    m_frontPanelX1 = 0;
    m_frontPanelSpeed = 0;
}

bool FakeSerialPort::getError(QString *pReturnStr)
{
    if(m_error.length())
    {
        *pReturnStr += ": ";
        *pReturnStr += m_error;
    }
    Q_UNUSED(pReturnStr);
    return false;
}

bool FakeSerialPort::open(QString portName)
{
    Q_UNUSED(portName);
    Sleepy::msleep(100);
    if(portName.endsWith("COM2"))
        return true;
    else
    {
        m_error = errMessage_ERROR_FILE_NOT_FOUND;
        return false;
    }
}

bool FakeSerialPort::close()
{
    return true;
}

bool FakeSerialPort::flushRX()
{
    return true;
}

bool FakeSerialPort::write(const QByteArray &data)
{
    m_command = data;
    parseCommand();
    return true;
}

bool FakeSerialPort::read(QByteArray *ans)
{
    ans->append(m_response);
    m_response.clear();
    return true;
}

bool FakeSerialPort::setPortSettings(long int speed, int dataBits, float stopBits, int parity, int flow)
{
    Q_UNUSED(dataBits);
    Q_UNUSED(stopBits);
    Q_UNUSED(parity);
    Q_UNUSED(flow);
    QList<long> speeds;
    speeds << 1200
                         << 2400
                         << 4800
                         << 9600
                         << 19200
                         << 28800
                         << 57600
                         << 115200
                         << 230400
                         << 460800;

    m_frontPanelSpeed = speeds.indexOf(speed);
    return true;
}

bool FakeSerialPort::setTimeouts(int readTotalTimeoutConstant)
{
    Q_UNUSED(readTotalTimeoutConstant);
    return true;
}

bool FakeSerialPort::cancel()
{
    return true;
}

bool FakeSerialPort::setOtherHandle(const FakeSerialPort *pOtherPort)
{
    m_command = pOtherPort->m_command;
    m_response = pOtherPort->m_response;
    m_bprState = pOtherPort->m_bprState;
    m_temperature = pOtherPort->m_temperature;
    m_frontPanelX10 = pOtherPort->m_frontPanelX10;
    m_frontPanelX1 = pOtherPort->m_frontPanelX1;
    m_frontPanelSpeed = pOtherPort->m_frontPanelSpeed;
    m_value_addr = pOtherPort->m_value_addr;
    return true;
}

void FakeSerialPort::func_Error(unsigned char number)
{
    m_response.clear();
    m_response.append(m_command.at(0));
    m_response.append(m_command.at(1) | 0x80);
    m_response.append(number);
    Send_data();
}

void FakeSerialPort::Send_data()
{
    unsigned char hi, lo;
    CRC16((unsigned char*)m_response.data(), m_response.size(), &hi, &lo);
    m_response.append(hi);
    m_response.append(lo);
}

void FakeSerialPort::parseCommand()
{
    unsigned char Hi, Lo;
    CRC16((unsigned char*)m_command.data(),m_command.size() - 2,&Hi,&Lo);
    unsigned char testH = m_command.at(m_command.size() - 2);
    unsigned char testL = m_command.at(m_command.size() - 1);
    if(
      (testH != Hi) ||
      (testL != Lo) ||
      (m_command.size() < 4)
    )
        return;
    m_response.clear();
    m_response.append(m_command.at(0));
    m_response.append(m_command.at(1));
    unsigned char command = m_command.at(1);
    switch(command)
    {
      case  0x01:
      case  0x02:  func_01();break;
      case  0x05:  func_05();break;
      case  0x08:  func_08();break;

    case    0x35:	func_35();break;

    case	0x40:	func_40();break;
    case	0x41:	func_41();break;
    //case	0x42:	func_42();break;
    case	0x44:	func_44();break;
    //case	0x45:	func_45();break;

    case	0x50:	func_50();break;
    case	0x51:	func_51();break;
    case	0x52:	func_52();break;
    case	0x53:	func_53();break;
    case	0x54:	func_54();break;

      default:
        func_Error(ERR_NO_SUCH_FUNCTION);
    }
}

unsigned char FakeSerialPort::func_check_size(unsigned char size)
{
    if(m_command.size()==size)
        return 0;
    func_Error(ERR_WRONG_BYTES_COUNT);
    return 1;
}

void FakeSerialPort::func_01(void)
{
if(func_check_size(8))
    return;
m_value_addr=m_modules[(int)m_command.at(2)];

if(m_value_addr.type & MODULE_NOT_EXIST)
    {
    func_Error(ERR_NO_SUCH_MODULE);
    return;
    }
if ( (m_value_addr.type & MODULE_IS_ANALOG ))
    { //битовые операции для аналоговых модулей не поддерживаются.
    func_Error(ERR_SLOT_IS_ANALOG);
    return;
    }
    {
    unsigned char bytes;
    unsigned char i, count;

    count=(m_command[4] <<8 ) | m_command[5];
    bytes=(count+7)>>3;
    m_response.append(bytes);

    unsigned char value = 0;
    for(i=0;i<count;i++)
    {
        if(m_value_addr.values[i] & 1)
        {
            value |= 1<<(i&7);
        }
        if((i % 8) == 7)
        {
            m_response.append(value);
            value = 0;
        }
    }
    Send_data();
    }
}

void FakeSerialPort::func_05(void)
{
if(func_check_size(8))
    return;

    {
    unsigned char ret=0;

    if (m_command[5])
    {
        func_Error(ERR_WRONG_FORMAT);
        return;
    }

    switch((unsigned char)m_command[4])
            {
        case 0x00:
            m_modules[(int)m_command.at(2)].values[(int)m_command[3]] = 0;
        break;
        case 0xff:
            m_modules[(int)m_command.at(2)].values[(int)m_command[3]] = 1;
        break;
        default:
                ret=ERR_WRONG_FORMAT;
            }

    if(ret)
        func_Error(ret);
    else
        {
        Send_data();
        }
    }
}

//-------------------------------------------
void FakeSerialPort::func_35(void)
{
if(func_check_size(8))
    return;
  {
    m_command[2] = 0;
unsigned char i;
for(i = 0;  i < 8;  i++)
{
    m_modules[m_command[3] >> 4].values[i] |= m_command[4] & (1 << i);
    m_modules[m_command[3] >> 4].values[i + 8] |= m_command[5] & (1 << i);

}
      Send_data();
  }
}

void FakeSerialPort::func_40(void)
{
    if (func_check_size(6))
        return;
    m_value_addr=m_modules[(int)m_command[3]>>4];

    m_response[2] = 0;
    m_response[3] = m_value_addr.values[(int)m_command[3] & 0x0f] >> 8;
    m_response[4] = m_value_addr.values[(int)m_command[3] & 0x0f] & 0xff;

    Send_data();
}


void FakeSerialPort::func_41(void)
{
    if(func_check_size(6))
        return;
    unsigned char i,j;
    m_value_addr=m_modules[(int)m_command[3]>>4];

    if(m_value_addr.type&MODULE_NOT_EXIST)
    {
        func_Error(ERR_NO_SUCH_MODULE);
        return;
    }
    m_response.append((char)0);
    j=m_value_addr.chCount;
    m_response.append(j << 1);
    for(i=0;i<j;i++)
    {
        m_response.append(m_value_addr.values[i] >> 8);
        m_response.append(m_value_addr.values[i] & 0xff);
    }
    Send_data();
}


/*
void FakeSerialPort::func_42(void)
{

if (func_check_size(5))
    return;

    unsigned char i,k,j=0,error=0;
    for(i=0;i<SLOTS_COUNT;i++)
        {
        if ((consist[i]&(MODULE_NOT_EXIST|MODULE_IS_ANALOG|MODULE_FOR_OUTPUT))==MODULE_IS_ANALOG)
                {
            if ((*CountPort(i))==consist[i])
                {
                for (k=0;k<consist_num[i];k++)
                    {
                        if	(
                        GetAnalogChannel(i,k,m_command+j+5,m_command+j+6)
                        )
                        error|=1<<i;
                    j+=2;
                    }
                }
            else
                {
                error|=1<<i;
                j+=2*consist_num[i];
                }
            }
        }
    m_command[2]=READ_RESERV();
    m_command[3]=error;
    m_command[4]=j;
    Send_data();
}
*/
void FakeSerialPort::func_44(void)
{
    if (func_check_size(8))
        return;
    m_response.append((char)0);
    int channel =   m_command[3]&0x0f;
    m_modules[(int)m_command[3]>>4].values[channel] = ((unsigned short)m_command[4] << 8) | (m_command[5] & 0xff);
    Send_data();
}

/*
void FakeSerialPort::func_45(void)
{
if (receive_size>8)
    {
    unsigned char i;
    i=m_command[3]>>4;

    if((*CountPort(i))&MODULE_NOT_EXIST)
        {
            func_Error(ERR_NO_SUCH_MODULE);
        return;
        }

    if (receive_size!=m_command[4]+7)
        {
            func_Error(ERR_WRONG_BYTES_COUNT);
        return;
        }

    if(*(CountPort(i)+2)<(m_command[4]>>1))
        {
        func_Error(ERR_UNREAL_CHANNEL);
        return;
        }

        }
else
    {
    func_Error(ERR_WRONG_BYTES_COUNT);
    return;
    }

    {
    unsigned char i,j,error=0;


    j=m_command[4]>>1;
    for(i=0;i<j;i++)
        error|=SetAnalogChannel
            (
                m_command[3]>>4,
            i,
                m_command[5+(i<<1)],
                m_command[6+(i<<1)]
            );

    m_command[2]=READ_RESERV();

  if (!error)
    Send_data(3);
  else
    func_Error(i);
  }
}
*/
void FakeSerialPort::func_50(void)
{
    if(func_check_size(4))
        return;
    unsigned char i;

    for (i=0;i<SLOTS_COUNT;i++)
        m_response.append(m_modules[i].type);

    Send_data();
}


void FakeSerialPort::func_51(void)
{
    if(func_check_size(4))
        return;
    unsigned char i;

    for (i=0;i<SLOTS_COUNT;i++)
        m_response.append(m_modules[i].id);

    Send_data();
}

void FakeSerialPort::func_52(void)
{
    if(func_check_size(4))
        return;
    unsigned char i;
    for (i=0;i<SLOTS_COUNT;i++)
    {
        if(m_modules[i].type & MODULE_NOT_EXIST)
            m_response.append((char)0);
        else
            m_response.append(m_modules[i].chCount);
    }
    Send_data();
}

void FakeSerialPort::func_53(void)
{
    unsigned char i;

    if(func_check_size(5))
        return;
    m_response.append((char)0);
    for (i=0;i<SLOTS_COUNT;i++)
    {
        m_response.append(m_modules[i].type);
        m_response.append(m_modules[i].id);
        m_response.append(m_modules[i].chCount);
    }
    Send_data();
}



void FakeSerialPort::func_54(void)
{
    if(func_check_size(4))
        return;
    m_response.append((char)0);
    Send_data();
}


void FakeSerialPort::func_08_0000(void)
{
    if (func_check_size(8))
        return;
    m_response = m_command;
    Send_data();
}

/*
void FakeSerialPort::func_08_0001(void)
{
if (func_check_size(8))
    return;

    {
    if (m_command[5]==0)
        {
        if (m_command[4]==0x00)
            {
            silent=0;
            while(eeprom_register) eeprom_write_counter();
            Send_data(6);

            wdt_enable(0);
            for(;;);

            }
        if (m_command[4]==0xff)
            {
            silent=0;
            Send_data(6);

            eeprom_clean();
                        while(eeprom_register) eeprom_write_counter();
            wdt_enable(0);
            for(;;);
            }
        }
    func_Error(ERR_WRONG_FORMAT);
    }
}

void FakeSerialPort::func_08_0002(void)
{
if (func_check_size(8))
    return;
  {
unsigned char i;
  m_command[4]=status_register>>8;
  m_command[5]=status_register;
  m_command[6]=status_register=0;
  for(i=0;i<SLOTS_COUNT;i++)
    if( consist[i] != *(CountPort(i)) )
    m_command[6]=1<<i;
  Send_data(7);
  }
}

void FakeSerialPort::func_08_0004(void)
{
if (func_check_size(8))
    return;
  {
  if ((m_command[5])||(m_command[4]))
    func_Error(ERR_WRONG_FORMAT);
  else
    {
    silent=1;
    timeout_stop();
    uart_receive_complete();
    }
  }
}

void func_08_000A(void)
{
if (func_check_size(8))
    return;
  {
  if ((m_command[5])||(m_command[4]))
    {
    func_Error(ERR_WRONG_FORMAT);
    return;
    }
  else
    {
    eeprom_clean();
    Send_data(6);
    }
  }
}


void FakeSerialPort::func_08_0F02(void)
{
if (func_check_size(8))
    return;
  {
unsigned char i;
  for(i=0;i<0x10;i++)
  m_command[i+4]=eeprom_counter[i];
  Send_data(4+16);
  }
}

void func_08_0F04(void)
{
if (func_check_size(8))
    return;
  {
  eeprom_counter[EEPROM_TIMEOUT_TIME_HI]=m_command[4];
  eeprom_counter[EEPROM_TIMEOUT_TIME_LO]=m_command[5];
  status_register|=(1<<EEPROM_TIMEOUT_TIME_LO)|(1<<EEPROM_TIMEOUT_TIME_HI);
  eeprom_register|=(1<<EEPROM_TIMEOUT_TIME_LO)|(1<<EEPROM_TIMEOUT_TIME_HI);


  Send_data(6);
  }
}

void FakeSerialPort::func_08_0F10(void)
{
if (func_check_size(8))
    return;
  {
  get_timeout_values(m_command[4]>>4,m_command+5);
  Send_data(5+EEPROM_TIMEOUT_SLOT_LENGTH);
      }
}

void FakeSerialPort::func_08_0F11(void)
{
if (func_check_size(12))
    return;
  {
  if( m_command[5]!=*(CountPort(m_command[4]>>4)) )
    func_Error(ERR_EEPROM_TIMEOUT_DATA_ERROR);
  else
    {
    set_timeout_values(
      m_command[4]>>4,
      m_command[5],
      m_command[6],
      m_command[8],
      m_command[7],
      m_command[9]);
    Send_data(4);
    }
      }
}


void FakeSerialPort::func_08_0F12(void)
{
if (func_check_size(10))
    return;
  {
  if( m_command[5]!=*(CountPort(m_command[4]>>4)) )
    func_Error(ERR_EEPROM_TIMEOUT_DATA_ERROR);
  else
    {
    set_timeout_values(
      m_command[4]>>4,
      m_command[5],
      m_command[4]&0x0f,
      0,
      m_command[6],
      m_command[7]);
    Send_data(4);
    }
      }
}

void FakeSerialPort::func_08_0F13(void)
{
if (func_check_size(8))
    return;
  {
  clear_all_timeout_values();
  Send_data(6);
confirm_request_from_master(0);
      }
}

void func_08_0F14(void)
{
if (func_check_size(8))
    return;
  {
  clear_one_timeout_value(m_command[4]>>4,0);
  Send_data(6);
confirm_request_from_master(0);
      }
}

void FakeSerialPort::func_08_0F15(void)
{
if (func_check_size(8))
    return;
  {
  clear_one_timeout_value(m_command[4]>>4,m_command[4]&7);
  Send_data(6);
      }
}

void FakeSerialPort::func_08_0FA1(void)
{
if(func_check_size(9))
    return;

    {
    unsigned char start;
    unsigned char i,count;
    m_value_addr=CountPort(m_command[4]);
    start=m_command[5];
    count=m_command[6];

    for(i=0;i<count;i++)
        m_command[i+5]=m_value_addr[start+i];

    Send_data(5+count);
confirm_request_from_master(0);
    }
}

void FakeSerialPort::func_08_0FA2(void)
{
unsigned char start;
unsigned char i,count;
m_value_addr=CountPort(m_command[4]);
start=m_command[5];
count=m_command[6];
if((receive_size<8) || (receive_size!=count+9))
    {
    func_Error(ERR_WRONG_BYTES_COUNT);
    return;
    }

for(i=0;i<count;i++)
    m_value_addr[start+i]=m_command[7+i];
Send_data(7+count);
confirm_request_from_master(0);
}


*/
void FakeSerialPort::func_08_0FF0(void)
{
    if (func_check_size(8))
        return;
    for(int i = 0;  i < SLOTS_COUNT;  i++)
    {
        if(m_modules[i].type & MODULE_IS_ANALOG)
            if(!(m_modules[i].type & MODULE_FOR_OUTPUT))
            {
                for(int ch = 0;  ch < 16;  ch++)
                {
                    m_modules[i].values[ch] = rand() % 10;
                }
            }
    }

    m_response.append(m_frontPanelSpeed);
    m_response.append(m_command.at(0) % 10);
    m_response.append((m_command.at(0) / 10) % 10);
    Send_data();
}
/*
#if	RESERV

void func_08_0FF1(void)
{
if (func_check_size(8))
    return;
  {
  m_command[4]=READ_RESERV();
  Send_data(5);
confirm_request_from_master(0);
      }
}

#endif

#ifdef	WITH_TERMO

void func_08_0FF2(void)
{
if (func_check_size(8))
    return;

    {
    termo_read(m_command+4,m_command+5);
    Send_data(6);
confirm_request_from_master(0);
    }
}

#endif

#if	RESERV

void func_08_0FF3(void)
{
if (func_check_size(8))
    return;
if (m_command[4])
    {
    func_Error(0x17);
    return;
    }
switch(m_command[5])
    {
    case 0x00:
        report_active&=~4;
        break;
    case 0xff:
        report_active|=4;
        break;
    default:
        func_Error(0x17);
        return;

    }

m_command[4]=READ_RESERV();
Send_data(5);
confirm_request_from_master(0);
}

#endif

void FakeSerialPort::func_08_0FF4(void)
{
if (func_check_size(8))
    return;
autocalibrate_all_modules();

Send_data(4);
confirm_request_from_master(0);
}

*/
void FakeSerialPort::func_08_0FFF(void)
{
    if (func_check_size(8))
        return;
    m_response.append(0x05);
    m_response.append(0x09);

    Send_data();
}

void FakeSerialPort::func_08(void)
{
    m_response.append(m_command.at(2));
    m_response.append(m_command.at(3));
switch(m_command[2])
  {
  case 0x00: switch(m_command[3])
      {
      case 0x00: func_08_0000();break;
      //case 0x01: func_08_0001();break;
      //case 0x02: func_08_0002();break;
      //case 0x04: func_08_0004();break;
      //case 0x0A: func_08_000A();break;

      default: func_Error(ERR_NO_SUCH_SUBFUNCTION);
      }; break;
  case 0x0f: switch((unsigned char)m_command[3])
      {
      //case 0x02: func_08_0F02();break;
      //case 0x04: func_08_0F04();break;
      //case 0x10: func_08_0F10();break;
      //case 0x11: func_08_0F11();break;
      //case 0x12: func_08_0F12();break;
      //case 0x13: func_08_0F13();break;
      //case 0x14: func_08_0F14();break;
      //case 0x15: func_08_0F15();break;
      case 0xf0: func_08_0FF0();break;

#if	RESERV
      case 0xf1: func_08_0FF1();break;
#endif

#ifdef	WITH_TERMO
      case 0xf2: func_08_0FF2();break;
#endif
#if	RESERV
      case 0xf3: func_08_0FF3();break;
#endif
      //case 0xf4: func_08_0FF4();break;


    //case 0xa1: func_08_0FA1();break;
    //case 0xa2: func_08_0FA2();break;

      case 0xff: func_08_0FFF();break;

      default: func_Error(ERR_NO_SUCH_SUBFUNCTION);
      }; break;

  default: func_Error(ERR_NO_SUCH_SUBFUNCTION);
  }
}

static const unsigned char auchCRCHi[] = {
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
};

// Table of CRC values for low-order byte
static const unsigned char auchCRCLo[] = {
0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06,
0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD,
0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A,
0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14, 0xD4,
0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3,
0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29,
0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED,
0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60,
0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67,
0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E,
0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71,
0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,
0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B,
0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B,
0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,
0x43, 0x83, 0x41, 0x81, 0x80, 0x40
};



void CRC16(unsigned char *puchMsg,unsigned char usDataLen,
            unsigned char *HiByte,unsigned char *LoByte)
{
unsigned char uIndex ; // will index into CRC lookup
*HiByte = 0xFF ; // high CRC byte initialized
*LoByte = 0xFF ; // low CRC byte initialized

while (usDataLen--)    // pass through message buffer
    {
    uIndex = (*HiByte)^ (*puchMsg++) ;
    *HiByte = (*LoByte) ^ auchCRCHi[uIndex] ;
    *LoByte = auchCRCLo[uIndex];
    }

}
