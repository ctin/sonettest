#ifndef WINDOWSSERIALPORT_H
#define WINDOWSSERIALPORT_H

#include <QObject>
#include "windows.h"

extern const char* errMessage_ERROR_FILE_NOT_FOUND;

class WindowsSerialPort : public QObject
{
    Q_OBJECT
public:
    explicit WindowsSerialPort(QObject *parent = 0);
    bool open(QString portName);
    bool close();
    bool flushRX();
    bool write(const QByteArray &data);
    bool read(QByteArray *ans);
    bool cancel();
    bool getError(QString *pReturnStr);
    bool setPortSettings(long int speed, int dataBits, float stopBits, int parity, int flow);
    bool setTimeouts(int readTotalTimeoutConstant);
    bool setOtherHandle(const WindowsSerialPort *pWindowsSerialPort);
private:
    HANDLE m_handle;

signals:
    
public slots:
    
};

#endif // WINDOWSSERIALPORT_H
