﻿#ifndef AGILENT34401A_H
#define AGILENT34401A_H

#include "modbusdevice.h"

class Agilent34401A : public ModbusDevice
{
    Q_OBJECT
    Q_PROPERTY(QString unit READ unit WRITE setunit NOTIFY unitChanged)
    Q_PROPERTY(QString currentState READ currentState WRITE setcurrentState NOTIFY currentStateChanged)
    Q_PROPERTY(qreal deviceValue READ deviceValue WRITE setdeviceValue NOTIFY deviceValueChanged)
    Q_PROPERTY(bool connected READ connected WRITE setconnected NOTIFY connectedChanged)

public:
    explicit Agilent34401A(QObject *parent = 0);
    ~Agilent34401A();
private:
    bool check(QByteArray *answer);
    void addCRC(QByteArray *puchMsg);
    bool getCommandData(QByteArray *command);
private:
    bool _getValue();
    bool _find();
    bool _refreshRegime();
    void _refreshState();
    bool _findCurrentDevice(const QByteArray &command);
    QFuture<void> m_future;
private:
    volatile bool m_needToRefresh;
    QTimer *m_pSurveyTimer;
private slots:
    void refreshState(); //for timer
    //properties
    //properties
public:             QString unit() const;
public slots:       void setunit(const QString unit);
signals:            void unitChanged(QString unit);
private:            QString m_unit ;
public:         QString currentState() const;
public slots:   void setcurrentState(const QString currentState);
signals:        void currentStateChanged(QString currentState);
private:        QString m_currentState;
public:                 qreal deviceValue() const;
public slots:           void setdeviceValue(const qreal deviceValue);
signals:                void deviceValueChanged(qreal deviceValue);
private:                qreal m_deviceValue;
public:             bool connected() const;
public slots:       void setconnected(const bool connected);
signals:            void connectedChanged(bool connected);
private:            bool m_connected;
    
};

#endif // AGILENT34401A_H
