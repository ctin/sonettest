﻿#ifndef MACRO_H
#define MACRO_H

#include <QObject>

#ifndef SET_PROPERTY
#define SET_PROPERTY(a) {if(m_##a == a) return; m_##a = a; emit a##Changed(a);}
#endif // SET_PROPERTY

#ifndef READ_PROPERTY
#define READ_PROPERTY(a) a() const {return m_##a;}
#endif // READ_PROPERTY

#ifndef PROPERTY_CPP
#define PROPERTY_CPP(type, class, name)\
type class :: name() const \
{\
    return m_##name;\
}\
void class :: set##name (const type name) \
{\
    if(m_##name == name)\
        return;\
    m_##name = name;\
    emit name##Changed(name);\
}
#endif  //PROPERTY_CPP

#ifndef PROPERTY_CPP_GUI
#define PROPERTY_CPP_GUI(type, class, name)\
type class :: name() const \
{\
    return m_##name;\
}\
void class :: set##name (const type name) \
{\
    if(m_##name == name)\
        return;\
    m_##name = name;\
}
#endif  //PROPERTY_CPP_GUI

#endif // MACRO_H
