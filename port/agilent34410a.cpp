﻿#include "agilent34410a.h"
#include "macro.h"

Agilent34410A::Agilent34410A(QObject *parent) :
    QObject(parent)
{
    m_pTcpSocket = new QTcpSocket(this);
    m_needSetRegime = false;
    m_connected = false;
    setunit("мА");
    setcurrentValue(0);
    setconnected(false);
    setport(5025);
    setip("010.000.253.010");
    connect(m_pTcpSocket, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(m_pTcpSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect(m_pTcpSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(setstate(QAbstractSocket::SocketState)));
    connectToDevice();
}

Agilent34410A::~Agilent34410A()
{
    m_pTcpSocket->abort();
    m_pTcpSocket->deleteLater();
}

void Agilent34410A::onConnected()
{
    setconnected(true);
}

void Agilent34410A::onDisconnected()
{
    setconnected(false);
}

void Agilent34410A::connectToDevice()
{
    m_pTcpSocket->abort();
    QHostAddress hostAddress(ip());
    m_pTcpSocket->connectToHost(hostAddress, port(), QIODevice::ReadWrite);
}

void Agilent34410A::refreshValue()
{
    if(!connected())
        return;
    m_pTcpSocket->flush();
    m_pTcpSocket->write("READ?\n");
    m_pTcpSocket->waitForReadyRead(500);
    QByteArray data = m_pTcpSocket->readAll();
    data.resize(data.indexOf('\n'));
    bool ok;
    qreal result = data.toDouble(&ok);
    if(!ok)
        return;
    if(unit() == "мА")
        result *= 1000;
    setcurrentValue(result);
}

void Agilent34410A::refreshRegime()
{
    if(!connected())
    {
        m_needSetRegime = true;
        return;
    }

    if(unit() == "мА")
    {
        m_pTcpSocket->write("CONFigure:CURRent:DC\n");
        m_pTcpSocket->write("READ?\n");
    }
    else
    {
        m_pTcpSocket->write("CONF:VOLT:DC\n");
        m_pTcpSocket->write("READ?\n");
    }
}

QString Agilent34410A::unit() const
{
    return m_unit;
}

void Agilent34410A::setunit(const QString unit)
{
    if(m_unit == unit)
        return;
    m_unit = unit;
    emit unitChanged(unit);
    m_needSetRegime = !connected();
    if(connected())
        refreshRegime();
}

bool Agilent34410A::connected() const
{
    return m_pTcpSocket->state() == QTcpSocket::ConnectedState;
}

void Agilent34410A::setconnected(const bool connected)
{
    if(m_connected == connected)
        return;
    m_connected = connected;
    emit connectedChanged(connected);
    if(connected)
        if(m_needSetRegime)
            refreshRegime();
}

int Agilent34410A::port() const
{
    return m_port;
}

void Agilent34410A::setport(const int port)
{
    if(m_port == port)
        return;
    m_port = port;
    emit portChanged(port);
    m_pTcpSocket->abort();
    connectToDevice();
}

QString Agilent34410A::ip() const
{
    return m_ip;
}

void Agilent34410A::setip(const QString ip)
{
    if(m_ip == ip)
        return;
    m_ip = ip;
    emit ipChanged(ip);
    m_pTcpSocket->disconnectFromHost();
    m_pTcpSocket->abort();
    connectToDevice();
}

void Agilent34410A::setstate(QAbstractSocket::SocketState state)
{
    QString currentState;
    switch(state)
    {
    case QAbstractSocket::UnconnectedState: currentState = "Не подключен";  break;
    case QAbstractSocket::HostLookupState:  currentState = "Поиск хоста";   break;
    case QAbstractSocket::ConnectingState:  currentState = "Подключение..."; break;
    case QAbstractSocket::ConnectedState:   currentState = "Подключен";     break;
    case QAbstractSocket::BoundState:       currentState = "Подключен";     break;
    case QAbstractSocket::ClosingState:     currentState = "Отключение...";     break;
    case QAbstractSocket::ListeningState:   currentState = "Ожидание";      break;
    default: currentState = "Неизвестное состояние"; break;
    }
    setstate(currentState);
}

PROPERTY_CPP(qreal, Agilent34410A, currentValue)
PROPERTY_CPP(QString, Agilent34410A, state)

