﻿#include "agilent34401a.h"
#include <QtConcurrent/QtConcurrent>

Agilent34401A::Agilent34401A(QObject *parent) :
    ModbusDevice(parent)
{
    m_needToRefresh = false;
    setreadTotalTimeoutConstant(10);
    setwaitTime(1000);
    setrateIndex(3);
    setstopBits(2);
    setdataBits(8);
    setdeviceValue(0);

    setunit("мА");
    setrunning(true);
    m_pSurveyTimer = new QTimer(this);
    m_pSurveyTimer->setInterval(50);
    connect(m_pSurveyTimer, SIGNAL(timeout()), this, SLOT(refreshState()));
    m_pSurveyTimer->start();
}

Agilent34401A::~Agilent34401A()
{
    setrunning(false);
    m_pSurveyTimer->stop();
    m_future.waitForFinished();
}

void Agilent34401A::addCRC(QByteArray *puchMsg)
{
    puchMsg->append(0x0D);
    puchMsg->append(0x0A);
}

void Agilent34401A::refreshState()
{
    if(m_future.isRunning())
        return;
    m_future = QtConcurrent::run(this, &Agilent34401A::_refreshState);
}

void Agilent34401A::_refreshState()
{
    if(!running())
        return;
    initPort();
    PortOpener opener(this);
    if(!deviceAviable())
    {
        m_needToRefresh = true;
        setcurrentState("поиск прибора");
        _find();
    }
    else if(m_needToRefresh)
    {
        setcurrentState("Устанавливаю режим...");
        if(_refreshRegime())
            setcurrentState("Режим установлен\nожидаю команд");
    }
    else
    {
        if(_getValue())
            setcurrentState(QString("считано: %1").arg(deviceValue()));
    }
    setconnected(deviceAviable());

}

bool Agilent34401A::_refreshRegime()
{
    if(!connected())
    {
        m_needToRefresh = true;
        return false;
    }

    if(unit() == "мА")
    {
        if(sendCommand("CONFigure:CURRent:DC\n"))
            if(sendCommand("READ?\n"))
            {
                m_needToRefresh = false;
                return true;
            }
    }
    else
    {
        if(sendCommand("CONF:VOLT:DC\n"))
            if(sendCommand("READ?\n"))
            {
                m_needToRefresh = false;
                return true;
            }
    }
    return false;
}

bool Agilent34401A::_find()
{
    return findCurrentDevice("*IDN?");
}

bool Agilent34401A::getCommandData(QByteArray *command)
{
    if(command->size() < 2)
        return false;
    addCRC(command);
    m_answer.clear();
    m_request = *command;
    return true;
}

bool Agilent34401A::_getValue()
{
    ByteArray ans;
    if(sendCommand("READ?\n", &ans))
    {
        ans.resize(ans.indexOf('\n'));
        bool ok;
        qreal result = ans.toDouble(&ok);
        if(!ok)
            return false;
        if(unit() == "мА")
            result *= 1000;
        setdeviceValue(result);
        return true;
    }
    return false;
}

PROPERTY_CPP(QString, Agilent34401A, currentState)
PROPERTY_CPP(qreal, Agilent34401A, deviceValue)
PROPERTY_CPP(bool, Agilent34401A, connected)

QString Agilent34401A::unit() const
{
    return m_unit;
}

void Agilent34401A::setunit(const QString unit)
{
    if(m_unit == unit)
        return;
    m_needToRefresh = true;
    m_unit = unit;
    emit unitChanged(unit);
}

bool Agilent34401A::check(QByteArray *answer)
{
    int ind = answer->indexOf('\n');
    if(ind < 0)
        return false;
    else if(ind > 0)
        answer->remove(0, ind);
    return answer->indexOf('\n') >= 0;
}
