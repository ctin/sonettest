﻿#include "icsu_device.h"


QByteArray START    = QByteArray(":1;11;469;1;1;2;");
QByteArray CONFIRM  = QByteArray(":1;11;469;1;1;1;");
QByteArray ENTER    = QByteArray(":1;11;37;0;1;64;");
QByteArray DOWN     = QByteArray(":1;11;37;0;1;66;");
QByteArray UP       = QByteArray(":1;11;37;0;1;67;");
QByteArray DIGIT_0  = QByteArray(":1;11;37;0;1;51;");
QByteArray DIGIT_1  = QByteArray(":1;11;37;0;1;52;");
QByteArray DIGIT_2  = QByteArray(":1;11;37;0;1;68;");
QByteArray DIGIT_3  = QByteArray(":1;11;37;0;1;84;");
QByteArray DIGIT_4  = QByteArray(":1;11;37;0;1;53;");
QByteArray DIGIT_5  = QByteArray(":1;11;37;0;1;69;");
QByteArray DIGIT_6  = QByteArray(":1;11;37;0;1;85;");
QByteArray DIGIT_7  = QByteArray(":1;11;37;0;1;54;");
QByteArray DIGIT_8  = QByteArray(":1;11;37;0;1;70;");
QByteArray DIGIT_9  = QByteArray(":1;11;37;0;1;86;");
QByteArray READ     = QByteArray(":1;1;1;");

ICSU_device::ICSU_device(QObject *parent) :
    ModbusDevice(parent)
{
    m_needToRefresh = false;
    setreadTotalTimeoutConstant(200);
    setwaitTime(1000);
    setrateIndex(3);
    setstopBits(1);
    setdataBits(8);
    setreadOnly(false);
    setuserValue(0);
    setdeviceValue(0);
    setunit("мА");
    setrunning(false);
}

ICSU_device::~ICSU_device()
{
    setrunning(false);
    m_future.waitForFinished();
}

void ICSU_device::__refreshState()
{
    setrunning(true);
    m_collecting = true;
    if(!g_autoSearchEnabled)
        setport(g_defaultPortICSU);
    else
        initPort();
    PortOpener opener(this);
    if(isOpen())
        unsetHardwareError();
    if(!deviceAviable())
    {
        m_needToRefresh = true;
        setcurrentState("поиск прибора");
        _find();
    }
    else if(m_needToRefresh)
    {
        setcurrentState("Устанавливаю режим...");
        if(_refreshRegime())
            setcurrentState("Режим установлен\nожидаю команд");
    }
    else if(readOnly())
    {
        if(_getValue())
            setcurrentState(QString("считано: %1").arg(deviceValue()));
    }
    else if(userValue() != deviceValue())
    {
        setcurrentState("отправляю команды");
        qreal lastValue = userValue();
        if(_setValue())
            setcurrentState(QString("выставлено:\n %1 %2").arg(lastValue).arg(unit()));
    }
    m_collecting = false;
    setconnected(deviceAviable());
    setrunning(false);
}

bool ICSU_device::getCommandData(QByteArray *command)
{
    if(command->size() < 2)
        return false;
    addCRC(command);
    m_answer.clear();
    m_request = *command;
    return true;
}

bool ICSU_device::_refreshRegime()
{
    setprogress(0);
    QList<QByteArray> commands;
    commands << START;
    if(readOnly())
        commands << DOWN << CONFIRM;
    commands << ENTER << CONFIRM;
    int unitType = (unit() == "В") * 2 + (unit() == "мВ");
    for(int i = 0;  i < unitType;  i++)
        commands << DOWN << CONFIRM;
    commands << ENTER << CONFIRM;
    int step = 0;

    bool k = true;

    for(QList<QByteArray>::ConstIterator iter = commands.constBegin();
        running() && iter != commands.constEnd() && k;
        iter++)
    {
        k = sendCommand(*iter);
        setprogress(100 * ++step / commands.size());
    }
    k &= running();
    m_needToRefresh = !k;
    setprogress(k * 100);
    return k;
}

bool ICSU_device::_find()
{
    return !findCurrentDevice(START);
}
bool ICSU_device::_setValue()
{
    setprogress(0);
    qreal lastValue = userValue();
    int valueToSet = lastValue * (unit() != "мВ" ? 1000 : 100);
    QByteArray b = QByteArray::number(valueToSet);
    QList<QByteArray> commands;
    for(int i = 0;  i < b.size();  i++)
    {
        switch (b.at(i) - '0')
        {
            case 0: commands << DIGIT_0; break;
            case 1: commands << DIGIT_1; break;
            case 2: commands << DIGIT_2; break;
            case 3: commands << DIGIT_3; break;
            case 4: commands << DIGIT_4; break;
            case 5: commands << DIGIT_5; break;
            case 6: commands << DIGIT_6; break;
            case 7: commands << DIGIT_7; break;
            case 8: commands << DIGIT_8; break;
            case 9: commands << DIGIT_9; break;

        }
        commands << CONFIRM;
    }
    commands << ENTER << CONFIRM;

    int step = 0;
    bool k = true;
    for(QList<QByteArray>::ConstIterator iter = commands.constBegin();
        running() && iter != commands.constEnd() && k;
        iter++)
    {
        k = sendCommand(*iter);
        setprogress(100 * ++step / commands.size());
    }
    k &= running();
    if(k)
    {
        setprogress(100);
        setdeviceValue(lastValue);
        emit finishRequest(lastValue);
    }
    return k;
}

bool ICSU_device::_getValue()
{
    ByteArray ans;
    setprogress(0);
    if(sendCommand(READ, &ans))
    {
        setprogress(100);
        ans.remove(0, ans.indexOf(';') + 1);
        ans.chop(ans.size() - ans.lastIndexOf(';'));
        qreal result = ans.toDouble();
        setdeviceValue(result);
        setuserValue(result);
        return true;
    }
    return false;
}

PROPERTY_CPP(QString, ICSU_device, currentState)
PROPERTY_CPP(qreal, ICSU_device, deviceValue)
PROPERTY_CPP(bool, ICSU_device, connected)
PROPERTY_CPP(qreal, ICSU_device, userValue)
PROPERTY_CPP(qreal, ICSU_device, progress)

QString ICSU_device::unit() const
{
    return m_unit;
}

void ICSU_device::setunit(const QString unit)
{
    if(m_unit == unit)
        return;
    m_needToRefresh = true;
    m_unit = unit;
    emit unitChanged(unit);
}

bool ICSU_device::readOnly() const
{
    return m_readOnly;
}

void ICSU_device::setreadOnly(const bool readOnly)
{
    if(m_readOnly == readOnly)
        return;
    m_needToRefresh = true;
    m_readOnly = readOnly;
    emit readOnlyChanged(readOnly);
}

bool checkCrcICSU(const QByteArray *data);

bool ICSU_device::check(QByteArray *answer)
{
    int ind = answer->indexOf('!');
    if(ind < 0 || !answer->contains(0x0D))
        return false;
    else if(ind > 0)
        answer->remove(0, ind);
    return checkCrcICSU(answer);
}

bool checkCrcICSU(const QByteArray *data)
{
    unsigned short crc = 0xFFFF;
    int len = data->lastIndexOf(';') + 1;
    for(int i = 1;  i < len;  i++)
    {
        crc = crc ^ (char)data->at(i);
        for (int j = 1; j <= 8; j++)
            if(((crc / 2) * 2) != crc)
                crc = (crc / 2) ^ 40961;
            else
                crc = crc / 2;
    }
    return data->mid(len, data->lastIndexOf(0x0D) - len).toUShort() == crc;
}

void ICSU_device::addCRC(QByteArray *data)
{
    unsigned short crc = 0xFFFF;
    for(int i = 1;  i < data->length();  i++)
    {
        crc = crc ^ (char)data->at(i);
        for (int j = 1; j <= 8; j++)
            if(((crc / 2) * 2) != crc)
                crc = (crc / 2) ^ 40961;
            else
                crc = crc / 2;
    }
    data->append(QByteArray::number(crc));
    data->push_back(13);
}
