﻿#ifndef ICSU_DEVICE_H
#define ICSU_DEVICE_H

#include "modbusdevice.h"
#include "macro.h"

class ICSU_device : public ModbusDevice
{
    Q_OBJECT
    Q_PROPERTY(QString unit READ unit WRITE setunit NOTIFY unitChanged)
    Q_PROPERTY(QString currentState READ currentState WRITE setcurrentState NOTIFY currentStateChanged)
    Q_PROPERTY(qreal userValue READ userValue WRITE setuserValue NOTIFY userValueChanged)
    Q_PROPERTY(qreal deviceValue READ deviceValue WRITE setdeviceValue NOTIFY deviceValueChanged)
    Q_PROPERTY(bool readOnly READ readOnly WRITE setreadOnly NOTIFY readOnlyChanged)
    Q_PROPERTY(bool connected READ connected WRITE setconnected NOTIFY connectedChanged)
    Q_PROPERTY(qreal progress READ progress WRITE setprogress NOTIFY progressChanged)
public:
    explicit ICSU_device(QObject *parent = 0);
    ~ICSU_device();
    void __refreshState();
signals:
    void progress(int progress);
    void finishRequest(qreal value);
private:
    bool check(QByteArray *answer);
    void addCRC(QByteArray *puchMsg);
    bool getCommandData(QByteArray *command);
private:
    bool _setValue();
    bool _getValue();
    bool _find();
    bool _refreshRegime();
    bool _findCurrentDevice(const QByteArray &command);
    QFuture<void> m_future;
private:
    volatile bool m_needToRefresh;
public slots:
    //properties
public:             QString unit() const;
public slots:       void setunit(const QString unit);
signals:            void unitChanged(QString unit);
private:            QString m_unit ;
public:         QString currentState() const;
public slots:   void setcurrentState(const QString currentState);
signals:        void currentStateChanged(QString currentState);
private:        QString m_currentState;
public:             qreal userValue() const;
public slots:       void setuserValue(const qreal userValue);
signals:            void userValueChanged(qreal userValue);
private:            qreal m_userValue;
public:                 qreal deviceValue() const;
public slots:           void setdeviceValue(const qreal deviceValue);
signals:                void deviceValueChanged(qreal deviceValue);
private:                qreal m_deviceValue;
public:         bool readOnly() const;
public slots:   void setreadOnly(const bool readOnly);
signals:        void readOnlyChanged(bool readOnly);
private:        bool m_readOnly;
public:             bool connected() const;
public slots:       void setconnected(const bool connected);
signals:            void connectedChanged(bool connected);
private:            bool m_connected;
public:         qreal progress() const;
public slots:   void setprogress(const qreal progress);
signals:        void progressChanged(qreal progress);
private:        qreal m_progress;
};

#endif // ICSU_DEVICE_H
