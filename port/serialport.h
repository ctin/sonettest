﻿#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QtCore>
#include "macro.h"

extern double   g_defaultStopBits;
extern int      g_defaultDataBits;
extern int      g_defaultFlowIndex;
extern int      g_defaultParityIndex;
extern int      g_defaultRateIndex;
extern QString  g_defaultPort;
extern QString  g_defaultPortICSU;
extern bool     g_autoSearchEnabled;

extern void writeGlobalSettings();
extern void readGlobalSettings();

#ifdef FAKE_SONET
class FakeSerialPort;
typedef FakeSerialPort SerialPortDriver;

#else
class WindowsSerialPort;
typedef WindowsSerialPort SerialPortDriver;
#endif //FAKE_SONET

class SerialPort : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int rateIndex READ rateIndex WRITE setrateIndex NOTIFY rateIndexChanged RESET unsetRateIndex)
    Q_PROPERTY(int dataBits READ dataBits WRITE setdataBits NOTIFY dataBitsChanged RESET unsetDataBits)
    Q_PROPERTY(int parityIndex READ parityIndex WRITE setparityIndex NOTIFY parityIndexChanged RESET unsetParityIndex)
    Q_PROPERTY(double stopBits READ stopBits WRITE setstopBits NOTIFY stopBitsChanged RESET unsetStopBits)
    Q_PROPERTY(int flowIndex READ flowIndex WRITE setflowIndex NOTIFY flowIndexChanged RESET unsetFlowIndex)
    Q_PROPERTY(QString port READ port WRITE setport NOTIFY portChanged RESET unsetPort)
    Q_PROPERTY(bool isOpen READ isOpen WRITE setisOpen NOTIFY isOpenChanged RESET unsetIsOpen)
    Q_PROPERTY(int readTotalTimeoutConstant READ readTotalTimeoutConstant WRITE setreadTotalTimeoutConstant NOTIFY readTotalTimeoutConstantChanged)
    Q_PROPERTY(bool rxEnabled READ rxEnabled WRITE setrxEnabled NOTIFY rxEnabledChanged)
    Q_PROPERTY(bool txEnabled READ txEnabled WRITE settxEnabled NOTIFY txEnabledChanged)
    Q_PROPERTY(QString hardwareError READ hardwareError WRITE sethardwareError NOTIFY hardwareErrorChanged RESET unsetHardwareError)
    Q_PROPERTY(unsigned long realRate READ realRate NOTIFY realRateChanged)
public:
    explicit SerialPort(QObject *parent = 0);
    virtual ~SerialPort();
    bool open();
    void close();
    bool flushRX();
    bool write(const QByteArray &data);
    bool read(QByteArray *ans);
    bool cancel();
private:
    bool setPortSettings();
    SerialPortDriver *m_pSerialPortDriver;
public:
    static QString portNameToSystemLocation(const QString &port);
    static QStringList createParityList(void);
    static QStringList createFlowList(void);
    static QStringList createSpeedListStr(void);
    static QList<long> createSpeedList(void);
    static unsigned long getRealRate(int currentIndex);
protected:
    bool getError(const char* info);
    bool m_scanMode;
private:
    const QList<long> m_speeds;
    const QStringList m_paritys, m_flows;
public:
    void setOtherHandle(SerialPort *pOtherPort);

    void emitAllGUISignals();
//properties
public:
    int rateIndex() const;
    int dataBits() const;
    int parityIndex() const;
    double stopBits() const;
    int flowIndex() const;
    QString port() const;
    bool isOpen() const;
    int readTotalTimeoutConstant() const;
    bool rxEnabled() const;
    bool txEnabled() const;
    QString hardwareError() const;
    unsigned long realRate() const;

public slots:
    void setrateIndex(const int rateIndex);
    void setdataBits(const int dataBits);
    void setparityIndex(const int parityIndex);
    void setstopBits(const double stopBits);
    void setflowIndex(const int flow);
    void setport(const QString port);
    void setisOpen(const bool isOpen);
    void setreadTotalTimeoutConstant(const int readTotalTimeoutConstant);
    void setrxEnabled(const bool rxEnabled);
    void settxEnabled(const bool rxEnabled);
    void sethardwareError(const QString hardwareError);

    void unsetRateIndex();
    void unsetDataBits();
    void unsetParityIndex();
    void unsetStopBits();
    void unsetFlowIndex();
    void unsetPort();
    void unsetIsOpen();
    void unsetHardwareError();
signals:
    void rateIndexChanged(int rateIndex);
    void dataBitsChanged(int dataBits);
    void parityIndexChanged(int parityIndex);
    void stopBitsChanged(double stopBits);
    void flowIndexChanged(int flowIndex);
    void portChanged(QString port);
    void isOpenChanged(bool isOpen);
    void readTotalTimeoutConstantChanged(int readTotalTimeoutConstant);
    void rxEnabledChanged(bool rxEnabled);
    void txEnabledChanged(bool rxEnabled);
    void hardwareErrorChanged(QString hardwareError);
    void realRateChanged(unsigned long realRate);

private:
    long m_rateIndex;
    int m_dataBits;
    double  m_stopBits;
    QString m_port;
    int m_parityIndex;
    int m_flowIndex;
    bool m_isOpen;
    int m_readTotalTimeoutConstant;
    bool m_rxEnabled;
    bool m_txEnabled;
    QString m_hardwareError;

    long m_old_rateIndex;
    int m_old_dataBits;
    double  m_old_stopBits;
    QString m_old_port;
    int m_old_parityIndex;
    int m_old_flowIndex;
    bool m_old_isOpen;
    int m_old_readTotalTimeoutConstant;
    bool m_old_rxEnabled;
    bool m_old_txEnabled;
    QString m_old_hardwareError;
};

struct PortOpener
{
    PortOpener(SerialPort *pSerialPort);
    ~PortOpener();
private:
    SerialPort *m_pSerialPort;
};

#endif // SERIALPORT_H
