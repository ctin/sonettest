﻿#ifndef COMMANDS_H
#define COMMANDS_H
#include <QByteArray>
#include <QVector>

extern const QByteArray addCRC(const char *data, int size);
extern bool checkCRC(const QByteArray *puchMsg);

extern QByteArray MODBUS_testComm(QString *commandName = 0);
extern QByteArray MODBUS_reset(const bool clearReg, QString *commandName = 0);
extern QByteArray MODBUS_get_types(QString *commandName = 0);
extern QByteArray MODBUS_get_deviceIDs(QString *commandName = 0);
extern QByteArray MODBUS_get_channelsCount(QString *commandName = 0);
extern QByteArray MODBUS_get_fullConfig(QString *commandName = 0);

extern QByteArray MODBUS_get_frontPanel(QString *commandName = 0);
extern QByteArray MODBUS_get_termo(QString *commandName = 0);
extern QByteArray MODBUS_direct_read(const uchar module, const uchar addr, const unsigned char bytesCount, QString *commandName = 0);
extern QByteArray MODBUS_direct_write(const uchar module, const uchar addr, const uchar byte,  QString *commandName = 0);
extern QByteArray MODBUS_get_registers(QString *commandName = 0);
extern QByteArray MODBUS_get_version(QString *commandName = 0);
extern QByteArray MODBUS_get_diskret_channels_N(const uchar module, const uchar N, QString *commandName = 0);
extern QByteArray MODBUS_get_powers(QString *commandName = 0);
extern QByteArray MODBUS_get_analog_channels(const char module, QString *commandName = 0);
extern QByteArray MODBUS_get_analog_channels_full(QString *commandName = 0);
extern QByteArray MODBUS_set_diskret_channel_N(const unsigned char module, const unsigned char channel, const bool state, QString *commandName = 0);
extern QByteArray MODBUS_set_analog_channel(const uchar module, const uchar channel, const ushort value, QString *commandName = 0);
extern QByteArray MODBUS_set_analog_channels(const uchar module, const QByteArray &data, QString *commandName = 0);
extern QByteArray MODBUS_set_diskret_channels(const char module, const ushort value, QString *commandName = 0);
//extern int MODBUS_run_autocalibration();
extern QByteArray MODBUS_get_analog_channel(const uchar module, const uchar channel, QString *commandName = 0);
extern QByteArray MODBUS_set_meandr(const bool active, QString *commandName = 0);
extern QByteArray MODBUS_get_meandr(QString *commandName = 0);
extern QByteArray MODBUS_SendRequestFromMaster(QString *commandName = 0);
extern QByteArray MODBUS_set_SPI(const unsigned char module, const uchar start, const ushort data, QString *commandName = 0);
extern QByteArray MODBUS_set_SPI(const unsigned char module, const uchar start, const ushort data1, const ushort data2, QString *commandName = 0);
extern QByteArray MODBUS_get_SPI(const unsigned char module, const uchar start, const uchar count, QString *commandName = 0);
extern QByteArray MODBUS_get_Diag(const uchar module, QString *commandName = 0);
extern QByteArray MODBUS_set_pref_register(const unsigned char module, const uchar byte, QString *commandName = 0);

#define	SPI_ZERO	0x50
#define	SPI_FULL	0x60
#define	SPI_ZERO_WR	0x70
#define	SPI_FULL_WR	0x80

#define DIAG_CODE	0x70 // + code_num + channel_num * code_count;

#define CALIBR_DATE			0x30
#define VERIFY_DATE			0x40
#define CALIBR_DATE_WR		0x90
#define VERIFY_DATE_WR		0xA0

#define CALIBR_FULL_NAME				0xB0
#define VERIFY_FULL_NAME				0xC0
#define CALIBR_FULL_NAME_WR			0xD0
#define VERIFY_FULL_NAME_WR			0xE0

#define FULL_NAME_MAX_LEN           50

#define WRITE_EEPROM_DELAY      10 //осциллограф говорит, что работа с eeprom занимает 3.3 мс. Берем с запасом.

#endif // COMMANDS_H
