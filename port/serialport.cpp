﻿#include "serialport.h"
#include "macro.h"
#include "objects.h"

#ifdef FAKE_SONET
#include "fakeserialport.h"
#else
#include "windowsserialport.h"
#endif

/**********************************************
**глобальные переменные для полностью ручного управления портом
***********************************************/

double g_defaultStopBits = 2;
int g_defaultDataBits = 8;
int g_defaultFlowIndex = 0;
int g_defaultParityIndex = 0;
int g_defaultRateIndex = 7;
QString g_defaultPort = "";
QString g_defaultPortICSU   = "";
bool g_autoSearchEnabled = true; //глобальное управление автопоиском

void writeGlobalSettings()
{
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("GlobalSettings");
    settings.setValue("g_defaultStopBits", g_defaultStopBits);
    settings.setValue("g_defaultDataBits", g_defaultDataBits);
    settings.setValue("g_defaultFlowIndex", g_defaultFlowIndex);
    settings.setValue("g_defaultParityIndex", g_defaultParityIndex);
    settings.setValue("g_defaultRateIndex", g_defaultRateIndex);
    settings.setValue("g_defaultPort", g_defaultPort);
    settings.setValue("g_defaultPortICSU", g_defaultPortICSU);
    settings.setValue("g_autoSearchEnabled", g_autoSearchEnabled);
    settings.endGroup();
}

void readGlobalSettings()
{
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("GlobalSettings");
    g_defaultStopBits = settings.value("g_defaultStopBits", 2).toInt();
    g_defaultDataBits = settings.value("g_defaultDataBits", 8).toInt();
    g_defaultFlowIndex = settings.value("g_defaultFlowIndex", 0).toInt();
    g_defaultParityIndex = settings.value("g_defaultParityIndex", 0).toInt();
    g_defaultRateIndex = settings.value("g_defaultRateIndex", 7).toInt();
    g_defaultPort = settings.value("g_defaultPort", "").toString();
    g_defaultPortICSU   = settings.value("g_defaultPortICSU", "").toString();
    g_autoSearchEnabled = settings.value("g_autoSearchEnabled", true).toBool(); //глобальное управление автопоиском
    settings.endGroup();
}



SerialPort::SerialPort(QObject *parent)
    : QObject(parent), m_speeds(createSpeedList()), m_paritys(createParityList()), m_flows(createFlowList())
{

    m_pSerialPortDriver = new  SerialPortDriver(this);
    unsetDataBits();
    unsetFlowIndex();
    unsetIsOpen();
    unsetParityIndex();
    unsetPort();
    unsetRateIndex();
    unsetStopBits();
    m_readTotalTimeoutConstant = 0;
    m_rxEnabled = false;
    m_txEnabled = false;
    m_scanMode = false;
    m_old_rateIndex                 =   rateIndex();
    m_old_dataBits                  =   dataBits();
    m_old_stopBits                  =   stopBits();
    m_old_port                      =   port();
    m_old_parityIndex               =   parityIndex();
    m_old_flowIndex                 =   flowIndex();
    m_old_isOpen                    =   isOpen();
    m_old_readTotalTimeoutConstant  =   readTotalTimeoutConstant();
    m_old_rxEnabled                 =   rxEnabled();
    m_old_txEnabled                 =   txEnabled();
    m_old_hardwareError             =   hardwareError();
}

SerialPort::~SerialPort()
{
    close();
    delete m_pSerialPortDriver;
}

void SerialPort::emitAllGUISignals()
{
}

void SerialPort::setOtherHandle(SerialPort *pOtherPort)
{
    m_pSerialPortDriver->setOtherHandle(pOtherPort->m_pSerialPortDriver);
}

bool SerialPort::getError(const char *info)
{
    QString pReturnStr(info);
    bool result = m_pSerialPortDriver->getError(&pReturnStr);
    if(result)
        if(!(pReturnStr.contains(errMessage_ERROR_FILE_NOT_FOUND) && m_scanMode && g_autoSearchEnabled)) // ситуация, когда программа перебирает все порты и ошибка "порта нет"
        {
            sethardwareError(pReturnStr);
            close();
        }
    if(result && hardwareError().length())
        qWarning() << hardwareError();
    return result;
}

bool SerialPort::open()
{
    if(port().simplified().isEmpty())
        return false;

    g_pAntiLockTimer->startAntiLockTimer();
    bool result = m_pSerialPortDriver->open(portNameToSystemLocation(port()));
    if(!result)
        result &= !getError("при открытии");
    g_pAntiLockTimer->stopAntiLockTimer();
    if(result)
        result &= setPortSettings();
    if(!result)
    {
        close();
    }
    setisOpen(result);
    return result;
}

bool SerialPort::cancel()
{
    g_pAntiLockTimer->startAntiLockTimer();
    bool result = m_pSerialPortDriver->cancel();
    if(!result)
        result &= !getError("при остановке");
    g_pAntiLockTimer->stopAntiLockTimer();
    return result;
}

bool SerialPort::setPortSettings()
{
    Q_ASSERT(rateIndex() >= 0 && rateIndex() < m_speeds.size());

    g_pAntiLockTimer->startAntiLockTimer();
    bool result = m_pSerialPortDriver->setPortSettings(m_speeds.at(rateIndex()), dataBits(), stopBits(), parityIndex(), flowIndex());
    if(!result)
        result &= !getError("при установке параметров порта");
    if(result)
    {
        result &= m_pSerialPortDriver->setTimeouts(readTotalTimeoutConstant());
        if(!result)
            result &= !getError("при установке таймаутов порта");
    }
    g_pAntiLockTimer->stopAntiLockTimer();
    return result;
}

void SerialPort::close()
{
    g_pAntiLockTimer->startAntiLockTimer();
    bool result = m_pSerialPortDriver->close();
    if(!result)
        result &= !getError("при закрытии");
    g_pAntiLockTimer->stopAntiLockTimer();
    if(result)
        setisOpen(false); //закрытие прошло успешно
}

bool SerialPort::flushRX()
{
    g_pAntiLockTimer->startAntiLockTimer();
    bool result = m_pSerialPortDriver->flushRX();
    if(!result)
        result &= !getError("при очистке порта");
    g_pAntiLockTimer->stopAntiLockTimer();
    return result;
}

bool SerialPort::write(const QByteArray &data)
{
    g_pAntiLockTimer->startAntiLockTimer();
    bool result = m_pSerialPortDriver->write(data);
#ifdef FAKE_SONET
    if(m_scanMode)
        if(m_pSerialPortDriver->m_command.at(0) != 44
            && (unsigned char)m_pSerialPortDriver->m_command.at(0) != 155)
            m_pSerialPortDriver->m_response.clear();
#endif //FAKE_SONET
    if(!result)
        result &= !getError("при записи");
    g_pAntiLockTimer->stopAntiLockTimer();
    settxEnabled(result);
    return result;
}

bool SerialPort::read(QByteArray *ans)
{
    Q_ASSERT(ans);
    g_pAntiLockTimer->startAntiLockTimer();
    bool result = m_pSerialPortDriver->read(ans);
    if(!result)
        result &= !getError("при чтении");
    g_pAntiLockTimer->stopAntiLockTimer();
    return result;
}

QString SerialPort::port() const
{
    return m_port;
}

void SerialPort::setport(const QString port)
{
    if(port != m_port)
    {
        m_port = port;
        emit portChanged(port);
    }
}

PROPERTY_CPP(int, SerialPort, dataBits)
PROPERTY_CPP(int, SerialPort, parityIndex)
PROPERTY_CPP(double, SerialPort, stopBits)
PROPERTY_CPP(int, SerialPort, flowIndex)
PROPERTY_CPP(bool, SerialPort, isOpen)
PROPERTY_CPP(int, SerialPort, readTotalTimeoutConstant)
PROPERTY_CPP(bool, SerialPort, rxEnabled)
PROPERTY_CPP(bool, SerialPort, txEnabled)
PROPERTY_CPP(QString, SerialPort, hardwareError)
PROPERTY_CPP(int, SerialPort, rateIndex)

unsigned long SerialPort::realRate() const
{
    if(rateIndex() < 0)
        return 0;
    return m_speeds.at(rateIndex());
}

void SerialPort::unsetRateIndex()
{
    setrateIndex(7);
}
void SerialPort::unsetDataBits()
{
    setdataBits(8);
}
void SerialPort::unsetParityIndex()
{
    setparityIndex(0);
}
void SerialPort::unsetStopBits()
{
    setstopBits(2);
}
void SerialPort::unsetFlowIndex()
{
    setflowIndex(0);
}
void SerialPort::unsetPort()
{
    setport("");
}

void SerialPort::unsetIsOpen()
{
    setisOpen(false);
}

void SerialPort::unsetHardwareError()
{
    QString hardwareError;
    if(m_hardwareError == hardwareError)
        return;
    m_hardwareError = hardwareError;
    emit hardwareErrorChanged(hardwareError);
}

QStringList SerialPort::createParityList(void)
{
    return QStringList() << "Нет (None)"
                         << "Нечетный (Odd)"
                         << "Четный (Even)"
                         << "Всегда 1 (Mark)"
                         << "Всегда 0 (Space)";
}

QStringList SerialPort::createFlowList(void)
{
    return QStringList() << "Нет (Off)"
            << "Аппаратно (Hardware)"
            << "XOn/XOff";
}

QStringList SerialPort::createSpeedListStr(void)
{
    QStringList list;
    foreach(int speed, createSpeedList())
        list << QString::number(speed);
    return list;
}

QList<long> SerialPort::createSpeedList()
{
    return QList<long>() << 1200
                         << 2400
                         << 4800
                         << 9600
                         << 19200
                         << 28800
                         << 57600
                         << 115200
                         << 230400
                         << 460800;
}

unsigned long SerialPort::getRealRate(int currentIndex)
{
    QList<long> speeds = createSpeedList();
    if(currentIndex < 0 || currentIndex > speeds.size())
        return 0;
    return speeds.at(currentIndex);
}

QString SerialPort::portNameToSystemLocation(const QString &port)
{
    QString ret = port;
    if (!ret.contains("\\\\.\\"))
        ret.prepend("\\\\.\\");
    return ret;
}

PortOpener::PortOpener(SerialPort *pSerialPort)
    : m_pSerialPort(pSerialPort)
{
    Q_ASSERT(m_pSerialPort);
    m_pSerialPort->open();
}

PortOpener::~PortOpener()
{
    m_pSerialPort->close();
}
