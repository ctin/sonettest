#include "windowsserialport.h"

const char* errMessage_ERROR_FILE_NOT_FOUND = "Нет такого порта";


WindowsSerialPort::WindowsSerialPort(QObject *parent) :
    QObject(parent)
{
    m_handle = NULL;

}

bool WindowsSerialPort::getError(QString *pReturnStr)
{
    DWORD err = ::GetLastError();
    /*if (err == ERROR_SUCCESS && m_handle != INVALID_HANDLE_VALUE)
    {
        return false;
    }*/
    *pReturnStr += ": ";

    switch (err) {
    case ERROR_SUCCESS:
        return false;
    case ERROR_FILE_NOT_FOUND:
        *pReturnStr += errMessage_ERROR_FILE_NOT_FOUND;
        break;
    case ERROR_INVALID_HANDLE:
        *pReturnStr += "Неверный дескриптор";
        break;
    case ERROR_ACCESS_DENIED:
        *pReturnStr += "Доступ закрыт или порт занят";
        break;
    case ERROR_INVALID_PARAMETER:
        *pReturnStr += "Порт не принял параметры";
        break;
    case ERROR_NOACCESS:
        *pReturnStr += "Нет доступа";
        break;
    case ERROR_OPERATION_ABORTED:
        *pReturnStr += "Операция была прервана потому, что завершился исполняющий поток или по запросу приложения.";
        break;
    case ERROR_GEN_FAILURE:
        *pReturnStr += "Данный прибор не функционирует";
        break;
    default:
        *pReturnStr += "Неизвестная ошибка № " + QString::number(err, 16);
        break;
    }
    return true;
}

bool WindowsSerialPort::open(QString portName)
{
    char *comname = (char*)portName.utf16();
    m_handle = CreateFile((WCHAR*)comname,GENERIC_READ|GENERIC_WRITE,0,NULL,
                          OPEN_EXISTING,0,0);
    return m_handle != INVALID_HANDLE_VALUE;
}

bool WindowsSerialPort::close()
{
    bool result = true;
    if(m_handle && m_handle != NULL && m_handle != INVALID_HANDLE_VALUE)
    {
        result = CloseHandle(m_handle);
        if(result)
            m_handle = NULL;
    }
    return result;
}

bool WindowsSerialPort::flushRX()
{
    return PurgeComm(m_handle, PURGE_RXCLEAR);
}

bool WindowsSerialPort::write(const QByteArray &data)
{
    unsigned long wlen = 0;
    bool result = WriteFile(m_handle, data.data(), data.size(), &wlen, NULL);
    return result;
}

bool WindowsSerialPort::read(QByteArray *ans)
{
    const int BUFF_SIZE = 256;
    char buffer[BUFF_SIZE];
    unsigned long int rlen = 0;
    bool result = ReadFile(m_handle,(void*)buffer,BUFF_SIZE,&rlen,NULL);
    if(result)
        ans->append(buffer, rlen);
    return result;
}

bool WindowsSerialPort::setPortSettings(long int speed, int dataBits, float stopBits, int parity, int flow)
{
    COMMCONFIG Win_CommConfig;
    DWORD confSize = sizeof(COMMCONFIG);
    Win_CommConfig.dwSize = confSize;
    GetCommConfig(m_handle, &Win_CommConfig, &confSize);
    GetCommState(m_handle, &(Win_CommConfig.dcb));

    /*set up parameters*/
    Win_CommConfig.dcb.fBinary=TRUE;
    Win_CommConfig.dcb.fInX=FALSE;
    Win_CommConfig.dcb.fOutX=FALSE;
    Win_CommConfig.dcb.fAbortOnError=FALSE;
    Win_CommConfig.dcb.fNull=FALSE;
    Win_CommConfig.dcb.XoffChar = FALSE;
    Win_CommConfig.dcb.XonChar = FALSE;
    Win_CommConfig.dcb.EofChar = FALSE;
    Win_CommConfig.dcb.ErrorChar = FALSE;
    Win_CommConfig.dcb.EvtChar = FALSE;
    Win_CommConfig.dcb.fRtsControl = FALSE;
    Win_CommConfig.dcb.fDtrControl = FALSE;
    //fill struct : COMMCONFIG
    Win_CommConfig.dcb.BaudRate = speed;
    Win_CommConfig.dcb.Parity = (BYTE)parity;
    Win_CommConfig.dcb.fParity = (parity == 0) ? FALSE : TRUE;
    Win_CommConfig.dcb.ByteSize = (BYTE)dataBits;
    if(stopBits == 1)
        Win_CommConfig.dcb.StopBits = ONESTOPBIT;
    else if(stopBits == 1.5)
        Win_CommConfig.dcb.StopBits = ONE5STOPBITS;
    else
        Win_CommConfig.dcb.StopBits = TWOSTOPBITS;
    switch(flow) {
    /*no flow control*/
    case 0:

        Win_CommConfig.dcb.fRtsControl = RTS_CONTROL_ENABLE;
            Win_CommConfig.dcb.XonLim = 0;
            Win_CommConfig.dcb.XoffLim = 1023;
        Win_CommConfig.dcb.fOutxCtsFlow=FALSE;
        //Win_CommConfig.dcb.fRtsControl=RTS_CONTROL_DISABLE;
        Win_CommConfig.dcb.fInX=FALSE;
        Win_CommConfig.dcb.fOutX=FALSE;
        break;
        /*hardware flow control*/
        case 1:
            Win_CommConfig.dcb.fOutxCtsFlow=TRUE;
            Win_CommConfig.dcb.fRtsControl=RTS_CONTROL_HANDSHAKE;
            Win_CommConfig.dcb.fInX=FALSE;
            Win_CommConfig.dcb.fOutX=FALSE;
            break;
    /*software (XON/XOFF) flow control*/
    case 2:
        Win_CommConfig.dcb.fOutxCtsFlow=FALSE;
        Win_CommConfig.dcb.fRtsControl=RTS_CONTROL_DISABLE;
        Win_CommConfig.dcb.fInX=TRUE;
        Win_CommConfig.dcb.fOutX=TRUE;
        break;
    }

    return SetCommConfig(m_handle, &Win_CommConfig, sizeof(COMMCONFIG));
}

bool WindowsSerialPort::setTimeouts(int readTotalTimeoutConstant)
{
    SetupComm(m_handle, 128, 128); // set buffer sizes
    COMMTIMEOUTS ct;
    GetCommTimeouts(m_handle,&ct);
    ct.ReadIntervalTimeout = 0xFFFFFFFF;	//
    ct.ReadTotalTimeoutMultiplier = 0; //1
    ct.ReadTotalTimeoutConstant = readTotalTimeoutConstant; //5
    ct.WriteTotalTimeoutMultiplier = 0; //1
    ct.WriteTotalTimeoutConstant = 0; //5
    return SetCommTimeouts(m_handle,&ct);
}

bool WindowsSerialPort::cancel()
{
    return CancelIo(m_handle) &&
        PurgeComm(m_handle, PURGE_TXABORT | PURGE_RXABORT);
}

bool WindowsSerialPort::setOtherHandle(const WindowsSerialPort *pOtherPort)
{
    m_handle = pOtherPort->m_handle;
    return true;
}

//
