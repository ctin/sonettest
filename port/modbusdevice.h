﻿#ifndef DEVICE_H
#define DEVICE_H

#include <QtCore>
#include "serialport.h"
#include "objects.h"

extern bool g_writePackets;
extern int g_additionalWaitTime;
void setAvailablePorts(const QStringList &availablePorts);
QStringList availablePorts();

//класс декорирует стандартный RS-232/485 прибор особенностями протокола MODBUS
class ModbusDevice : public SerialPort
{
    Q_OBJECT
    Q_PROPERTY(int commandNum READ commandNum WRITE setcommandNum NOTIFY commandNumChanged)
    Q_PROPERTY(int deviceNum READ deviceNum WRITE setdeviceNum NOTIFY deviceNumChanged)
    Q_PROPERTY(int waitTime READ waitTime WRITE setwaitTime NOTIFY waitTimeChanged)
    Q_PROPERTY(int lastReadTime READ lastReadTime WRITE setlastReadTime NOTIFY lastReadTimeChanged)
    Q_PROPERTY(QString lastError READ lastError WRITE setlastError NOTIFY lastErrorChanged RESET unsetLastError)
    Q_PROPERTY(QString info READ info WRITE setinfo NOTIFY infoChanged RESET unsetInfo)
    Q_PROPERTY(bool deviceAviable READ deviceAviable WRITE setdeviceAviable NOTIFY deviceAviableChanged RESET unsetDeviceAviable)
    Q_PROPERTY(bool autoSearch READ autoSearch WRITE setautoSearch NOTIFY autoSearchChanged)
    Q_PROPERTY(QString pack READ pack WRITE setpack NOTIFY packChanged)
    Q_PROPERTY(bool running READ running WRITE setrunning NOTIFY runningChanged)
    Q_PROPERTY(int packetComplexity READ packetComplexity WRITE setpacketComplexity NOTIFY packetComplexityChanged)
    Q_PROPERTY(QString commandName READ commandName WRITE setcommandName NOTIFY commandNameChanged RESET unsetcommandName)
public:
    explicit ModbusDevice(QObject *parent = 0);
    ~ModbusDevice();
    bool findCurrentDevice(const QByteArray &command);
    bool sendCommand(QByteArray command, ByteArray *answer = 0);
    void createPack();
    QString *getCommandNamePtr();
    void initPort();
    void toNextPort();
    volatile bool m_collecting;
    QQueue<QString> m_errorList;
signals:
    void errorListChanged();
protected:

    const QMap<char, QString> m_errorCodes;
    bool read(QByteArray *answer);
    virtual bool check(QByteArray *answer);
    virtual void addCRC(QByteArray *puchMsg);
    virtual bool getCommandData(QByteArray *command);
    bool getData(QByteArray *answer);
    QTime m_readTimer;//говорит как долго шло считывание.
protected:
    QByteArray m_request, m_answer; //итоговые запрос и ответ.
    int m_portIndex;//какой порт в списке использовать.
private:
    unsigned char m_lastdeviceNum;
public:
    void emitAllGUISignals();
    //properties
public:
    int commandNum() const;
    int deviceNum() const;
    int waitTime() const;
    int lastReadTime() const;
    QString lastError() const;
    QString info() const;
    bool deviceAviable() const;
    bool autoSearch() const;
    QString pack() const;
    bool running() const;
    int packetComplexity() const;
    QString commandName() const;

public slots:
    void setcommandNum(const int commandNum);
    void setdeviceNum(const int deviceNum);
    void setwaitTime(const int waitTime);
    void setlastReadTime(const int lastReadTime);
    void setlastError(const QString lastError);
    void setinfo(const QString info);
    void setdeviceAviable(const bool deviceAviable);
    void setautoSearch(const bool autoSearch);
    void setpack(const QString pack);
    void unsetLastError();
    void unsetInfo();
    void unsetDeviceAviable();
    void setrunning(const bool running);
    void setpacketComplexity(const int packetComplexity);
    void setcommandName(const QString commandName);
    void unsetcommandName();

signals:
    void commandNumChanged(int commandNum);
    void deviceNumChanged(int deviceNum);
    void waitTimeChanged(int waitTime);
    void lastReadTimeChanged(int lastReadTime);
    void lastErrorChanged(QString);
    void infoChanged(QString);
    void deviceAviableChanged(bool deviceAviable);
    void autoSearchChanged(bool autoSearch);
    void packChanged(QString pack);
    void runningChanged(bool running);
    void packetComplexityChanged(int packetComplexity);
    void commandNameChanged(QString commandName);

private:
    int m_commandNum;
    int m_deviceNum;
    int m_waitTime;
    int m_lastReadTime;
    QString m_lastError;
    QString m_info;
    bool m_deviceAviable;
    bool m_autoSearch;
    QString m_pack;
    volatile bool m_running;
    int m_packetComplexity;
    QString m_commandName;

    int m_old_commandNum;
    int m_old_deviceNum;
    int m_old_waitTime;
    int m_old_lastReadTime;
    QString m_old_lastError;
    QString m_old_info;
    bool m_old_deviceAviable;
    bool m_old_autoSearch;
    QString m_old_pack;
    volatile bool m_old_running;
    int m_old_packetComplexity;
    QString m_old_commandName;
};

#endif // DEVICE_H
