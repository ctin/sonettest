#ifndef FAKESERIALPORT_H
#define FAKESERIALPORT_H

#include <QObject>
#include "objects.h"

extern const char* errMessage_ERROR_FILE_NOT_FOUND;

class FakeSerialPort : public QObject
{
    Q_OBJECT
public:
    explicit FakeSerialPort(QObject *parent = 0);
    bool open(QString portName);
    bool close();
    bool flushRX();
    bool write(const QByteArray &data);
    bool read(QByteArray *ans);
    bool cancel();
    bool getError(QString *pReturnStr);
    bool setPortSettings(long int speed, int dataBits, float stopBits, int parity, int flow);
    bool setTimeouts(int readTotalTimeoutConstant);
    bool setOtherHandle(const FakeSerialPort *pFakeSerialPort);

    QByteArray m_command, m_response;
    QString m_error;
private:
    struct Module {
        int id;
        int chCount;
        int type;
        unsigned short values[16];
    };
    Module m_modules[SLOTS_COUNT];
    unsigned char func_check_size(unsigned char size);
    void parseCommand();
    void func_Error(unsigned char number);
    void Send_data();
    void func_01(void);
    void func_05(void);
    void func_08(void);
    void func_08_0000(void);
    void func_08_0FF0(void);
    void func_08_0FFF(void);

    void func_35(void);
    void func_40(void);
    void func_41(void);
    //void func_42(void);
    void func_44(void);
    //void func_45(void);
    void func_50(void);
    void func_51(void);
    void func_52(void);
    void func_53(void);
    void func_54(void);
signals:
    
public slots:
private:
    int m_bprState;
    qreal m_temperature;
    int m_frontPanelX10;
    int m_frontPanelX1;
    int m_frontPanelSpeed;
    Module m_value_addr;
};


#endif // FAKESERIALPORT_H
