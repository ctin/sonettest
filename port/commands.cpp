﻿#include "commands.h"
#include <QtCore>
//return value - func size;
#define	SPI_ZERO	0x50
#define	SPI_FULL	0x60
#define	SPI_ZERO_WR	0x70
#define	SPI_FULL_WR	0x80

#define DIAG_CODE	0x70      // + code_num + channel_num * code_count;
#define ACTIVE      true


const QByteArray addCRC(const char *, int size);

QByteArray MODBUS_testComm(QString *commandName)
{
    if(commandName)
        *commandName = "проверка связи";
    unsigned char command[] = {0x08, 0x0, 0x0, 0x1, 0x1};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_reset(const bool clearReg, QString *commandName)
{
    if(commandName)
        *commandName = "Перезагрузить";
    unsigned char byte = (clearReg ? 0xff : 0x00);
    unsigned char command[] = {0x08, 0x00, 0x01, byte, 0x00};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_get_types(QString *commandName)
{
    if(commandName)
        *commandName = "Считать типы модулей";
    unsigned char command[] = {0x50};
    return QByteArray((char*)command, 1);
}

QByteArray MODBUS_get_deviceIDs(QString *commandName)
{
    if(commandName)
        *commandName = "считать ID модулей";
    unsigned char command[] = {0x51};
    return QByteArray((char*)command, 1);
}



QByteArray MODBUS_get_channelsCount(QString *commandName)
{
    if(commandName)
        *commandName = "считать количество каналов модулей";
    unsigned char command[] = {0x52};
    return QByteArray((char*)command, 1);
}

QByteArray MODBUS_get_fullConfig(QString *commandName)
{
    if(commandName)
        *commandName = "считать полную конфигурацию";
    unsigned char command[] = {0x53, ACTIVE};
    return QByteArray((char*)command, 2);
}

QByteArray MODBUS_get_frontPanel(QString *commandName)
{
    if(commandName)
        *commandName = "запрос передней панели";
    unsigned char command[] = {0x08, 0x0f, 0xf0, 0x00, 0x00};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_get_termo(QString *commandName)
{
    if(commandName)
        *commandName = "запрос температуры";
    unsigned char command[] = {0x08, 0x0f, 0xf2, 0x00, 0x00};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_direct_read(const uchar module, const uchar addr, const unsigned char byteCount, QString *commandName)
{
    if(commandName)
        *commandName = QString("выдать данные MODBUS напрямую: модуль %1, адрес %2, количество байт %3")
            .arg((int)module + 1).arg((int)addr).arg((int)byteCount);
    unsigned char command[] = {0x08, 0x0f, 0xa1, module, addr, byteCount};
    return QByteArray((char*)command, 6);
}

QByteArray MODBUS_direct_write(const uchar module, const uchar addr, const uchar byte, QString *commandName)
{
    if(commandName)
        *commandName = QString("записать данные MODBUS напрямую: модуль %1, адрес %2, количество байт 1, данные: %3")
            .arg((int)module + 1).arg((int)addr).arg((int)byte);
    unsigned char command[] = {0x08, 0x0f, 0xa2, module, addr, 1, byte};
    return QByteArray((char*)command, 7);
}

QByteArray MODBUS_get_registers(QString *commandName)
{
    if(commandName)
        *commandName = "выдать регистры";
    unsigned char command[] = {0x08, 0x0f, 0x02, 0x00, 0x00};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_get_version(QString *commandName)
{
    if(commandName)
        *commandName = "считать версию";
    unsigned char command[] = {0x08, 0x0f, 0xff, 0x00, 0x00};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_get_diskret_channels_N(const uchar module, const uchar N, QString *commandName)
{
    if(commandName)
        *commandName = QString("получить значения дискретных каналов: модуль %1, каналов %2")
            .arg((int) module + 1).arg((int)N);
    unsigned char command[] = {0x01, module, 0x00, 0x00, N};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_get_powers(QString *commandName)
{
    if(commandName)
        *commandName = QString("узнать выдает ли модуль 24 вольта");
    unsigned char command[] = {0x54};
    return QByteArray((char*)command, 1);
}

QByteArray MODBUS_get_analog_channels(const char module, QString *commandName)
{
    if(commandName)
        *commandName = QString("получить все каналы аналогового модуля %1")
            .arg((int)module + 1);
    unsigned char command[] = {0x41, ACTIVE, (unsigned char)(module << 4)};
    return QByteArray((char*)command, 3);
}

QByteArray MODBUS_get_analog_channels_full(QString *commandName)
{
    if(commandName)
        *commandName = QString("получить все каналы всех аналоговых модулей");
    unsigned char command[] = {0x42, ACTIVE};
    return QByteArray((char*)command, 2);
}

QByteArray MODBUS_set_diskret_channel_N(const unsigned char module, const unsigned char channel, const bool state, QString *commandName)
{
    if(commandName)
        *commandName = QString("установить состояние дискретного канала %1 модуля %2: %3")
            .arg((int)channel + 1).arg((int)module + 1).arg((int)state);
    unsigned char byte = state ? 0xff : 0x00;
    unsigned char command[] = {0x05, module, channel, byte, 0x00};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_set_analog_channel(const uchar module, const uchar channel, const ushort value, QString *commandName)
{
    if(commandName)
        *commandName = QString("установить значение аналогового канала %1 модуля %2: %3")
            .arg((int)channel + 1).arg((int)module + 1).arg((int)value);
    unsigned char command[] = {0x44, ACTIVE, (unsigned char)((module << 4) | channel), (uchar)(value >> 8), (uchar)(value & 0xff)};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_set_analog_channels(const uchar module, const QByteArray &data, QString *commandName)
{
    if(commandName)
        *commandName = QString("установить значения аналоговых каналов модуля %1: ").
            arg((int)module + 1);
        for(int i = 0;  i < data.size();  i++)
            *commandName += QString("%1 ").arg((uchar)data.at(i), 2, 16, QChar('0'));
        unsigned char command[] = {0x45, ACTIVE, (unsigned char)(module << 4), (unsigned char)data.length()};
    return QByteArray((char*)command, 4).append(data);
}

QByteArray MODBUS_set_diskret_channels(const char module, const ushort value, QString *commandName)
{
    if(commandName)
        *commandName = QString("установить значения дискретных каналов модуля %1: 0b%2")
            .arg((int)module + 1).arg((int)value, 0, 2);
    unsigned char command[] = {0x35,  ACTIVE, (unsigned char)(module << 4), (uchar)(value >> 8), (uchar)(value & 0xff)};
    return QByteArray((char*)command, 5);
}

//QByteArray MODBUS_run_autocalibration(uchar node));
QByteArray MODBUS_get_analog_channel(const uchar module, const uchar channel, QString *commandName)
{
    if(commandName)
        *commandName = QString("получить значение канала %1 аналогового модуля %2")
            .arg((int)channel + 1).arg((int)module + 1);
    unsigned char command[] = {0x40, ACTIVE, (unsigned char)((module << 4) | channel)};
    return QByteArray((char*)command, 3);
}

QByteArray MODBUS_set_meandr(const bool active, QString *commandName)
{
    if(commandName)
        *commandName = QString("установить меандр. Активность: %1")
            .arg((int)active);
    const unsigned char byte = active ? 0xff : 0x00;
    unsigned char command[] = {0x08, 0x0f, 0xf3, 0x00, byte};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_get_meandr(QString *commandName)
{
    if(commandName)
        *commandName = QString("считать состояние меандра");
    unsigned char command[] = {0x08, 0x0f, 0xf1, 0x00, 0x00};
    return QByteArray((char*)command, 5);
}

QByteArray MODBUS_SendRequestFromMaster(QString *commandName)
{
    if(commandName)
        *commandName = QString("отправить запросы от мастера.");
    unsigned char command[] = {0x32, 1};
    return QByteArray((char*)command, 2);
}

QByteArray MODBUS_set_SPI(const unsigned char module, const uchar start, ushort data, QString *commandName)
{
    if(commandName)
        *commandName = QString("выставить значение по SPI модулю %1, стартовый адрес 0x%2, данные 0x%3")
            .arg((int)module + 1).arg((int)start, 0, 16).arg((int)data, 0, 16);
    unsigned char command[] = {0x71, module, start, 1, (uchar)(data >> 8), (uchar)(data & 0xff)};
    return QByteArray((char*)command, 6);
}

QByteArray MODBUS_set_SPI(const unsigned char module, const uchar start, ushort data1, ushort data2, QString *commandName)
{
    if(commandName)
        *commandName = QString("выставить значение по SPI модулю %1, стартовый адрес 0x%2, данные 0x%3, 0x%4")
            .arg((int)module + 1).arg((int)start, 0, 16).arg((int)data1, 0, 16).arg((int)data2, 0, 16);
    unsigned char command[] = {0x71, module, start, 2, (uchar)(data1 >> 8), (uchar)(data1 & 0xff), (uchar)(data2 >> 8), (uchar)(data2 & 0xff)};
    return QByteArray((char*)command, 8);
}

QByteArray MODBUS_get_SPI(const unsigned char module, const uchar start, const uchar count, QString *commandName)
{
    if(commandName)
        *commandName = QString("считать значение по SPI модуля %1, стартовый адрес 0x%2, объем данных 0x%3")
            .arg((int)module + 1).arg((int)start, 0, 16).arg((int)count, 0, 16);
    unsigned char command[] = {0x70, module, start, count};
    return QByteArray((char*)command, 4);
}

QByteArray MODBUS_get_Diag(const uchar module, QString *commandName)
{
    if(commandName)
        *commandName = QString("считать значение диагностики. Номер модуля: %1").arg((int)module + 1);
    unsigned char command[] = {0x56, module};
    return QByteArray((char*)command, 2);
}

extern QByteArray MODBUS_set_pref_register(const unsigned char module, const uchar byte, QString *commandName )
{
    if(commandName)
        *commandName = QString("запись регистра управления модуля %1").arg((int)module + 1);
    unsigned char command[] = {0x60, (unsigned char)(module << 4), byte};
    return QByteArray((char*)command, 3);
}

const uchar auchCRCHi[] = {
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
} ;

const uchar auchCRCLo[] = {
0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06,
0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD,
0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A,
0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14, 0xD4,
0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3,
0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29,
0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED,
0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60,
0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67,
0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E,
0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71,
0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,
0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B,
0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B,
0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,
0x43, 0x83, 0x41, 0x81, 0x80, 0x40
} ;

/*void WOlf(void)
{
//функция Wolf - void на луну)))))))
    void* MOON;
}*/

const QByteArray addCRC(const char *data, int size)
{
    QByteArray req(data, size);
    uchar uIndex ; // will index into CRC lookup
    uchar HiByte = 0xFF ; // high CRC byte initialized
    uchar LoByte = 0xFF ; // low CRC byte initialized
    while(size--)    // pass through message buffer
    {
        uIndex = (HiByte) ^ (*data++) ;
        HiByte = (LoByte) ^ (*(auchCRCHi+uIndex)) ;
        LoByte = (*(auchCRCLo+uIndex));
    }
    req.push_back(HiByte);
    req.push_back(LoByte);
    return req;
}

bool checkCRC(const QByteArray *puchMsg)
{
    if(!puchMsg)
        return false;
    uchar len = puchMsg->size() - 2;
    uchar uIndex ; // will index into CRC lookup
    uchar HiByte = 0xFF ; // high CRC byte initialized
    uchar LoByte = 0xFF ; // low CRC byte initialized
    int j = 0;
    while(len--)    // pass through message buffer
    {
        uIndex = (HiByte) ^ (puchMsg->at(j++)) ;
        HiByte = (LoByte) ^ (*(auchCRCHi+uIndex)) ;
        LoByte = (*(auchCRCLo+uIndex));
    }
    return (HiByte == (uchar)puchMsg->at(puchMsg->size() - 2) && LoByte == (uchar)puchMsg->at(puchMsg->size() - 1));
}

