﻿#ifndef AGILENT34410A_H
#define AGILENT34410A_H

#include <QtCore>
#include <QtNetwork>

class Agilent34410A : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString unit READ unit WRITE setunit NOTIFY unitChanged)
    Q_PROPERTY(qreal currentValue READ currentValue WRITE setcurrentValue NOTIFY currentValueChanged)
    Q_PROPERTY(bool connected READ connected WRITE setconnected NOTIFY connectedChanged)
    Q_PROPERTY(int port READ port WRITE setport NOTIFY portChanged)
    Q_PROPERTY(QString ip READ ip WRITE setip NOTIFY ipChanged)
    Q_PROPERTY(QString state READ state WRITE setstate NOTIFY stateChanged)
public:
    explicit Agilent34410A(QObject *parent = 0);
    ~Agilent34410A();
public slots:
    void refreshValue();
    void refreshRegime();
    void connectToDevice();

private:
    bool m_needSetRegime;
    QTcpSocket *m_pTcpSocket;
private slots:
    void onConnected();
    void onDisconnected();
    //properties
public:
    QString unit() const;
public slots:
    void setunit(const QString unit);
signals:
    void unitChanged(QString unit);
private:
    QString m_unit;
public:
    qreal currentValue() const;
public slots:
    void setcurrentValue(const qreal currentValue);
signals:
    void currentValueChanged(qreal currentValue);
private:
    qreal m_currentValue;
public:
    bool connected() const;
public slots:
    void setconnected(const bool connected);
signals:
    void connectedChanged(bool connected);
private:
    bool m_connected;
public:
    int port() const;
public slots:
    void setport(const int port);
signals:
    void portChanged(int port);
private:
    int m_port;
public:
    QString ip() const;
public slots:
    void setip(const QString ip);
signals:
    void ipChanged(QString ip);
private:
    QString m_ip;

public:
    QString state() const;
public slots:
    void setstate(const QString state);
    void setstate(QAbstractSocket::SocketState state);
signals:
    void stateChanged(QString state);
private:
    QString m_state;
};

#endif // AGILENT34410A_H
