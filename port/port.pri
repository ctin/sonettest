INCLUDEPATH  += $$PWD
DEPENDPATH   += $$PWD

#QMAKE_CXXFLAGS_DEBUG += -pg
#QMAKE_LFLAGS_DEBUG += -pg

PUBLIC_HEADERS += \
    $$PWD/serialport.h \
    $$PWD/modbusdevice.h \
    $$PWD/commands.h

SOURCES += \
    $$PWD/serialport.cpp \
    $$PWD/modbusdevice.cpp \
    $$PWD/commands.cpp \
    $$PWD/icsu_device.cpp \
    $$PWD/agilent34410a.cpp \
    port/agilent34401a.cpp
CONFIG(FAKE_SONET) {
    SOURCES += port/fakeserialport.cpp
} else {
    SOURCES += port/windowsserialport.cpp
}

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS \
    $$PWD/macro.h \
    $$PWD/icsu_device.h \
    $$PWD/agilent34410a.h \
    port/agilent34401a.h

CONFIG(FAKE_SONET) {
    HEADERS += port/fakeserialport.h
} else {
    HEADERS += port/windowsserialport.h
}
