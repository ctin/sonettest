﻿#include "modbusdevice.h"
#include <QtCore>
#include "commands.h"
#include "macro.h"

bool g_writePackets = false; //говорит каждому экземпляру, нужно ли записывать пакеты.
int g_additionalWaitTime = 0; //дополнительное время ожидания ответа от порта

QStringList g_availablePorts; //общий список портов в системе. Заполняется со стороны по событиям системы
QMutex      g_availablePortsMutex; // собсно

QStringList availablePorts()
{
    QMutexLocker lock(&g_availablePortsMutex);
    return g_availablePorts;
}

void addAvailablePort(const QString port)
{
    QMutexLocker lock(&g_availablePortsMutex);

    if(g_availablePorts.contains(port))
        return;
    g_availablePorts.append(port);
    qSort(g_availablePorts);
}

void setAvailablePorts(const QStringList &availablePorts)
{
    QMutexLocker lock(&g_availablePortsMutex);
    g_availablePorts = availablePorts;
    qSort(g_availablePorts);
}

QMap<char, QString> makeErrorMap(void)
{
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QMap<char, QString> errors;
    errors[0x1] = codec->toUnicode("Ошибка № 1: Запрос к отсутствующей функции. Контроллер не знает такой функции. ");
    errors[0x2] = codec->toUnicode("Ошибка № 2: Запрос к отсутствующему модулю. В данном контроллере модуль по такому адресу не обнаружен. ");
    errors[0x3] = codec->toUnicode("Ошибка № 3: Количество байт в запросе не соответствует данной функции.");
    errors[0x4] = codec->toUnicode("Ошибка № 4: Ошибка (неисправность) модуля. В модуле вывода, при записи в него значения сразу же читаем его обратно. \n Если значения не совпадают - значит (по видимому) модуль вышел из строя.");
    errors[0x5] = codec->toUnicode("Ошибка № 5: Ошибка при работе с расширенным регистром.");
    errors[0x11] = codec->toUnicode("Ошибка № 11: Запрос к аналоговому модулю, а в наличии дискретный.");
    errors[0x12] = codec->toUnicode("Ошибка № 12: Запрос к дискретному модулю, а в наличии аналоговый.");
    errors[0x13] = codec->toUnicode("Ошибка № 13: Запрос на чтение, а модуль предназначен для записи.");
    errors[0x14] = codec->toUnicode("Ошибка № 14: Запрос на запись, а модуль предназначен для чтения.");
    errors[0x15] = codec->toUnicode("Ошибка № 15: Изменилась конфигурация с момента старта.");
    errors[0x16] = codec->toUnicode("Ошибка № 16: Запрос к каналу, которого нет в данном модуле.");
    errors[0x17] = codec->toUnicode("Ошибка № 17: Ошибка в запросе функции.");
    errors[0x20] = codec->toUnicode("Ошибка № 20: Подфункции с таким номером нет в данном контроллере.");
    errors[0x31] = codec->toUnicode("Ошибка № 31: Ошибка при попытке установить значения, выставляемые при тайм-ауте.");
    errors[0x40] = codec->toUnicode("Ошибка № 40: Попытка работы с дополнительным регистром настроек модуля, а в данном модуле такого регистра нет.");
    errors[0x50] = codec->toUnicode("Ошибка № 50: Сбой в прошивке или в EEPROM.");
    return errors;
}

ModbusDevice::ModbusDevice(QObject *parent) :
    SerialPort(parent), m_errorCodes(makeErrorMap())
{
    setcommandNum(0);
    setdeviceNum(44);
    setwaitTime(1500);
    setlastReadTime(-1);
    unsetLastError();
    unsetInfo();
    setdeviceAviable(false);
    setautoSearch(true);
    setpack("");
    setrunning(true);
    setpacketComplexity(1);
    m_portIndex = 0;
    m_collecting = false;
    connect(this, SIGNAL(deviceNumChanged(int)), this, SLOT(unsetDeviceAviable()), Qt::DirectConnection);
    connect(this, SIGNAL(rateIndexChanged(int)), this, SLOT(unsetDeviceAviable()), Qt::DirectConnection);
    connect(this, SIGNAL(portChanged(QString)), this, SLOT(unsetDeviceAviable()), Qt::DirectConnection);

    m_old_commandNum = commandNum();
    m_old_deviceNum = deviceNum();
    m_old_waitTime = waitTime();
    m_old_lastReadTime = lastReadTime();
    m_old_lastError = lastError();
    m_old_info = info();
    m_old_deviceAviable = deviceAviable();
    m_old_autoSearch = autoSearch();
    m_old_pack = pack();
    m_old_running = running();
    m_old_packetComplexity = packetComplexity();
    m_old_commandName = commandName();
}

ModbusDevice::~ModbusDevice()
{
}

void ModbusDevice::emitAllGUISignals()
{
    /*if(m_old_deviceAviable != deviceAviable())
        emit deviceAviableChanged(deviceAviable());
    m_old_deviceAviable = deviceAviable();*/
    SerialPort::emitAllGUISignals();
}

void ModbusDevice::initPort()
{
    QStringList avPorts = availablePorts();
    if(!port().isEmpty() && !avPorts.contains(port()) && deviceAviable())
        return;
    if(avPorts.isEmpty())
    {
        unsetPort();
        return;
    }
    if(!g_autoSearchEnabled)
    {
        setport(g_defaultPort);
        return;
    }
    if(m_portIndex < 0 || m_portIndex >= avPorts.size())
        m_portIndex = 0;
    setport(avPorts.at(m_portIndex));
}

bool ModbusDevice::getCommandData(QByteArray *command)
{
    if(command->isEmpty())
        return false;
    command->push_front(deviceNum());
    addCRC(command);
    setcommandNum(command->at(1));
    m_lastdeviceNum = deviceNum();
    m_answer.clear();
    m_request = *command;
    return commandNum() != 0 && m_lastdeviceNum != 0;
}

QString *ModbusDevice::getCommandNamePtr()
{
    return &m_commandName;
}

bool ModbusDevice::findCurrentDevice(const QByteArray &commTest)
{
    if(!autoSearch())
    {
        return sendCommand(commTest);
    }

    if(!port().isEmpty())
    {
        if(sendCommand(commTest))
            return true;
    }
    toNextPort();
    return false;
}

void ModbusDevice::toNextPort()
{
    if(!autoSearch())
        return;
    QStringList avPorts = availablePorts();
    if(avPorts.isEmpty())
    {
        unsetPort();
        return;
    }
    if(++m_portIndex >= avPorts.size() || m_portIndex <= 0)
        m_portIndex = 0;
}

void ModbusDevice::createPack()
{
    QString pack;
    //pack += QTime::currentTime().toString("hh:mm:ss.zzz\r\n");
    pack += QString("Контроллер №%1, скорость %2, %3; Отклик: %4мс из %5мс\r\n")
            .arg((int)deviceNum()).arg(realRate()).arg(port()).arg(lastReadTime()).arg(waitTime());
    pack += "запрос: ";
    if(!m_request.isEmpty())
    {
        for(int i = 0;  i < m_request.size();  i++)
            pack += QString("%1 ").arg((uchar)m_request.at(i), 2, 16, QChar('0'));
    }
    else
        pack += "отсутствует";
    pack += ";\r\n";
    pack += "ответ:  ";
    if(!m_answer.isEmpty())
    {
        for(int i = 0;  i < m_answer.size();  i++)
            pack += QString("%1 ").arg((uchar)m_answer.at(i), 2, 16, QChar('0'));
    }
    else
        pack += "отсутствует";
    if(!hardwareError().isEmpty())
        pack += QString("\n\rОшибка COM порта: %1\r\n.").arg(hardwareError());
    /*if(!lastError().isEmpty())
        pack += QString("Ошибка ответа от устройства: %1\r\n.").arg(lastError());*/
    setpack(pack);
}

bool ModbusDevice::getData(QByteArray *answer)
{
    Q_ASSERT(answer);
    if(!read(answer) || answer->size() < 3)
    {
        if(answer->isEmpty())
        {
            if(deviceAviable())
                 setlastError(QString("Нет ответа от устройства."));
        }
        else if(answer->size() < 3 || answer->indexOf(m_lastdeviceNum) < 0)
            setlastError(QString("Ответ меньше 3х байт и не содержет номера устройства."));
        else
            setlastError(QString("CRC ответа не сходится."));
    }
    else if(commandNum() && (uchar)(answer->at(1)) == ((uchar)(commandNum()) ^ 128)) //если read вернул 1, значит ответ больше 3х байт
    {
        QMap<char, QString>::const_iterator i = m_errorCodes.find(answer->at(2));
        if(i != m_errorCodes.constEnd())
            setlastError(i.value());
        else
            setlastError(QString("Неизвестная ошибка: 0x%1.").arg((int)answer->at(2), 0, 16));
    }
    else
        return true;
    //createPack();
    return false;
}


bool ModbusDevice::sendCommand(QByteArray command, ByteArray *answer)
{
    ByteArray tmpAns; // if answer is empty
    if(!isOpen() || !running())
        return false;
    if(!getCommandData(&command))
        return false;
    if(!answer)
        answer = &tmpAns;
    else
        answer->clear();
    bool result = false;
    m_readTimer.start();
    //if(flushRX())
        if(write(command))
            result = getData(answer);
    setrxEnabled(!m_answer.isEmpty());
    setlastReadTime(m_readTimer.elapsed());
    setdeviceAviable(result);
    if(g_writePackets)
    {
        static QMutex filemutex;
        QMutexLocker locker(&filemutex);
        QFile outFile("packets.txt");
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);
        QTextStream ts(&outFile);
        ts << "\r\n" << QDate::currentDate().toString() + " " + pack() << endl;
    }
    return result;
}

bool ModbusDevice::read(QByteArray *answer)
{
    Q_ASSERT(answer);
    int pCompl = packetComplexity() >= 0 && packetComplexity() < 10 ? packetComplexity() + 1 : 1;
    int checkCounter = pCompl;
    do
    {
        m_answer = *answer;
        check(answer) ? checkCounter-- : checkCounter = pCompl;
        if(!checkCounter)
            return true;
        Sleepy::msleep(2);
    }
    while(running() && m_readTimer.elapsed() <= waitTime() && SerialPort::read(answer));
    if(!isOpen())
        setlastError("Обрыв связи");
    if(!running())
        flushRX();
    return false;
}

bool ModbusDevice::check(QByteArray *answer)
{
    Q_ASSERT(answer);
    if(answer->size() < 3)
        return false;
    int ind = answer->indexOf(m_lastdeviceNum);
    if(ind != 0)
        return false;
    return checkCRC(answer);
}

PROPERTY_CPP(int, ModbusDevice, commandNum)

int ModbusDevice::deviceNum() const
{
    return m_deviceNum;
}

void ModbusDevice::setdeviceNum(const int deviceNum)
{
    if(m_deviceNum == deviceNum || deviceNum < 0)
        return;
    m_deviceNum = deviceNum;
    emit deviceNumChanged(deviceNum);
    setrxEnabled(false);
    settxEnabled(false);
}

QString ModbusDevice::lastError() const
{
    return m_lastError;
}

void ModbusDevice::setlastError(const QString lastError)
{
    if(!running() || m_lastError == lastError)
        return;
    m_lastError = lastError;
    m_lastError.append("\nКоманда: ");
    m_lastError.append(commandName());
    emit errorListChanged();
    setlastReadTime(m_readTimer.elapsed());
    createPack();
    m_errorList.enqueue(QString("№%1, ").arg(deviceNum()) + QDateTime::currentDateTime().toString() + QString("\n\r") + m_lastError + QString("\n\r") + pack());
    if(m_errorList.size() > 200)
        m_errorList.dequeue();
    emit lastErrorChanged(m_lastError);
}

bool ModbusDevice::autoSearch() const
{
    return m_autoSearch && g_autoSearchEnabled;
}

void ModbusDevice::setautoSearch(const bool autoSearch) SET_PROPERTY(autoSearch)

PROPERTY_CPP(QString, ModbusDevice, info)
PROPERTY_CPP(bool, ModbusDevice, deviceAviable)
PROPERTY_CPP(QString, ModbusDevice, pack)
PROPERTY_CPP(int, ModbusDevice, packetComplexity)
PROPERTY_CPP(int, ModbusDevice, waitTime)
PROPERTY_CPP(int, ModbusDevice, lastReadTime)
PROPERTY_CPP(QString, ModbusDevice, commandName)

void ModbusDevice::unsetcommandName()
{
    setcommandName("");
}

bool ModbusDevice::running() const
{
    return m_running && this && qApp;
}

void ModbusDevice::setrunning(const bool running)
{
    if(m_running == running)
        return;
    m_running = running;
    emit runningChanged(running);
}

void ModbusDevice::unsetLastError()
{
    QString lastError;
    if(m_lastError == lastError)
        return;
    m_lastError = lastError;
    emit lastErrorChanged(lastError);
}

void ModbusDevice::unsetInfo()
{
    setinfo("");
}

void ModbusDevice::unsetDeviceAviable()
{
     setdeviceAviable(false);
}

const uchar auchCRCHi[] = {
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
} ;

const uchar auchCRCLo[] = {
0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06,
0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD,
0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A,
0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14, 0xD4,
0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3,
0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29,
0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED,
0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60,
0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67,
0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E,
0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71,
0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,
0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B,
0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B,
0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,
0x43, 0x83, 0x41, 0x81, 0x80, 0x40
} ;

void ModbusDevice::addCRC(QByteArray *puchMsg)
{
    uchar len = puchMsg->size();
    uchar uIndex ; // will index into CRC lookup
    uchar HiByte = 0xFF ; // high CRC byte initialized
    uchar LoByte = 0xFF ; // low CRC byte initialized
    int j = 0;
    while(len--)    // pass through message buffer
    {
        uIndex = (HiByte) ^ (puchMsg->at(j++)) ;
        HiByte = (LoByte) ^ (*(auchCRCHi+uIndex)) ;
        LoByte = (*(auchCRCLo+uIndex));
    }
    puchMsg->push_back(HiByte);
    puchMsg->push_back(LoByte);
    return;
}
