#ifndef CORE_H
#define CORE_H

#include <QtCore>
#include "sonetdata.h"
#include "parentwindow.h"
#include "devicelist.h"
#include "mainwindow.h"
#include "dialogcomsettings.h"

class Core : public QObject
{
    Q_OBJECT
public:
    explicit Core(QObject *parent = 0);
    ~Core();
    void begin();
    SonetData       *m_pSonetData;
    MainWindow     *m_pMainWindow;
    QDeclarativeView *m_pCentralView;
    DeviceList *m_pDeviceList;
    static Core *self;
    static Core *instance() { return self; }
    Q_DISABLE_COPY(Core)

signals:
    void lastWindowClosed();
    void dataLoaded();

protected slots:
    void startTest(QString id, int deviceCurrentID = -1, int slot = -1);
    void startCommTest();
    void startMODBUSLine();

    void emitAllGUISignals();
    void showComSettings();
private slots:
    void writePackets();
    void setBaseAdress();
    void setReserveAdress();
    void useBase();
    void useReserve();
    void onCurrentChanged(int currentIndex);
private:
    TestWindows::ParentWindow* createTest(DeviceWithData *pDevice, const QString &id);
    void loadData();

    void connectActions();
};
Q_DECLARE_METATYPE(Core*)


#endif // CORE_H
