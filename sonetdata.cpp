﻿#include "sonetdata.h"
#include "objects.h"

const QString keyMin("min");
const QString keyMax("max");
const QString keyUnit("unit");
const QString keyDChan("dchan");
const QString keyPercentOTK("percentOTK");
const QString keyPercentCLIT("percentCLIT");
const QString keyAdjustment("adjustment");
const QString keyResistor("resistor");
const QString keyMinMetrol("minmetrol");
const QString keyMaxMetrol("maxmetrol");
const QString keyPoints("points");
const QString keyCalibrators("calibrators");
const QString keyData("data");
const QString keyFirmware("firmware");

static const QString global_fileName("sonetData");

class SettingsGrouper
{
public:
    explicit SettingsGrouper(QSettings *pSettings, const QString &groupName)    : m_pSettings(pSettings)
    {
        m_pSettings->beginGroup(groupName);
    }

    ~SettingsGrouper()
    {
        m_pSettings->endGroup();
    }
private:
    QSettings   *m_pSettings;
};

SonetData::SonetData(QObject *parent) :
    QObject(parent)
{
    m_pView = new SonetDataView(qApp->activeWindow());
    connect(m_pView->m_pPushButtonSave, SIGNAL(clicked()), this, SLOT(saveTableData()));
    connect(m_pView->m_pPushButtonSaveToIni, SIGNAL(clicked()), this, SLOT(saveTableDataToIni()));
}

void SonetData::on_comboboxIDActivated(int ID)
{
    m_pView->placeCofig(g_parameterStorage.getParameters(ID));
}

void SonetData::updateView()
{
    for(int ID = 0;  ID < 0xff;  ID++) //size of ID
        if(g_parameterStorage.containsID(ID))
        {
            m_pView->m_pComboBoxID->addItem(QString::number(ID));
            QString text = g_parameterStorage.getKYNI(ID);
            m_pView->m_pComboBoxKYNI->addItem(text.remove("КУНИ."));
            m_pView->m_pComboBoxName->addItem(g_parameterStorage.getName(ID));
        }
    else
        m_pView->m_pComboBoxFreeID->addItem(QString::number(ID));
    //m_pView->blockSignals(false);
    on_comboboxIDActivated(m_pView->m_pComboBoxID->currentText().toInt());
    connect(m_pView, SIGNAL(IDChanged(int)), this, SLOT(on_comboboxIDActivated(int)));
}

void SonetData::updateData()
{
    emit dataLoaded();
    updateView();
}

void SonetData::loadData()
{
    QFile file(global_fileName);
    if(file.exists())
    {
        if(file.open(QIODevice::ReadOnly))
        {
            QDataStream stream(&file);
            ParameterMap tmpMap;
            stream >> tmpMap;
            file.close();
            g_parameterStorage.setParameterMap(tmpMap);
        }
        else
        {
            QErrorMessage   *msg = new QErrorMessage(m_pView);
            msg->showMessage("Файл data.dll занят.");

        }
    }
    else
    {
        loadDataFromIni("analog05.ini");
    }
    updateData();
}

void SonetData::saveTableData()
{
    DeviceParameters p = m_pView->getData();
    g_parameterStorage.setParameters(p.m_ID, p);
    saveData();
}

void SonetData::saveTableDataToIni()
{
    DeviceParameters p = m_pView->getData();
    g_parameterStorage.setParameters(p.m_ID, p);
    saveDataToIni();
}

void SonetData::saveData()
{
    QFile file(global_fileName);
    if(file.open(QIODevice::WriteOnly))
    {
        QDataStream stream(&file);
        stream << g_parameterStorage.getParameterMap();
        file.close();
    }
}


void SonetData::loadDataFromIni()
{
    QString addr = QFileDialog::getOpenFileName(m_pView, tr("открыть файл"), "", tr("*.ini"));
    if(!addr.isEmpty())
        loadDataFromIni(addr);
}

void SonetData::loadDataFromIni(const QString &fileName)
{
    QSettings settings(fileName, QSettings::IniFormat, this);
    settings.setIniCodec("CP1251");
    QStringList keys = settings.allKeys();
    if(keys.isEmpty())
        return;
    for(uchar ID = 0;  ID < 0xff;  ID++)
    {
        SettingsGrouper grouper(&settings, QString("%1").arg(ID));
        if(settings.allKeys().isEmpty())
            continue;
        DeviceParameters parameters;
        parameters.m_ID = ID;
        parameters.m_min = settings.value(keyMin, DeviceParameters::errorValue()).toDouble();
        parameters.m_max = settings.value(keyMax, DeviceParameters::errorValue()).toDouble();
        parameters.m_unit = settings.value(keyUnit).toString();
        parameters.m_dchan = settings.value(keyDChan, 0).toDouble();
        parameters.m_percentOTK = settings.value(keyPercentOTK, DeviceParameters::errorValue()).toDouble();
        parameters.m_percentCLIT    = settings.value(keyPercentCLIT, DeviceParameters::errorValue()).toDouble();
        parameters.m_adjustment = settings.value(keyAdjustment, DeviceParameters::errorValue()).toDouble();
        parameters.m_resistor = settings.value(keyResistor, DeviceParameters::errorValue()).toDouble();
        parameters.m_minMetrol = settings.value(keyMinMetrol, DeviceParameters::errorValue()).toDouble();
        parameters.m_maxMetrol = settings.value(keyMaxMetrol, DeviceParameters::errorValue()).toDouble();
        parameters.setPoints(settings.value(keyPoints).toString());
        parameters.setCalibrators(settings.value(keyCalibrators).toString());
        parameters.setStringData(settings.value(keyData).toString());
        parameters.m_firmware = settings.value(keyFirmware).toString();

        g_parameterStorage.setParameters(ID, parameters);
    }

    updateData();
}

void SonetData::saveDataToIni() const
{
    QString addr = QFileDialog::getSaveFileName(m_pView, tr("сохранить как"), "analog", tr("*.ini"));
    if(!addr.isEmpty())
        saveDataToIni(addr);
}

void SonetData::saveDataToIni(const QString &fileName) const
{
    QSettings settings(fileName, QSettings::IniFormat);
    settings.setIniCodec("CP1251");
    QMap <uchar, DeviceParameters >::const_iterator iter;
    for(iter = g_parameterStorage.getParameterMap().constBegin();  iter != g_parameterStorage.getParameterMap().constEnd();  ++iter)
    {
        DeviceParameters parameters = iter.value();
        SettingsGrouper grouper(&settings, QString("%1").arg((int)parameters.m_ID));
        if(DeviceParameters::isDataValid(parameters.m_min))
            settings.setValue(keyMin, parameters.m_min);
        if(DeviceParameters::isDataValid(parameters.m_max))
            settings.setValue(keyMax, parameters.m_max);
        if(DeviceParameters::isDataValid(parameters.m_minMetrol))
            settings.setValue(keyMinMetrol, parameters.m_minMetrol);
        if(DeviceParameters::isDataValid(parameters.m_maxMetrol))
            settings.setValue(keyMaxMetrol, parameters.m_maxMetrol);
        if(DeviceParameters::isDataValid(parameters.m_unit))
            settings.setValue(keyUnit, parameters.m_unit);
        if(DeviceParameters::isDataValid(parameters.getPoints()))
            settings.setValue(keyPoints, parameters.getPoints());
        if(DeviceParameters::isDataValid(parameters.m_resistor))
            settings.setValue(keyResistor, parameters.m_resistor);
        if(DeviceParameters::isDataValid(parameters.m_percentOTK))
            settings.setValue(keyPercentOTK, parameters.m_percentOTK);
        if(DeviceParameters::isDataValid(parameters.m_adjustment))
            settings.setValue(keyAdjustment, parameters.m_adjustment);
        if(DeviceParameters::isDataValid(parameters.m_percentCLIT))
            settings.setValue(keyPercentCLIT, parameters.m_percentCLIT);
        if(DeviceParameters::isDataValid(parameters.m_dchan))
            settings.setValue(keyDChan, parameters.m_dchan);
        if(DeviceParameters::isDataValid(parameters.getCalibratorsString()))
            settings.setValue(keyCalibrators, parameters.getCalibratorsString());
        if(DeviceParameters::isDataValid(parameters.getStringData()))
            settings.setValue(keyData, parameters.getStringData());
        if(DeviceParameters::isDataValid(parameters.m_firmware))
            settings.setValue(keyFirmware, parameters.m_firmware);
    }
}
