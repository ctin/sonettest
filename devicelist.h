 #ifndef DEVICELIST_H
#define DEVICELIST_H

#include <QtCore>
#include "devicewithdata.h"
#include "searchdevice.h"
#include "icsu_device.h"

class DeviceList : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int length READ length NOTIFY lengthChanged)
    Q_PROPERTY(int currentIndex READ currentIndex WRITE setcurrentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(int nextCollectedIndex READ nextCollectedIndex WRITE setnextCollectedIndex NOTIFY nextCollectedIndexChanged)
    Q_PROPERTY(int targetDeviceIndex READ targetDeviceIndex WRITE settargetDeviceIndex NOTIFY targetDeviceIndexChanged)
    enum DeviceRoles {
        DeviceElementRole = Qt::UserRole + 1
    };
public:
    enum {
        Neither = -2,
        All = -1
    };
    explicit DeviceList(QObject *parent = 0);
    ~DeviceList();
    QHash<int, QByteArray> roleNames() const;
    void stop();
    void start();
    Q_INVOKABLE void removeCurrent(int index);
    Q_INVOKABLE bool addNew();
    Q_INVOKABLE DeviceWithData *getDevice(int index) const;
    DeviceWithData *getDeviceByNum(int deviceNum) const;

    QVariant data(const QModelIndex &index,  int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

    bool isDublicate(DeviceWithData *pDevice);
    bool isDublicate(const int deviceNum, const QString port);
    volatile bool m_icsu_enabled;
    QList <DeviceWithData*> *getDeviceList();

    void emitAllGUISignals();
public slots:
    void append(bool deviceAviable, int rateIndex, int deviceNum, int autoSearch, QString port);
    void clear();
    void clearToDelete();
    void saveList();
    bool loadList();

    void setStopBitsForAll(double);
    void setDataBitsForAll(int);
    void setFlowIndexForAll(int);
    void setParityIndexForAll(int);
    void setRateIndexForAll(int);
    void setPortForAll(int);
public:
    Q_INVOKABLE SearchDevice* getSearchDevice() const;
    Q_INVOKABLE ICSU_device* getICSUDevice() const;
    Q_INVOKABLE SerialPort * getSurveyPort() const;
    Q_INVOKABLE DeviceWithData *getcurrentCollectedDevice() const;
    volatile bool m_running;

private:
    QHash<int, QByteArray> m_roleNames;
    SearchDevice *m_pSearchDevice;
    ICSU_device *m_pICSU_2000;
    QList <DeviceWithData*> m_deviceList;
    SerialPort  *m_pSurveyPort;
    QMutex *m_pDeviceListMutex;

    void __start();
    void __collectDevice(int pos);
    QFuture<void> m_future;
    DeviceWithData *m_pCurrentCollectedDevice;
    void createAndAdd(bool deviceAviable, int pos, int rateIndex, int deviceNum, int autoSearch, QString port);

private slots:
    void onLengthChanged(int length);
    //properties
public:
    int currentIndex() const;
    int targetDeviceIndex() const;
    int length() const;
    int nextCollectedIndex() const;

public slots:
    void setcurrentIndex(const int currentIndex);
    void settargetDeviceIndex(int arg);
    void setnextCollectedIndex(int arg);

signals:
    void currentIndexChanged(int currentIndex);
    void targetDeviceIndexChanged(int arg);
    void lengthChanged(int length);
    void nextCollectedIndexChanged(int arg);

private:
    int m_currentIndex;
    int m_targetDeviceIndex;
    int m_nextCollectedIndex;
};
Q_DECLARE_METATYPE(DeviceList*)


#endif // DEVICELIST_H
