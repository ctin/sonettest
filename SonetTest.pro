#-------------------------------------------------
#
# Project created by QtCreator 2012-07-19T16:12:22
#
#-------------------------------------------------
INCLUDEPATH  += $$PWD
DEPENDPATH   += $$PWD
#CONFIG += FAKE_SONET

CONFIG(FAKE_SONET) {
    DEFINES += FAKE_SONET=1
}

#QMAKE_CXXFLAGS_DEBUG += -pgb
#QMAKE_LFLAGS_DEBUG += -pg
QT       += core gui network declarative printsupport
win32::RC_FILE += content/myicon1.rc
win32::OTHER_FILES += content/myicon1.rc

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG(FAKE_SONET) {
VERSION = 01.02_fake
}
else{
VERSION = 01.02
}

DEFINES += VERSION=\\\"$$VERSION\\\"
TARGET = $$join(VERSION,,SonetTest_)


TEMPLATE = app

QTPLUGIN += windowsprintersupport
LIBS += -L"C:\\Qt\\5.1.0_static\\qtbase\\plugins\\printsupport"
include(survey/survey.pri)
include(sonetdevice/sonetdevice.pri)
include(desctopComponents/desctopComponents.pri)
include(infoDialogs/infodialogs.pri)
include(port/port.pri)

SOURCES += main.cpp\
    core.cpp \
    sonetdataview.cpp \
    sonetdata.cpp \
    devicelist.cpp \
    mainwindow.cpp \
    peripheral.cpp

HEADERS  += \
    core.h \
    sonetdataview.h \
    sonetdata.h \
    devicelist.h \
    mainwindow.h \
    peripheral.h

FORMS    += \
    mainwindow.ui \

OTHER_FILES += \
    qml/ValueInput.qml \
    qml/Switch.qml \
    qml/ProcessorModule.qml \
    qml/Module.qml \
    qml/main.qml \
    qml/LedLabel.qml \
    qml/Led.qml \
    qml/CustomDial.qml \
    qml/ControllerList.qml \
    qml/Controller.qml \
    qml/AOitem.qml \
    qml/AIitem.qml \
    qml/verify/VerifyItem.qml \
    qml/verify/ModuleForVerify.qml \
    qml/verify/ICSU2000.qml \
    qml/verify/EmptyCalibrator.qml \
    qml/verify/AOitemVerify.qml \
    qml/verify/AIitemVerify.qml \
    qml/verify/Agilent34410A.qml \
    qml/SearchDeviceWidget.qml \
    qml/ControllerForSurvey.qml \
    qml/DiagnosticModule.qml \
    qml/DiagnosticModuleGrid.qml \
    qml/DiagnosticModuleForSurvey.qml \
    qml/verify/VerifyItemText.qml \
    qml/verify/Agilent34401A.qml \
    .gitignore \


RESOURCES += \
    $$PWD/sonetData/sonetData.qrc \
    $$PWD/content/content.qrc \
    $$PWD/qml/qml.qrc
