﻿#include <QtCore>
#include <QtDeclarative>
#include "core.h"
#include "moduleqmladapter.h"
#include "icsu_device.h"
#include "agilent34410a.h"
#include "agilent34401a.h"
#include "devicelist.h"
#include "styleplugin.h"
#include "peripheral.h"
#include "modbusdevice.h"
#include "searchdevice.h"
#include <QtWidgets>
#include <QtGui>

void installTranslator()
{
    QTranslator *qt_translator = new QTranslator();
    if(!qt_translator->load(":/content/qt_ru.qm"))
    {
        qWarning() << "unable to load translator!";
        delete qt_translator;
        return;
    }

    qApp->installTranslator(qt_translator);
}

void setTextCodec()
{
    QTextCodec *pCyrillicCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(pCyrillicCodec);
}

int main(int argc, char *argv[])
{
    setTextCodec();

    Application a(argc, argv);
    a.setOrganizationDomain("www.ezan.ac.ru");
    a.setOrganizationName("EZAN");
    a.setApplicationName("SonetTest");
    a.setApplicationVersion(VERSION);
    qRegisterMetaType<QVector<int> >("VectorInt");
    qRegisterMetaType<DeviceParameters>("DeviceParameters");
    qRegisterMetaTypeStreamOperators<DeviceParameters>("DeviceParameters");
    qRegisterMetaType<SonetConfig>("SonetConfig");
    qRegisterMetaType<ByteArray>("ByteArray");
    qRegisterMetaType<QList<ByteArray> >("QList<ByteArray>");
    qRegisterMetaTypeStreamOperators<SonetConfig>("SonetConfig");
    QFont font = a.font();
    font.setPointSize(12);
    a.setFont(font);
    if((DWORD)(LOBYTE(LOWORD(GetVersion()))) > 5)
        a.setFont(QFont("Calibri"));
    qApp->setStyle(QStyleFactory::create("fusion"));

    qmlRegisterType<ModuleQMLAdapter>("CustomComponents", 1, 0, "ModuleQMLAdapter");
    qmlRegisterType<DeviceWithData>("CustomComponents", 1, 0, "DeviceWithData");
    qmlRegisterType<SonetDevice>("CustomComponents", 1, 0, "SonetDevice");
    qmlRegisterType<DeviceList>("CustomComponents", 1, 0, "DeviceList");
    qmlRegisterType<ICSU_device>("CustomComponents", 1, 0, "ICSUdevice");
    qmlRegisterType<Agilent34410A>("CustomComponents", 1, 0, "Agilent34410A");
    qmlRegisterType<Agilent34401A>("CustomComponents", 1, 0, "Agilent34401A");
    qmlRegisterType<ModuleDataModel>("CustomComponents", 1, 0, "ModuleDataModel");
    qmlRegisterType<SearchDevice>("CustomComponents", 1, 0, "SearchDevice");
    qmlRegisterType<Core>("CustomComponents", 1, 0, "Core");

    installTranslator();

    StylePlugin::registerCurrentTypes();

    //qInstallMsgHandler(customMessageHandler);
    setAvailablePorts(getAllPorts());
    bool started = 1;//startProcess();

    Core *pCore = 0;
    if(started)
    {
        pCore = new Core();
        pCore->begin();
    }
    else
    {
        QMessageBox *box = new QMessageBox;
        box->setText("Процесс уже запущен!");
        box->setInformativeText("Программа уже открыта и работает.");
        box->setDetailedText("Если активное окно программы нигде не отображается, подождите несколько минут и если программа не закрылась - закройте программу через диспетчер устройств или перезагрузите компьютер.");
        box->show();

    }
    a.setQuitOnLastWindowClosed(true);

    a.exec();
    if(started)
    {
        DeviceList *pDevList = pCore->m_pDeviceList;
        pDevList->getICSUDevice()->setrunning(false);
        pDevList->getSearchDevice()->setrunning(false);
        pDevList->m_running = false;
        if(pDevList->getcurrentCollectedDevice())
            pDevList->getcurrentCollectedDevice()->setrunning(false);
        int counter = 60 * 10; //10 secs
        while(--counter && (pDevList->getICSUDevice()->m_collecting
              || pDevList->getSearchDevice()->m_collecting
              || pDevList->getcurrentCollectedDevice()))
            Sleepy::msleep(100);
        if(!counter)
        {
            g_pAntiLockTimer->startAntiLockTimer(0);
            g_pAntiLockTimer->onTimeOut();
        }
        else
        {
            pCore->m_pDeviceList->clearToDelete();
            pCore->m_pDeviceList->stop();
        }
    }
    return 0;
}
