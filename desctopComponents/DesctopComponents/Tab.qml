import QtQuick 1.1
import DesctopComponents 0.1

Item {
    id:tab
    anchors.fill: parent
    property string title
    property int contentMargin
}
