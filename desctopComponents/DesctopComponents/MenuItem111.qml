import QtQuick 1.1
import DesctopComponents 0.1

Item {
    property string text
    property string iconName
    signal hovered
    signal selected
}
