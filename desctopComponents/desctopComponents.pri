INCLUDEPATH  += $$PWD
DEPENDPATH   += $$PWD

#QMAKE_CXXFLAGS_DEBUG += -pg
#QMAKE_LFLAGS_DEBUG += -pg

HEADERS += $$PWD/qtmenu.h \
           $$PWD/qtmenubar.h \
           $$PWD/qrangemodel_p.h \
           $$PWD/qrangemodel.h \
           $$PWD/styleplugin.h \
           $$PWD/qdeclarativefolderlistmodel.h \
           $$PWD/qstyleitem.h \
           $$PWD/qwheelarea.h \
           $$PWD/qtmenuitem.h \
           $$PWD/qwindowitem.h \
           $$PWD/qdesktopitem.h \
           $$PWD/qtoplevelwindow.h \
           $$PWD/qcursorarea.h \
           $$PWD/qtooltiparea.h \
           $$PWD/qtsplitterbase.h \
           $$PWD/qdeclarativelayout.h \
           $$PWD/qdeclarativelinearlayout.h \
           $$PWD/qdeclarativelayoutengine_p.h \
           $$PWD/settings.h

SOURCES += $$PWD/qtmenu.cpp \
           $$PWD/qtmenubar.cpp \
           $$PWD/qrangemodel.cpp \
           $$PWD/styleplugin.cpp \
           $$PWD/qdeclarativefolderlistmodel.cpp \
           $$PWD/qstyleitem.cpp \
           $$PWD/qwheelarea.cpp \
           $$PWD/qtmenuitem.cpp \
           $$PWD/qwindowitem.cpp \
           $$PWD/qdesktopitem.cpp \
           $$PWD/qtoplevelwindow.cpp \
           $$PWD/qcursorarea.cpp \
           $$PWD/qtooltiparea.cpp \
           $$PWD/qtsplitterbase.cpp \
           $$PWD/qdeclarativelayout.cpp \
           $$PWD/qdeclarativelinearlayout.cpp \
           $$PWD/qdeclarativelayoutengine.cpp \
           $$PWD/settings.cpp

OTHER_FILES += \
    $$PWD/DesctopComponents/ToolButton.qml \
    $$PWD/DesctopComponents/ToolBar.qml \
    $$PWD/DesctopComponents/TextField.qml \
    $$PWD/DesctopComponents/TextArea.qml \
    $$PWD/DesctopComponents/TableView.qml \
    $$PWD/DesctopComponents/TableColumn.qml \
    $$PWD/DesctopComponents/TabFrame.qml \
    $$PWD/DesctopComponents/TabBar.qml \
    $$PWD/DesctopComponents/Tab.qml \
    $$PWD/DesctopComponents/StatusBar.qml \
    $$PWD/DesctopComponents/SplitterRow.qml \
    $$PWD/DesctopComponents/SplitterColumn.qml \
    $$PWD/DesctopComponents/SpinBox.qml \
    $$PWD/DesctopComponents/Slider.qml \
    $$PWD/DesctopComponents/ScrollBar.qml \
    $$PWD/DesctopComponents/ScrollArea.qml \
    $$PWD/DesctopComponents/RadioButton.qml \
    $$PWD/DesctopComponents/ProgressBar.qml \
    $$PWD/DesctopComponents/MenuItem111.qml \
    $$PWD/DesctopComponents/Label.qml \
    $$PWD/DesctopComponents/GroupBox.qml \
    $$PWD/DesctopComponents/Frame.qml \
    $$PWD/DesctopComponents/Dialog.qml \
    $$PWD/DesctopComponents/Dial.qml \
    $$PWD/DesctopComponents/ContextMenu.qml \
    $$PWD/DesctopComponents/ComboBox.qml \
    $$PWD/DesctopComponents/CheckBox.qml \
    $$PWD/DesctopComponents/ButtonRow.qml \
    $$PWD/DesctopComponents/ButtonColumn.qml \
    $$PWD/DesctopComponents/Button.qml \
    $$PWD/DesctopComponents/ApplicationWindow.qml \
    $$PWD/DesctopComponents/custom/ButtonGroup.js \
    $$PWD/DesctopComponents/custom/TextField.qml \
    $$PWD/DesctopComponents/custom/Splitter.qml \
    $$PWD/DesctopComponents/custom/SpinBox.qml \
    $$PWD/DesctopComponents/custom/Slider.qml \
    $$PWD/DesctopComponents/custom/ProgressBar.qml \
    $$PWD/DesctopComponents/custom/GroupBox.qml \
    $$PWD/DesctopComponents/custom/CheckBox.qml \
    $$PWD/DesctopComponents/custom/ButtonRow.qml \
    $$PWD/DesctopComponents/custom/ButtonColumn.qml \
    $$PWD/DesctopComponents/custom/Button.qml \
    $$PWD/DesctopComponents/custom/BasicButton.qml \
    $$PWD/DesctopComponents/custom/behaviors/ModalPopupBehavior.qml \
    $$PWD/DesctopComponents/custom/behaviors/ButtonBehavior.qml \
    $$PWD/DesctopComponents/images/folder_new.png \
    $$PWD/DesctopComponents/private/ScrollAreaHelper.qml

RESOURCES += \
    $$PWD/desctopComponents.qrc
