﻿#include "sonetdataview.h"

SonetDataView::SonetDataView(QWidget *parent)
    : QWidget(parent)
{
    m_pGridLayout = new QGridLayout(this);
    setLayout(m_pGridLayout);
    m_pComboBoxID = new QComboBox(this);
    m_pComboBoxFreeID = new QComboBox(this);
    m_pComboBoxFreeID->setFixedSize(150, 30);
    m_rows  << "ID"
            << "Минимум шкалы"
            << "Максимум шкалы"
            << "Мин метрол"
            << "Макс метрол"
            << "Размерность"
            << "Точки калибровки"
            << "Резистор"
            << "Погрешность для ТУ"
            << "Погрешность для ЦЛИТ"
            << "Поправка для ТУ"
            << "Каналов диагностики"
            << "Наименование"
            << "КУНИ"
            << "Описание"
            << "Калибраторы";

    m_pComboBoxName = new QComboBox(this);
    m_pComboBoxKYNI = new QComboBox(this);
    //c->setCompletionPrefix("\\D+");
    //m_pComboBoxKYNI->setCompleter(c);
    QGroupBox   *pSearchBox = new QGroupBox("Поиск данных", this);
    QGridLayout *pSearchLayout = new QGridLayout(pSearchBox);
    pSearchBox->setLayout(pSearchLayout);

    pSearchLayout->addWidget(new QLabel(tr("номер ID"), this), 0, 0);
    pSearchLayout->addWidget(m_pComboBoxID, 1, 0);
    pSearchLayout->addWidget(new QLabel(tr("Наименование"), this), 0, 1);
    pSearchLayout->addWidget(m_pComboBoxName, 1, 1);
    pSearchLayout->addWidget(new QLabel(tr("Номер КУНИ"), this), 0, 2);
    pSearchLayout->addWidget(m_pComboBoxKYNI, 1, 2, 1, 2);

    m_pGridLayout->addWidget(pSearchBox, 2, 0, 2, 4);

    m_pTableContent = new QTableWidget(m_rows.size(), 1, this);
    m_pGridLayout->addWidget(m_pTableContent, 4, 0, 1, 4);
    m_pPushButtonSave = new QPushButton(tr("Сохранить"), this);
    m_pPushButtonSaveToIni = new QPushButton(tr("сохранить в INI файл"), this);
    QPushButton *pAddButton = new QPushButton(QIcon(":/content/+.ico"), "добавить новый", this);
    QWidget *pAddID = new QWidget(this);
    pAddID->setLayout(new QVBoxLayout());
    pAddID->layout()->addWidget(new QLabel(tr("Доступные ID:"), pAddID));
    pAddID->layout()->addWidget(m_pComboBoxFreeID);
    connect(pAddButton, SIGNAL(clicked()), pAddID, SLOT(show()));
    m_pGridLayout->addWidget(pAddButton, 5, 0);

    m_pGridLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Expanding), 3, 0, 1, 2);
    m_pGridLayout->addWidget(m_pPushButtonSave, 5, 2);
    m_pGridLayout->addWidget(m_pPushButtonSaveToIni, 5, 3);
    connect(m_pComboBoxFreeID, SIGNAL(activated(QString)), this, SLOT(addNewParam(QString)));
    connect(m_pComboBoxFreeID, SIGNAL(activated(QString)), pAddID, SLOT(hide()));

    connect(m_pComboBoxKYNI, SIGNAL(activated(int)), m_pComboBoxID, SLOT(setCurrentIndex(int)));
    connect(m_pComboBoxKYNI, SIGNAL(activated(int)), m_pComboBoxName, SLOT(setCurrentIndex(int)));
    connect(m_pComboBoxName, SIGNAL(activated(int)), m_pComboBoxID, SLOT(setCurrentIndex(int)));
    connect(m_pComboBoxName, SIGNAL(activated(int)), m_pComboBoxKYNI, SLOT(setCurrentIndex(int)));
    connect(m_pComboBoxID, SIGNAL(activated(int)), m_pComboBoxName, SLOT(setCurrentIndex(int)));
    connect(m_pComboBoxID, SIGNAL(activated(int)), m_pComboBoxKYNI, SLOT(setCurrentIndex(int)));
    connect(m_pComboBoxID, SIGNAL(activated(QString)), this, SLOT(changeID(QString)));
    createTable();
    resize(800, 600);
}

void SonetDataView::addNewParam(QString text)
{
    for(int i = 0;  i < m_rows.size();  i++)
        m_pTableContent->item(i, 0)->setText("");
    m_pTableContent->item(0, 0)->setText(text);
}

void SonetDataView::createTable()
{
    m_pTableContent->setVerticalHeaderLabels(m_rows);
    for(int i = 0;  i < m_rows.size();  i++)
        m_pTableContent->setItem(i, 0, new QTableWidgetItem());
    m_pTableContent->item(0, 0)->setFlags(m_pTableContent->item(0, 0)->flags() & (~Qt::ItemIsEditable));
    m_pTableContent->horizontalHeader()->setVisible(false);
    m_pTableContent->verticalHeader()->setDefaultSectionSize(  m_pTableContent->height() / m_pTableContent->rowCount());
    m_pTableContent->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_pTableContent->horizontalHeader()->setStretchLastSection(true);
    m_pTableContent->verticalHeader()->setStretchLastSection(true);
}

QString SonetDataView::fromDataToView(QVariant data)
{
    if(data.canConvert(QVariant::String))
        if(data.toString().isEmpty() || data.toDouble() == DeviceParameters::errorValue())
            return "Na";
    return data.toString();
}

void SonetDataView::changeID(QString str)
{
    emit IDChanged(str.toInt());
}

void SonetDataView::placeCofig(const DeviceParameters &p)
{
    int row = 0;
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_ID));
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_min));
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_max));
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_minMetrol));
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_maxMetrol));
    m_pTableContent->item(row++, 0)->setText(p.m_unit);
    m_pTableContent->item(row++, 0)->setText(p.getPoints());
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_resistor));
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_percentOTK));
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_percentCLIT));
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_adjustment));
    m_pTableContent->item(row++, 0)->setText(fromDataToView(p.m_dchan));
    m_pTableContent->item(row++, 0)->setText(p.m_name);
    m_pTableContent->item(row++, 0)->setText(p.m_KYNI);
    m_pTableContent->item(row++, 0)->setText(p.m_comments);
    m_pTableContent->item(row++, 0)->setText(p.getCalibratorsString());
}

DeviceParameters SonetDataView::getData() const
{
    int row = 0;
    DeviceParameters param;
    param.m_ID      =   (uchar)m_pTableContent->item(row++,0)->text().toInt();
    param.m_min     =   m_pTableContent->item(row++,0)->text().toDouble();
    param.m_max     =   m_pTableContent->item(row++,0)->text().toDouble();
    param.m_minMetrol   =   m_pTableContent->item(row++,0)->text().toDouble();
    param.m_maxMetrol   =   m_pTableContent->item(row++,0)->text().toDouble();
    param.m_unit    =   m_pTableContent->item(row++,0)->text();
    param.setPoints(m_pTableContent->item(row++,0)->text());
    param.m_resistor    =   m_pTableContent->item(row++,0)->text().toDouble();
    param.m_percentOTK  =   m_pTableContent->item(row++,0)->text().toDouble();
    param.m_percentCLIT =   m_pTableContent->item(row++, 0)->text().toDouble();
    param.m_adjustment  =   m_pTableContent->item(row++, 0)->text().toDouble();
    param.m_dchan   =   m_pTableContent->item(row++,0)->text().toDouble();
    param.m_name    =   m_pTableContent->item(row++,0)->text();
    param.m_KYNI    =   m_pTableContent->item(row++,0)->text();
    param.m_comments    =   m_pTableContent->item(row++,0)->text();
    param.setCalibrators(m_pTableContent->item(row++, 0)->text());
    return param;
}

void SonetDataView::resizeEvent(QResizeEvent *)
{
    m_pTableContent->verticalHeader()->setDefaultSectionSize(m_pTableContent->height() / m_pTableContent->rowCount());
}
