import QtQuick 1.1

Rectangle {
    id: mainRow
    objectName: "mainRow"
    color:"transparent"
    width: 1024
    height:  400


    SearchDeviceWidget {
        id: searchDevicesWidget
        width: 290
        height: mainRow.height
        z: controllerList.z + 1
    }

    ControllerList {
            id: controllerList
            width: 805
            height: mainRow.height /  transformScale.xScale
            transform: Scale {
                id: transformScale
                xScale: (mainRow.width - searchDevicesWidget.width - searchDevicesWidget.x) / 805
                yScale: xScale
            }

            anchors.top: parent.top
            anchors.left: searchDevicesWidget.right
    }


}


