import QtQuick 1.1

Rectangle {
    id: background
    property real min: 0
    property real max: 0xFFFF
    property real valueToSet: 0
    property real displayValue: 0
    property bool showHex: false
    property int textLength: 4
    property alias readOnly: textInput.readOnly
    property alias textColor: textInput.color
    property bool isCode: true
    function  startAnimation() {colorAnimation.start()}
    Binding {
        target: textInput;
        property: "text";
        value: getText()
    }
    function refreshDisplayedValue() {
        textInput.text = getText()
    }
    function getText()
    {
        var code = valueToSet > 0xffff || valueToSet < 0 ? displayValue : valueToSet;
        if(showHex)
        {
            return zeroFillHex(code)
        }
        else if(isCode)
        {
            return toRealString(code)
        }
        else
        {
            return realToString(code)
        }
    }

    function toRealString(code) {
        var res = (code / 0xffff * (max - min) + min);
        res = realToString(res)
        return res
    }
    function toCode(value) {
        return ((value - min) / (max - min) * 0xffff).toFixed();
    }
    function zeroFillHex(number) {
        var res = ("000000000".slice(0, 4 - number.toString(16).length) + number.toString(16));
        return res
    }
    function realToString(real) {
        return real < 1 ? real.toFixed(textLength) : real.toPrecision(textLength + 1)
    }

    property alias pointSize: textInput.font.pointSize
    onDisplayValueChanged: {
        if(!readOnly)
            if(valueToSet == displayValue)
               colorAnimation.restart()
    }
    signal newValueRequest(variant value);
    color: "transparent"
    radius: height / 4
    width: textInput.width
    height: textInput.height
    PropertyAnimation {
        id: colorAnimation
        target: foreground
        properties: "opacity"
        from: 1
        to: 0
        duration: 1000
    }
    Rectangle {
        color: "#ff20f020"
        id: foreground
        anchors.fill: parent
        opacity: 0
        z: 1
    }
    TextInput {
        id: textInput
        z: 2
        smooth: true
        activeFocusOnPress: true
        selectByMouse: true
        function refreshText() {
            if(readOnly)
                return;
            var result = showHex ? parseInt(text, 16) :
                                   isCode ? toCode(parseFloat(text)) : parseFloat(text)// value -> code if needed
            if(!text.length) //if empty
                result = 0
            else if(result > 0xFFFF)
                result = 0xFFFF;
            else if(result < 0)
                result = 0
            if(result == displayValue)
                refreshDisplayedValue();
            newValueRequest(result)
            parent.forceActiveFocus()
        }
        Timer {
            id: timer
            repeat: false
            interval: 1
            onTriggered: textInput.selectAll() //по умолчанию нельзя выстваить selectAll на фокусе, так как select сразу сбивается изменеием позиции курсора.
        }

        onActiveFocusChanged: {
            focus = activeFocus
            if(focus)
                timer.start()
            if(!activeFocus)
                refreshText()
        }
        Keys.onEnterPressed: refreshText()
        Keys.onReturnPressed: refreshText()
        font.family: "Courier New"
        font.bold: valueToSet !== 0x10000 && valueToSet !== -2 && displayValue != valueToSet
        validator: RegExpValidator {regExp: showHex ? /[0-9a-fA-F]*/ : /\d*\.\d+"/}
    }
}

