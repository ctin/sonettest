import QtQuick 1.1
import CustomComponents 1.0
import "../desctopComponents/DesctopComponents"

Item {
    id: diagModule
    objectName: "diagModule"
    width:640
    height: width * 0.37
    function getDevice() {
        if(index1 < 0 || index1 >= deviceList.length)
            return;
        diagnosticModuleDevice = deviceList.getDevice(index1)
    }

    property DeviceWithData diagnosticModuleDevice: getDevice()
    property bool showCodes: false
    property bool showDiagChan: false

    Connections {
        target: diagnosticModuleDevice
        onLastErrorChanged: if(diagnosticModuleDevice.lastError.length) colorAnimation.start()
        onHardwareErrorChanged: if(diagnosticModuleDevice.hardwareError.length) colorAnimation.start()
    }

    PropertyAnimation {
        id: colorAnimation
        target: foreground
        properties: "opacity"
        from: 3
        to: 0
        duration: 500
    }
    Rectangle {
        color: "red"
        id: foreground
        anchors.fill: parent
        opacity: 0
        z: container.z - 1
    }

    Rectangle
    {
        id: container
        smooth: true
        anchors.fill: parent
        anchors.margins: 7
        color: "#D5D6D4"
        border.color: "black"
        border.width: 2
        //source: "../content/DO16_s_1.png"

        Image {
            id: image_modbus2
            x: 10
            y: 50
            width: 105
            height: 37.500
            anchors.top: parent.top
            anchors.topMargin: 50
            anchors.left: parent.left
            anchors.leftMargin: 10
            smooth: true
            source: "../content/modbus.png"

            MouseArea {
                anchors.fill: parent
                onClicked: if(diagnosticModuleDevice.deviceNum < 210) diagnosticModuleDevice.deviceNum += 10
                Rectangle {
                    id: selectionbackgroundModbus2
                    anchors.fill: parent
                    color: "#6431ccca"
                    opacity:  diagnosticModuleDevice.deviceNum >= 210
                    radius: width / 2
                }
            }
        }

        Image {
            id: image_modbus1
            width: 105
            height: 37.500
            anchors.left: image_modbus2.right
            anchors.leftMargin: 20
            anchors.verticalCenter: image_modbus2.verticalCenter
            smooth: true
            source: "../content/modbus.png"

            MouseArea {
                anchors.fill: parent
                onClicked: if(diagnosticModuleDevice.deviceNum >= 210) diagnosticModuleDevice.deviceNum -= 10
                Rectangle {
                    id: selectionbackgroundModbus1
                    color: "#6431ccca"
                    radius: width / 2
                    anchors.fill: parent
                    opacity: diagnosticModuleDevice.deviceNum < 210
                }
            }
        }

        CustomDial {
            id: customdialNum
            value: diagnosticModuleDevice.frontPanelX1
            anchors.right: relays.left
            anchors.verticalCenter: image_modbus1.verticalCenter
        }

        CustomDial {
            id: customdialSpeed
            value: diagnosticModuleDevice.frontPanelSpeed
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: image_modbus1.verticalCenter
            transform: Rotation {origin.x: customdialSpeed.width / 2; origin.y: customdialSpeed.height / 2; angle: -90;}
        }

        ListView {
            id: relays
            anchors.top: customdialNum.bottom
            anchors.topMargin: -7
            anchors.right: parent.right
            anchors.rightMargin: 15
            model: diagnosticModuleDevice.getDataModel(4)
            width: 100 * 1.2
            height: 70
            interactive: false
            delegate: Image {
                visible: index < (relays.count)
                x: width * 0.2 * index
                width: 100
                height: 35
                source: "../content/relay.png"
                Text {
                    x: -width - 10
                    text: "реле " + (index + 1).toString()
                    anchors.verticalCenter: parent.verticalCenter
                    font.bold: requestToSetChannelValue
                    color: diagnosticModuleDevice.deviceAviable ? "black" : "red"
                }

                Led {
                    width: 23
                    height: 22
                    anchors.left: parent.left
                    anchors.leftMargin: 2
                    anchors.verticalCenterOffset: -2
                    anchors.verticalCenter: parent.verticalCenter
                    on: deviceValue
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: diagnosticModuleDevice.setModelData(4, index, !deviceValue, "userValue")
                }
            }
        }

        ListModel {
            id: sensorsModel
            ListElement {name: "Напряжение 1"; unit: "В"}
            ListElement {name: "Напряжение 2"; unit: "В"}
            ListElement {name: "Температура"; unit: "°C"}
            ListElement {name: "Влажность"; unit: "%"}
        }

        ListView {
            anchors.left: parent.left
            anchors.leftMargin: 20
            height: 135
            spacing: 7
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            model: diagnosticModuleDevice.getDataModel(0)
            delegate: Rectangle {
                height: rectangle.height
                width: 150
                visible: index < sensorsModel.count && index >= 0
                color: "transparent"
                Text {
                    id: typetext
                    text: index < sensorsModel.count && index >= 0 ? sensorsModel.get(index).name : 0
                    anchors.verticalCenter: rectangle.verticalCenter
                    font.pixelSize: 12
                    color: diagnosticModuleDevice.deviceAviable ? "black" : "red"
                }
                Led {
                    id: ledHasPower
                    opacity: index < 2 && diagnosticModuleDevice.deviceIDs[0] === 112
                    anchors.left: typetext.right
                    anchors.leftMargin: 10
                    width: 27
                    height: 27
                    Connections {
                        target: diagnosticModuleDevice.getDataModel(5)
                        onDataChanged: ledHasPower.on = index < 2 ? diagnosticModuleDevice.getModelData(5, index, "deviceValue") : 0
                    }

                    on: index < 2 ? diagnosticModuleDevice.getModelData(5, index, "deviceValue") : 0
                }

                Rectangle {
                    opacity: diagnosticModuleDevice.deviceIDs[0] !== 112 || index > 1
                    id: rectangle
                    anchors.right: parent.right
                    width: 56
                    height: 27
                    color: "#00000000"
                    border.color: "#000000"
                    smooth: true
                    Text {
                        id: text__value
                        text: {
                            var result
                            if(index > 1)
                            {
                                result = (deviceValue / 10).toFixed(1)
                                if(index === 3)
                                    if(result < 30)
                                        result = " < 30"
                            }
                            else if(deviceValue <= 15 * 24.0 / 0xf8)
                                result = " < 15"
                            else
                                result = (24.0 * deviceValue / 0xf8).toFixed(2)
                            if(index < sensorsModel.count && index >= 0)
                                result += " " + sensorsModel.get(index).unit
                            return result

                        }
                        anchors.fill: parent
                        font.pixelSize: 12
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }

            }
        }

        DiagnosticModuleGrid {
            id: dataGrid3
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            slot: 3
        }

        DiagnosticModuleGrid {
            id: dataGrid2
            anchors.bottom: parent.bottom
            anchors.right: dataGrid3.left
            slot: 2
        }

        DiagnosticModuleGrid {
            id: dataGrid1
            anchors.bottom: parent.bottom
            anchors.right: dataGrid2.left
            slot: 1
        }

        Text {
            id: text1
            text: qsTr("Адрес")
            anchors.bottom: customdialNum.top
            anchors.horizontalCenter: customdialNum.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: text2
            x: 301
            y: 53
            text: qsTr("Скорость")
            anchors.bottom: customdialSpeed.top
            anchors.horizontalCenter: customdialSpeed.horizontalCenter
            font.pixelSize: 12
        }
        Text {
            id: text3
            x: 20
            y: 0
            text: qsTr("MODBUS 2")
            anchors.bottom: image_modbus2.top
            anchors.bottomMargin: 0
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: image_modbus2.horizontalCenter
            font.pixelSize: 12
        }
        Text {
            id: text4
            x: 162
            y: 0
            text: qsTr("MODBUS 1")
            anchors.horizontalCenter: image_modbus1.horizontalCenter
            anchors.bottom: image_modbus1.top
            anchors.bottomMargin: 0
            font.pixelSize: 12
        }
        ModuleQMLAdapter {
            id: adapter
            slot: 0
            deviceID: diagnosticModuleDevice.deviceIDs[slot]
            type: diagnosticModuleDevice.types[slot]
            channelsCount:  diagnosticModuleDevice.channelsCount[slot]
        }
        Text {
            id: text7
            x: 10
            /*text: qsTr("Модуль диагностики")
                  + (diagnosticModuleDevice.deviceIDs[0] === 101 ? " КУНИ.468224.002" : "")
                  + ((diagnosticModuleDevice.deviceIDs[0] === 111 || diagnosticModuleDevice.deviceIDs[0] === 112) ? " КУНИ.468224.003" : "");
            */
            text: adapter.KYNI
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 10
            font.pixelSize: 16
        }

        Text {
            id: text_TX
            x: 10
            y: 10
            text: qsTr("TX")
            anchors.verticalCenterOffset: 0
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.verticalCenter: text7.verticalCenter
            font.pixelSize: 12
        }

        Led {
            id: led_tx
            x: 25
            y: 10
            width: 16
            height: 14
            anchors.verticalCenter: text_TX.verticalCenter
            on: diagnosticModuleDevice != null && diagnosticModuleDevice.turnedOn && diagnosticModuleDevice.rxEnabled

        }
        Text {
            id: text_RX
            y: 0
            text: qsTr("RX")
            anchors.left: led_tx.right
            anchors.leftMargin: 15
            anchors.verticalCenter: text_TX.verticalCenter
            font.pixelSize: 12
        }
        Led {
            id: led_rx
            y: 10
            width: 16
            height: 14
            anchors.left: text_RX.right
            anchors.leftMargin: 0
            anchors.verticalCenter: text_RX.verticalCenter
            on: diagnosticModuleDevice != null && diagnosticModuleDevice.turnedOn && diagnosticModuleDevice.txEnabled
        }
    }
}
