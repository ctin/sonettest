import QtQuick 1.1
import CustomComponents 1.0
import DesctopComponents 0.1
import "../desctopComponents/DesctopComponents"

Item {
    id: procModule
    width: 113
    height: 292
    ContextMenu {
        id: procModuleContextMenu
        onMenuClosed: procModule.opacity = 1
        MenuItem {
            text : "Коды"
            checkable: true
            checked: showCodes
            onCheckedChanged: showCodes = checked
        }
        MenuItem {
            text : "Канал диагностики"
            checkable: true
            checked: showDiagChan
            onCheckedChanged: showDiagChan = checked
        }
        MenuItem {
            text: "Установить минимум на все выходные модули"
            onTriggered: device.setMinToAll();
            iconSource: "../content/diodGreenOff.png"
        }
        MenuItem {
            text: "Установить максимум на все выходные модули"
            onTriggered: device.setMaxToAll();
            iconSource: "../content/diodGreenOn.png"
        }

        Separator {}
        Menu {
            text: "Проверки контроллера"

            MenuItem {
                text: "Информация о составе контроллера"
                onTriggered: core.startTest(text, device.componentID, -1)
            }
            MenuItem {
                text: "Проверка переключателей"
                onTriggered: core.startTest(text, device.componentID, -1)
            }
            MenuItem {
                text: "Обзор всех каналов"
                onTriggered: core.startTest(text, device.componentID, -1)
            }
            MenuItem {
                text: "Проверка регистров диагностики"
                onTriggered: core.startTest(text, device.componentID, -1)
            }
            MenuItem {
                text: "Измерение температуры"
                onTriggered: core.startTest(text, device.componentID, -1)
            }
            MenuItem {
                text: "Замер времени ответа"
                onTriggered: core.startTest(text, device.componentID, -1)
            }
            MenuItem {
                text: "Чтение из модулей"
                onTriggered: core.startTest(text, device.componentID, -1)
            }

        }
    }


//крест на крест - чтоб не смущать пользователя. Лучше пусть в программе будет как на столе 1 к 1.
    Image {
        width: parent.width
        height: parent.height
        MouseArea {
            id: selectMenuArea
            anchors.fill: parent
            acceptedButtons: Qt.RightButton
            onClicked: {
                if(disableMenu)
                    return
                procModule.opacity = 0.8
                var point = mapToItem(null, mouseX, mouseY)
                procModuleContextMenu.showPopup(point.x, point.y)
            }
        }

        smooth: true
        source: "../content/procBackground.png"

        CustomDial {
            id: dial10
            x: 68.5
            y: 92.3
            value: device ? device.frontPanelX10 : 0
        }
        CustomDial {
            id: dial1
            anchors.left: dial10.left
            anchors.top: dial10.bottom
            value: device ? device.frontPanelX1 : 0
        }
        CustomDial {
            id: dialSpeed
            anchors.left: dial1.left
            anchors.top: dial1.bottom
            anchors.topMargin: 6
            value: device ? device.frontPanelSpeed : 0
        }

        Image {
            id: switchesOkImage
            opacity: (device && switch1.turnedOn && device.deviceAviable && !ok)
            anchors.left: dialSpeed.left
            anchors.top: dialSpeed.bottom
            anchors.bottomMargin: 5
            width: 13
            height: width
            smooth: true
            property bool ok: device.deviceNum % 100 == (device.frontPanelX10 * 10 + device.frontPanelX1) && device.rateIndex == device.frontPanelSpeed
            source: "../content/alert.png"
        }

        /*Text {
            id: temperatureText
            font.pointSize: 11
            opacity: (device && switch1.turnedOn && device.deviceAviable && (device.temperature > 5))
            anchors.horizontalCenter: modbus1MouseArea.horizontalCenter
            anchors.top: dialSpeed.top
            text: device ? device.temperature.toPrecision(3) + " °C" : ""
        }*/


        Image {
            id: lcdModbusActive
            anchors.top: dialSpeed.bottom
            anchors.topMargin: 13
            anchors.right: lcd10.left
            width: lcd1.width
            height: lcd1.height
            source: "../content/digits/d0" + Math.floor(device.deviceNum / 100).toString() + "_3.png"
        }

        Image {
            id: lcd10
            anchors.top: lcdModbusActive.top
            anchors.right: lcd1.left
            width: lcd1.width
            height: lcd1.height
            source: "../content/digits/d0" + Math.floor((device.deviceNum / 10) % 10).toString() + "_3.png"
        }
        Image {
            id: lcd1
            anchors.top: lcdModbusActive.top
            anchors.right: parent.right
            anchors.rightMargin: 5
            width: 20
            height: 26
            source: "../content/digits/d0" + Math.floor(device.deviceNum % 10).toString() + "_3.png"
        }
        Text {
            anchors.top: lcdModbusActive.bottom
            anchors.topMargin: 20
            anchors.left: lcdModbusActive.left
            anchors.leftMargin: 5
            font.pixelSize: 20
            font.family: "Arial"
            text: fake ? "FAKE" : ""
        }

        MouseArea {
            id: modbus1MouseArea
            x: 10.5
            y: 54
            width: 40
            height: 79
            onClicked: if(device.deviceNum > 100) device.deviceNum -= 100
            Rectangle {
                id: selectionbackgroundModbus1
                anchors.fill: parent
                color: "#6431ccca"
                opacity:  device.deviceNum <= 100
                radius: width / 2
            }
        }


        MouseArea {
            anchors.left: modbus1MouseArea.left
            y: 210.6
            width: modbus1MouseArea.width
            height: modbus1MouseArea.height
            onClicked: if(device.deviceNum <= 100) device.deviceNum += 100
            Rectangle {
                id: selectionbackgroundModbus2
                anchors.fill: parent
                color: "#6431ccca"
                opacity:  device.deviceNum > 100
                radius: width / 2
            }
        }

        Switch {
            id: switch1
            anchors.horizontalCenter: imageReset.horizontalCenter
            y: 62
            width: 15
            height: 28.5
            turnedOn: device.turnedOn
            state: device.turnedOn ? "on" : "off"
            onTurnedOnChanged: {
                device.turnedOn = turnedOn
            }
            Connections {
                target: device
                onTurnedOnChanged: {
                    if(device.turnedOn)
                    {
                        device.unsetLastError()
                        device.unsetHardwareError()
                    }
                    switch1.turnedOn = device.turnedOn
                }
            }
        }

        Led {
            id: ledBreakdown
            type: "Red"
            x: 79
            y: 31.5
            width: 11.6
            height: 10.6
            on: device.hardwareError.length > 0
        }

        Led {
            id: ledWork
            x: 79
            y: 17.6
            width: 11.6
            height: 10.6
            on: device.turnedOn && device.deviceAviable && device.hardwareError.length === 0
        }

        Led {
            id: imageTX1
            x: 30.5
            y: 17.7
            width: 11.6
            height: 10.6
            on: device != null && device.turnedOn && device.rxEnabled && !Math.floor(device.deviceNum / 101)
        }

        Led {
            id: imageRX1
            x: 30.5
            y: 32
            width: 11.6
            height: 10.6
            on: device != null && device.turnedOn && device.txEnabled && !Math.floor(device.deviceNum / 101)
        }

        Led {
            id: imageTX2
            x: 31
            y: 173
            width: 11.6
            height: 10.6
            on: device != null && device.turnedOn && device.rxEnabled && Math.floor(device.deviceNum / 101)
        }

        Led {
            id: imageRX2
            x: 31
            y: 187
            width: 11.6
            height: 10.6
            on: device != null && device.turnedOn && device.txEnabled && Math.floor(device.deviceNum / 101)
        }
        Image {
            id: imageReset
            x: 78
            y: 47
            width: 13.6
            height: 13.6
            source: "../content/reset" + (resetArea.pressedAndHold ? "_pressed" : "") + ".png"
            smooth: true
            MouseArea {
                id: resetArea
                property bool pressedAndHold:false
                anchors.fill: parent
                onPressAndHold: {
                    pressedAndHold = true
                    device.addReset()
                }
                onReleased: pressedAndHold = false
            }
        }
    }
}

