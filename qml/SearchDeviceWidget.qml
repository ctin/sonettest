import QtQuick 1.1
import CustomComponents 1.0
import "../desctopComponents/DesctopComponents"
import "../desctopComponents/DesctopComponents/private" as Private


Rectangle {
    id: rectangle1
    width: 270
    height: 600
    color: sysPalette.window
    SystemPalette {id: sysPalette}
    property SearchDevice searchDevice: deviceList.getSearchDevice()
    Connections {
        target: searchDevice
        onDeviceFounded: deviceList.append(1, rateIndex, deviceNum, autoSearch, port)
        onDeviceNumChanged: if(deviceNum && searchDevice.port.length) text_searchStatus.text = searchDevice.port.replace("\\\\.\\", "") + ": №" + searchDevice.deviceNum
        onHardwareErrorChanged: {
            rect_searchStatus.color = searchDevice.hardwareError.length ? "red" : "transparent"
            if(searchDevice.hardwareError.length) text_searchStatus.text = searchDevice.port.replace("\\\\.\\", "") + ": нельзя открыть"
        }
        onFinished: {
            if(!searchDevice.founded) {
                text_searchStatus.text = "Приборы не найдены"
                rect_searchStatus.color = "red"
            }
            else {
                text_searchStatus.text = "Найдено приборов: " + searchDevice.founded
                rect_searchStatus.color = "green"
            }
        }

        onRunningChanged: {
            button_add.enabled = !searchDevice.running
            button_remove.enabled = !searchDevice.running
            if(searchDevice.running) {
                rect_searchStatus.color = "transparent"
            }
        }
    }

    ProgressBar {
        id: progressbar1
        x: 112
        y: 22
        width: 127
        height: 23
        anchors.top: parent.top
        anchors.topMargin: 22
        anchors.left: spinboxSpeed.right
        anchors.leftMargin: 15
        anchors.right: parent.right
        anchors.rightMargin: 10
        maximumValue: 219
        value: searchDevice.deviceNum
        Rectangle {
            id: rect_searchStatus
            color: "transparent"
            anchors.fill: parent
            opacity: 0.15
        }
        Text {
            id: text_searchStatus
            anchors.fill: parent
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            font.pointSize: 11
            //color: searchDevice.deviceNum < 219/2 ? "black" : "white"
        }
    }

    Button {
        id: textbutton1
        x: 10
        y: 57
        width: 87
        height: 36
        text: "Поиск"
        tooltip: "Найти все контроллеры по всем портам"
        anchors.left: parent.left
        anchors.leftMargin: 10
        onClicked: {
            deviceList.clear()
            searchDevice.findAllFast()
        }

        enabled: !searchDevice.running
    }

    Button {
        id: textbutton2
        x: 151
        y: 57
        width: 89
        height: 36
        text: "Стоп"
        defaultbutton: false
        anchors.right: parent.right
        anchors.rightMargin: 10
        onClicked: {
            searchDevice.stop()
        }
        enabled: searchDevice.running
    }

    Component {
        id: listDelegate
        Rectangle {
            id: delegateItem
            width: scroller.viewportWidth - 1
            height: textType.height + infoRow.height + textStatus.height + okButton.opacity * okButton.height
            color: index == list_view1.currentIndex ? sysPalette.highlight : sysPalette.base
            border.color: "black"
            border.width: area.pressed ? 3 : 1
            Behavior on height {
                NumberAnimation { duration: 200}
            }
            ListView.onAdd: SequentialAnimation {
                PropertyAction { target: delegateItem; property: "x"; value: -delegateItem.width }
                NumberAnimation { target: delegateItem; property: "x"; to: 0; duration: 250; easing.type: Easing.InOutQuad }
            }

            ListView.onRemove: SequentialAnimation {
                PropertyAction { target: delegateItem; property: "ListView.delayRemove"; value: true }
                NumberAnimation { target: delegateItem; property: "x"; to: delegateItem.width; duration: 250; easing.type: Easing.InOutQuad }
                PropertyAction { target: delegateItem; property: "ListView.delayRemove"; value: false }
            }
            MouseArea {
                id: area
                anchors.fill: parent
                onClicked: list_view1.currentIndex = index
            }
            property DeviceWithData device: deviceList.getDevice(index)
            Button {
                id: okButton
                text: "Ок"
                anchors.right: delegateItem.right
                anchors.bottom: delegateItem.bottom
                anchors.rightMargin: 5
                anchors.bottomMargin: 5
                onClicked: {
                    device.unsetLastError()
                    device.unsetHardwareError()
                    device.turnedOn = true
                }
                opacity: device.lastError.length || device.hardwareError.length || !device.turnedOn
            }
            Column {
                id: ledColumn
                anchors.right: parent.right
                anchors.top: parent.top
                spacing: 5
                Led {
                    anchors.right: parent.right
                    on: device.turnedOn && device.deviceAviable && device.hardwareError.length === 0
                    height: 15; width: height
                }
                Led
                {
                    anchors.right: parent.right
                    on: device.isOpen
                    type: "Yellow"
                    height: 15; width: height
                }
                Led
                {
                    anchors.right: parent.right
                    type: "Red"
                    on: device.hardwareError.length > 0
                    height: 15; width: height
                }

            }
            Text {
                id: textType
                anchors.left: delegateItem.left
                anchors.leftMargin: 5
                anchors.right: delegateItem.right
                color: index == list_view1.currentIndex ? sysPalette.highlightedText : sysPalette.text
                text: (!device.isSonet ? "Модуль диагностики" : "Контроллер СОНЕТ")
            }
            Row {
                id: infoRow
                anchors.top: textType.bottom
                anchors.left: delegateItem.left
                anchors.leftMargin: 5
                anchors.right: delegateItem.right
                Text {
                    text: "Номер: "
                    anchors.verticalCenter: spinboxDeviceNum.verticalCenter
                    color: index == list_view1.currentIndex ? sysPalette.highlightedText : sysPalette.text
                }
                SpinBox {
                    id: spinboxDeviceNum
                    Component.onCompleted: value = device.deviceNum
                    onValueChanged: device.deviceNum = value
                    Connections {
                        target: device
                        onDeviceNumChanged: spinboxDeviceNum.value = device.deviceNum
                    }
                    minimumValue: 0
                    maximumValue: 219
                    width: 50
                }
                Text {
                    text: "   скорость: "
                    anchors.verticalCenter: spinboxDeviceNum.verticalCenter
                    color: index == list_view1.currentIndex ? sysPalette.highlightedText : sysPalette.text
                }
                SpinBox {
                    id: spinboxRateIndex
                    minimumValue: 3
                    maximumValue: 8
                    value: device.rateIndex
                    onValueChanged: device.rateIndex = value
                    Connections {
                        target: device
                        onRateIndexChanged: {
                                spinboxRateIndex.value = device.rateIndex}
                    }

                    width: 40
                }
            }
            Text {
                id: textStatus
                color: index == list_view1.currentIndex ? sysPalette.highlightedText : sysPalette.text
                anchors.top: infoRow.bottom
                anchors.left: delegateItem.left
                anchors.leftMargin: 5
                anchors.right: delegateItem.right
                wrapMode: Text.Wrap
                z: delegateItem.z - 1
                text: {
                    var txt = "порт: " + device.port.replace("\\\\.\\", "")
                    var slot;

                    txt += "\nстатус: " + device.info
                    if(device.hardwareError.length)
                        txt += "\nОшибка ОС " + device.hardwareError
                    else if(device.lastError.length)
                        txt += "\nПоследняя ошибка: " + device.lastError
                    if(device.isSonet && device.deviceAviable)
                    {
                        txt += "\n"
                        if(!device.bprState & 0x08)
                            txt += "Критическая ошибка flash или EEPROM!\nтребуется перезагрузка!"
                        else {
                            txt += "БПР "
                        if(device.bprState & 0x02)
                            txt += "работает, " + (device.bprState & 1 ? "резервный" : "основной") + "."
                        else
                            txt += "отключен."
                        }
                    }
                    return txt
                }
            }
        }
    }

    ScrollArea {
        id: scroller
        anchors.top: parent.top
        anchors.topMargin: 103
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 45
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        contentHeight: list_view1.contentHeight
        contentWidth: list_view1.contentWidth
        contentX: list_view1.contentX
        contentY: list_view1.contentY
        frame: false
        ListView {
            id: list_view1
            height: deviceList.length * 250
            Binding { target: list_view1; property: "currentIndex"; value: deviceList.currentIndex }
            onCurrentIndexChanged: deviceList.currentIndex = currentIndex
            boundsBehavior: Flickable.StopAtBounds
            model: deviceList.length //по другому не работает. FFFFFFFUUUUUUUU
            delegate: listDelegate
        }
    }

    SpinBox {
        id: spinboxSpeed
        enabled: !searchDevice.running
        x: 10
        y: 22
        width: 87
        height: 23
        Component.onCompleted: value = 7
        Binding {
            target: spinboxSpeed
            property: "value"
            value: searchDevice.rateIndex
        }

        onValueChanged: searchDevice.rateIndex = value
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 22
        minimumValue: 3
        maximumValue: 8
    }

    Text {
        id: text1
        y: 8
        height: 14
        text: qsTr("скорость")
        anchors.right: spinboxSpeed.left
        anchors.rightMargin: 0
        anchors.left: spinboxSpeed.right
        anchors.leftMargin: 0
        anchors.bottom: spinboxSpeed.top
        anchors.bottomMargin: 0
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 12
    }

    Text {
        id: text2
        text: qsTr("процесс поиска")
        anchors.bottom: progressbar1.top
        anchors.left: progressbar1.right
        anchors.right: progressbar1.left
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 12
    }

    Button {
        id: button_remove
        x: 132
        y: 567
        //width: 130
        height: 23
        text: "Удалить выделенный"
        tooltip: "Удаляет активный виртуальный прибор из списка"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 20
        onClicked: deviceList.removeCurrent(deviceList.currentIndex)
        //onClicked: deviceList.clear()
        enabled: deviceList.length && deviceList.currentIndex >= 0 && deviceList.currentIndex < deviceList.length
    }

    Button {
        id: button_add
        //width: 110
        height: 23
        text: "Добавить"
        tooltip: "Добавляет новый контроллер со стандартными параметрами"
        styleHint: ""
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        onClicked: deviceList.addNew()
        enabled: deviceList.length < 0xf0
    }

    Rectangle {
        id: rectangle2
        width: 10
        height: 40
        anchors.left: parent.right
        anchors.leftMargin: -width / 6 * 5
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        color: mouseArea.containsMouse ? sysPalette.dark : (rectangle1.state == "" ? sysPalette.window : sysPalette.midlight )


        Image {
            id: arrowImage
            width: parent.width
            height: width
            anchors.verticalCenter: parent.verticalCenter
            smooth: true
            source: "../content/arrow_" + (rectangle1.state == "" ? "left" : "right") + ".png"
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: rectangle1.state = (rectangle1.state.length ? "" : "widgetHidden")
        }
    }
    states: State {
            name: "widgetHidden"
            PropertyChanges {target: rectangle1; x: -rectangle1.width}
            PropertyChanges {
                target: rectangle2; anchors.leftMargin: 0 }
            //PropertyChanges { target: rectangle2; opacity: 1 }
            //PropertyChanges {target: textMiniRect; anchors.margins: 0}
            //PropertyChanges {target: textMiniRect; text: "Показать панель"}
            //PropertyChanges {target: textMiniRect; verticalAlignment: Text.AlignBottom}
            PropertyChanges { target: controllerList; interactive: true }
        }
    transitions: Transition {
        NumberAnimation { properties: "x"; duration: 500 }
    }

}
