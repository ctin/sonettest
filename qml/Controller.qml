import QtQuick 1.1
import CustomComponents 1.0
import "../desctopComponents/DesctopComponents"

Item {
    id: controller
    objectName: "Controller"
    property bool underSurvey: false
    property bool disableMenu: false
    property alias delegate: controller_repeater.delegate //для замены делегата модуля
    height: container.height
    function getDevice() {
        if(index1 < 0 || index1 >= deviceList.length)
            return 0;
        device = deviceList.getDevice(index1)
    }

    property DeviceWithData device: getDevice()
    property bool showCodes: false
    property bool showDiagChan: false

    Connections {
        target: device
        onLastErrorChanged: if(device.lastError.length) colorAnimation.start()
        onHardwareErrorChanged: if(device.hardwareError.length) colorAnimation.start()
    }

    PropertyAnimation {
        id: colorAnimation
        target: foreground
        properties: "opacity"
        from: 3
        to: 0
        duration: 500
    }
    Rectangle {
        color: "red"
        id: foreground
        anchors.fill: parent
        opacity: 0
        z: container.z - 1
    }

    Item
    {
        id: container
        anchors.margins: 7
        anchors.left: controller.left
        anchors.right: controller.right
        anchors.top: controller.top
        height: processorModule.height + anchors.margins * 2
        Row {
            id: row
            objectName: "controllerRow"
            width: container.width
            height: processorModule.height
            ProcessorModule {
                objectName: "procModule"
                id: processorModule
                width: row.width  / 7
            }
            Repeater {
                id: controller_repeater
                objectName: "repeater"
                model: 8 //slots count
                delegate: Module {
                    objectName: "module" + index.toString()
                    slot: index
                    width: (container.width - processorModule.width) / 8
                }
            }
        }
    }
}
