import QtQuick 1.1
Item {
    width: dataRow.width
    height: textInput.height
    Row {
        id: dataRow
        spacing: 2
        Text {
            font.family: "Courier New"
            font.pointSize: 10
            text: (index + 1).toString() + ":"
            anchors.verticalCenter: dataRow.verticalCenter
            color: device.deviceAviable ? "black" : "red"
        }
        Rectangle {
            color:"transparent"
            border.color: "grey"
            width:textInput.width
            height: textInput.height * 0.8
            anchors.verticalCenter: dataRow.verticalCenter

            ValueInput {
                id: textInput
                anchors.verticalCenter: parent.verticalCenter
                min: adapter.displayMin
                max: adapter.displayMax
                onNewValueRequest: device.setModelData(slot, index, value, "userValue")
                displayValue: deviceValue
                valueToSet: userValue
                showHex: adapter.rangeInfo.indexOf("Na") !== -1 ? 1 : showCodes
                pointSize: 10
                textLength: showCodes ? 4 : 5
                textColor: device.deviceAviable ? "black" : "red"
            }
        }
    }
}
