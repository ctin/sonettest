/****************************************************************************
**
** Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.1
 Item {
    id: toggleItem
    property bool turnedOn: true
    Item {
        id: toggleswitch
        width: parent.width; height: parent.height
        property int dragMax: background.height - knob.height
        function toggle() { toggleItem.turnedOn = !toggleItem.turnedOn}
        Image {
            id: background
            smooth:true
            source: "../content/slideBackground.png"
            width: parent.width
            height: parent.height
            MouseArea {
                anchors.fill: parent
                onClicked: toggleswitch.toggle()
            }
        }
        Image {
            id: knob
            smooth:true
            source: "../content/ball.png"
            width: parent.width
            height: width
            MouseArea {
                anchors.fill: parent
                drag.target: knob
                drag.axis: Drag.YAxis
                drag.minimumY: 0
                drag.maximumY: toggleswitch.dragMax
                onClicked: toggleswitch.toggle()
                onReleased: {
                    if (knob.y == 0)
                        if (toggleItem.turnedOn)
                            return;
                    if (knob.y == toggleswitch.dragMax)
                        if (!toggleItem.turnedOn)
                            return;
                    toggleswitch.toggle();
                }
            }
        }
        states: [
            State {
                name: "off"
                when: !toggleItem.turnedOn
                PropertyChanges { target: knob; y: toggleswitch.dragMax }
                PropertyChanges { target: toggleItem; turnedOn: false }
            },
            State {
                name: "on"
                when: toggleItem.turnedOn
                PropertyChanges { target: knob; y: 0 }
                PropertyChanges { target: toggleItem; turnedOn: true }
            }
        ]
        transitions: Transition {
            NumberAnimation { properties: "y"; easing.type: Easing.InOutQuad; duration: 200 }
        }
    }
}
