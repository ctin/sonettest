import QtQuick 1.1

Item {
    id: container
    width: 20
    height: 20
    Led {
        id: led
        on: deviceValue
        anchors.fill: parent
        anchors.bottomMargin: container.height / 3
        anchors.leftMargin: container.width / 3
    }

    Text {
        id: text1
        anchors.bottom: parent.bottom
        anchors.right: led.left
        anchors.rightMargin: -4
        verticalAlignment: Text.AlignTop
        horizontalAlignment: Text.AlignRight
        text: (index + 1).toString()
        font.pointSize: 8
        font.bold: requestToSetChannelValue
        color: device.deviceAviable ? "black" : "red"
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onPressed: {
            if(adapter.input)
                return
            device.setModelData(slot, index, !deviceValue, "userValue")
        }
    }
}
