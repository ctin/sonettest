import QtQuick 1.1
import CustomComponents 1.0
import "../desctopComponents/DesctopComponents"

Item {
    id: container
    property alias filter: controller.filter
    property bool selectionMode: false
    signal selected(int slot)
    function setMaxToAllChannels() {
        controller.device.setMaxToAll()
    }
    function setMinToAllChannels() {
        controller.device.setMinToAll()
    }
    Column {
        Row {
            CheckBox {
                text: "Показать коды"
                checked: controller.showCodes
                onCheckedChanged: controller.showCodes = checked
            }

            CheckBox {
                text: "Показать диагностические каналы"
                checked: controller.showDiagChan
                onCheckedChanged: controller.showDiagChan = checked
            }
        }

        Controller {
            id: controller
            width: 805
            device: deviceList.getDevice(deviceList.currentIndex)
            underSurvey: true
            property string filter: "ADIO"
            disableMenu: true
            function getState(slot) {
                if(device.valid(slot)) {
                    if(device.analog(slot) && filter.indexOf('A') !== -1
                            || !device.analog(slot) && filter.indexOf('D') !== -1)
                        if(device.input(slot) && filter.indexOf('I') !== -1
                                || !device.input(slot) && filter.indexOf('O') !== -1)
                            return "valid"
                }
                return ""
            }
            transform: Scale {
                id: transformScale
                xScale: (container.width) / 805
                yScale: xScale
            }
            delegate: Module {
                id: module
                objectName: "module" + index.toString()
                slot: index
                width: (container.width - processorModule.width) / 8
                valid: controller.getState(index)
                Connections {
                    target: device
                    onDeviceIDsChanged: module.valid = controller.getState(index);
                    onTypesChanged: module.valid = controller.getState(index);
                    onChannelsCountChanged: module.valid = controller.getState(index);

                }

                Connections {
                    target: controller
                    onFilterChanged: {
                        var state = controller.getState(index)
                        module.state = state;
                        module.valid = state;
                    }
                }
                MouseArea {
                    id: area
                    anchors.fill: parent
                    hoverEnabled: true
                    enabled: selectionMode && module.valid
                    onClicked: selected(index + 1)
                }
                Rectangle {
                    anchors.fill: parent
                    id: selectionRect
                    color: "blue"
                    opacity: 0.15
                    visible: area.containsMouse && module.valid && selectionMode
                }
            }
        }
    }
}
