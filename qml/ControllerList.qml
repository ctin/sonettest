import QtQuick 1.1
import "../desctopComponents/DesctopComponents"

Rectangle {
    id: container
    width: 1024
    height: mainRow.height
    color: sysPalette.midlight
    property alias interactive: controllerListView.interactive
    SystemPalette {id: sysPalette}
    Component {
        id: delegateController
        Controller {
            id: delegateItem
        }
    }

    Component {
        id: delegateDiagnosticModule
        DiagnosticModule {
            id: delegateItem
        }
    }

    Component {
        id: emptyDelegate
        Rectangle {
            color: sysPalette.dark
            width: 100
            height: width * 0.37
        }
    }

    ListView {
        id: controllerListView
        cacheBuffer: 10
        anchors.fill: parent
        snapMode: ListView.SnapToItem
        preferredHighlightBegin: 0
        preferredHighlightEnd: container.height
        highlight: Rectangle {
            color: sysPalette.highlight
            width: controllerListView.width
        }
        highlightMoveDuration: 1000
        highlightRangeMode: ListView.StrictlyEnforceRange
        currentIndex: deviceList.currentIndex
        onCurrentIndexChanged: deviceList.currentIndex = currentIndex
        model: deviceList
        delegate: Loader {
            id: loader
            property int index1: index
            sourceComponent:    if(index >= 0)
                                    (deviceElement.isSonet ? delegateController : delegateDiagnosticModule)
                                else
                                    emptyDelegate
            width: container.width
            ListView.onAdd: SequentialAnimation {
                PropertyAction { target: loader; property: "x"; value: container.width }
                NumberAnimation { target: loader; property: "x"; to: 0; duration: 250; easing.type: Easing.InOutQuad }
            }

            ListView.onRemove: SequentialAnimation {
                PropertyAction { target: loader; property: "ListView.delayRemove"; value: true }
                NumberAnimation { target: loader; property: "x"; to: container.width; duration: 250; easing.type: Easing.InOutQuad }
                PropertyAction { target: loader; property: "ListView.delayRemove"; value: false }
            }
        }
    }
}

