import QtQuick 1.1
import CustomComponents 1.0

Item{
    id: dataComponent
    width: dataRow.width
    height: textValue.height

    Row {
        id: dataRow
        spacing: 2
        Text {
            font.family: "Courier New"
            font.pointSize: 10
            text: (index + 1).toString() + ":"
            color: device.deviceAviable ? "black" : "red"
        }

        Text {
            id: textValue
            font.family: "Courier New"
            font.pointSize: 10
            text: ((adapter.rangeInfo.indexOf("Na") !== -1 || showCodes) ? adapter.toHex(deviceValue) : adapter.toReal(deviceValue))
            smooth: true
            color: device.deviceAviable ? "black" : "red"
        }
    }
}
