import QtQuick 1.1

Image {
    id: background
    property int value: 0
    property double side: 25
    width: sourceSize.width / 2
    height: sourceSize.height / 2
    smooth: true
    source: "../content/ArrowBackground.png"
    Image {
        id: arrow
        width: sourceSize.width / 2
        height: sourceSize.height / 2
        anchors.verticalCenterOffset: 1
        anchors.horizontalCenterOffset: 0
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        smooth: true
        source: "../content/Arrow.png"
        transform: Rotation {
            origin.x: arrow.width / 2
            origin.y: arrow.height / 2
            angle: (value % 10) * 36.0
            Behavior on angle {
                SpringAnimation {damping: 0.45; spring : 5; modulus: 360; }
            }
        }
    }
}
