import QtQuick 1.1
import CustomComponents 1.0
import DesctopComponents 0.1
import "../desctopComponents/DesctopComponents"

Item
{
    id: container
    width: 83.75
    height: 292
    property alias adapter: adapter
    property alias slot: adapter.slot
    property bool valid: adapter.valid
    state: valid ? "valid" : (adapter.type == 0xff ? "" : "configureNeeded")

    ListModel {
        id: aiItems
        ListElement { text: "Калибровка";}
        ListElement { text: "Поверка модуля ввода по ТУ"}
        ListElement { text: "Поверка модуля ввода    ЦЛИТ"}
        ListElement { text: "Чтение регистров модуля"}
        ListElement { text: "Чтение данных SPI"}
    }
    ListModel {
        id: aoItems
        ListElement { text: "Поверка модуля вывода по ТУ"}
        ListElement { text: "Поверка модуля вывода    ЦЛИТ"}
        ListElement { text: "Проверка времени установления каналов аналогового вывода"}
        ListElement { text: "Чтение регистров модуля"}
        ListElement { text: "Подать ноль на все каналы"}
        ListElement { text: "Подать максимум на все каналы"}
    }
    ListModel {
        id: doItems
        ListElement { text: "Чтение регистров модуля"}
        ListElement { text: "Выключить все каналы"}
        ListElement { text: "Включить все каналы"}
    }
    ListModel {
        id: diItems
        ListElement { text: "Чтение регистров модуля"}
    }
    ListModel {
        id: configureNeeded
        ListElement { text: "Чтение регистров модуля"}
    }

    function getModel() {
        if(state == "configureNeeded")
            return configureNeeded
        if(adapter.analog && adapter.input)
            return aiItems
        else if(adapter.analog && !adapter.input)
            return aoItems
        else if(!adapter.analog && adapter.input)
            return diItems
        else
            return doItems
    }

    ContextMenu {
        id: moduleContextMenu
        onMenuClosed: container.opacity = 1
        onSelectedIndexChanged:  {
            processResult(itemTextAt(selectedIndex))
            core.startTest(itemTextAt(selectedIndex), device.componentID, slot)
        }
        function refresh() {
            model = getModel()
            rebuildMenu()
        }

    }
    function processResult(result) {
        if((result === "Подать ноль на все каналы")
                || (result === "Выключить все каналы"))
            device.setMinToAll(slot);
        else if(result === "Подать максимум на все каналы"
                || (result === "Включить все каналы"))
            device.setMaxToAll(slot);
    }
    ModuleQMLAdapter {
        id: adapter
        objectName: "moduleQMLAdapter" + slot.toString()
        deviceID: device.deviceIDs[slot]
        type: device.types[slot]
        channelsCount:  device.channelsCount[slot]
        Connections {
            target: core
            onDataLoaded: adapter.notifyParam()
        }
    }

    Image {
        id: background
        anchors.fill: parent
        source: "../content/moduleBackground.jpg"
        smooth: true;
    }
    /*Image {
        id: canSurveyImage
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 7
        height: 20
        width: 20
        source: "../content/" + (device.canSurveyModules[slot] ? "ok" : "remove") + ".png";
        MouseArea {
            anchors.fill: parent
            onClicked: device.setcanSurveyModule(slot, !device.canSurveyModules[slot])
        }
        z: 99
    }*/
    Image {
        objectName: "mainImage"
        anchors.fill: parent
        anchors.rightMargin: 1
        id: module
        smooth: true
        opacity: 0
        source: "../content/DO16_s_1.png"

        MouseArea {
            id: selectMenuArea
            anchors.fill: parent
            acceptedButtons: Qt.RightButton
            onClicked: {
                if(disableMenu)
                    return
                moduleContextMenu.refresh()
                container.opacity = 0.8
                var point = mapToItem(null, mouseX, mouseY)
                moduleContextMenu.showPopup(point.x, point.y)
            }
        }
        Column {
            opacity: 0
            objectName: "columnConfigureNeeded"
            id: columnConfigureNeeded
            spacing: 5
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: 5.6
            anchors.leftMargin: 5.6
            Text {
                id: configureNeededText
                font.pointSize: 10
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.rightMargin: 5
                smooth: true
                wrapMode: Text.Wrap
                text: "Требует настройки"

            }
            Button {
                text: "Задать ID"
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.rightMargin: 5
                onClicked:  core.startTest("Чтение регистров модуля", device.componentID, slot)
            }
        }

        Text {
            id: slotText
            text: "слот № " + (slot + 1)
            opacity: 0.15
            font.pointSize: 12
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.right: parent.right
            anchors.rightMargin: 7
            verticalAlignment: Qt.AlignBottom
            horizontalAlignment: Qt.AlignRight
        }
        Text {
            text: if(adapter.deviceID != 0xff)
                      "ID: " + "000000000".slice(0, 3 - adapter.deviceID.toString().length) + adapter.deviceID
                  else
                      ""
            opacity: 0.15
            font.pointSize: 12
            anchors.bottom: slotText.top
            anchors.right: slotText.right
            verticalAlignment: Qt.AlignBottom
            horizontalAlignment: Qt.AlignRight
        }

        Image {
            anchors.top: parent.top
            anchors.topMargin: 22
            anchors.right: parent.right
            anchors.rightMargin: 5
            width: 20
            height: 22
            smooth: true
            source: if(adapter.deviceID != 0xff)
                        ("../content/" + (adapter.input ? "blue.png" : "green.png"))
                    else
                        ""
        }
        Column {
            objectName: "columnData"
            id: column
            spacing: 5
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: 5.6
            anchors.leftMargin: 5.6
            Text {
                id: typeText
                font.pointSize: 10
                smooth: true
                wrapMode: Text.Wrap
                text: if(adapter.deviceID != 0xff)
                          (adapter.analog ? "аналоговый" : "дискретный") + "\n" + (adapter.input ? "ввод" : "вывод")
                      else
                          ""
            }
            Item {
                anchors.right: parent.right
                anchors.left: parent.left
                height: rangeInfoText.height
                Text {
                    id: rangeInfoText
                    font.pointSize: 10
                    smooth: true
                    wrapMode: Text.Wrap
                    text: if(adapter.deviceID != 0xff)
                              adapter.rangeInfo
                          else
                              ""
                }
                Text{
                    id: text24V
                    text: "24В"
                    font.pointSize: 6
                    anchors.verticalCenter: rangeInfoText.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: 3
                    opacity: underSurvey && device.hasPower[slot]
                }
                Led {
                    width: 10
                    height: 10
                    anchors.verticalCenter: text24V.verticalCenter
                    anchors.right: text24V.left
                    on: device.hasPower[slot]
                    opacity: underSurvey && device.hasPower[slot]
                }
            }

            GridView {
                id: dataGrid
                currentIndex: -1
                interactive: false
                property bool eepromCodes: false
                boundsBehavior: Flickable.StopAtBounds
                anchors.left: parent.left
                anchors.right: parent.right
                height: 130
                cellWidth: adapter.analog ? column.width : 19
                cellHeight: adapter.analog ? 13.5 : 20.3
                x: 4 * !adapter.analog
                model: if(adapter.deviceID != 0xff)
                           device.getDataModel(slot)
                       else
                           0
                delegate: !adapter.analog ? ledlabelComponent : adapter.input
                                          ? textComponent : textInputComponent;
                highlight: highlight
                highlightFollowsCurrentItem: true
            }
        }

        Column {
            id:infoColumn
            anchors.top: column.bottom
            anchors.left: module.left
            anchors.right: module.right
            anchors.leftMargin: 3
            spacing: 1
            property int targetWidth: module.width - 5
            Text {
                font.pointSize: 7
                smooth: true
                wrapMode: Text.NoWrap
                color: adapter.input ? "#005BE8" : "#1f7312"
                opacity: underSurvey
                text: "<b>Прошивка:</b>"
            }

            Text {
                id: firmwareText
                font.pointSize: 7
                smooth: true
                wrapMode: Text.NoWrap
                transform: Scale {
                    xScale: firmwareText.width > infoColumn.targetWidth ? infoColumn.targetWidth / firmwareText.width : 1
                    yScale: xScale
                }
                opacity: underSurvey
                text: if(adapter.deviceID != 0xff)
                          adapter.firmware
                      else
                          ""
            }
            Text {
                font.pointSize: 7
                smooth: true
                wrapMode: Text.NoWrap
                color: adapter.input ? "#005BE8" : "#1f7312"
                text: "<b>Обозначение:</b>"
                opacity: kynitext.text.length != 0
            }
            Text {
                id: kynitext
                font.pointSize: 7
                smooth: true
                wrapMode: Text.NoWrap
                transform: Scale {
                    xScale: kynitext.width > infoColumn.targetWidth ? infoColumn.targetWidth / kynitext.width : 1
                    yScale: xScale
                }
                text: if(adapter.deviceID != 0xff)
                          adapter.KYNI
                      else
                          ""
            }
            Text {
                id: nameText
                font.pointSize: 7
                smooth: true
                wrapMode: Text.NoWrap
                transform: Scale {
                    xScale: nameText.width > infoColumn.targetWidth ? infoColumn.targetWidth / nameText.width : 1
                    yScale: xScale
                }
                text: if(adapter.deviceID != 0xff)
                          adapter.name
                      else
                          ""
            }
        }
        Component {
           id: textInputComponent
           AOitem {
               visible: index < dataGrid.count - (!showDiagChan ? adapter.dchan : 0)
           }
        }
        Component {
           id: textComponent
           AIitem {
               visible: index < dataGrid.count - (!showDiagChan ? adapter.dchan : 0)
           }
        }
        Component {
            id: ledlabelComponent
            LedLabel {
                visible: index < dataGrid.count - (!showDiagChan ? adapter.dchan : 0)
            }
        }
    }
    states: [
        State {
            name: "valid"
            PropertyChanges {target: module; opacity: 1}
            PropertyChanges {target: background; opacity: 0}
        },
        State {
            name: "configureNeeded"
            PropertyChanges {target: module; opacity: 0.8}
            PropertyChanges {target: background; opacity: 1}
            PropertyChanges {target: columnConfigureNeeded; opacity: 1 }
        }

    ]
    transitions: Transition {
             NumberAnimation { properties: "opacity"; duration: 300 }
    }

}
