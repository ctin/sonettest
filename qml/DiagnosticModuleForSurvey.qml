import QtQuick 1.1
import CustomComponents 1.0
import "../desctopComponents/DesctopComponents"

Item {
    id: container
    DiagnosticModule {
        width: 805
        diagnosticModuleDevice: device
        transform: Scale {
            id: transformScale
            xScale: (container.width) / 805
            yScale: xScale
        }
    }
}
