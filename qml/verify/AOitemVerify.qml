import QtQuick 1.1
import "../"

Item {
    height: dataRow.height
    width: dataRow.width
    MouseArea {
        anchors.fill: parent
        onClicked: {
            dataGrid.currentIndex = index;
        }
    }
    Row {
        id: dataRow
        spacing: 2
        Image {
            id: radioButton
            height: textInput.height
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: (index == dataGrid.currentIndex ? ("../../content/RadioButtonON_PNG" + (enabled ? "" : "-disabled") + ".png") : "../../content/RadioButtonOFF_PNG.png")
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    dataGrid.currentIndex = index;
                }
            }
        }
        Text {
            objectName: "Num text"
            font.family: "Courier New"
            font.pointSize: fontPointSize
            text: (index + 1).toString() + ": "
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    dataGrid.currentIndex = index;
                }
            }
        }
        ValueInput {
            id: textInput
            min: adapter.displayMin
            max: adapter.displayMax
            valueToSet: userValue
            onNewValueRequest: device.setModelData(globalSlot, index, value, "userValue")
            displayValue: deviceValue
            pointSize: fontPointSize
            textLength: 5
        }
    }
}
