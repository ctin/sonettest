import QtQuick 1.1
import CustomComponents 1.0
import"../../desctopComponents/DesctopComponents"
import DesctopComponents 0.1
import "../"

VerifyItem {
    id: container
    property alias unit: agilent34401A.unit
    function getCurrentValue() {return agilent34401A.deviceValue}
    function connected()  { return agilent34401A.connected }

    Agilent34401A {
        id: agilent34401A
        onConnectedChanged: signal_connectedChanged(agilent34401A.connected)
    }

    function realToString(real) {
        return real < 1 ? real.toFixed(5) : real.toPrecision(5 + 1)
    }
    Image {
        anchors.fill: parent
        id: module
        smooth: true
        source: "../../content/DO16_s_1.png"

       Column {
            id: column
            spacing:  5
            anchors.fill: parent
            anchors.margins: margins
            VerifyItemText {
                id: moduleTypeText
                font.bold: true
                text: "Agilent 34401A"
            }

            VerifyItemText {
                text: "значение: "
                font.bold: true
            }
            Row {
                Text {
                    text: agilent34401A.connected ? realToString(agilent34401A.deviceValue) : "NaN"
                    font.pointSize: fontPointSize
                }
                Text {
                    text: unit
                    font.pointSize: fontPointSize
                }
            }
        }
    }
}
