import QtQuick 1.1

Text {
    font.pointSize: fontPointSize
    anchors.left: parent.left
    anchors.right: parent.right
    wrapMode: Text.Wrap
}
