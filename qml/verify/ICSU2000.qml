import QtQuick 1.1
import CustomComponents 1.0
import "../../desctopComponents/DesctopComponents"
import "../"
import DesctopComponents 0.1


VerifyItem{
    id: container
    function setCurrentRow(row) {
        currentRow = row
    }
    property ICSUdevice icsuDevice: deviceList.getICSUDevice()

    function getCurrentValue() {return icsuDevice.deviceValue}
    function connected()  { return icsuDevice.deviceAviable;}


    function setOutputValue(value) {
        if(value !== undefined)
        {
            icsuDevice.userValue = value
        }
    }
    Connections {
        target: icsuDevice
        onDeviceValueChanged: {
            textInput.displayValue = icsuDevice.deviceValue
            textInput.startAnimation()
        }
        onConnectedChanged: signal_connectedChanged(icsuDevice.connected)
    }

    function getMaximum() {
        if(icsuDevice.unit == "мА")
            return 25
        else if(icsuDevice.unit == "мВ")
            return 100
        else if(icsuDevice.unit == "В")
            return 12
    }

    Image {
        objectName: "mainImage"
        anchors.fill: parent
        id: module
        smooth: true
        source: "../../content/DO16_s_1.png"

        Column {
            id: column
            spacing: 5
            anchors.fill: parent
            anchors.margins: margins
            VerifyItemText {
                id: moduleTypeText
                font.bold: true
                text: "Калибратор ИКСУ-2000"
            }

            VerifyItemText {
                id: rangeInfoText
                text: "0 - " + getMaximum() + icsuDevice.unit;
            }

            VerifyItemText {
                text: "значение: "
                font.bold: true
            }
            ValueInput {
                id: textInput
                min: 0
                max: getMaximum()
                valueToSet: icsuDevice.userValue
                onNewValueRequest: {
                    if(!icsuDevice.readOnly)
                        icsuDevice.userValue = value
                }
                displayValue: icsuDevice.deviceValue
                pointSize: fontPointSize
                textLength: 5
                readOnly: icsuDevice.readOnly
                isCode: false
            }
        }
        Column {
            id: columnData
            spacing: 5
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: margins
            VerifyItemText {
                text: "Режим:"
                font.bold: true
            }
            VerifyItemText {
                text: icsuDevice.readOnly ? "измерение" : "эмуляция"
            }
            VerifyItemText {
                text: "Состояние:"
                font.bold: true
            }
            VerifyItemText {
                id: infoText
                text: icsuDevice.currentState
            }

            ProgressBar {
                id: progressbar1
                value: icsuDevice.progress
                maximumValue: 100
                anchors.right: parent.right
                anchors.left: parent.left
                height: 30
                Rectangle {
                    id: rect_searchStatus
                    color: icsuDevice.hardwareError.length ? "red" : "transparent"
                    anchors.fill: parent
                    opacity: 0.15
                }
                Text {
                    id: text_searchStatus
                    anchors.fill: parent
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    font.pointSize: 11
                    text: (icsuDevice.hardwareError.length ? icsuDevice.port.replace("\\\\.\\", "") + ": нельзя открыть" : "")
                }
            }
            VerifyItemText {
                id: text1
                text: "<b>порт:</b> " + icsuDevice.port.replace("\\\\.\\", "")
            }
        }
    }
}
