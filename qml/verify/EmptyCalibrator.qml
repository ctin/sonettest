import QtQuick 1.1
import "../"
VerifyItem{
    id: container
    function connected() {return true}

    property real currentValue
    function getCurrentValue() {return 0}

    onCurrentValueChanged: {
        valueText.text = "Установите значение: <BR><b>" + (currentValue < 1 ? currentValue.toFixed(4) : currentValue.toPrecision(5))
        + " " + unit + "</b>"
     }
    function setOutputValue(value) {
        if(value === undefined)
            return;
        currentValue = value;
    }
    property string unit: "NAN"
    Image {
        objectName: "mainImage"
        anchors.fill: parent
        id: module
        smooth: true
        source: "../../content/DO16_s_1.png"
        Column {
            objectName: "columnData"
            id: column
            spacing: 5
            anchors.fill: parent
            anchors.margins: margins
            VerifyItemText {
                id: moduleTypeText
                font.bold: true
                text: "Внешний калибратор"
            }

            VerifyItemText {
                id: valueText
                text: "Ожидаю команды"
            }
        }
    }
}
