import QtQuick 1.1
import CustomComponents 1.0
import"../../desctopComponents/DesctopComponents"
import DesctopComponents 0.1
import "../"

VerifyItem {
    id: container
    property alias unit: agilent34410A.unit
    function getCurrentValue() {return agilent34410A.currentValue}
    function connected()  { return agilent34410A.connected;}
    Timer {
        id: timer
        running: agilent34410A.connected
        interval: 100
        repeat: true
        onTriggered: agilent34410A.refreshValue()
    }
    Agilent34410A {
        id: agilent34410A
        Component.onCompleted: connectToDevice()
        onConnectedChanged: signal_connectedChanged(agilent34410A.connected)
    }

    function realToString(real) {
        return real < 1 ? real.toFixed(5) : real.toPrecision(5 + 1)
    }
    Image {
        anchors.fill: parent
        id: module
        smooth: true
        source: "../../content/DO16_s_1.png"

       Column {
            id: column
            spacing:  5
            anchors.fill: parent
            anchors.margins: margins
            VerifyItemText {
                id: moduleTypeText
                font.bold: true
                text: "Agilent 34410A"
            }
            VerifyItemText {
                text: "ip адрес: "
                font.bold: true
            }
            TextInput {
                id: text_input_ip
                text: agilent34410A.ip
                onTextChanged: agilent34410A.ip = text
                inputMask: "000.000.000.000;_"
                font.pointSize: fontPointSize
                anchors.left: parent.left
                anchors.right: parent.right
            }
            VerifyItemText {
                text: "порт: "
                font.bold: true
            }
            TextInput {
                id: text_input_port
                text: agilent34410A.port
                onTextChanged: agilent34410A.port = text
                inputMask: "9999;_"
                font.pointSize: fontPointSize

            }
            VerifyItemText {
                text: "значение: "
                font.bold: true
            }
            Row {
                Text {
                    text: agilent34410A.connected ? realToString(agilent34410A.currentValue) : "NaN"
                    font.pointSize: fontPointSize
                }
                Text {
                    text: unit
                    font.pointSize: fontPointSize
                }
            }
        }

        VerifyItemText {
            anchors.bottom: button_connect.top
            anchors.left: button_connect.left
            text: "Статус: " + agilent34410A.state
            font.bold: true
        }

        Button {
            id: button_connect
            y: 527
            text: "Подключить"
            opacity: agilent34410A.state === "Не подключен"
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            onClicked: agilent34410A.connectToDevice()
        }
    }
}
