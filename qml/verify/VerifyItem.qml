import QtQuick 1.1

Item {
    signal signal_connectedChanged(bool connected)
    opacity: 0
    Component.onCompleted: opacity = 1
    Behavior on opacity {
        NumberAnimation {duration: 300}
    }
    property int fontPointSize: 16
    property int margins: 10
    width: 180
    height: 518
}
