import QtQuick 1.1
import CustomComponents 1.0
import "../../desctopComponents/DesctopComponents"
import "../"

VerifyItem{
    id: container
    property bool showCodes: false
    property bool showEEPROMcodes: false
    property bool canShowCodes: false
    function connected()  { return device.deviceAviable;}
    Connections {
        target: device
        onDeviceAviableChanged: signal_connectedChanged(device.deviceAviable)
    }

    function getCurrentValue() {
        if(currentRow < 0)
            return 0
        if(!device.isSonet)
            return (24.0 * device.getModelData(globalSlot, currentRow, "deviceValue") / 0xf8)
        return (device.getModelData(globalSlot, currentRow, "deviceValue") / 0xffff
                *(adapter.displayMax - adapter.displayMin) + adapter.displayMin);
    }
    function setCurrentRow(row) {
        currentRow = row
        dataGrid.currentIndex = row
    }

    property int currentRow: dataGrid.currentIndex
    signal currentRowChanged(int currentIndex)
    function setOutputValue(value) {
        if(adapter.input)
            return;
        value = ((value - adapter.displayMin) / (adapter.displayMax - adapter.displayMin) * 0xffff).toFixed()
        for(var i = dataGrid.count;  i > 0;  i--) {
            device.setModelData(globalSlot, i - 1, (i - 1 == dataGrid.currentIndex ? value : 0), "userValue")
        }
    }
    ModuleQMLAdapter {
        id: adapter
        slot: globalSlot
        objectName: "moduleQMLAdapter" + globalSlot.toString()
        deviceID: device.deviceIDs[globalSlot]
        type: device.types[globalSlot]
        channelsCount:  device.channelsCount[globalSlot]
    }
    Image {
        objectName: "mainImage"
        anchors.fill: parent
        id: module
        smooth: true
        source: "../../content/DO16_s_1.png"
        Text {
            id: globalSlotText
            text: "слот № " + (globalSlot + 1)
            opacity: 0.15
            font.pointSize: 20
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.bottomMargin: 10
            verticalAlignment: Qt.AlignBottom
            horizontalAlignment: Qt.AlignRight
        }
        Text {
            id: idText
            text: "ID: " + "000000000".slice(0, 3 - adapter.deviceID.toString().length) + adapter.deviceID
            opacity: 0.15
            font.pointSize: 20
            anchors.bottom: globalSlotText.top
            anchors.right: globalSlotText.right
            verticalAlignment: Qt.AlignBottom
            horizontalAlignment: Qt.AlignRight
        }

        Column {
            objectName: "columnData"
            id: column
            spacing: 3
            anchors.fill: parent
            anchors.margins: margins
            property int targetWidth: module.width - 15
            VerifyItemText {
                id: moduleTypeText
                font.bold: true
                text: "Модуль " + (!device.isSonet ? "диагностики" : (adapter.input ? "ввода" : "вывода"))
            }

            VerifyItemText {
                font.pointSize: fontPointSize
                text: adapter.rangeInfo
            }

            Component {
                 id: highlight
                 Rectangle {
                     color: "lightsteelblue"; radius: 5
                     Behavior on x { NumberAnimation { duration: 500; easing.type: Easing.OutQuart } }
                     Behavior on y { NumberAnimation { duration: 500; easing.type: Easing.OutQuart } }
                 }
             }


            Row {
                opacity: showEEPROMcodes
                spacing: (device.isSonet ? 12 : 7)
                x: -5
                Text {
                    font.pointSize: fontPointSize
                    text: "№"
                    anchors.verticalCenter: parent.verticalCenter
                }
                Text {
                    font.pointSize: fontPointSize
                    text: (adapter.valid && !device.isSonet ? "Коды" : "мин.")
                }
                Text {
                    font.pointSize: fontPointSize
                    text: (adapter.valid && !device.isSonet ? "Значения" : "макс.")
                }
            }
            ListView {
                id: dataGrid
                interactive: false
                currentIndex: currentRow
                onCurrentIndexChanged: currentRowChanged(currentIndex)
                property bool eepromCodes: false
                boundsBehavior: Flickable.StopAtBounds
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: -7
                height: 200 + (device.isSonet ? 0 : 30)
                spacing: 2
                model: device.getDataModel(globalSlot)
                delegate: adapter.input ? textComponent : textInputComponent;
                highlight: highlight
                highlightFollowsCurrentItem: true
            }
            Text {
                font.pointSize: fontPointSize
                smooth: true
                wrapMode: Text.NoWrap
                color: adapter.input ? "#005BE8" : "#1A960D"
                text: "<b>Обозначение:</b>"
            }
            Text {
                id: kynitext
                font.pointSize: fontPointSize
                smooth: true
                wrapMode: Text.NoWrap
                transform: Scale {
                    xScale: kynitext.width > column.targetWidth ? column.targetWidth / kynitext.width : 1
                    yScale: xScale
                }
                text: adapter.KYNI
            }
            Text {
                id: nameText
                font.pointSize: fontPointSize
                smooth: true
                wrapMode: Text.NoWrap
                transform: Scale {
                    xScale: nameText.width > column.targetWidth ? column.targetWidth / nameText.width : 1
                    yScale: xScale
                }
                text: adapter.name
                opacity: device.isSonet
            }
            Text {
                id: warningAboutResetText
                font.pointSize: fontPointSize
                smooth: true
                wrapMode: Text.NoWrap
                color: "#FC0101"
                text: "требует перезагрузку"
                transform: Scale {
                    xScale: warningAboutResetText.width > column.targetWidth ? column.targetWidth / warningAboutResetText.width : 1
                    yScale: xScale
                }
                opacity: device.isOldModule(adapter.slot) && adapter.input && showEEPROMcodes
            }
            Button {
                text: "ок"
                onClicked: {
                    device.unsetLastError()
                    device.unsetHardwareError()
                    device.turnedOn = true
                }
                opacity: globalSlot >= 0 && (device.lastError.length || device.hardwareError.length || !device.turnedOn)
            }
        }
       Component {
          id: textInputComponent
          AOitemVerify {
              visible: index < dataGrid.count - adapter.dchan
          }
       }
       Component {
          id: textComponent
          AIitemVerify {
              visible: index < dataGrid.count - adapter.dchan
          }
       }
       Component {
           id: ledlabelComponent
           LedLabel {
               visible: index < dataGrid.count - adapter.dchan
           }
       }
       CheckBox {
           height: 40
           anchors.left: column.left
           anchors.bottom: idText.top
           anchors.bottomMargin: -5
           opacity: adapter.input && canShowCodes

           text: " Показывать\n коды EEPROM"
           pointSize: fontPointSize * 0.8
           onCheckedChanged: {
               showEEPROMcodes = checked
           }
           checked: showEEPROMcodes
       }
       Column {
           id: ledColumn
           anchors.bottomMargin: 10
           anchors.leftMargin: 10
           anchors.left: parent.left
           anchors.bottom: parent.bottom
           Led {
               on: device.turnedOn && device.deviceAviable && device.hardwareError.length === 0
               height: 15; width: height
           }
           Led
           {
               on: device.isOpen
               type: "Yellow"
               height: 15; width: height
           }
           Led
           {
               type: "Red"
               on: device.hardwareError.length > 0
               height: 15; width: height
           }
       }
    }

}
