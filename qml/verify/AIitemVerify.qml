import QtQuick 1.1
import CustomComponents 1.0
import "../"

Item{
    id: dataComponent
    height: showEEPROMcodes ? eepromRow.height : dataRow.height
    width: showEEPROMcodes ? eepromRow.width : dataRow.width
    opacity: (!device.isSonet ? index < 2 : 1)
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: {
            dataGrid.currentIndex = index;
        }
    }

    Row {
        spacing: 2
        id: dataRow
        opacity: !showEEPROMcodes
        Image {
            height: textValue.height
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: (index == dataGrid.currentIndex ? "../../content/RadioButtonON_PNG.png" : "../../content/RadioButtonOFF_PNG.png")
        }

        Text {
            objectName: "Num text"
            font.family: "Courier New"
            font.pointSize: fontPointSize
            text: (index + 1).toString() + ":"
        }
        Text {
            id: textValue
            font.family: "Courier New"
            font.pointSize: fontPointSize
            text: showCodes ? adapter.toHex(deviceValue) : device.isSonet ? adapter.toReal(deviceValue) : adapter.toRealDiagModule(deviceValue)
        }
        Image {
            width: textValue.height
            height: width
            smooth: true
            source: "../../content/" + (isEmpty ? "remove"
                                             : (!isValid ? "alert"
                                                         : "ok")) + ".png";
        }
    }
    Row {
        id:  eepromRow
        opacity: showEEPROMcodes
        spacing: 5
        Text {
            objectName: "Num text"
            font.family: "Courier New"
            anchors.verticalCenterOffset: 0
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: fontPointSize
            text: (index + 1).toString() + ":"
        }
        Grid {
            id: eepromValuesGrid
            spacing: 5
            columns: 2
            property int channel: index
            Repeater {
                model: device.isSonet ? 2 : 8
                ValueInput {
                    id: textInput1
                    valueToSet: userEEPROMValues[index]
                    onNewValueRequest: device.setEEPROMUserValue(globalSlot, eepromValuesGrid.channel, index, value)
                    displayValue: deviceEEPROMValues[index]
                    showHex: true
                    pointSize: fontPointSize
                }
            }
        }
        Image {
            x: -eepromRow.spacing
            width: textValue.height
            height: width
            smooth: true
            anchors.verticalCenter: parent.verticalCenter
            source: "../../content/" + (isEmpty ? "remove"
                                             : (!isValid ? "alert"
                                                         : "ok")) + ".png";
        }
    }
}
