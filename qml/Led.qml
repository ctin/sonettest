import QtQuick 1.1

Rectangle {
    id: container
    width: 90
    height: 83
    property string type: "Green"
    property bool on: false
    color: "transparent"
    Image {
        id: turnedOnImage
        anchors.fill: parent
        smooth: true
        source: "../content/diod" + type + (on ? "On" : "Off") + ".png"
    }
}
