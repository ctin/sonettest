import QtQuick 1.1
import CustomComponents 1.0
import "../desctopComponents/DesctopComponents"

GridView {
    id: dataGrid1
    property alias slot: adapter.slot
    ModuleQMLAdapter {
        id: adapter
        objectName: "moduleQMLAdapter" + slot.toString()
        deviceID: diagnosticModuleDevice.deviceIDs[slot]
        type: diagnosticModuleDevice.types[slot]
        channelsCount:  diagnosticModuleDevice.channelsCount[slot]
    }
    currentIndex: -1
    interactive: false
    width: cellWidth * 4
    height: cellHeight * 2
    boundsBehavior: Flickable.StopAtBounds
    anchors.margins: 15
    cellWidth: 30
    cellHeight: 30
    onCountChanged: {
        positionViewAtIndex(2, GridView.Beginning)
    }

    model: diagnosticModuleDevice.channelsCount[slot] !== 8 ? 8 :diagnosticModuleDevice.getDataModel(slot)
    delegate: Item {
        id: item
        visible: index < dataGrid1.count
        property int num: index + (index < 4 ? index + 1 : index - 8)
        width: 30
        height: 30

        Led {
            on: diagnosticModuleDevice.channelsCount[slot] !== 8 ? false: deviceValue
            id: led
            x: 10
            width: 20
            height: 20
        }

        Text {
            id: text
            text: num + 1 + (slot - 1) * 8 // приводим реальные данныые к отображаемому виду
            anchors.bottom: parent.bottom
            anchors.right: led.left
            anchors.bottomMargin: 3
            anchors.rightMargin: -7
            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 13
        }
    }
}
