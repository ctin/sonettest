#ifndef SONETDATAVIEW_H
#define SONETDATAVIEW_H

#include <QtGui>
#include "objects.h"

class SonetDataView : public QWidget
{
    Q_OBJECT
public:
    SonetDataView(QWidget *parent = 0);
    QGridLayout *m_pGridLayout;
    QComboBox   *m_pComboBoxID, *m_pComboBoxFreeID, *m_pComboBoxKYNI, *m_pComboBoxName;
    QPushButton *m_pPushButtonSave, *m_pPushButtonSaveToIni, *m_pPushButtonAddDevice;
    QTableWidget    *m_pTableContent;
    void createTable();
    void placeCofig(const DeviceParameters &p);
    DeviceParameters getData() const;
protected:
    void resizeEvent(QResizeEvent *);
private slots:
    void addNewParam(QString text);
    void changeID(QString);
signals:
    void IDChanged(int);
private:
    QString fromDataToView(QVariant data);
    QStringList m_rows;

};


#endif // SONETDATAVIEW_H
