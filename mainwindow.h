#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"

class MainWindow : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
public slots:
    void onDeviceListLengthChanged(int length);
    void readSettings();
private slots:
    void about_triggered();
    void writeSettings();
protected:
    void closeEvent(QCloseEvent *);
};

#endif // MAINWINDOW_H
