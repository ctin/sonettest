#ifndef SONETDATA_H
#define SONETDATA_H

#include <QtCore>
#include "sonetdataview.h"


class SonetData : public QObject
{
    Q_OBJECT
public:
    explicit SonetData(QObject *parent = 0);
    SonetDataView   *m_pView;

    void loadData();
    void loadDataFromIni(const QString&);
    void saveDataToIni(const QString&) const;
signals:
    void dataLoaded();
public slots:
    void saveTableData();
    void saveTableDataToIni() ;
    void saveData();
    void loadDataFromIni();
    void saveDataToIni() const;
private slots:
    void on_comboboxIDActivated(int);
private:
    void updateView();
    void updateData();
};

#endif // SONETDATA_H
