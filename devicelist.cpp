#include "devicelist.h"
#include "devicewithdata.h"
#include <QtConcurrent/QtConcurrent>

DeviceList::DeviceList(QObject *parent) :
    QAbstractListModel(parent)
{    
    m_pICSU_2000 = new ICSU_device(this);
    m_pSearchDevice = new SearchDevice(this);
    m_pSurveyPort   = new SerialPort(this);
    m_roleNames.insert(DeviceElementRole, "deviceElement");
    connect(this, SIGNAL(lengthChanged(int)), this, SLOT(onLengthChanged(int)), Qt::DirectConnection);
    m_pDeviceListMutex = new QMutex();
    m_pCurrentCollectedDevice = 0;
    m_icsu_enabled = false;
    setnextCollectedIndex(-1);
    setcurrentIndex(-1);
    settargetDeviceIndex(Neither);
}

DeviceList::~DeviceList()
{
    stop();
}

QHash<int, QByteArray> DeviceList::roleNames() const
{
    return m_roleNames;
}

void DeviceList::stop()
{
    m_running = false;
    m_future.waitForFinished();
    m_pSearchDevice->close();
}

void DeviceList::emitAllGUISignals()
{
    for(QList <DeviceWithData*>::ConstIterator iter = m_deviceList.constBegin();  iter != m_deviceList.constEnd();  iter++)
    {
        (*iter)->emitAllGUISignals();
    }
}

bool DeviceList::isDublicate(DeviceWithData *pDevice)
{
    for(QList <DeviceWithData*>::ConstIterator iter = m_deviceList.constBegin();  iter != m_deviceList.constEnd();  iter++)
    {
        DeviceWithData *iterDevice = *iter;
        if(iterDevice->deviceAviable())
            if(iterDevice->componentID() != pDevice->componentID())
                if(iterDevice->deviceNum() == pDevice->deviceNum()
                    && iterDevice->port() == pDevice->port())
                    return true;
    }
    return false;
}

QList <DeviceWithData*> *DeviceList::getDeviceList()
{
    return &m_deviceList;
}

bool DeviceList::isDublicate(const int deviceNum, const QString port)
{
    QMutexLocker lock(m_pDeviceListMutex);
    for(QList <DeviceWithData*>::ConstIterator iter = m_deviceList.constBegin();  iter != m_deviceList.constEnd();  iter++)
    {
        DeviceWithData *iterDevice = *iter;
        if(iterDevice->deviceAviable())
            if(iterDevice->deviceNum() == deviceNum
                && iterDevice->port() == port)
                return true;
    }
    return false;
}


SearchDevice* DeviceList::getSearchDevice() const
{
    return m_pSearchDevice;
}

ICSU_device* DeviceList::getICSUDevice() const
{
    return m_pICSU_2000;
}

SerialPort* DeviceList::getSurveyPort() const
{
    return m_pSurveyPort;
}

DeviceWithData *DeviceList::getcurrentCollectedDevice() const
{
    return m_pCurrentCollectedDevice;
}

void DeviceList::start()
{
    if(m_future.isRunning())
        return;
    m_future = QtConcurrent::run(this, &DeviceList::__start);
}

void DeviceList::__collectDevice(int pos)
{
    if(m_targetDeviceIndex == All || pos == m_targetDeviceIndex)
    {
        m_pCurrentCollectedDevice = getDevice(pos);
        if(!m_pCurrentCollectedDevice->turnedOn() || !m_pCurrentCollectedDevice->deviceNum())
        {
            m_pCurrentCollectedDevice = 0;
            return;
        }
        m_pCurrentCollectedDevice->initPort();
        if(m_pSurveyPort->port() != m_pCurrentCollectedDevice->port()
                || m_pSurveyPort->rateIndex() != m_pCurrentCollectedDevice->rateIndex())
        {
            m_pSurveyPort->close();
            m_pSurveyPort->setport(m_pCurrentCollectedDevice->port());
            m_pSurveyPort->setrateIndex(m_pCurrentCollectedDevice->rateIndex());
            m_pSurveyPort->open();
        }
        if(!m_pSurveyPort->isOpen())
            m_pSurveyPort->open();
        if(m_pSurveyPort->isOpen())
        {
            m_pCurrentCollectedDevice->setOtherHandle(m_pSurveyPort);
            m_pCurrentCollectedDevice->collectControllerDataWrapper();
            if(m_pCurrentCollectedDevice->deleteMe())
            {
                delete m_pCurrentCollectedDevice;
            }
        }
        else
        {
            m_pCurrentCollectedDevice->sethardwareError(m_pSurveyPort->hardwareError());
            m_pCurrentCollectedDevice->setturnedOn(false);
            m_pCurrentCollectedDevice->toNextPort();
        }
        m_pCurrentCollectedDevice = 0;
    }
}

struct Counter {
    Counter() : len(0), pos(0), currentPos(0) {}
    volatile int len;
    volatile int pos;
    volatile int currentPos;
    bool validPos(int arg) {return arg >= 0 && arg < len; }
    void refreshPos() {
        while(len > 1)
        {
            if(!validPos(++pos))
                pos = 0;
            if(pos != currentPos)
                return;
        }
    }
};

void DeviceList::__start()
{
    m_running = true;
    Counter counter;
    bool toggler = true;
    while(m_running)
    {
        Sleepy::msleep(50);
        if(m_pSearchDevice->m_requestToStart)
        {
            m_pSurveyPort->close();
            m_pSearchDevice->findAllProcess();
        }
        if(m_icsu_enabled)
        {
            m_pSurveyPort->close();
            m_pICSU_2000->__refreshState();
        }
        counter.len = length();
        if(counter.len == 0 || targetDeviceIndex() == Neither)
        {
            counter.pos = 0;
            continue;
        }
        else
        {
            QMutexLocker lock(m_pDeviceListMutex);
            while(counter.validPos(nextCollectedIndex())) //для нажатых лампочек и пр.
            {
                int tmpInex = nextCollectedIndex();
                setnextCollectedIndex(-1);
                __collectDevice(tmpInex);
            }
            if(toggler)
            {
                counter.currentPos = currentIndex();
                if(counter.validPos(counter.currentPos))
                    __collectDevice(currentIndex());
            }
            else
            {
                counter.refreshPos();
                __collectDevice(counter.pos);
                if(counter.pos == counter.len - 1) //если последний
                    m_pSurveyPort->close();
            }
            toggler = !toggler;
        }
    }
    m_pCurrentCollectedDevice = 0;
    m_pSurveyPort->close();
    m_running = false;
}

void DeviceList::onLengthChanged(int length)
{
    if(currentIndex() < 0 && length > 0)
        setcurrentIndex(0);
    if(currentIndex() >= length)
        setcurrentIndex(length - 1);
}

int DeviceList::length() const
{
    return m_deviceList.size();
}

void DeviceList::clearToDelete()
{
    m_running = false;
    if(m_pCurrentCollectedDevice)
        m_pCurrentCollectedDevice->setrunning(false);
    m_pSearchDevice->setrunning(false);
    m_pICSU_2000->setrunning(false);
    settargetDeviceIndex(Neither);
    saveList();
    clear();
}

void DeviceList::clear()
{
    removeRows(0, rowCount());
}

void DeviceList::removeCurrent(int index)
{
    removeRow(index);
}

bool DeviceList::addNew()
{
    return insertRow(rowCount());
}

void DeviceList::createAndAdd(bool deviceAviable, int pos, int rateIndex, int deviceNum, int autoSearch, QString port)
{
    DeviceWithData *pDeviceWithData = new DeviceWithData(this, rateIndex, deviceNum, autoSearch, port);
    pDeviceWithData->setcomponentID(length());
    pDeviceWithData->setdeviceAviable(deviceAviable);
    {
        QMutexLocker lock(m_pDeviceListMutex);
        m_deviceList.insert(pos, pDeviceWithData);
    }
}

void DeviceList::append(bool deviceAviable, int rateIndex, int deviceNum, int autoSearch, QString port)
{
    if(isDublicate(deviceNum, port))
        return;
    int len = length();
    beginInsertRows(QModelIndex(), len, len);
    createAndAdd(deviceAviable, length(), rateIndex, deviceNum, autoSearch, port);
    endInsertRows();
    emit lengthChanged(length());
}

DeviceWithData *DeviceList::getDevice(int index) const
{
    if(index < 0 || index >= length())
    {
        qWarning() << "index failure!" << index << length();
    }
    Q_ASSERT(index >= 0);
    Q_ASSERT(index < length());
    return m_deviceList.at(index);
}

DeviceWithData *DeviceList::getDeviceByNum(int deviceNum) const
{
    for(QList <DeviceWithData*>::ConstIterator iter = m_deviceList.constBegin();  iter != m_deviceList.constEnd();  iter++)
    {
        if((*iter)->deviceNum() == deviceNum)
            return *iter;
    }
    return 0;
}

QVariant DeviceList::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= rowCount())
        return QVariant();
    if(role == DeviceElementRole)
        return QVariant::fromValue(getDevice(index.row()));

    return QVariant();
}

int DeviceList::rowCount(const QModelIndex &/*parent*/) const
{
    return length();
}

bool DeviceList::insertRows(int row, int count, const QModelIndex &parent)
{
    if(!count)
        return false;
    beginInsertRows(parent, row, row + count - 1);
    for(int i = 0;  i < count;  i++)
    {
        createAndAdd(false, row + i, 0, 0, true, " ");
    }
    endInsertRows();
    emit lengthChanged(length());
    return true;
}

bool DeviceList::removeRows(int row, int count, const QModelIndex &parent)
{
    if(!count)
        return false;
    int len = length();
    if(!len || len < row + count)
        return false;
    m_pSearchDevice->close();
    int lastTarget = targetDeviceIndex();
    settargetDeviceIndex(Neither); //останавливает процесс
    beginRemoveRows(parent, row, row + count - 1);
    {
        QMutexLocker lock(m_pDeviceListMutex);
        for(int i = 0;  i < count;  i++)
        {
            Q_ASSERT(row >= 0 && row < m_deviceList.length());
            DeviceWithData *pDevice = m_deviceList.takeAt(row);
            pDevice->blockSignals(true);
            pDevice->setdeleteMe(true);
        }
    }
    len = length();
    for(int i = len - 1;  i >= row;  i--)
    {
        getDevice(i)->setcomponentID(i);
    }
    endRemoveRows();
    emit lengthChanged(len);
    settargetDeviceIndex(lastTarget);
    return true;
}

void DeviceList::saveList()
{
    QSettings settings;
    QList<QVariant> devices;
    foreach(DeviceWithData *pDevice, m_deviceList)
        if(pDevice)
            devices.append(QVariant::fromValue(SonetConfig(pDevice->rateIndex(), pDevice->deviceNum(), pDevice->autoSearch(), pDevice->port())));
    QVariant var;
    var.setValue(devices);
    settings.setValue("devices", var);
}

bool DeviceList::loadList()
{
    QSettings settings;
    if(settings.allKeys().contains("devices"))
    {
        QList<QVariant> devices = settings.value("devices").toList();
        clear();
        for(QList<QVariant>::Iterator iter = devices.begin();  iter != devices.end();  ++iter)
        {
            if(!(*iter).canConvert<SonetConfig>())
                continue;
            SonetConfig config = (*iter).value<SonetConfig>();
            append(false, config.rateIndex, config.deviceNum, config.autoSearch, config.port);
        }
    }
    QMutexLocker lock(m_pDeviceListMutex);
    for(QList<DeviceWithData*>::Iterator iter = m_deviceList.begin();  iter != m_deviceList.end();  iter++)
        if(*iter)
            (*iter)->setdeviceAviable(false);
    return length();
}

void DeviceList::setStopBitsForAll(double stopBits)
{
    QMutexLocker lock(m_pDeviceListMutex);
    for(QList<DeviceWithData*>::Iterator iter = m_deviceList.begin();  iter != m_deviceList.end();  iter++)
        if(*iter)
            (*iter)->setstopBits(stopBits);
    m_pSearchDevice->setstopBits(stopBits);
}

void DeviceList::setDataBitsForAll(int dataBits)
{
    QMutexLocker lock(m_pDeviceListMutex);
    for(QList<DeviceWithData*>::Iterator iter = m_deviceList.begin();  iter != m_deviceList.end();  iter++)
        if(*iter)
            (*iter)->setdataBits(dataBits);
    m_pSearchDevice->setdataBits(dataBits);
}

void DeviceList::setFlowIndexForAll(int flowIndex)
{
    QMutexLocker lock(m_pDeviceListMutex);
    for(QList<DeviceWithData*>::Iterator iter = m_deviceList.begin();  iter != m_deviceList.end();  iter++)
        if(*iter)
            (*iter)->setflowIndex(flowIndex);
    m_pSearchDevice->setflowIndex(flowIndex);
}

void DeviceList::setParityIndexForAll(int parityIndex)
{
    QMutexLocker lock(m_pDeviceListMutex);
    for(QList<DeviceWithData*>::Iterator iter = m_deviceList.begin();  iter != m_deviceList.end();  iter++)
        if(*iter)
            (*iter)->setparityIndex(parityIndex);
    m_pSearchDevice->setparityIndex(parityIndex);
}

void DeviceList::setRateIndexForAll(int rateIndex)
{
    QMutexLocker lock(m_pDeviceListMutex);
    for(QList<DeviceWithData*>::Iterator iter = m_deviceList.begin();  iter != m_deviceList.end();  iter++)
        if(*iter)
            (*iter)->setrateIndex(rateIndex);
    m_pSearchDevice->setrateIndex(rateIndex);
}

void DeviceList::setPortForAll(int port)
{
    QMutexLocker lock(m_pDeviceListMutex);
    for(QList<DeviceWithData*>::Iterator iter = m_deviceList.begin();  iter != m_deviceList.end();  iter++)
        if(*iter)
            (*iter)->setport(QString("COM%1").arg(port));
    m_pSearchDevice->setport(QString("COM%1").arg(port));
}

int DeviceList::currentIndex() const
{
    return m_currentIndex;
}

void DeviceList::setcurrentIndex(const int currentIndex)
{
    if(m_currentIndex == currentIndex)
        return;
    m_currentIndex = currentIndex;
    emit currentIndexChanged(currentIndex);
}

int DeviceList::targetDeviceIndex() const
{
    return m_targetDeviceIndex;
}

void DeviceList::settargetDeviceIndex(int arg)
{
    if (m_targetDeviceIndex != arg) {
        int len = length();
        if(m_targetDeviceIndex > len)
            m_targetDeviceIndex = All;
        m_targetDeviceIndex = arg;
        stop();
        if(m_targetDeviceIndex != Neither)
            start();
        /*if(arg == Neither)
        {
            stop();
        }
        else
        {
            start();
        }*/
        emit targetDeviceIndexChanged(arg);
    }
}

int DeviceList::nextCollectedIndex() const
{
    return m_nextCollectedIndex;
}

void DeviceList::setnextCollectedIndex(int arg)
{
    if (m_nextCollectedIndex != arg) {
        m_nextCollectedIndex = arg;
        emit nextCollectedIndexChanged(arg);
    }
}
