﻿#include "peripheral.h"
#include <QtCore>
#include <QtGui>
#include "windows.h"
#include "dbt.h"
#include "core.h"
#include "modbusdevice.h"

class CAutoHeapAlloc
{
public:
//Constructors / Destructors
  CAutoHeapAlloc(HANDLE hHeap = GetProcessHeap(), DWORD dwHeapFreeFlags = 0) : m_pData(NULL),
                                                                               m_hHeap(hHeap),
                                                                               m_dwHeapFreeFlags(dwHeapFreeFlags)
  {
  }

  BOOL Allocate(SIZE_T dwBytes, DWORD dwFlags = 0)
  {
    //Validate our parameters
    Q_ASSERT(m_pData == NULL);

    m_pData = HeapAlloc(m_hHeap, dwFlags, dwBytes);
    return (m_pData != NULL);
  }

  ~CAutoHeapAlloc()
  {
    if (m_pData != NULL)
    {
      HeapFree(m_hHeap, m_dwHeapFreeFlags, m_pData);
      m_pData = NULL;
    }
  }

//Methods

//Member variables
  LPVOID m_pData;
  HANDLE m_hHeap;
  DWORD  m_dwHeapFreeFlags;
};

AntiLockTimer::AntiLockTimer(QObject *parent)
    : QTimer(parent)
{
    setInterval(1000);
    setSingleShot(false);
    connect(this, SIGNAL(timeout()), this, SLOT(onTimeOut()));
}

void AntiLockTimer::startAntiLockTimer(int timeLimit)
{
    static QMutex mutex;
    QMutexLocker lock(&mutex);
    m_deathCounter = timeLimit;
    m_isRunning = true;
}

void AntiLockTimer::stopAntiLockTimer()
{
    static QMutex mutex;
    QMutexLocker lock(&mutex);
    m_deathCounter = ANTILOCKTIME;
    m_isRunning = false;
}

void AntiLockTimer::onTimeOut()
{
    if(!m_isRunning)
    {
        m_deathCounter = ANTILOCKTIME;
        return;
    }
    if(m_deathCounter-- > 0)
        return;
    Core *pCore = Core::instance();
    if(pCore && qApp)
    {
        if(pCore->m_pDeviceList && pCore->m_pDeviceList->getICSUDevice() && pCore->m_pDeviceList->getSearchDevice())
        {
            QString port;
            if(pCore->m_pDeviceList->getSearchDevice()->running())
                port = pCore->m_pDeviceList->getSearchDevice()->port();
            else if(pCore->m_pDeviceList->getICSUDevice()->running())
                port = pCore->m_pDeviceList->getICSUDevice()->port();
            else
                port = pCore->m_pDeviceList->getSurveyPort()->port();

            QMessageBox msgBox(qApp->activeWindow());
            msgBox.setWindowTitle("Критическая ошибка");
            msgBox.setText(QString("Критическая ошибка! ОС заблокировала работу програмы по причине неисправности порта %1.").arg(port));
            msgBox.setDetailedText("Операционная система зависла при вызове системной функции, продолжение работы невозможно. Рекомендуется удалить COM порт из диспетчера устройств перед повторным запуском.");
            msgBox.exec();
        }
    }
    //qFatal("Критическая ошибка! ОС заблокировала работу програмы по причине неисправности порта");
    ((void(*)(void))0)(); //очень сильное колдунство.
}

QStringList getAllPorts()
{
      //What will be the return value from this function (assume the worst)
    QStringList currentAvilablePorts;

    //Determine what OS we are running on
    OSVERSIONINFO osvi;
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    BOOL bGetVer = GetVersionEx(&osvi);

    //On NT use the QueryDosDevice API
    if (bGetVer && (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT))
    {
        //Use QueryDosDevice to look for all devices of the form COMx. Since QueryDosDevice does
        //not consitently report the required size of buffer, lets start with a reasonable buffer size
        //of 4096 characters and go from there
        int nChars = 0xffff;
        BOOL bWantStop = FALSE;
        while (nChars && !bWantStop)
        {
            CAutoHeapAlloc devices;
            if (devices.Allocate(nChars * sizeof(TCHAR)))
            {
                LPTSTR pszDevices = static_cast<LPTSTR>(devices.m_pData);
                DWORD dwChars = QueryDosDevice(NULL, pszDevices, nChars);
                if (dwChars == 0)
                {
                    DWORD dwError = GetLastError();
                    if (dwError == ERROR_INSUFFICIENT_BUFFER)
                    {
                        //Expand the buffer and  loop around again
                        nChars *= 2;
                    }
                    else
                        bWantStop = TRUE;
                }
                else
                {
                    bWantStop = TRUE;
                    size_t i=0;

                    while (pszDevices[i] != '\0')
                    {
                        //Get the current device name
                        TCHAR* pszCurrentDevice = &(pszDevices[i]);

                        //If it looks like "COMX" then
                        //add it to the array which will be returned
                        QString str = QString::fromWCharArray(pszCurrentDevice);
                        size_t nLen = str.length();
                        if (nLen > 3)
                        {
                            if (str.contains(QRegExp("COM\\d")))
                            {
                              currentAvilablePorts.append(str);
                            }
                        }

                        //Go to next device name
                        i += (nLen + 1);
                    }
                }
            }
            else
            {
              bWantStop = TRUE;
              SetLastError(ERROR_OUTOFMEMORY);
            }
        }
    }
    else
        SetLastError(ERROR_CALL_NOT_IMPLEMENTED);

    return currentAvilablePorts;
}

class AbstractNativeEventFilter : public QAbstractNativeEventFilter
{
public:
   bool nativeEventFilter(const QByteArray &eventType, void *message, long *result)
   {
       Q_UNUSED(eventType);
       Q_UNUSED(result);
       if((static_cast<MSG*>(message))->message == WM_DEVICECHANGE)
       {
           setAvailablePorts(getAllPorts());
       }
       return false;
   }
};

Application:: Application(int &argc, char *argv[])
    : QApplication(argc, argv)
{
    m_pErrorBox = new QMessageBox(activeWindow());
    m_pErrorBox->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    m_pErrorBox->setMinimumWidth(320);
    m_pErrorBox->setIcon(QMessageBox::Question);
    m_pErrorBox->setWindowTitle("Ошибка!");
    m_refreshPortsTimer = new QTimer(this);
    installNativeEventFilter(new AbstractNativeEventFilter());
    connect(this, SIGNAL(aboutToQuit()), this, SLOT(beforeQuit()));
}

bool Application::showErrorMessage(const QString &title, const QString &text, const QString &details)
{
    m_pErrorBox->setText(title);
    m_pErrorBox->setInformativeText(text);
    m_pErrorBox->setDetailedText(details);
    return m_pErrorBox->exec() == QMessageBox::Yes;
}

void Application::beforeQuit()
{
    writeGlobalSettings();
}

bool Application::winEventFilter (MSG *message, long *result)
{
    Q_UNUSED(result);
    if(message->message == WM_DEVICECHANGE)
        setAvailablePorts(getAllPorts());
    return false;
}

void customMessageHandler(QtMsgType type, const char *msg)
{
    //if(type == QtWarningMsg || type == QtDebugMsg)
        //return;
    QString txt;
    txt = QString("%1 :").arg(QDateTime::currentDateTime().toString());
    switch (type) {
    case QtDebugMsg:
        txt += QString("Debug: %1").arg(msg);
        break;

    case QtWarningMsg:
        txt += QString("Warning: %1").arg(msg);
    break;
    case QtCriticalMsg:
        txt += QString("Critical: %1").arg(msg);
    break;
    case QtFatalMsg:
        txt += QString("Fatal: %1").arg(msg);
    break;
    }
    txt += "\r\n";

    QFile outFile("debuglog.txt");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << txt << endl;
    if(type == QtFatalMsg)
        abort();
    outFile.close();
}

bool startProcess()
{
    QSharedMemory *pSharedMemory = new QSharedMemory("SonetTest");
    const int size = sizeof(void*);
    return pSharedMemory->create(size, QSharedMemory::ReadWrite);
}
