﻿#include "dialogshowerrorlist.h"

using namespace TestWindows;

DialogShowErrorList::DialogShowErrorList(DeviceList *pDeviceList, QWidget *parent) :
    ParentWindow(pDeviceList, parent)
{
    m_pErrorListWidget = new QListWidget(this);
    setLayout(new QGridLayout());
    layout()->addWidget(m_pErrorListWidget);
    setstartFlags(SurveyNeither);
    m_isOnline = false;
}

bool DialogShowErrorList::beforeShow()
{
    setWindowTitle(QString("Просмотр лога ошибок контроллера №%1. Ошибок: №%2.").arg(deviceNum()).arg(m_pInputDevice->m_errorList.size()));
    ParentWindow::beforeShow();
    updateErrorList();
    connect(m_pInputDevice, SIGNAL(errorListChanged()), this, SLOT(updateErrorList()));
    resize(800, 600);
    return true;
}

void DialogShowErrorList::updateErrorList()
{
    m_pErrorListWidget->clear();
    for(QList <DeviceWithData*>::ConstIterator iter = m_pDeviceList->getDeviceList()->constBegin();
        iter != m_pDeviceList->getDeviceList()->constEnd();  iter++)
    {
        DeviceWithData *iterDevice = *iter;
        QListIterator<QString> errIter(iterDevice->m_errorList);
        errIter.toBack();
        while(errIter.hasPrevious())
        {
            m_pErrorListWidget->addItem(new QListWidgetItem(errIter.previous() + "\r\n"));
        }
    }
}

