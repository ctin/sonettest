#ifndef DIALOGREPLYTIME_H
#define DIALOGREPLYTIME_H

#include "ui_dialogreplytime.h"
#include "parentwindow.h"

namespace TestWindows {
class DialogReplyTime : public ParentWindow, private Ui::DialogReplyTime
{
    Q_OBJECT
    Q_PROPERTY(int min READ min WRITE setmin NOTIFY minChanged)
    Q_PROPERTY(int max READ max WRITE setmax NOTIFY maxChanged)
    Q_PROPERTY(int avg READ avg WRITE setavg NOTIFY avgChanged)
    Q_PROPERTY(int count READ count WRITE setcount NOTIFY countChanged)
    Q_PROPERTY(int notSent READ notSent WRITE setnotSent NOTIFY notSentChanged)
    Q_PROPERTY(int notReceived READ notReceived WRITE setnotReceived NOTIFY notReceivedChanged)
    Q_PROPERTY(int errors READ errors WRITE seterrors NOTIFY errorsChanged)
    Q_PROPERTY(int crcerrors READ crcerrors WRITE setcrcerrors NOTIFY crcerrorsChanged)

public:
    explicit DialogReplyTime(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogReplyTime();
    bool beforeShow();
protected:
    void __functionStart();
    void __precessRequest();
private slots:
    void on_pushButton_stop_clicked();
    void on_pushButton_start_clicked();
    void warning(QString title, QString text);
private:
    double m_allTime;
    //properties
public:         int min() const;
public slots:   void setmin(const int min);
signals:        void minChanged(int );
private:        int m_min;

public:         int max() const;
public slots:   void setmax(const int max);
signals:        void maxChanged(int max);
private:        int m_max;

public:         int avg() const;
public slots:   void setavg(const int avg);
signals:        void avgChanged(int avg);
private:        int m_avg;

public:         int count() const;
public slots:   void setcount(const int count);
signals:        void countChanged(int count);
private:        int m_count;

public:         int notSent() const;
public slots:   void setnotSent(const int notSent);
signals:        void notSentChanged(int notSent);
private:        int m_notSent;

public:         int notReceived() const;
public slots:   void setnotReceived(const int notReceived);
signals:        void notReceivedChanged(int notReceived);
private:        int m_notReceived;

public:         int errors() const;
public slots:   void seterrors(const int errors);
signals:        void errorsChanged(int errors);
private:        int m_errors;

public:         int crcerrors() const;
public slots:   void setcrcerrors(const int crcerrors);
signals:        void crcerrorsChanged(int crcerrors);
private:        int m_crcerrors;
};
}

#endif // DIALOGREPLYTIME_H
