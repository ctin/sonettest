﻿#include "dialogverifymodule.h"
#include "macro.h"
#include "dialogwatchtypes.h"
#include <QSettings>

static const int survey_frequrency = 5; //сколько раз в секунду
using namespace TestWindows;
static const QString verifyLastCalibrKey("VerifyLastCalibr");

DialogVerifyModule::DialogVerifyModule(const bool calibr, DeviceList *pDeviceList, QWidget *parent)
    : ParentWindow(pDeviceList, parent), m_calibr(calibr)
{
    setupUi(this);
    setVerifyType(InputCLIT);
    gridLayout->setMargin(5);

    setAutoRegimeParametersFromID();

    declarativeView_input->setStyleSheet("background-color: transparent");
    declarativeView_output->setStyleSheet("background-color: transparent");

    setLayout(gridLayout);
    setcurrentColumn(-1);
    setverifying(false);
    setautoRegime(false);

    tableWidget->setEditTriggers(QTableWidget::NoEditTriggers);
    tableWidget->setSelectionBehavior(QAbstractItemView::SelectItems);
    tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    connect(comboBox_calibrator, SIGNAL(activated(QString)), this, SLOT(refreshOutput(QString)));
    connect(comboBox_input, SIGNAL(activated(QString)), this, SLOT(refreshInput(QString)));
    connect(this, SIGNAL(channelChanged(int)), this, SLOT(selectCurrentCell()));
    connect(this, SIGNAL(channelChanged(int)), this, SLOT(onChannelChanged()));
    connect(this, SIGNAL(currentColumnChanged(int)), this, SLOT(selectCurrentCell()));
    connect(tableWidget, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(cellDoubleClicked(int,int)), Qt::QueuedConnection);
    m_pSurveyTimer = new QTimer(this);
    m_pSurveyTimer->setInterval(1000 / survey_frequrency);
    connect(m_pSurveyTimer, SIGNAL(timeout()), this, SLOT(collectData()));
    connect(pushButton_auto, SIGNAL(clicked()), this, SLOT(startAuto()));
    connect(pushButton_manual, SIGNAL(clicked()), this, SLOT(startManual()));
    connect(pushButton_write, SIGNAL(clicked()), this, SLOT(whenProcessCollectedData()));
    connect(pushButton_stop, SIGNAL(clicked()), this, SLOT(stopProcess()));
    connect(this, SIGNAL(verifyingChanged(bool)), this, SLOT(setControlButtonsEnabled()));
    connect(m_pDeviceList->getICSUDevice(), SIGNAL(finishRequest(qreal)), this, SLOT(onIcsuSetData(qreal)));

    progressBar_collecting->hide();

    m_pQMLObjectInput = 0;
    m_pQMLObjectOutput = 0;

    declarativeView_input->rootContext()->setContextProperty("deviceList", QVariant::fromValue(m_pDeviceList));
    declarativeView_output->rootContext()->setContextProperty("deviceList", QVariant::fromValue(m_pDeviceList));
    for(int slot = m_resultsCalibr.size();  slot < SLOTS_COUNT;  slot++)
    {
        m_resultsCalibr.append(ResultModule());
    }
    for(int slot = m_resultsVerify.size();  slot < SLOTS_COUNT;  slot++)
    {
        m_resultsVerify.append(ResultModule());
    }
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("DialogVerifyModule");
    resize(settings.value("size", QSize(800, 600)).toSize());
    move(settings.value("pos").toPoint());
    settings.endGroup();
}

void DialogVerifyModule::setVerifyType(VerifyType verifyType)
{
    m_verifyType = verifyType;
    QString title = "Поверка модулей ";
    title += (m_verifyType == InputOTK || m_verifyType == InputCLIT) ? "ввода" : "вывода";
    title += " ";
    title += (m_verifyType == InputOTK || m_verifyType == OutputOTK) ? "по ТУ" : "   ЦЛИТ";
    if(m_verifyType == InputOTK || m_verifyType == InputCLIT)
    {
        label->setText("Текущий калибратор");
        label_2->setText(m_calibr ? "Калибруемый модуль" : "Поверяемый модуль");
    }
    else
    {
        label->setText("Поверяемый модуль");
        label_2->setText("Измеритель");
    }
    setWindowTitle(title);
}

VerifyType DialogVerifyModule::getVerifyType() const
{
    return m_verifyType;
}

void DialogVerifyModule::closeEvent(QCloseEvent *)
{
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("DialogVerifyModule");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
}

ResultController &DialogVerifyModule::getCurrentResultsAddr()
{
    return m_calibr ? m_resultsCalibr : m_resultsVerify;
}

DialogVerifyModule::~DialogVerifyModule()
{
    QMetaObject::invokeMethod(m_pInputDevice, "createBaseSurveyFunctions", Qt::QueuedConnection);
    m_pDeviceList->m_icsu_enabled = false;
}

void DialogVerifyModule::clearChannelValues()
{
    m_channelValues.clear();
    pushButton_write->setEnabled(false);
}

bool DialogVerifyModule::addChannelValues(qreal value)
{
    m_channelValues.enqueue(value);
    int targetSize = timeToConfirm() * survey_frequrency;
    int valuesSize = m_channelValues.size();
    bool retval = !(valuesSize < targetSize);
    if(valuesSize < targetSize)
    {
        progressBar_collecting->setValue((valuesSize * 100.0) / targetSize);
    }
    else
    {
        progressBar_collecting->setFormat("Сбор и обработка информации: %p%");
        m_channelValues.dequeue();
    }
    if(!m_channelValues.isEmpty() && !autoRegime())
        pushButton_write->setEnabled(true);
    return retval;
}

const QQueue<qreal> *DialogVerifyModule::getChannelValues() const
{
    return &m_channelValues;
}

void DialogVerifyModule::onIcsuSetData(qreal value)
{
    Q_UNUSED(value);
    m_collectTimer.restart();
    clearChannelValues();
    progressBar_collecting->setFormat("Сбор информации: %p%");
}

bool DialogVerifyModule::beforeShow()
{
    return refreshAll();
}

bool DialogVerifyModule::refreshAll()
{
    if(verifying())
        stopProcess();
    m_pQMLObjectInput = 0;
    m_pQMLObjectOutput = 0;
    if(slot() < 0)
        setslot(getSlotNum());
    if(slot() < 0)
    {
        return false;
    }
    if(!m_pInputDevice->valid(slot()) || !m_pInputDevice->analog(slot()))
    {
        if(m_verifyType == InputCLIT || m_verifyType == InputOTK)
        {
            if(!m_pInputDevice->input(slot()))
            {
                setslot(getSlotNum());
                if(slot() < 0)
                    return false;
            }
        }
        else
        {
            if(m_pInputDevice->input(slot()))
            {
                setslot(getSlotNum());
                if(slot() < 0)
                    return false;
            }
        }
    }
    setchannelsCount(m_pInputDevice->channelsCount().at(slot()) - g_parameterStorage.getDchan(deviceID()));
    refreshTableContent();
    refreshOutputList();
    refreshOutput(comboBox_calibrator->itemText(comboBox_calibrator->currentIndex()));
    refreshInputList();
    refreshInput(comboBox_input->itemText(comboBox_input->currentIndex()));
    QMetaObject::invokeMethod(m_pInputDevice, "createCalibrSurveyFunctions", Qt::QueuedConnection);
    return true;
}

bool DialogVerifyModule::toNextColumn(bool checkNonValidData)
{
    QVector<qreal> points = g_parameterStorage.getPoints(deviceID());
    const ResultMap *pMap = &getCurrentResultsAddr().at(slot()).at(channel());
    for(int i = 0;  i < points.size();  i++)
        if(!pMap->contains(points.at(i))
            || (checkNonValidData && tableWidget->item(channel(), i)->textColor() == Qt::red))
        {
            setcurrentColumn(i);
            return true;
        }
    return false;
}

void DialogVerifyModule::initRegime(bool autoRegime)
{
    Q_ASSERT(slot() >= 0);
    Q_ASSERT(slot() < SLOTS_COUNT);
    if(channel() < 0)
        setchannel(0);
    if(m_calibr)
        clearChannelResults(slot(), channel());
    if(!toNextColumn())
    {
        if(QMessageBox::information(this, "Канал заполнен", "Канал заполнен! Очистить его и измерить ещё раз?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
        {
            clearChannelResults(slot(), channel());
            toNextColumn();
        }
        else
            return;
    }
    setverifying(true);
    setautoRegime(autoRegime);
    disableControls();
    if(m_pInputDevice)
        connect(m_pInputDevice, SIGNAL(deviceIDsChanged(QList<int>)), this, SLOT(refreshAll()));

    label_status->setText(QString("Запущена ") + QString(autoRegime ? "автоматическая " : "") + QString(m_calibr ? "калибровка" : "поверка") + QString(" ") + QString(autoRegime ? "" : "вручную"));
    m_pQMLObjectInput->setProperty("showEEPROMcodes", false);
    whenEnterRegime(); //init channel, clear results if needed
    m_pSurveyTimer->start();
    setColumnValue();
}

void DialogVerifyModule::startAuto()
{
    initRegime(true);
}

void DialogVerifyModule::startManual()
{
    initRegime(false);
}

void DialogVerifyModule::stopProcess()
{
    label_status->setText("Работа прервана.");
    m_pSurveyTimer->stop();
    unsetcurrentColumn();
    enableControls();
    whenLeaveRegime();
    setverifying(false);
}

void DialogVerifyModule::endProcess()
{
    label_status->setText("Работа выполнена!");
    m_pSurveyTimer->stop();
    unsetcurrentColumn();
    enableControls();
    whenFinishProcess();
    setverifying(false);
}

void DialogVerifyModule::collectData()
{
    QVariant retVal = 0;
    QMetaObject::invokeMethod(m_pQMLObjectInput, "getCurrentValue", Qt::DirectConnection, Q_RETURN_ARG(QVariant, retVal));
    if(m_pDeviceList->getICSUDevice()->userValue() != m_pDeviceList->getICSUDevice()->deviceValue())
    {
        progressBar_collecting->setFormat("Ожидаю установку значений на ИКСУ-2000");
        return;
    }
    int checkCollectedData = addChannelValues(retVal.toReal());
    whenProcessNewData();
    if(autoRegime() && checkCollectedData) //else wait for pushButton_write will be clicked
    {
        if(checkDisp())
        {
            if(whenProcessCollectedData())
                endProcess();
        }
        else if(m_collectTimer.elapsed() > (allowableTime() * 60 * 1000)) //если allowableTime == timeToConfirm
        {
            stopProcess();
            QMessageBox::information(this, "Прерывание процесса", QString("Ожидание стабильного значения превысило %1 мин., процесс остановлен!").arg(allowableTime()));
        }
    }
}

bool DialogVerifyModule::checkDisp()
{
    int valuesSize = getChannelValues()->size();
    qreal disp, max, min, last = getChannelValues()->last();
    min = max = last;
    disp = !m_pInputDevice->isSonet() && m_calibr ? (accuracyQuant())
                                                  : codeToReal(accuracyQuant()) - g_parameterStorage.displayMin(deviceID()) + 0.00005;

    for(int i = valuesSize - 1;  i >= 0;  i--)
    {
        qreal value = getChannelValues()->at(i);
        if(min < value)
            min = value;
        if(max > value)
            max = value;
        if(qAbs(max - min) > disp)
        {
            qreal pos = (100.0 * (valuesSize - i)) / valuesSize;
            progressBar_collecting->setValue(pos);
            return false;
        }
    }
    return true;
}

void DialogVerifyModule::loadResultsToTable(int row, int column, double first, double second)
{
    if(m_calibr)
    {
        tableWidget->item(row, column)->setText(QString::number((unsigned short)first));
        if(m_pInputDevice->isSonet() && currentColumn() > 0)
        {
            qreal point = g_parameterStorage.getPoints(deviceID()).at(currentColumn() - 1);
            if(getCurrentResultsAddr().at(slot()).at(channel()).contains(point))
            {
                unsigned short lastCode = getCurrentResultsAddr().at(slot()).at(channel()).constFind(point).value().first;
                bool valid = std::abs((unsigned short)first - lastCode) > 3;
                tableWidget->item(row, column)->setTextColor(valid ? Qt::black : Qt::red);
            }
        }
    }
    else
    {
        double currentAdjustment = (float)0.01*(g_parameterStorage.realMax(deviceID()) - g_parameterStorage.realMin(deviceID()));
        if(m_verifyType == InputOTK)
            currentAdjustment *= g_parameterStorage.getPercentOTK(deviceID());
        else
            currentAdjustment *= g_parameterStorage.getPercentCLIT(deviceID());
        double point = g_parameterStorage.getPoints(deviceID()).at(column);
        bool valid = first >= point - currentAdjustment + 0.0001 && second <= point + currentAdjustment - 0.0001;
        tableWidget->item(row, column)->setTextColor(valid ? Qt::black : Qt::red);
        tableWidget->item(row, column)->setText(QString("мин: %1\nмакс: %2").arg(first, 6, 'f', 3).arg(second, 6, 'f', 3));
    }
}

void DialogVerifyModule::addPointToResults(int slotNum, int chan, int col, double first, double second)
{
    double point = g_parameterStorage.getPoints(deviceID()).at(col);
    getCurrentResultsAddr()[slotNum][chan].insert(point, qMakePair(first, second));
    if(slotNum == slot())
    {
        loadResultsToTable(chan, col, first, second);
    }
}

void DialogVerifyModule::clearChannelResults(int slotNum, int chan)
{
    getCurrentResultsAddr()[slotNum][chan].clear();
    if(slotNum == slot())
    {
        for(int column = 0;  column < tableWidget->columnCount();  column++)
        {
            tableWidget->item(chan, column)->setText("");
            tableWidget->item(chan, column)->setTextColor(Qt::black);
        }
    }
}

int DialogVerifyModule::pointsCollected(int slotNum, int chan)
{
    return getCurrentResultsAddr().at(slotNum).at(chan).size();
}

qreal DialogVerifyModule::codeToReal(unsigned short code)
{
    return g_parameterStorage.displayMin(deviceID()) + (g_parameterStorage.displayMax(deviceID()) - g_parameterStorage.displayMin(deviceID())) * code / 0xffff;
}

unsigned short DialogVerifyModule::realToCode(qreal value)
{
    return (value - g_parameterStorage.displayMin(deviceID())) / (g_parameterStorage.displayMax(deviceID()) - g_parameterStorage.displayMin(deviceID())) * 0xffff;
}

void DialogVerifyModule::disableControls()
{
    comboBox_calibrator->setEnabled(false);
    comboBox_input->setEnabled(false);
    pushButton_preferences->setEnabled(false);
    if(autoRegime())
    {        
        progressBar_collecting->show();
        declarativeView_input->setEnabled(false);
        declarativeView_output->setEnabled(false);
        tableWidget->setEnabled(false);
        pushButton_write->setEnabled(false);
    }
    else
        pushButton_write->setEnabled(true);

    pushButton_stop->setEnabled(true);
}

void DialogVerifyModule::enableControls()
{
    pushButton_preferences->setEnabled(true);
    progressBar_collecting->hide();
    progressBar_collecting->setValue(progressBar_collecting->minimum());
    comboBox_calibrator->setEnabled(true);
    comboBox_input->setEnabled(true);
    declarativeView_input->setEnabled(true);
    declarativeView_output->setEnabled(true);
    tableWidget->setEnabled(true);

    pushButton_write->setEnabled(false);
    pushButton_stop->setEnabled(false);
}

void DialogVerifyModule::selectCurrentCell()
{
    m_collectTimer.start();
    tableWidget->clearSelection();
    static QColor myFavoiteColor = QColor::fromRgb(198, 236, 189);
    for(int row = 0;  row < tableWidget->rowCount();  row++)
        for(int column = 0;   column < tableWidget->columnCount();  column++)
        {
            if(row == channel() && column == currentColumn())
                tableWidget->item(row, column)->setBackgroundColor(myFavoiteColor);
            else
                tableWidget->item(row, column)->setBackgroundColor(Qt::white);
        }
    setColumnValue();
    clearChannelValues();
    progressBar_collecting->setFormat("Сбор информации: %p%");
}

void DialogVerifyModule::setColumnValue()
{
    if(currentColumn() < 0)
        return;
    Q_ASSERT(currentColumn() >= 0 && currentColumn() < g_parameterStorage.getPoints(deviceID()).size());
    qreal value = g_parameterStorage.getPoints(deviceID()).at(currentColumn());
    QMetaObject::invokeMethod(m_pQMLObjectOutput, "setOutputValue", Q_ARG(QVariant, value));
}

void DialogVerifyModule::setAutoRegimeParametersFromID()
{
    if(slot() >= 0 && slot() < SLOTS_COUNT
            && m_pInputDevice->isOldModule(slot()))
    {
        setaccuracyQuant(m_calibr ? 2 : 4);
        setallowableTime(10);
        settimeToConfirm(50);
    }
    switch(deviceID())
    {
    default:
        setaccuracyQuant(m_calibr ? 2 : 3);
        setallowableTime(5);
        settimeToConfirm(20);
    break;
    case 63: //R500
        setaccuracyQuant(m_calibr ? 5 : 8);
        setallowableTime(5);
        settimeToConfirm(20);
    break;
    case 60:
    case 65: //10V
        setaccuracyQuant(m_calibr ? 2 : 2);
        setallowableTime(10);
        settimeToConfirm(40);
    break;
    case 100:
    case 101:
    case 111: //diag module
    case 112:
        setaccuracyQuant(m_calibr ? 8 : 10);
        setallowableTime(10);
        settimeToConfirm(50);
    break;
    }
}

void DialogVerifyModule::refreshInput(QString text)
{
    QRegExp reg("\\D+");
    QStringList list = text.split(reg, QString::SkipEmptyParts);
    Q_ASSERT(!list.isEmpty());
    setslot(list.at(0).toInt() - 1);
    setchannel(0);
    Q_ASSERT(m_pInputDevice);
    declarativeView_input->rootContext()->setContextProperty("device", m_pInputDevice);
    declarativeView_input->rootContext()->setContextProperty("globalSlot", slot());
    declarativeView_input->setSource(QUrl(QString::fromUtf8("qrc:/qml/verify/ModuleForVerify.qml")));
    m_pQMLObjectInput = declarativeView_input->rootObject();

    connect(this, SIGNAL(channelChanged(QVariant)), m_pQMLObjectInput, SLOT(setCurrentRow(QVariant)));
    connect(m_pQMLObjectInput, SIGNAL(currentRowChanged(int)), this, SLOT(setchannel(int)));
    connect(m_pQMLObjectInput, SIGNAL(signal_connectedChanged(bool)), this, SLOT(setControlButtonsEnabled()));
    setControlButtonsEnabled();

    setAutoRegimeParametersFromID();

    m_pQMLObjectInput->setProperty("currentRow", 0);
    refreshTableContent();
    /*if(comboBox_calibrator->currentText().contains("ИКСУ-2000") || comboBox_calibrator->currentText().contains("Внешний"))
    {
        QString unit = g_parameterStorage.getUnit(deviceID());
        m_pQMLObjectOutput->setProperty("unit", unit);
    }*/
    refreshOutputList();
}

void DialogVerifyModule::refreshOutput(QString text)
{
    QSettings settings;
    settings.setValue(verifyLastCalibrKey, text);

    m_pDeviceList->m_icsu_enabled = text.contains("ИКСУ-2000");
    m_calibrators[slot()] = text;
    if(text.isEmpty())
        return;
    if(text.contains("слот"))
    {
        QRegExp reg("\\D+");
        QStringList list = text.split(reg, QString::SkipEmptyParts);
        Q_ASSERT(list.size() >= 2);
        int outputdeviceCurrentID = list.at(0).toInt() - 1;
        DeviceWithData *pDevice = m_pDeviceList->getDevice(outputdeviceCurrentID);
        Q_ASSERT(pDevice);
        int AOSlot = list.at(1).toInt() - 1;
        declarativeView_output->rootContext()->setContextProperty("globalSlot", AOSlot);
        declarativeView_output->rootContext()->setContextProperty("device", pDevice);
        declarativeView_output->setSource(QUrl(QString::fromUtf8("qrc:/qml/verify/ModuleForVerify.qml")));
        m_pQMLObjectOutput = declarativeView_output->rootObject();

        m_pQMLObjectOutput->setProperty("currentRow", 0);
        connect(m_pQMLObjectOutput, SIGNAL(currentRowChanged(int)), this, SLOT(setColumnValue()));
    }
    else if(text.contains("ИКСУ-2000"))
    {
        declarativeView_output->setSource(QUrl(QString::fromUtf8("qrc:/qml/verify/ICSU2000.qml")));
        m_pQMLObjectOutput = declarativeView_output->rootObject();
        QString unit = g_parameterStorage.getUnit(deviceID());
        m_pDeviceList->getICSUDevice()->setunit(unit);
        m_pDeviceList->getICSUDevice()->setreadOnly(false);

    }
    else if(text.contains("Внешний"))
    {
        declarativeView_output->setSource(QUrl(QString::fromUtf8("qrc:/qml/verify/EmptyCalibrator.qml")));
        m_pQMLObjectOutput = declarativeView_output->rootObject();
        m_pQMLObjectOutput->setProperty("unit", g_parameterStorage.getUnit(deviceID()));
    }
    Q_ASSERT(m_pQMLObjectOutput);
    connect(m_pQMLObjectOutput, SIGNAL(signal_connectedChanged(bool)), this, SLOT(setControlButtonsEnabled()));
    setControlButtonsEnabled();
}

void DialogVerifyModule::refreshInputList()
{
    comboBox_input->clear();
    DeviceWithData *pDevice = m_pDeviceList->getDevice(deviceCurrentID());
    for(int currentSlot = 0;  currentSlot < SLOTS_COUNT;  currentSlot++)
        if(pDevice->valid(currentSlot))
            if(pDevice->analog(currentSlot))
                if(pDevice->input(currentSlot))
                    comboBox_input->addItem(QString("слот %1").arg(currentSlot + 1));
    comboBox_input->setCurrentIndex(comboBox_input->findText(QString("слот %1").arg(slot() + 1)));
}


void DialogVerifyModule::refreshOutputList()
{
    QSettings settings;
    QString text = settings.value(verifyLastCalibrKey).toString();
    comboBox_calibrator->clear();
    comboBox_calibrator->addItem("Внешний");
    if(g_parameterStorage.getCalibrators(deviceID()).contains("ИКСУ-2000"))
        comboBox_calibrator->addItem("ИКСУ-2000");

    QString unit = g_parameterStorage.getUnit(deviceID());
    int length = m_pDeviceList->length();
    for(int pos = 0;  pos < length;  pos++)
    {
        DeviceWithData *pDevice = m_pDeviceList->getDevice(pos);
        if(pDevice)
        {
            for(int currentSlot = 0;  currentSlot < SLOTS_COUNT;  currentSlot++)
                if(pDevice->valid(currentSlot))
                    if(pDevice->analog(currentSlot))
                        if(!pDevice->input(currentSlot))
                        {
                            Q_ASSERT(currentSlot < pDevice->deviceIDs().size());
                            if(unit == g_parameterStorage.getUnit(pDevice->deviceIDs().at(currentSlot)))
                                comboBox_calibrator->addItem(QString("Контроллер %1, слот %3").arg(pDevice->componentID() + 1).arg(currentSlot + 1));

                        }
        }
    }
    int index = comboBox_calibrator->findText(text);
    comboBox_calibrator->setCurrentIndex(qMax(index, 0));
    QFontMetrics fm(comboBox_calibrator->font());
    comboBox_calibrator->setMinimumWidth(fm.width(comboBox_calibrator->itemText(comboBox_calibrator->count() - 1)) + 30);
    text = comboBox_calibrator->currentText();
    refreshOutput(comboBox_calibrator->currentText());
}

void DialogVerifyModule::refreshTableContent()
{
    commandLinkButton_createReport->setEnabled(false);
    QVector<qreal> points = g_parameterStorage.getPoints(deviceID());
    int pointsSize = points.size();

    int rowCount = channelsCount();
    if(!m_pInputDevice->isSonet())
        rowCount = 2;
    for(int chan = getCurrentResultsAddr().at(slot()).size();  chan < rowCount;  chan++)
         getCurrentResultsAddr()[slot()].append(ResultMap());
    tableWidget->clear();
    tableWidget->setColumnCount(pointsSize);
    tableWidget->setRowCount(rowCount);
    QStringList verticalHeaders;
    for(int chan = 0;  chan < rowCount;  chan++)
        verticalHeaders.push_back(QString("Канал %1").arg(chan + 1));
    tableWidget->setVerticalHeaderLabels(verticalHeaders);
    QStringList horizontalHeaders;
    for(QVector<qreal>::ConstIterator pointIter  = points.constBegin();  pointIter != points.constEnd();  pointIter++)
        horizontalHeaders << QString("%1").arg(*pointIter);
    tableWidget->setHorizontalHeaderLabels(horizontalHeaders);
    for(int row = tableWidget->rowCount() - 1;  row >= 0;  row--)
    {
        const ResultMap *pMap = &getCurrentResultsAddr().at(slot()).at(row);
        for(int col = tableWidget->columnCount() - 1;  col >= 0;  col--)
        {
            tableWidget->setItem(row, col, new QTableWidgetItem( ));
            double point = g_parameterStorage.getPoints(deviceID()).at(col);
            ResultMap::const_iterator iter = pMap->constFind(point);
            if(iter != pMap->constEnd())
                loadResultsToTable(row, col, (*iter).first, (*iter).second);
        }
    }
    tableWidget->setFixedHeight(tableWidget->horizontalHeader()->height() + tableWidget->verticalHeader()->sectionSize(0) * rowCount + rowCount + 1);
}

void DialogVerifyModule::cellDoubleClicked(int row, int column)
{
    if(row < 0 || column < 0)
        return;
    setchannel(row);
    setcurrentColumn(column);
    setColumnValue(); //force set value
    tableWidget->clearSelection();
}

void DialogVerifyModule::onChannelChanged()
{
    if(verifying())
    {
        int current = m_channel;
        m_channel = m_lastChannel;
        whenLeaveRegime();
        m_channel = current;
        whenEnterRegime();
    }
}

PROPERTY_CPP(int, DialogVerifyModule, currentColumn)
PROPERTY_CPP(bool, DialogVerifyModule, autoRegime)
PROPERTY_CPP(bool, DialogVerifyModule, verifying)
PROPERTY_CPP(int, DialogVerifyModule, allowableTime)
PROPERTY_CPP(int, DialogVerifyModule, timeToConfirm)
PROPERTY_CPP(qreal, DialogVerifyModule, accuracy)
int DialogVerifyModule::accuracyQuant() const
{
    qreal val = 0xffff / 100.0;

    return val * accuracy();
}

void DialogVerifyModule::setaccuracyQuant(qreal arg)
{
    qreal val = arg / 0xffff;
    val *= 100.0;
    setaccuracy(val);
}

void DialogVerifyModule::unsetcurrentColumn()
{
    setcurrentColumn(-1);
}

void DialogVerifyModule::setControlButtonsEnabled()
{
    if(!m_pQMLObjectInput || !m_pQMLObjectOutput)
        return;
    QVariant enabledI = false, enabledO = false;
    QMetaObject::invokeMethod(m_pQMLObjectInput, "connected", Qt::DirectConnection, Q_RETURN_ARG(QVariant, enabledI));
    QMetaObject::invokeMethod(m_pQMLObjectOutput, "connected", Qt::DirectConnection, Q_RETURN_ARG(QVariant, enabledO));
    bool enabled = enabledI.toBool() & enabledO.toBool();
    pushButton_auto->setEnabled(enabled & !verifying());
    pushButton_manual->setEnabled(enabled & !verifying());
    if(!enabled)
        if(verifying())
        {
            stopProcess();
            QMessageBox::warning(this, "Обрыв соедиения", "Обрыв соединения\nНеобходимо переподключится");
        }
}

int DialogVerifyModule::getSlotNum()
{
    QString format;
    if(m_verifyType == InputOTK || m_verifyType == InputCLIT)
        format = "AI";
    else
        format = "AO";
    return DialogSelectModule::getSelectedModule(qApp->activeWindow(), m_pDeviceList, deviceCurrentID(), format);
}

void TestWindows::DialogVerifyModule::on_pushButton_preferences_clicked()
{
    DialogAutoVerifyPreferences dialogAutoVerifyPreferences(this);
    dialogAutoVerifyPreferences.setunit(g_parameterStorage.getUnit(deviceID()));
    dialogAutoVerifyPreferences.setrange(g_parameterStorage.displayMax(deviceID()) - (g_parameterStorage.displayMin(deviceID())));
    if(m_verifyType == InputOTK)
        dialogAutoVerifyPreferences.setpercent(g_parameterStorage.getPercentOTK(deviceID()) / 2.5);
    else
        dialogAutoVerifyPreferences.setpercent(g_parameterStorage.getPercentCLIT(deviceID()) / 2.5);

    dialogAutoVerifyPreferences.doubleSpinBox_accuracy->setValue(accuracy());
    dialogAutoVerifyPreferences.dial_timeToConfirm->setValue(timeToConfirm());
    if(dialogAutoVerifyPreferences.exec())
    {
        setaccuracy(dialogAutoVerifyPreferences.doubleSpinBox_accuracy->value());
        settimeToConfirm(dialogAutoVerifyPreferences.spinBox_timeToConfirm->value());
        setallowableTime(timeToConfirm() * 7);
    }

}
