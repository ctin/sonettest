﻿#ifndef DIALOGDIAGNOSTICMODULE_H
#define DIALOGDIAGNOSTICMODULE_H

#include "dialogallchannels.h"

namespace TestWindows {
class DialogDiagnosticModule : public DialogAllChannels
{
public:
    DialogDiagnosticModule(DeviceList *pDeviceList, QWidget *parent = 0);
    bool beforeShow();
};
}

#endif // DIALOGDIAGNOSTICMODULE_H
