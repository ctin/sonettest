﻿#include "dialogcheckreserve.h"

using namespace TestWindows;

DialogCheckReserve::DialogCheckReserve(DeviceList *pDeviceList, QWidget *parent) :
    ParentWindow(pDeviceList, parent)
{
    setWindowTitle("Проверка переключения резерва");
    m_pLabelInfo = new QLabel("Ожидание команды", this);
    m_pRadioButtonBase = new QRadioButton("Основной контроллер", this);
    m_pRadioButtonReserve = new QRadioButton("Резервный контроллер", this);
    m_pButtonStart = new QPushButton("Старт", this);
    setLayout(new QVBoxLayout());
    layout()->addWidget(m_pLabelInfo);
    layout()->addWidget(m_pRadioButtonBase);
    layout()->addWidget(m_pRadioButtonReserve);
    layout()->addWidget(m_pButtonStart);
    connect(m_pButtonStart, SIGNAL(clicked()), this, SLOT(switchReserveTwice()));
    setstartFlags(SurveyNeither);
}

DialogCheckReserve::~DialogCheckReserve()
{
}

void DialogCheckReserve::onSecondHardwareError()
{
    if(m_pSecondDevice->hardwareError().isEmpty())
        return;
    abort();
    QMessageBox::warning(qApp->activeWindow(), "Ошибка порта!", "Произошла ошибка подключения к COM порту! Работа будет остановлена.");
}

bool DialogCheckReserve::beforeShow()
{
    int count = 0;
    for(int i = m_pDeviceList->length() - 1;  i >= 0 && count < 3;  i--)
    {
        DeviceWithData *pDevice = m_pDeviceList->getDevice(i);
        if(pDevice->deviceAviable() && pDevice->bprState() & 10)
        {
            switch(++count)
            {
            case 1: m_pSecondDevice = pDevice; break;
            case 2: m_pInputDevice = pDevice; break;
            default: break;
            }
        }
    }
    if(!count)
    {
        QMessageBox::warning(0, "Ошибка запуска", "Не обнаружено приборов, подключенных к БПР!");
    }
    else if(count == 1)
    {
        QMessageBox::warning(0, "Ошибка запуска", "К БПР подключен только один прибор!");
    }
    else if(count > 2)
    {
        QMessageBox::warning(0, "Ошибка запуска", "Обнаружно больше двух приборов, подключенных к БПР!\nУдалите все виртуальные приборы, кроме проверяемых.");
    }
    else
    {
        connect(m_pSecondDevice, SIGNAL(hardwareErrorChanged(QString)), this, SLOT(onSecondHardwareError()));
        ParentWindow::beforeShow();
        if(m_pInputDevice->bprState() & 1) // если основной прибор имеет статус "резервный"
            m_pRadioButtonReserve->setChecked(true);
        else
            m_pRadioButtonBase->setChecked(true);

        m_pRadioButtonBase->setText(QString("Контроллер %1").arg(m_pInputDevice->deviceNum()));
        m_pRadioButtonReserve->setText(QString("Контроллер %1").arg(m_pSecondDevice->deviceNum()));
        switchReserveTwice();
        connect(m_pRadioButtonBase, SIGNAL(toggled(bool)), this, SLOT(switchReserve()));
        return true;
    }
    return false;
}

void DialogCheckReserve::switchReserve()
{
    if(m_future.isRunning())
        return;
    setrunning(false);
    m_future.waitForFinished();
    m_future = QtConcurrent::run(this, &DialogCheckReserve::__switchReserveWrapper, m_pRadioButtonBase->isChecked(), false);
}

void DialogCheckReserve::switchReserveTwice()
{
    if(m_future.isRunning())
        return;
    setrunning(false);
    m_future.waitForFinished();
    m_future = QtConcurrent::run(this, &DialogCheckReserve::__switchReserveWrapper, m_pRadioButtonBase->isChecked(), true);
}

void DialogCheckReserve::__reportError()
{
    setrunning(false);
    QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, "Ошибка, нет данных"));
    QMetaObject::invokeMethod(this, "onWarning", Q_ARG(QString, "Нет ответа от устройства."), Q_ARG(QString, "Нет ответа от устройства.\nПроверка прекращена"));
}

bool DialogCheckReserve::__sendRequest(DeviceWithData *pDevice, const bool active)
{
    bool result = true;
    ByteArray ans;
    {
        PortOpener opener(pDevice);
        result = pDevice->isOpen();
        if(result)
            result = request(MODBUS_SendRequestFromMaster(pDevice->getCommandNamePtr()), &ans, pDevice);
        if(result)
            result = request(MODBUS_set_meandr(active, pDevice->getCommandNamePtr()), &ans, pDevice);
    }
    return result;
}

bool DialogCheckReserve::__waitForChange(DeviceWithData *pDevice, int *pTimeToSwitch)
{
    PortOpener opener(pDevice);
    ByteArray ans;
    m_timer.restart();
    while(m_timer.elapsed() < 3000 && running())
    {
        Sleepy::msleep(10);
        if(request(MODBUS_get_meandr(pDevice->getCommandNamePtr()), &ans, pDevice))
        {
            if(!ans.isEmpty())
                if(ans.at(0) & 1)
                {
                    *pTimeToSwitch = m_timer.elapsed();
                    pDevice->setbprState(ans.at(0));
                    return true;
                }
        }
        //else
           // __reportError();
    }
    return false;
}

void DialogCheckReserve::__switchReserveWrapper(const bool baseChecked, const bool twice)
{
    QMetaObject::invokeMethod(m_pRadioButtonBase, "setEnabled", Q_ARG(bool, false));
    QMetaObject::invokeMethod(m_pRadioButtonReserve, "setEnabled", Q_ARG(bool, false));
    QMetaObject::invokeMethod(m_pButtonStart, "setEnabled", Q_ARG(bool, false));

    QApplication::setOverrideCursor(Qt::WaitCursor);
    __switchReserve(baseChecked, twice);
    QApplication::restoreOverrideCursor();
    QMetaObject::invokeMethod(m_pRadioButtonBase, "setEnabled", Q_ARG(bool, true));
    QMetaObject::invokeMethod(m_pRadioButtonReserve, "setEnabled", Q_ARG(bool, true));
    QMetaObject::invokeMethod(m_pButtonStart, "setEnabled", Q_ARG(bool, true));
}

void DialogCheckReserve::__switchReserve(const bool baseChecked, const bool twice)
{
    setrunning(true);
    ByteArray ans;
    int timeToSwitch[2] = {0};
    bool base = twice ? !baseChecked : baseChecked;
    for(int cycle = 0;  cycle <= twice && running();  cycle++)
    {
        if(cycle)
        {
            base = !base;
        }
        QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, QString("Идет включение ") + QString(base ? "основного" : "резервного") + QString(" крейта...")));

        DeviceWithData *pDevice = base ? m_pInputDevice : m_pSecondDevice;
        if(!__sendRequest(pDevice, true))
        {
            QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection,
                Q_ARG(QString, QString("Не удалось включить ") + QString(base ? "основной" : "резервный") + QString(" крейт.")));
            return;
        }

        QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, QString("Идет отключение ") + QString(base ? "резервного" : "основного") + QString(" крейта...")));
        pDevice = base ? m_pSecondDevice : m_pInputDevice;
        {
            if(!__sendRequest(pDevice, false))
            {
                QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection,
                    Q_ARG(QString, QString("Не удалось выключить ") + QString(base ? "резервный" : "основной") + QString(" крейт.")));
                return;
            }
            if(!__waitForChange(pDevice, &timeToSwitch[cycle]))
            {
                QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, QString("Не удалось отключить ") + QString(base ? "резервный" : "основной") + QString(" крейт")));
                return;
            }
            if(!__sendRequest(pDevice, true))
            {
                QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection,
                    Q_ARG(QString, QString("Не удалось включить ") + QString(base ? "резервный" : "основной") + QString(" крейт.")));
                return;
            }
            if(!(pDevice->bprState() & 1))
            {
                QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, QString("Не удалось отключить ") + QString(base ? "резервный" : "основной") + QString(" крейт")));
                return;
            }

        }
        pDevice = base ? m_pInputDevice : m_pSecondDevice;
        {
            PortOpener opener(pDevice);
            if(request(MODBUS_get_meandr(pDevice->getCommandNamePtr()), &ans, pDevice))
            {
                pDevice->setbprState(ans.at(0));
                if(ans.at(0) & 1)
                {
                    QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, QString("Не удалось включить ") + QString(base ? "основной" : "резервный") + QString(" крейт.")));
                    return;
                }
            }
            else
            {
                //__reportError();
                //QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, QString("Не удалось получить состояние ") + QString(base ? "основного" : "резервного") + QString(" крейта.")));
                return;
            }
            QMetaObject::invokeMethod(base ? m_pRadioButtonBase : m_pRadioButtonReserve,
                "setChecked", Qt::QueuedConnection, Q_ARG(bool, baseChecked));
        }
    }
    QString text = "Оба крейта функционируют нормально\n";
    text += QString(baseChecked ? "Основной -> резервный" : "Резервный -> основной") + QString(": ") + QString("%1 мс.").arg(timeToSwitch[0]);
    if(twice)
        text += QString("\n") + QString(baseChecked ? "Резервный -> основной" : "Основной -> резервный") + QString(": ") + QString("%1 мс.").arg(timeToSwitch[1]);
    QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, text));
}
