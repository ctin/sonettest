﻿#ifndef DIALOGREGISTERSPROCMODULE_H
#define DIALOGREGISTERSPROCMODULE_H

#include "parentwindow.h"

namespace TestWindows {
class DialogRegistersProcModule : public ParentWindow
{
    Q_OBJECT
public:
    explicit DialogRegistersProcModule(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogRegistersProcModule();
    bool beforeShow();
private:
    QList<QLCDNumber*> m_LCDNumbers;
    void __functionStart();
private slots:
    void displayVer(float version);
    void displayData(ByteArray data);
};
}

#endif // DIALOGREGISTERSPROCMODULE_H
