INCLUDEPATH  += $$PWD
DEPENDPATH   += $$PWD

#QMAKE_CXXFLAGS_DEBUG += -pg
#QMAKE_LFLAGS_DEBUG += -pg

SOURCES +=  \
    $$PWD/parentwindow.cpp \
    $$PWD/dialogcalibr.cpp \
    $$PWD/dialogverifymodule.cpp \
    $$PWD/dialogverifyinput.cpp \
    $$PWD/dialogverifyoutput.cpp \
    survey/dialogcontrollerconsist.cpp \
    survey/dialogcheckswitches.cpp \
    survey/dialogallchannels.cpp \
    survey/dialogcommtest.cpp \
    survey/dialogfullpower.cpp \
    survey/dialogtimetosetallanalogchannels.cpp \
    survey/dialogregistersprocmodule.cpp \
    survey/dialogwatchtypes.cpp \
    survey/dialogcheckreserve.cpp \
    survey/dialogtemperature.cpp \
    survey/dialogreplytime.cpp \
    survey/dialogreadmodules.cpp \
    survey/dialogreadmodulesspi.cpp \
    survey/dialogcalibrdiagmodule.cpp \
    survey/dialogdiagnosticmodule.cpp \
    survey/dialogshowerrorlist.cpp \
    survey/dialogmodbusline.cpp

HEADERS  +=  \
    $$PWD/parentwindow.h \
    $$PWD/dialogcalibr.h \
    $$PWD/dialogverifymodule.h \
    $$PWD/dialogverifyinput.h \
    $$PWD/dialogverifyoutput.h \
    survey/dialogcontrollerconsist.h \
    survey/dialogcheckswitches.h \
    survey/dialogallchannels.h \
    survey/dialogcommtest.h \
    survey/dialogfullpower.h \
    survey/dialogtimetosetallanalogchannels.h \
    survey/dialogregistersprocmodule.h \
    survey/dialogwatchtypes.h \
    survey/dialogcheckreserve.h \
    survey/dialogtemperature.h \
    survey/dialogreplytime.h \
    survey/dialogreadmodules.h \
    survey/dialogreadmodulesspi.h \
    survey/dialogcalibrdiagmodule.h\
    survey/dialogdiagnosticmodule.h \
    survey/dialogshowerrorlist.h \
    survey/dialogmodbusline.h

FORMS += \
    $$PWD/dialogverifymodule.ui \
    survey/dialogcommtest.ui \
    survey/dialogreplytime.ui \
    survey/dialogmodbusline.ui


