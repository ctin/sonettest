﻿#ifndef DIALOGFULLPOWER_H
#define DIALOGFULLPOWER_H

#include "dialogallchannels.h"

namespace TestWindows {
class DialogFullPower : public DialogAllChannels
{
    Q_OBJECT
public:
    explicit DialogFullPower(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogFullPower();
    bool beforeShow();
};
}

#endif // DIALOGFULLPOWER_H
