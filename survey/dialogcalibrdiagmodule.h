﻿#ifndef DIALOGCALIBRDIAGMODULE_H
#define DIALOGCALIBRDIAGMODULE_H

#include "dialogcalibr.h"

namespace TestWindows {
class DialogCalibrDiagModule : public DialogCalibr
{
    Q_OBJECT
public:
    DialogCalibrDiagModule(DeviceList *pDeviceList, QWidget *parent = 0);
    bool beforeShow();
protected slots:
    virtual bool whenProcessCollectedData();
    virtual void refreshInput(QString text);
public slots:
protected:
    void initCalibrChannel();
    void calculateFinalValues();
    virtual void whenProcessNewData();
};
}

#endif // DIALOGCALIBRDIAGMODULE_H
