﻿#ifndef DIALOGCALIBRAUTO_H
#define DIALOGCALIBRAUTO_H

#include "dialogverifymodule.h"

namespace TestWindows {
class DialogCalibr : public DialogVerifyModule
{
Q_OBJECT
public:
    explicit DialogCalibr(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogCalibr();
    bool beforeShow();
protected slots:
    virtual bool whenProcessCollectedData();
protected:
    virtual void whenEnterRegime();
    virtual void whenLeaveRegime();
    virtual void whenFinishProcess();
    virtual void whenProcessNewData();
protected:
    virtual void initCalibrChannel();
    virtual void saveCalibrationData();
    virtual void restoreCalibrationData();
    virtual void calculateFinalValues();

    void warningHardwareReset();
    void warningHardwareResetMsgBox();
protected slots:
    virtual void refreshInput(QString text);
protected:
    QMap<int, QList<unsigned short> > m_lastEEPROMValues, m_newEEPROMValues;
};

}

#endif // DIALOGCALIBRAUTO_H
