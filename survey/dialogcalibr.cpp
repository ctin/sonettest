﻿#include "dialogcalibr.h"
#include "dialogloginpassword.h"
#include "macro.h"

using namespace TestWindows;

DialogCalibr::DialogCalibr(DeviceList *pDeviceList, QWidget *parent)
    : DialogVerifyModule(true, pDeviceList, parent)
{
    commandLinkButton_createReport->hide();
    setWindowTitle("Калибровка");
}

DialogCalibr::~DialogCalibr()
{
    QSplashScreen screen(this);
    screen.show();
    screen.showMessage("Ожидаю отправки всех неотправленных данных");
    stopProcess();
    QApplication::setOverrideCursor(Qt::WaitCursor);
    while(m_pInputDevice->m_needSetEEPROMValues)
        Sleepy::msleep(100);//сохранить данные в eeprom если не успел.
    screen.finish(this);
    QApplication::restoreOverrideCursor();
}

bool DialogCalibr::beforeShow()
{
    if(!DialogLoginPassword::validate())
        return false;
    DialogVerifyModule::beforeShow();
    if(slot() < 0)
        return false;
    if(m_pQMLObjectInput)
        m_pQMLObjectInput->setProperty("canShowCodes", true);
    return true;
}

void DialogCalibr::whenEnterRegime()
{
    clearChannelResults(slot(), channel());
    initCalibrChannel();
    m_pQMLObjectInput->setProperty("canShowCodes", false);

}

bool DialogCalibr::whenProcessCollectedData()
{
    unsigned short code = realToCode(getChannelValues()->last());
    Q_ASSERT(currentColumn() >= 0 && currentColumn() < g_parameterStorage.getPoints(deviceID()).size());
    addPointToResults(slot(), channel(), currentColumn(), code, getChannelValues()->last());

    if(IS_MODULE_NEED_REFLASH(deviceID()))
    {
        if(toNextColumn(autoRegime()))
            return false;
       for(int chan = 0;  chan < channelsCount();  chan++)
       {
           if(pointsCollected(slot(), chan) < g_parameterStorage.getPoints(deviceID()).size())
           {
               setchannel(chan);
               if(toNextColumn(autoRegime()))
                   return false;
           }
           else
           {
               calculateFinalValues();
           }
       }
    }
    else if(toNextColumn(autoRegime()))
        return false;
    if(!autoRegime())
        endProcess();
    return true;
}

void DialogCalibr::whenProcessNewData()
{
    unsigned short code = realToCode(getChannelValues()->last());
    Q_ASSERT(currentColumn() >= 0 && currentColumn() < g_parameterStorage.getPoints(deviceID()).size());
    addPointToResults(slot(), channel(), currentColumn(), code, getChannelValues()->last());
}

void DialogCalibr::whenLeaveRegime()
{
    if(verifying())
        restoreCalibrationData();
    if(m_pQMLObjectInput)
        m_pQMLObjectInput->setProperty("canShowCodes", true);
}

void DialogCalibr::whenFinishProcess()
{
    calculateFinalValues();
    saveCalibrationData();
    m_pQMLObjectInput->setProperty("canShowCodes", true);
    if(!m_pInputDevice->isSonet())
    {
        QString results;
        for(int row = 0;  row < m_newEEPROMValues[channel()].size() / 2;  row++)
        {
            results += QString("Код: 0x%1, значение: %2\n\r").arg((ushort)m_newEEPROMValues[channel()].at(row * 2), 4, 16, QChar('0')).arg((ushort) m_newEEPROMValues[channel()].at(row * 2 + 1));
        }
        QMessageBox::information(this, "Калибровка выполнена!", results);
    }
    else if(IS_MODULE_NEED_REFLASH(deviceID())) //адские модули
    {
        QString results;
        for(int chan = 0;  chan < channelsCount();  chan++)
        {
            results += QString("Канал: %1, код нуля: 0x%2, код максимума: 0x%3\n\r").arg(chan + 1).arg((ushort) m_newEEPROMValues[chan].at(0), 4, 16, QChar('0')).arg((ushort)m_newEEPROMValues[chan].at(1), 4, 16, QChar('0'));
        }
        QMessageBox::information(this, "Калибровка выполнена!", results);

        QFile outFile("codes.txt");
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);
        QTextStream ts(&outFile);
        ts << "\n\r" << QDateTime::currentDateTime().toString() << "\n\r" << QString("Контроллер %1, слот %2, модуль ID %3").arg(deviceCurrentID()).arg(slot()).arg(deviceID()) << results << endl;
        outFile.close();
    }
    else
        QMessageBox::information(this, "Калибровка выполнена!", QString("Калибровка выполнена!\nКанал: %1, код нуля: 0x%2, код максимума: 0x%3").arg(channel() + 1).arg((ushort)m_newEEPROMValues[channel()].at(0), 4, 16, QChar('0')).arg((ushort)m_newEEPROMValues[channel()].at(1), 4, 16, QChar('0')));
}

void DialogCalibr::warningHardwareResetMsgBox()
{
    QTime timer;
    QString defPort = g_defaultPort;
    QString defPortICSU = g_defaultPortICSU;
    bool wasEnabled = g_autoSearchEnabled;
    g_autoSearchEnabled = false;
    g_defaultPort = m_pInputDevice->port();
    g_defaultPortICSU = m_pDeviceList->getICSUDevice()->port();
    QMessageBox msgBox(this);
    msgBox.setWindowTitle("Важно!");
    msgBox.setText("Данные EEPROM были изменены, требуется записать их.");
    msgBox.setInformativeText("Идет запись");
    while(m_pInputDevice->m_needSetEEPROMValues)
    {
        qApp->processEvents();
        Sleepy::msleep(10);
    }
    msgBox.setInformativeText("Зажмите кнопку Reset на процессорном модуле или отключите питание контроллера.");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setStandardButtons(QMessageBox::NoButton);
    timer.start();
    msgBox.show();

    while(m_pInputDevice->deviceAviable())
    {
        m_collectTimer.restart();
        qApp->processEvents();
        Sleepy::msleep(3);
        if(timer.elapsed() > 60000)
        {
            if(QMessageBox::warning(this, "Ошибка", "Не удалось дождаться отключения контроллера. Подождать ещё?", QMessageBox::Ok, QMessageBox::Cancel) == QMessageBox::Ok)
                timer.restart();
            else
                return;
        }
    }
    msgBox.setText("Сброс выполнен, ожидаю установки связи.");
    msgBox.setInformativeText("Убедитесь что не зажат Reset, и что на контроллер поступает питание.");
    timer.start();
    while(!m_pInputDevice->deviceAviable())
    {
        m_collectTimer.restart();
        qApp->processEvents();
        Sleepy::msleep(3);
        if(timer.elapsed() > 60000)
        {
            if(QMessageBox::warning(this, "Ошибка", "Не удалось дождаться подключения контроллера. Подождать ещё?", QMessageBox::Ok, QMessageBox::Cancel) == QMessageBox::Ok)
                timer.restart();
            else
                return;
        }
    }
    msgBox.close();
    g_defaultPortICSU = defPortICSU;
    g_defaultPort = defPort;
    g_autoSearchEnabled = wasEnabled;
}

void DialogCalibr::warningHardwareReset()
{
    disconnect(m_pQMLObjectInput, SIGNAL(signal_connectedChanged(bool)), this, SLOT(setControlButtonsEnabled()));
    disconnect(m_pQMLObjectOutput, SIGNAL(signal_connectedChanged(bool)), this, SLOT(setControlButtonsEnabled()));
    warningHardwareResetMsgBox();
    m_collectTimer.restart();
    connect(m_pQMLObjectInput, SIGNAL(signal_connectedChanged(bool)), this, SLOT(setControlButtonsEnabled()));
    connect(m_pQMLObjectOutput, SIGNAL(signal_connectedChanged(bool)), this, SLOT(setControlButtonsEnabled()));
}

void DialogCalibr::initCalibrChannel()
{
    Q_ASSERT(channel() >= 0);
    Q_ASSERT(channel() < channelsCount());
    Q_ASSERT(slot() >= 0);
    Q_ASSERT(slot() < SLOTS_COUNT);
    m_lastEEPROMValues.insert(channel(), m_pInputDevice->getDataModel(slot())->getItem(channel()).deviceEEPROMValues);
    QList<unsigned short> values;
    values.append(0);
    values.append(0xffff);
    m_pInputDevice->getDataModel(slot())->setData(channel(), QVariant::fromValue(values), ModuleDataModel::UserEEPROMValuesRole);
    m_pInputDevice->m_needSetEEPROMValues = true;
    if(m_pInputDevice->isOldModule(slot()))
         warningHardwareReset();
}


void DialogCalibr::saveCalibrationData()
{
    Q_ASSERT(slot() >= 0);
    Q_ASSERT(slot() < SLOTS_COUNT);
    m_lastEEPROMValues.insert(channel(), m_newEEPROMValues[channel()]);
    QTime timer;
    timer.start();
    while(m_pInputDevice->m_writingEEPROM)
    {
        Sleepy::msleep(3);
        if(timer.elapsed() > 10000)
        {
            if(QMessageBox::warning(this, "Ошибка", "Не удалось дождаться ответа контроллера. Подождать ещё?", QMessageBox::Ok, QMessageBox::Cancel) == QMessageBox::Ok)
                timer.restart();
            else
                return;
        }
        continue;
    }
    ModuleDataModel *pModel = m_pInputDevice->getDataModel(slot());
    Q_ASSERT(pModel);
    pModel->setData(channel(), QVariant::fromValue(m_newEEPROMValues[channel()]), ModuleDataModel::UserEEPROMValuesRole);
    if(pModel->data(QModelIndex(), ModuleDataModel::ContainsDataFullNameRole).toBool())
    {
        pModel->setData(0, QDate::currentDate(), ModuleDataModel::UserCalibrDateRole);
        QSettings settings;
        if(settings.allKeys().contains(loginKey))
        {
            QByteArray fullName = settings.value(loginKey).toByteArray();
            pModel->setData(0, fullName, ModuleDataModel::UserCalibrFullNameRole);
        }
    }
    m_pInputDevice->m_needSetEEPROMValues = true;
    if(m_pInputDevice->isOldModule(slot()))
         warningHardwareReset();
}

void DialogCalibr::restoreCalibrationData()
{
    Q_ASSERT(slot() >= 0);
    Q_ASSERT(slot() < SLOTS_COUNT);
    ModuleDataModel *pModel = m_pInputDevice->getDataModel(slot());
    Q_ASSERT(pModel);
    pModel->setData(channel(), QVariant::fromValue(m_lastEEPROMValues[channel()]), ModuleDataModel::UserEEPROMValuesRole);
    pModel->setData(channel(), QVariant::fromValue(true), ModuleDataModel::DontUnsetRole);
    m_pInputDevice->m_needSetEEPROMValues = true;
    if(m_pInputDevice->isOldModule(slot()))
         warningHardwareReset();
    pModel->setData(channel(), QVariant::fromValue(false), ModuleDataModel::DontUnsetRole);
}

void DialogCalibr::calculateFinalValues()
{
    float A=0,C=0,D=0,F=0;
    Q_ASSERT(channel() >= 0 && channel() < channelsCount());
    int pointsCount = pointsCollected(slot(), channel());
    for (ResultMap::ConstIterator iter = getCurrentResultsAddr().at(slot()).at(channel()).constBegin();
         iter != getCurrentResultsAddr().at(slot()).at(channel()).constEnd();
         ++iter)
    {
        A += iter.key();
        C += iter.value().first;
        D += iter.key() * iter.key();
        F += iter.key() * iter.value().first;
    }
    if(!(pointsCount*D-A*A))
    {
        m_newEEPROMValues[channel()] = m_lastEEPROMValues[channel()];
        return;
    }
    float tmpA = (F*pointsCount-C*A)/(pointsCount*D-A*A);
    float tmpB = (C*D-A*F)/(pointsCount*D-A*A);
    unsigned short zero = (unsigned short)(tmpB+tmpA*g_parameterStorage.displayMin(deviceID())+0.5);
    unsigned short full = (unsigned short)(tmpB+tmpA*g_parameterStorage.displayMax(deviceID())+0.5);
    full -= zero;
    m_newEEPROMValues[channel()].clear();
    m_newEEPROMValues[channel()].append(zero);
    m_newEEPROMValues[channel()].append(full);
    Q_ASSERT(m_newEEPROMValues[channel()].size() >= 2);
}

void DialogCalibr::refreshInput(QString text)
{
    DialogVerifyModule::refreshInput(text);
    if(m_pQMLObjectInput)
        m_pQMLObjectInput->setProperty("canShowCodes", true);
}
