﻿#ifndef DIALOGREADMODULESSPI_H
#define DIALOGREADMODULESSPI_H

#include "parentwindow.h"

namespace TestWindows {
class DialogReadModulesSpi : public ParentWindow
{
    Q_OBJECT

public:
    explicit DialogReadModulesSpi(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogReadModulesSpi();
    bool beforeShow();

protected:
    QTableWidget *m_pTableWidget;
    void __functionStart();
private slots:
    void addData(int col, int chan, ByteArray data);
};
}

#endif // DIALOGREADMODULESSPI_H
