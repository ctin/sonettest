﻿#include "dialogverifyinput.h"
#include "dialogreportfactory.h"
#include "macro.h"
#include "dialogloginpassword.h"

using namespace TestWindows;

DialogVerifyInput::DialogVerifyInput(const VerifyType verifyType, DeviceList *pDeviceList, QWidget *parent)
    : DialogVerifyModule(false, pDeviceList, parent)
{
    setVerifyType(verifyType);
    connect(commandLinkButton_createReport, SIGNAL(clicked()), this, SLOT(createReport()), Qt::QueuedConnection);
}



DialogVerifyInput::~DialogVerifyInput()
{
    QSplashScreen screen(this);
    screen.show();
    screen.showMessage("Ожидаю отправки всех неотправленных данных");
    stopProcess();
    QApplication::setOverrideCursor(Qt::WaitCursor);
    while(m_pInputDevice->m_needSetEEPROMValues)
        Sleepy::msleep(100);//сохранить данные в eeprom если не успел.
    screen.finish(this);
    QApplication::restoreOverrideCursor();
}

bool DialogVerifyInput::whenProcessCollectedData()
{
    bool skipRedCells = !autoRegime();
    if(toNextColumn(!skipRedCells))
        return false;
    // row is collected?
    commandLinkButton_createReport->setEnabled(true);
    // now can create report. Check other row if needed
    if(!autoRegime()) {
        if(toNextColumn(autoRegime()))
            return false;
       for(int chan = 0;  chan < channelsCount();  chan++)
       {
           if(pointsCollected(slot(), chan) < g_parameterStorage.getPoints(deviceID()).size())
           {
               setchannel(chan);
               if(toNextColumn(!skipRedCells))
                   return false;
           }
       }
    }
    // all cells collected, create report immediately
    if(!autoRegime())
        endProcess();
    //if(!autoRegime())
        //createReport();
    return true;
}

void DialogVerifyInput::createReport()
{
    endProcess();
    DialogReportFactory dialogReportFactory(true, m_pInputDevice, slot(), &getCurrentResultsAddr(), m_calibrators, getVerifyType(), this);
    dialogReportFactory.exec();
}

void DialogVerifyInput::whenFinishProcess()
{
    saveVerificationData();
    if(autoRegime() && verifying())
        QMessageBox::information(this, "Поверка канала ввода выполнена!", QString("Поверка канала ввода выполнена!"));
}

void DialogVerifyInput::saveVerificationData()
{
    Q_ASSERT(slot() >= 0);
    Q_ASSERT(slot() < SLOTS_COUNT);
    ModuleDataModel *pModel = m_pInputDevice->getDataModel(slot());
    Q_ASSERT(pModel);
    if(pModel->data(QModelIndex(), ModuleDataModel::ContainsDataFullNameRole).toBool())
    {
        pModel->setData(0, QDate::currentDate(), ModuleDataModel::UserVerifyDateRole);
        /*QSettings settings;
        if(settings.allKeys().contains(loginKey))
        {
            QByteArray fullName = settings.value(loginKey).toByteArray();
            pModel->setData(0, fullName, ModuleDataModel::UserVerifyFullNameRole);
        }*/
    }
    m_pInputDevice->m_needSetEEPROMValues = true;
}

void DialogVerifyInput::whenEnterRegime()
{
}

void DialogVerifyInput::whenLeaveRegime()
{
    saveVerificationData();
}

void DialogVerifyInput::whenProcessNewData()
{
    const int pointsCount = 20;
    int size = getChannelValues()->size();
    double min = getChannelValues()->last(), max = getChannelValues()->last();
    int count = size > pointsCount ? pointsCount : size;
    for(int i = 0;  i < count;  i++)
    {
        Q_ASSERT(size - i - 1 >= 0 && size - i - 1 < getChannelValues()->size());
        qreal element = getChannelValues()->at(size - i - 1);
        if(min > element)
            min = element;
        if(max < element)
            max = element;
    }
    addPointToResults(slot(), channel(), currentColumn(), min, max);
    if(pointsCollected(slot(), channel()) == tableWidget->columnCount())
        commandLinkButton_createReport->setEnabled(true);
}



