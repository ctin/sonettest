﻿#ifndef DIALOGTIMETOSETALLANALOGCHANNELS_H
#define DIALOGTIMETOSETALLANALOGCHANNELS_H

#include "parentwindow.h"

namespace TestWindows {
class DialogTimeToSetAllAnalogChannels : public ParentWindow
{
    Q_OBJECT
public:
    explicit DialogTimeToSetAllAnalogChannels(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogTimeToSetAllAnalogChannels();
    bool beforeShow();
private:
    QLabel *m_pLabelInfo;
    void __functionStart();
};
}

#endif // DIALOGTIMETOSETALLANALOGCHANNELS_H
