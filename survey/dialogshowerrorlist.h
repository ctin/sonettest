﻿#ifndef DIALOGSHOWERRORLIST_H
#define DIALOGSHOWERRORLIST_H

#include "parentwindow.h"

namespace TestWindows {
class DialogShowErrorList : public ParentWindow
{
    Q_OBJECT
public:
    explicit DialogShowErrorList(DeviceList *pDeviceList, QWidget *parent = 0);
    bool beforeShow();
    QListWidget *m_pErrorListWidget;
public slots:
    void updateErrorList();
};

}

#endif // DIALOGSHOWERRORLIST_H
