﻿#include "dialogcheckswitches.h"



using namespace TestWindows;

DialogCheckSwitches::DialogCheckSwitches(DeviceList *pDeviceList, QWidget *parent)
    : ParentWindow(pDeviceList, parent)
{
    setWindowTitle("Проверка переключателей");
    const int minHeight = 25;
    m_pLCDNumberAddress = new QLCDNumber(this);
    m_pLCDNumberAddress->setMinimumHeight(minHeight);
    m_pLCDNumberAddress->setSegmentStyle(QLCDNumber::Flat);

    m_pLCDNumberAddress1 = new QLCDNumber(this);
    m_pLCDNumberAddress1->setMinimumHeight(minHeight);
    m_pLCDNumberAddress1->setSegmentStyle(QLCDNumber::Flat);

    m_pLCDNumberAddress10 = new QLCDNumber(this);
    m_pLCDNumberAddress10->setMinimumHeight(minHeight);
    m_pLCDNumberAddress10->setSegmentStyle(QLCDNumber::Flat);

    m_pLCDNumberSpeed = new QLCDNumber(this);
    m_pLCDNumberSpeed->setMinimumHeight(minHeight);
    m_pLCDNumberSpeed->setSegmentStyle(QLCDNumber::Flat);

    m_pLCDNumberRealSpeed = new QLCDNumber(this);
    m_pLCDNumberRealSpeed->setMinimumHeight(minHeight);
    m_pLCDNumberRealSpeed->setSegmentStyle(QLCDNumber::Flat);
    m_pLCDNumberRealSpeed->setDigitCount(6);

    QGridLayout *pLayout = new QGridLayout(this);
    int row = 0;
    pLayout->addWidget(new QLabel("Скорость: ", this), row, 0);
    pLayout->addWidget(m_pLCDNumberSpeed, row++, 1);
    pLayout->addWidget(new QLabel("Cкорость порта: ", this));
    pLayout->addWidget(m_pLCDNumberRealSpeed, row++, 1);

    pLayout->addWidget(new QLabel(" ", this), row++, 0);

    pLayout->addWidget(new QLabel("Адрес X10: ", this), row, 0);
    pLayout->addWidget(m_pLCDNumberAddress10, row++, 1);
    pLayout->addWidget(new QLabel("Адрес Х1: ", this), row, 0);
    pLayout->addWidget(m_pLCDNumberAddress1, row++, 1);
    pLayout->addWidget(new QLabel("Полный адрес", this), row, 0);
    pLayout->addWidget(m_pLCDNumberAddress, row++, 1);

    setstartFlags(SurveyNeither);
}

DESTRUCT_FUNC_CLASS(DialogCheckSwitches)

void DialogCheckSwitches::__functionStart()
{
    START_FUNC
    PortOpener opener(m_pInputDevice);
    if(request(&DeviceWithData::getProcessorModuleData, m_pInputDevice) && running())
        QMetaObject::invokeMethod(this, "displayResult", Qt::QueuedConnection);
    END_FUNC
}

bool DialogCheckSwitches::beforeShow()
{
    ParentWindow::beforeShow();
    m_future = QtConcurrent::run(this, &DialogCheckSwitches::__functionStart);
    displayResult();
    connect(m_pInputDevice, SIGNAL(frontPanelSpeedChanged(int)), this, SLOT(displayResult()));
    connect(m_pInputDevice, SIGNAL(frontPanelX10Changed(int)), this, SLOT(displayResult()));
    connect(m_pInputDevice, SIGNAL(frontPanelX1Changed(int)), this, SLOT(displayResult()));
    return true;
}

void DialogCheckSwitches::displayResult()
{
    m_pLCDNumberAddress1->display(m_pInputDevice->frontPanelX1());
    m_pLCDNumberAddress10->display(m_pInputDevice->frontPanelX10());
    m_pLCDNumberSpeed->display(m_pInputDevice->frontPanelSpeed());
    m_pLCDNumberRealSpeed->display((int)m_pInputDevice->getRealRate(m_pInputDevice->frontPanelSpeed()));
    m_pLCDNumberAddress->display(m_pInputDevice->frontPanelX1() % 10 + (m_pInputDevice->frontPanelX10() % 10) * 10);

}
