﻿#include "dialogreadmodules.h"



using namespace TestWindows;

DialogReadModules::DialogReadModules(DeviceList *pDeviceList, QWidget *parent)
    : ParentWindow(pDeviceList, parent)
{
    setWindowTitle("Чтение из модулей");
    m_pTableWidget = new QTableWidget(this);
    m_pTableWidget->setEditTriggers(QTableWidget::NoEditTriggers);
    m_pTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    m_pTableWidget->horizontalHeader()->setMinimumHeight(30);
    connect(m_pTableWidget, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(onCellDoubleClicked(int, int)));
    setLayout(new QGridLayout());
    layout()->addWidget(m_pTableWidget);
    resize(800, 600);
    connect(this, SIGNAL(newData(int, ByteArray)), this, SLOT(addData(int, ByteArray)), Qt::QueuedConnection);
    setstartFlags(SurveyNeither);
}

void DialogReadModules::onCellDoubleClicked(int row, int column)
{
    if(!(row % 6))//table subheader
        return;
    QLineEdit *pLineEdit = new QLineEdit(this);
    pLineEdit->setValidator(new QRegExpValidator(QRegExp("[0-9a-fA-F]{0,2}"), pLineEdit));
    pLineEdit->setFixedSize(m_pTableWidget->columnWidth(1), m_pTableWidget->rowHeight(1));
    pLineEdit->setAlignment(Qt::AlignCenter);
    pLineEdit->setProperty("row", (row % 6) - 1); //subheader
    pLineEdit->setProperty("column", column);
    pLineEdit->setProperty("slot", slot() < 0 ? row / 6 : slot());
    m_pTableWidget->setCellWidget(row, column, pLineEdit);
    connect(pLineEdit, SIGNAL(returnPressed()), this, SLOT(onEditingFinished()));
}

void DialogReadModules::onEditingFinished()
{
    QLineEdit *pLineEdit = qobject_cast<QLineEdit*>(sender());
    if(!pLineEdit)
        return;
    int row = pLineEdit->property("row").toInt();
    int column = pLineEdit->property("column").toInt();
    int module = pLineEdit->property("slot").toInt();
    bool ok = true;
    unsigned char value = pLineEdit->text().toInt(&ok, 16);
    m_pTableWidget->removeCellWidget(slot() < 0 ? (module * 6 + row + 1) : row + 1, column);
    if(!ok)
        return;
    CellDataInfo cellDataInfo = {row, column, module, value};
    QMutexLocker locker(&m_requestsMutex);
    m_requestsToSendData.enqueue(cellDataInfo);

}


DESTRUCT_FUNC_CLASS(DialogReadModules)

bool DialogReadModules::beforeShow()
{
    ParentWindow::beforeShow();
    m_future = QtConcurrent::run(this, &DialogReadModules::__functionStart);
    createTable();
    return true;
}

void DialogReadModules::createTable()
{
    m_pTableWidget->setColumnCount(COLUMN_COUNT);

    QStringList str;
    for(int col = 0;  col < COLUMN_COUNT;  col++)
        str.append(QString("%1").arg(col, 2, 16, QChar('0')).toUpper());
    m_pTableWidget->setHorizontalHeaderLabels(str);
    const int rowCount = slot() < 0 ? ROW_COUNT : 6;
    m_pTableWidget->setRowCount(rowCount);
    str.clear();
    for(int row = 0;  row < rowCount;  row++)
        str.append(row % 6 ? QString("%1").arg((row % 6) - 1, 2, 10, QChar('0')) : "");
    m_pTableWidget->setVerticalHeaderLabels(str);
    for(int row = 0;  row < rowCount;  row++)
    {
        if(!(row % 6))
        {
            QTableWidgetItem *pItem = new QTableWidgetItem(QString("---------------> слот %1 <---------------").arg(slot() < 0 ? (row / 6 + 1) : slot() + 1));
            pItem->setTextAlignment(Qt::AlignCenter);
            m_pTableWidget->setSpan(row, 0, 1, COLUMN_COUNT);
            m_pTableWidget->setItem(row, 0, pItem);
        }
        else
            for(int col = 0;  col < COLUMN_COUNT;  col++)
            {
                QTableWidgetItem *pItem = new QTableWidgetItem();
                pItem->setTextAlignment(Qt::AlignCenter);
                m_pTableWidget->setItem(row, col, pItem);
            }
    }
}


void DialogReadModules::addData(int currentRow, ByteArray data)
{
    for(int col = 0;  col < COLUMN_COUNT;  col++)
    {
        if(m_pTableWidget->cellWidget(currentRow, col))
        {
            continue;
        }
        QTableWidgetItem *pItem = m_pTableWidget->item(currentRow, col);
        int res = data.at(col) & 0xff;
        pItem->setText(QString("%1").arg(res, 2, 16, QChar('0')));
        QColor color;
        switch(res)
        {
        case 0x00: color = Qt::white; break;
        case 0xff: color.setNamedColor("#A2EBF9"); break;
        default:  color.setNamedColor("#F9DE8A");  break;
        }
        pItem->setBackgroundColor(color);
    }
}

void DialogReadModules::__setData()
{
    CellDataInfo cellDataInfo;
    for(int counter = 0;  counter < 10;  counter++)
    {
        {
            QMutexLocker locker(&m_requestsMutex);
            if(m_requestsToSendData.isEmpty())
                return;
            cellDataInfo = m_requestsToSendData.head();
        }
        int row = cellDataInfo.row;
        int col = cellDataInfo.column;
        int module = cellDataInfo.module;
        unsigned char data = cellDataInfo.value;
        bool result = true;

        if(row == 2 && col == 2 && m_pInputDevice->isOldModule(module))
        {
            result = m_pInputDevice->eeprom_set(module, 16, data);
            if(result)
            {
                QMetaObject::invokeMethod(static_cast<Application*>(qApp),
                                             "showErrorMessage",
                                             Qt::BlockingQueuedConnection,
                                             Q_ARG(QString, "Требуется перезагрузка"),
                                          Q_ARG(QString, "Нажмите reset или сбросьте питание на 20 секунд"),
                                          Q_ARG(QString, "Значения будут приняты только после перезагрузки"));
            }
        }
        else
            result = request(MODBUS_direct_write(module, row * COLUMN_COUNT + col, data, m_pInputDevice->getCommandNamePtr()));
        if(result)
            {
            QMutexLocker locker(&m_requestsMutex);
            m_requestsToSendData.dequeue();
        }
        else
            return;
    }
 }

void DialogReadModules::__getData(const int module)
{
    for(int row = 0;  row < 5 && running();  row++)
    {
        __setData();
        const int packetSize = COLUMN_COUNT;
        ByteArray ans;
        if(request(MODBUS_direct_read(module, (module * 80) + row * packetSize, packetSize, m_pInputDevice->getCommandNamePtr()), &ans) && running())
            emit newData((slot() < 0 ? module * 6 : 0) + row + 1, ans);
    }
}

void DialogReadModules::__functionStart()
{
    START_FUNC
    PortOpener opener(m_pInputDevice);
    if(slot() < 0) {
        for(int slot = 0;  slot < SLOTS_COUNT && running();  slot++)
            __getData(slot);
    }
    else
        __getData(slot());
    END_FUNC
}
