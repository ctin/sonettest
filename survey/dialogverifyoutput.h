﻿#ifndef DIALOGVERIFYOUTPUT_H
#define DIALOGVERIFYOUTPUT_H

#include "dialogverifymodule.h"

namespace TestWindows {
class DialogVerifyOutput : public DialogVerifyModule
{
    Q_OBJECT
public:
    DialogVerifyOutput(const VerifyType type, DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogVerifyOutput();
protected:
    void refreshOutputList();
    void refreshInputList();
protected slots:
    void createReport();
    void refreshOutput(QString text);
    void refreshInput(QString text);
protected slots:
    bool whenProcessCollectedData();
    void whenFinishProcess();
protected:
    void whenEnterRegime();
    void whenLeaveRegime();
    void whenProcessNewData();
};
}

#endif // DIALOGVERIFYOUTPUT_H
