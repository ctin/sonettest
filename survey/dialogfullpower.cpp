﻿#include "dialogfullpower.h"

using namespace TestWindows;

DialogFullPower::DialogFullPower(DeviceList *pDeviceList, QWidget *parent) :
    DialogAllChannels(pDeviceList, parent)
{
    setWindowTitle("Проверка мощности");
    setstartFlags(SurveyCurrent);
}

DialogFullPower::~DialogFullPower()
{
    m_pInputDevice->setMinToAll();
}

bool DialogFullPower::beforeShow()
{
    DialogAllChannels::beforeShow();
    m_pInputDevice->setMaxToAll();
    return true;
}
