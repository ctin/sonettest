﻿#include "dialogtimetosetallanalogchannels.h"

using namespace TestWindows;

DialogTimeToSetAllAnalogChannels::DialogTimeToSetAllAnalogChannels(DeviceList *pDeviceList, QWidget *parent) :
    ParentWindow(pDeviceList, parent)
{
    setWindowTitle("Проверка времени выставления каналов аналогового вывода");
    m_pLabelInfo = new QLabel(this);
    QHBoxLayout *pLayout = new QHBoxLayout(this);
    pLayout->addWidget(m_pLabelInfo);
    setstartFlags(SurveyNeither);
}

DESTRUCT_FUNC_CLASS(DialogTimeToSetAllAnalogChannels)

void DialogTimeToSetAllAnalogChannels::__functionStart()
{
    unsigned short value = 0;
    START_FUNC
    PortOpener opener(m_pInputDevice);
    request(&DeviceWithData::refreshConsist);

    bool k = false;
    for(int slot = 0;  slot < SLOTS_COUNT && running();  slot++)
    {
        if(m_pInputDevice->valid(slot) && m_pInputDevice->analog(slot) && !m_pInputDevice->input(slot))
        {
            k = true;
            m_pInputDevice->getDataModel(slot)->setData(0, m_pInputDevice->channelsCount().at(slot), value, ModuleDataModel::UserValueRole);
        }
    }
    if(!k)
    {
        QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, "Нет ни одного модуля аналогового вывода!"));
        return;
    }
    if(request(&DeviceWithData::sendChangedData) && running())
    {
        QMetaObject::invokeMethod(m_pLabelInfo, "setText", Qt::QueuedConnection, Q_ARG(QString, QString("Установлено <b>") + QString(value ? "максимальное" : "минимальное") + QString("</b> значение")));
        value = ~value;
    }
    surveyDelay(1000);
    END_FUNC
}

bool DialogTimeToSetAllAnalogChannels::beforeShow()
{
    m_pLabelInfo->setText("Идет процесс выставления значений");
    ParentWindow::beforeShow();
    m_future = QtConcurrent::run(this, &DialogTimeToSetAllAnalogChannels::__functionStart);
    return true;
}
