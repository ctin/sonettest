#ifndef DIALOGVERIFYMODULE_H
#define DIALOGVERIFYMODULE_H

#include "ui_dialogverifymodule.h"
#include <QtCore>
#include <QtGui>
#include <QtDeclarative>
#include "parentwindow.h"
#include "devicewithdata.h"
#include "objects.h"
#include "dialogautoverifypreferences.h"

typedef QMap<double, QPair<double, double> > ResultMap;
typedef QList<ResultMap > ResultModule;
typedef QList<ResultModule> ResultController;
enum VerifyType{
    InputOTK,
    InputCLIT,
    OutputOTK,
    OutputCLIT
};

namespace TestWindows {
class DialogVerifyModule : public ParentWindow, protected Ui::DialogVerifyModule
{
    Q_OBJECT
    Q_PROPERTY(int currentColumn READ currentColumn WRITE setcurrentColumn NOTIFY currentColumnChanged RESET unsetcurrentColumn)
    Q_PROPERTY(bool autoRegime READ autoRegime WRITE setautoRegime NOTIFY autoRegimeChanged)
    Q_PROPERTY(bool verifying READ verifying WRITE setverifying NOTIFY verifyingChanged)
    Q_PROPERTY(int allowableTime READ allowableTime WRITE setallowableTime NOTIFY allowableTimeChanged)
    Q_PROPERTY(int timeToConfirm READ timeToConfirm WRITE settimeToConfirm NOTIFY timeToConfirmChanged)
    Q_PROPERTY(qreal accuracy READ accuracy WRITE setaccuracy NOTIFY accuracyChanged)

public:
    explicit DialogVerifyModule(const bool calibr, DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogVerifyModule();
    bool beforeShow();
protected:
    bool toNextColumn(bool checkNonValidData = true);
    int getSlotNum();
    virtual void refreshOutputList();
    virtual void refreshInputList();
protected slots:
    virtual void refreshOutput(QString text);
    virtual void refreshInput(QString text);
    virtual bool whenProcessCollectedData() = 0;

    //widgets
    void cellDoubleClicked(int row, int column);
    void endProcess();
    void setControlButtonsEnabled();
    bool refreshAll();
protected:
    void closeEvent(QCloseEvent *);
    void setVerifyType(VerifyType verifyType);
    VerifyType getVerifyType() const;
    //process
    virtual void whenEnterRegime() = 0;
    virtual void whenLeaveRegime() = 0;
    virtual void whenFinishProcess() = 0;
    virtual void whenProcessNewData() = 0;

    QTime m_collectTimer;
    //survey
    QTimer *m_pSurveyTimer;
    bool addChannelValues(qreal value);
    void clearChannelValues();
    const QQueue<qreal> *getChannelValues() const;
    ResultController m_resultsVerify, m_resultsCalibr; // Они статические на тот случай, если человек случайно закроет окно.
    ResultController &getCurrentResultsAddr();
    QMap<int, QString> m_calibrators;
    void loadResultsToTable(int row, int column, double first, double second);
    void addPointToResults(int slot, int channel, int col, double first, double second);
    void clearChannelResults(int slot, int channel);
    int pointsCollected(int slot, int channel);
    qreal codeToReal(unsigned short code);
    unsigned short realToCode(qreal value);
    bool checkDisp();

    //window
    QObject *m_pQMLObjectInput, *m_pQMLObjectOutput;
    void setAutoRegimeParametersFromID();
private:
    QQueue <qreal> m_channelValues;
    VerifyType m_verifyType;

protected:
    const bool m_calibr;
    void initRegime(bool autoRegime);

    void refreshTableContent();
    void disableControls();
    void enableControls();
protected slots:
    //process
    void collectData();
    void startManual();
    void startAuto();
    void stopProcess();
    void selectCurrentCell();
    //data
    void setColumnValue();
    void onChannelChanged();
    void onIcsuSetData(qreal value);
    //properties
public:         int currentColumn() const;
public slots:   void setcurrentColumn(const int currentColumn);
                void unsetcurrentColumn();
signals:        void currentColumnChanged(int currentColumn);
private:        int m_currentColumn;
public:             bool autoRegime() const;
public slots:       void setautoRegime(const bool autoRegime);
signals:            void autoRegimeChanged(bool autoRegime);
private:            bool m_autoRegime;
public:         bool verifying() const;
                int allowableTime() const;
                int timeToConfirm() const;
                qreal accuracy() const;
                int accuracyQuant() const;

public slots:   void setverifying(const bool verifying);
                void setallowableTime(int arg);
                void settimeToConfirm(int arg);
                void setaccuracy(qreal arg);
                void setaccuracyQuant(qreal arg);

signals:        void verifyingChanged(bool verifying);
                void allowableTimeChanged(int arg);

                void timeToConfirmChanged(int arg);

                void accuracyChanged(qreal arg);

private slots:
                void on_pushButton_preferences_clicked();

private:        bool m_verifying;
                int m_allowableTime;
                int m_timeToConfirm;
                qreal m_accuracy;
};
}

#endif // DIALOGVERIFYMODULE_H
