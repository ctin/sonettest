﻿#include "dialogdiagnosticmodule.h"
#include "core.h"

using namespace TestWindows;

DialogDiagnosticModule::DialogDiagnosticModule(DeviceList *pDeviceList, QWidget *parent) :
    DialogAllChannels(pDeviceList, parent)
{
    setWindowTitle("Проверка модуля диагностики");
    setstartFlags(SurveyCurrent);
}

bool DialogDiagnosticModule::beforeShow()
{
    if(m_pInputDevice->isSonet())
    {
        int count = 0, validDeviceRow = -1;
        for(int i = m_pDeviceList->length() - 1;  i >= 0;  i--)
            if(!m_pDeviceList->getDevice(i)->isSonet())
                if(!count++)
                    validDeviceRow = i;
        if(count != 1)
        {
            if(!count)
                QMessageBox::warning(qApp->activeWindow(), "Не обнаружено модулей диагностики", "Подключите модуль диагностики!");
            if(count > 1)
                QMessageBox::warning(qApp->activeWindow(), "Обнаружено более одного модуля диагностики", "Выберите один из подключенных модулей диагностики и запустите проверку ещё раз");
            return false;
        }
        setdeviceCurrentID(validDeviceRow);
    }

    ParentWindow::beforeShow();
    m_pCentralView->rootContext()->setContextProperty("core", QVariant::fromValue(Core::instance()));
    m_pCentralView->rootContext()->setContextProperty("deviceList", QVariant::fromValue(m_pDeviceList));
    m_pCentralView->rootContext()->setContextProperty("device", m_pDeviceList->getDevice(deviceCurrentID()));
#ifdef FAKE_SONET
    m_pCentralView->rootContext()->setContextProperty("fake", QVariant::fromValue(true));
#else
    m_pCentralView->rootContext()->setContextProperty("fake", QVariant::fromValue(false));
#endif
    m_pCentralView->setSource(QUrl(QString::fromUtf8("qrc:qml/DiagnosticModuleForSurvey.qml")));
    return true;
}

