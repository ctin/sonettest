﻿#include "dialogcalibrdiagmodule.h"

using namespace TestWindows;

DialogCalibrDiagModule::DialogCalibrDiagModule(DeviceList *pDeviceList, QWidget *parent)
    : DialogCalibr(pDeviceList, parent)
{
    setWindowTitle("Калибровка модуля диагностики");
}

bool DialogCalibrDiagModule::beforeShow()
{
    if(m_pInputDevice->isSonet())
    {
        int count = 0, validDeviceRow = -1;
        for(int i = m_pDeviceList->length() - 1;  i >= 0;  i--)
            if(!m_pDeviceList->getDevice(i)->isSonet())
                if(!count++)
                    validDeviceRow = i;
        if(count != 1)
        {
            if(!count)
                QMessageBox::warning(qApp->activeWindow(), "Не обнаружено модулей диагностики", "Подключите модуль диагностики!");
            if(count > 1)
                QMessageBox::warning(qApp->activeWindow(), "Обнаружено более одного модуля диагностики", "Выберите один из подключенных модулей диагностики и запустите калибровку ещё раз");
            return false;
        }
        setdeviceCurrentID(validDeviceRow);
    }
    if(!DialogCalibr::beforeShow())
        return false;
    return true;
}

bool DialogCalibrDiagModule::whenProcessCollectedData()
{
    unsigned short code = getChannelValues()->last();
    Q_ASSERT(currentColumn() >= 0 && currentColumn() < g_parameterStorage.getPoints(deviceID()).size());
    addPointToResults(slot(), channel(), currentColumn(), code, getChannelValues()->last());
    if(toNextColumn())
        return false;
    if(!autoRegime())
        endProcess();
    return true;
}

void DialogCalibrDiagModule::whenProcessNewData()
{
    unsigned short code = getChannelValues()->last();
    Q_ASSERT(currentColumn() >= 0 && currentColumn() < g_parameterStorage.getPoints(deviceID()).size());
    addPointToResults(slot(), channel(), currentColumn(), code, getChannelValues()->last());
}

void DialogCalibrDiagModule::initCalibrChannel()
{
    Q_ASSERT(channel() >= 0);
    Q_ASSERT(channel() < channelsCount());
    Q_ASSERT(slot() >= 0);
    Q_ASSERT(slot() < SLOTS_COUNT);
    const ModuleDataItem &pItem = m_pInputDevice->getDataModel(slot())->getItem(channel());
    m_lastEEPROMValues.insert(channel(), pItem.deviceEEPROMValues);
    QList<unsigned short> tmpValues;
    for(int i = m_lastEEPROMValues[channel()].size() - 1;  i >= 0;  i--)
        tmpValues.append(0);
    m_pInputDevice->getDataModel(slot())->setData(channel(), QVariant::fromValue(tmpValues), ModuleDataModel::UserEEPROMValuesRole);
    m_pInputDevice->m_needSetEEPROMValues = true;
}

void DialogCalibrDiagModule::calculateFinalValues()
{
    m_newEEPROMValues[channel()].clear();
    QVector<qreal> points = g_parameterStorage.getPoints(deviceID());
    for(QVector<qreal>::ConstIterator pointIter  = points.constBegin();  pointIter != points.constEnd();  pointIter++)
    {
        if(!getCurrentResultsAddr().at(slot()).at(channel()).contains(*pointIter))
            qWarning() << "point not detected! calibr not correct!";
        m_newEEPROMValues[channel()].append(getCurrentResultsAddr().at(slot()).at(channel()).constFind(*pointIter).value().first);
        m_newEEPROMValues[channel()].append(*pointIter);
    }
    while(m_lastEEPROMValues[channel()].size() > m_newEEPROMValues[channel()].size())
        m_newEEPROMValues[channel()].append(0);
}

void DialogCalibrDiagModule::refreshInput(QString text)
{
    DialogCalibr::refreshInput(text);
}
