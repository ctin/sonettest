﻿#ifndef DIALOGMODBUSLINE_H
#define DIALOGMODBUSLINE_H

#include "parentwindow.h"
#include "dialogmodbuslinereport.h"
#include "ui_dialogmodbusline.h"

namespace TestWindows {

class DialogMODBUSLine : public QDialog, public Ui::DialogMODBUSLine
{
    Q_OBJECT
public:
    explicit DialogMODBUSLine(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogMODBUSLine();
    bool beforeShow();
private:
    SearchDevice *m_pSearchDevice;
    DeviceList *m_pDeviceList;
signals:
    
private slots:
    void on_pushButton_stop_clicked();
    void on_pushButton_start_clicked();
    void onDeviceFounded(int bprState, int rateIndex, int deviceNum, int autoSearch, QString port);
    void onDeviceFailed(int rateIndex, int deviceNum, int autoSearch, QString port, QString lastError);
    void refreshProgressBarFomat();
    void onError();

    void on_commandLinkButton_report_clicked();

    void on_lineEdit_name_textChanged(const QString &arg1);

    void on_lineEdit_numbersRes_textChanged(const QString &arg1);

    void on_lineEdit_numbersNoRes_textChanged(const QString &arg1);
    void onSearchDeviceRunningChanged(const bool running);
    void on_pushButton_getFile_clicked();

private:
    ModbusLineData m_modbusLineData;
    void refreshText();
};

}

#endif // DIALOGMODBUSLINE_H
