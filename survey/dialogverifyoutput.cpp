﻿#include "dialogverifyoutput.h"
#include "dialogreportfactory.h"
#include "macro.h"
#include "dialogwatchtypes.h"

using namespace TestWindows;
static const QString verifyLastCalibrKey("VerifyLastCalibr");

DialogVerifyOutput::DialogVerifyOutput(const VerifyType verifyType, DeviceList *pDeviceList, QWidget *parent)
    : DialogVerifyModule(false, pDeviceList, parent)
{
    setVerifyType(verifyType);
    connect(commandLinkButton_createReport, SIGNAL(clicked()), this, SLOT(createReport()), Qt::QueuedConnection);
}

DialogVerifyOutput::~DialogVerifyOutput()
{
    stopProcess();
}

bool DialogVerifyOutput::whenProcessCollectedData()
{
    // row is collected?
    bool skipRedCells = !autoRegime();
    if(toNextColumn(!skipRedCells))
        return false;
    commandLinkButton_createReport->setEnabled(true);
    // now can create report. Check other row if needed
    if(!autoRegime()) {
        if(toNextColumn(autoRegime()))
            return false;
       for(int chan = 0;  chan < channelsCount();  chan++)
           if(pointsCollected(slot(), chan) < g_parameterStorage.getPoints(deviceID()).size())
           {
               setchannel(chan);
               if(toNextColumn(!skipRedCells))
                   return false;
           }
    }
    // all cells collected, create report immediately
    if(!autoRegime())
        endProcess();
    //if(!autoRegime())
        //createReport();
    return true;
}

void DialogVerifyOutput::whenFinishProcess()
{
    if(autoRegime() && verifying())
        QMessageBox::information(this, "Поверка канала вывода выполнена!", QString("Поверка канала вывода выполнена!"));
}

void DialogVerifyOutput::whenEnterRegime()
{
}

void DialogVerifyOutput::whenLeaveRegime()
{
}

void DialogVerifyOutput::whenProcessNewData()
{
    const int pointsCount = 20;
    double min = getChannelValues()->last(), max = getChannelValues()->last();
    int size = getChannelValues()->size();
    int count = qMin(size, pointsCount);
    for(int i = size - 1;  i >= size - count;  i--)
    {
        qreal element = getChannelValues()->at(i);
        if(min > element)
            min = element;
        if(max < element)
            max = element;
    }
    addPointToResults(slot(), channel(), currentColumn(), min, max);
    if(pointsCollected(slot(), channel()) == tableWidget->columnCount())
        commandLinkButton_createReport->setEnabled(true);
}

void DialogVerifyOutput::refreshInput(QString text)
{
    QSettings settings;
    settings.setValue(verifyLastCalibrKey, comboBox_calibrator->currentText());
    m_calibrators[slot()] = text;
    m_pDeviceList->m_icsu_enabled = text.contains("ИКСУ-2000");
    if(text.isEmpty())
        return;
    if(text.contains("слот"))
    {
        QRegExp reg("\\D+");
        QStringList list = text.split(reg, QString::SkipEmptyParts);
        Q_ASSERT(list.size() >= 2);
        int inputdeviceCurrentID = list.at(0).toInt() - 1;
        DeviceWithData *pDevice = m_pDeviceList->getDevice(inputdeviceCurrentID);
        Q_ASSERT(pDevice);
        int AISlot = list.at(1).toInt() - 1;
        m_pDeviceList->setcurrentIndex(inputdeviceCurrentID);
        declarativeView_input->rootContext()->setContextProperty("globalSlot", AISlot);
        declarativeView_input->rootContext()->setContextProperty("device", pDevice);
        declarativeView_input->setSource(QUrl(QString::fromUtf8("qrc:/qml/verify/ModuleForVerify.qml")));
        m_pQMLObjectInput = declarativeView_input->rootObject();
        m_pQMLObjectInput->setProperty("currentRow", 0);
    }
    else if(text.contains("ИКСУ-2000"))
    {
        declarativeView_input->setSource(QUrl(QString::fromUtf8("qrc:/qml/verify/ICSU2000.qml")));
        m_pQMLObjectInput = declarativeView_input->rootObject();
        m_pDeviceList->getICSUDevice()->setunit(g_parameterStorage.getUnit(deviceID()));
        m_pDeviceList->getICSUDevice()->setreadOnly(true);
    }
    else if(text.contains("Agilent 34410A"))
    {
        declarativeView_input->setSource(QUrl(QString::fromUtf8("qrc:/qml/verify/Agilent34410A.qml")));
        m_pQMLObjectInput = declarativeView_input->rootObject();
        QString unit = g_parameterStorage.getUnit(deviceID());
        m_pQMLObjectInput->setProperty("unit", unit);
    }
    else if(text.contains("Agilent 34401A"))
    {
        declarativeView_input->setSource(QUrl(QString::fromUtf8("qrc:/qml/verify/Agilent34401A.qml")));
        m_pQMLObjectInput = declarativeView_input->rootObject();
        QString unit = g_parameterStorage.getUnit(deviceID());
        m_pQMLObjectInput->setProperty("unit", unit);
    }
    Q_ASSERT(m_pQMLObjectInput);
    connect(m_pQMLObjectInput, SIGNAL(signal_connectedChanged(bool)), this, SLOT(setControlButtonsEnabled()));
    setControlButtonsEnabled();
}

void DialogVerifyOutput::refreshOutput(QString text)
{
    if(text.isEmpty())
        return;
    if(text.contains("слот"))
    {
        QRegExp reg("\\D+");
        QStringList list = text.split(reg, QString::SkipEmptyParts);
        Q_ASSERT(!list.isEmpty());
        setslot(list.at(0).toInt() - 1);
        setchannel(0);
        declarativeView_output->rootContext()->setContextProperty("globalSlot", slot());
        declarativeView_output->rootContext()->setContextProperty("device", m_pDeviceList->getDevice(deviceCurrentID()));
        declarativeView_output->setSource(QUrl(QString::fromUtf8("qrc:/qml/verify/ModuleForVerify.qml")));
        m_pQMLObjectOutput = declarativeView_output->rootObject();
        connect(m_pQMLObjectOutput, SIGNAL(signal_connectedChanged(bool)), this, SLOT(setControlButtonsEnabled()));
        setControlButtonsEnabled();
        connect(this, SIGNAL(channelChanged(QVariant)), m_pQMLObjectOutput, SLOT(setCurrentRow(QVariant)));
        connect(m_pQMLObjectOutput, SIGNAL(currentRowChanged(int)), this, SLOT(setchannel(int)));
        m_pQMLObjectOutput->setProperty("currentRow", 0);
        m_pQMLObjectOutput->setProperty("unit", g_parameterStorage.getUnit(deviceID()));
        refreshTableContent();
        refreshOutputList();
    }
}

void DialogVerifyOutput::refreshInputList()
{
    QSettings settings;
    QString text = settings.value(verifyLastCalibrKey).toString();
    comboBox_input->clear();
    QStringList calibrators = g_parameterStorage.getCalibrators(deviceID());
    if(calibrators.contains("ИКСУ-2000"))
        comboBox_input->addItem("ИКСУ-2000");
    if(calibrators.contains("Agilent-34410A"))
        comboBox_input->addItem("Agilent 34410A");
    if(calibrators.contains("Agilent-34401A"))
        comboBox_input->addItem("Agilent 34401A");
    QString unit = g_parameterStorage.getUnit(deviceID());
    int length = m_pDeviceList->length();
    for(int pos = 0;  pos < length;  pos++)
    {
        DeviceWithData *pDevice = m_pDeviceList->getDevice(pos);
            for(int currentSlot = 0;  currentSlot < SLOTS_COUNT;  currentSlot++)
                if(pDevice->valid(currentSlot))
                    if(pDevice->analog(currentSlot))
                        if(pDevice->input(currentSlot))
                        {
                            Q_ASSERT(currentSlot < pDevice->deviceIDs().size());
                            if(unit == g_parameterStorage.getUnit(pDevice->deviceIDs().at(currentSlot)))
                                comboBox_input->addItem(QString("Контроллер %1, слот %3").arg(pDevice->componentID() + 1).arg(currentSlot + 1));
                        }
    }
    int index = comboBox_input->findText(text);
    comboBox_input->setCurrentIndex(qMax(index, 0));
    QFontMetrics fm(comboBox_input->font());
    comboBox_input->setMinimumWidth(fm.width(comboBox_input->itemText(comboBox_input->count() - 1)) + 25);
    refreshInput(comboBox_input->currentText());
}

void DialogVerifyOutput::refreshOutputList()
{
    comboBox_calibrator->clear();
    DeviceWithData *pDevice = m_pDeviceList->getDevice(deviceCurrentID()); //для данного контроллера
    for(int currentSlot = 0;  currentSlot < SLOTS_COUNT;  currentSlot++) //ищем в каждом слоте любой модуль вывода.
        if(pDevice->valid(currentSlot))
            if(pDevice->analog(currentSlot))
                if(!pDevice->input(currentSlot))
                    comboBox_calibrator->addItem(QString("слот %1").arg(currentSlot + 1));
}

void DialogVerifyOutput::createReport()
{
    endProcess();
    DialogReportFactory dialogReportFactory(false, m_pInputDevice, slot(), &getCurrentResultsAddr(), m_calibrators, getVerifyType(), this);
    dialogReportFactory.exec();
}




