﻿#ifndef DIALOGTEMPERATURE_H
#define DIALOGTEMPERATURE_H

#include "parentwindow.h"

namespace TestWindows {
class DialogTemperature : public ParentWindow
{
    Q_OBJECT
public:
    explicit DialogTemperature(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogTemperature();
    bool beforeShow();
private:
    QLabel *m_pLabelInfo;
    void __functionStart();
private slots:
    void refresh(qreal temp);
};
}

#endif // DIALOGTEMPERATURE_H
