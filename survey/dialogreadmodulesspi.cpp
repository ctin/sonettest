﻿#include "dialogreadmodulesspi.h"
#include "dialogwatchtypes.h"

using namespace TestWindows;

DialogReadModulesSpi::DialogReadModulesSpi(DeviceList *pDeviceList, QWidget *parent)
    : ParentWindow(pDeviceList, parent)
{
    setWindowTitle("Чтение из модулей по SPI");
    m_pTableWidget = new QTableWidget(this);
    setLayout(new QGridLayout());
    layout()->addWidget(m_pTableWidget);
    m_pTableWidget->setEditTriggers(QTableWidget::NoEditTriggers);
    m_pTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    m_pTableWidget->horizontalHeader()->setMinimumHeight(25);
    m_pTableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    QStringList str;
    str << "Данные"
        << "Осн. фон"
        << "Фон осн."
        << "Осн."
        << "Фон"
        << "Ноль"
        << "Макс.";
    m_pTableWidget->setColumnCount(str.count());
    m_pTableWidget->setHorizontalHeaderLabels(str);
    str.clear();
    resize(800, 600);
    setstartFlags(SurveyNeither);
}

DESTRUCT_FUNC_CLASS(DialogReadModulesSpi)

bool DialogReadModulesSpi::beforeShow()
{
    if(slot() < 0)
        setslot(DialogSelectModule::getSelectedModule(qApp->activeWindow(), m_pDeviceList, deviceCurrentID(), "ADIO"));
    if(slot() < 0)
        return false;
    ParentWindow::beforeShow();
    m_future = QtConcurrent::run(this, &DialogReadModulesSpi::__functionStart);

    m_pTableWidget->clearContents();
    m_pTableWidget->setRowCount(channelsCount());
    QStringList str;
    for(int chan = 0; chan < channelsCount();  chan++)
        str += QString::number(chan + 1);
    m_pTableWidget->setVerticalHeaderLabels(str);
    for(int row = m_pTableWidget->rowCount() - 1;  row >= 0;  row--)
        for(int col = m_pTableWidget->columnCount() - 1;  col >= 0;  col--)
        {
            QTableWidgetItem *pItem = new QTableWidgetItem();
            pItem->setTextAlignment(Qt::AlignCenter);
            m_pTableWidget->setItem(row, col, pItem);
        }
    return true;
}

void DialogReadModulesSpi::addData(int col, int chan, ByteArray data)
{
    //for(int chan = 0;  chan < channelsCount();  chan++)
    {
        m_pTableWidget->item(chan, col)->setText(QString("%1").arg(ushort((data.at(0) << 8) | data.at(1)), 4, 16, QChar('0')).toUpper());
        //pItem->setBackgroundColor(data.at(col) ? QColor(150, 150, 75, 75) : Qt::white);
    }
}

void DialogReadModulesSpi::__functionStart()
{
    START_FUNC
    ByteArray ans;
    PortOpener opener(m_pInputDevice);
    for(int chan = 0;  chan < channelsCount() && running();  chan++)
    {
        for(int column = 0;  column < m_pTableWidget->columnCount() && running();  column++)
            if(request(MODBUS_get_SPI(slot(), (column << 4) | chan, 1, m_pInputDevice->getCommandNamePtr()), &ans) && running())
                QMetaObject::invokeMethod(this, "addData", Q_ARG(int, column), Q_ARG(int, chan), Q_ARG(ByteArray, ans));
    }
    END_FUNC
}
