﻿#ifndef DIALOGREADMODULES_H
#define DIALOGREADMODULES_H

#include "parentwindow.h"

namespace TestWindows {
class DialogReadModules : public ParentWindow
{
    Q_OBJECT

public:
    explicit DialogReadModules(DeviceList *pDeviceList, QWidget *parent = 0);
    enum
    {
        COLUMN_COUNT = 16,
        ROW_COUNT = (5 + 1)  * SLOTS_COUNT
    };
    struct CellDataInfo
    {
        int row;
        int column;
        int module;
        unsigned char value;
    };
    ~DialogReadModules();
    bool beforeShow();
protected:
    QQueue<CellDataInfo> m_requestsToSendData;
    QMutex m_requestsMutex;
    void createTable();
    QTableWidget *m_pTableWidget;
    void __functionStart();
    void __getData(const int slot);
    void __setData();
private slots:
    void addData(int pos, ByteArray data);
    void onCellDoubleClicked(int row, int column);
    void onEditingFinished();
signals:
    void newData(int pos, ByteArray data);
protected:
    void addSlot(int slot);
};
}

#endif // DIALOGREADMODULES_H
