﻿#include "dialogreplytime.h"

using namespace TestWindows;

DialogReplyTime::DialogReplyTime(DeviceList *pDeviceList, QWidget *parent) :
    ParentWindow(pDeviceList, parent)
{
    setupUi(this);
    setWindowTitle("Замер времени ответа");
    setLayout(gridLayout);
    gridLayout->setMargin(5);
    connect(this, SIGNAL(minChanged(int)), lcdNumber_min, SLOT(display(int)));
    connect(this, SIGNAL(maxChanged(int)), lcdNumber_max, SLOT(display(int)));
    connect(this, SIGNAL(avgChanged(int)), lcdNumber_mid, SLOT(display(int)));
    connect(this, SIGNAL(countChanged(int)), lcdNumber_count, SLOT(display(int)));
    connect(this, SIGNAL(notSentChanged(int)), lcdNumber_notSent, SLOT(display(int)));
    connect(this, SIGNAL(notReceivedChanged(int)), lcdNumber_noReply, SLOT(display(int)));
    connect(this, SIGNAL(errorsChanged(int)), lcdNumber_errCount, SLOT(display(int)));
    connect(this, SIGNAL(crcerrorsChanged(int)), lcdNumber_CRCErrorsCount, SLOT(display(int)));
    setmin(0);
    setmax(0);
    setavg(0);
    setcount(0);
    setnotSent(0);
    setnotReceived(0);
    seterrors(0);
    setcrcerrors(0);
    connect(pushButton_quit, SIGNAL(clicked()), this, SLOT(reject()));
    setstartFlags(SurveyNeither);
}

DialogReplyTime::~DialogReplyTime()
{
    m_pInputDevice->setrunning(true);
    setrunning(false);
    m_future.waitForFinished();
}

void DialogReplyTime::warning(QString title, QString text)
{
    QMessageBox::warning(qApp->activeWindow(), title, text);
}

bool DialogReplyTime::beforeShow()
{
    ParentWindow::beforeShow();
    m_pInputDevice->setrunning(false);
    m_pInputDevice->m_underTests = false; // здесь нужен учет ошибок
    connect(m_pInputDevice, SIGNAL(runningChanged(bool)), pushButton_stop, SLOT(setEnabled(bool)));
    connect(m_pInputDevice, SIGNAL(runningChanged(bool)), pushButton_start, SLOT(setDisabled(bool)));
    connect(m_pInputDevice, SIGNAL(runningChanged(bool)), comboBox_type, SLOT(setDisabled(bool)));
    return true;
}

PROPERTY_CPP(int, DialogReplyTime, min)
PROPERTY_CPP(int, DialogReplyTime, max)
PROPERTY_CPP(int, DialogReplyTime, avg)
PROPERTY_CPP(int, DialogReplyTime, count)
PROPERTY_CPP(int, DialogReplyTime, notSent)
PROPERTY_CPP(int, DialogReplyTime, notReceived)
PROPERTY_CPP(int, DialogReplyTime, errors)
PROPERTY_CPP(int, DialogReplyTime, crcerrors)

void DialogReplyTime::__precessRequest()
{
    int currentReadTime = m_pInputDevice->lastReadTime();
    setcount(count() + 1);
    m_allTime += currentReadTime;
    if(currentReadTime < min())
        setmin(currentReadTime);
    if(currentReadTime > max())
        setmax(currentReadTime);
    setavg(m_allTime / count());

    if(!m_pInputDevice->txEnabled())
        setnotSent(notSent() + 1);
    if(!m_pInputDevice->rxEnabled())
        setnotReceived(notReceived() + 1);
    else if(!m_pInputDevice->lastError().isEmpty())
    {
        seterrors(errors() + 1);
        if(m_pInputDevice->lastError().contains("CRC"))
            setcrcerrors(crcerrors() + 1);
    }
    m_pInputDevice->unsetLastError();
    if(!m_pInputDevice->hardwareError().isEmpty())
    {
        setrunning(false);
        QMetaObject::invokeMethod(this, "warning", Q_ARG(QString, "Критическая ошибка"), Q_ARG(QString, "ОС заблокирвала порт!\nОстановка опроса"));
    }
}

void DialogReplyTime::__functionStart()
{
    m_pInputDevice->setrunning(true);
    setrunning(true);
    int currentType = comboBox_type->currentIndex();
    setmin(0); m_min = m_pInputDevice->waitTime();
    setmax(0);
    setavg(0);
    setcount(0);
    setnotSent(0);
    setnotReceived(0);
    seterrors(0);
    setcrcerrors(0);
    m_allTime = 0;
    bool k = false;
    {
        PortOpener lock(m_pInputDevice);
        m_pInputDevice->refreshConsist();
        if(!currentType)
        {
            while(running())
            {
                m_pInputDevice->request(MODBUS_get_version(m_pInputDevice->getCommandNamePtr()));
                __precessRequest();
                Sleepy::msleep(10);
            }
        }
        if(currentType == 1)
        {
            while(running())
            {
                for(int slot = 0; slot < SLOTS_COUNT;  slot++)
                {
                    ByteArray ans;
                    if(!m_pInputDevice->valid(slot))
                        continue;
                    int chCount = m_pInputDevice->channelsCount().at(slot);
                    if(!IS_CHANNEL_VALID(chCount))
                        continue;
                    if(!m_pInputDevice->analog(slot))
                    {
                        k = true;
                        if(m_pInputDevice->request(MODBUS_get_diskret_channels_N(slot, chCount, m_pInputDevice->getCommandNamePtr()), &ans) && ans.size() >= (1 + (chCount - 1) / 8 + 1))
                        {
                            QVariantList list;
                            for(int chan = 0;  chan < chCount;  chan++)
                                list.append((bool)((ans.at(chan / 8 + 1) & (1 << chan % 8)) >> chan % 8));
                            m_pInputDevice->getDataModel(slot)->setData(0, chCount, list, ModuleDataModel::DeviceValueRole);
                        }
                        __precessRequest();
                    }
                }
                if(!k)
                {
                    setrunning(false);
                    QMetaObject::invokeMethod(this, "warning", Q_ARG(QString, "Невозможно провести проверку"), Q_ARG(QString, "Нет дискретных модулей"));

                }
            }
        }
        else
        {
            while(running())
            {
                for(int slot = 0; slot < SLOTS_COUNT;  slot++)
                {
                    ByteArray ans;
                    if(!m_pInputDevice->valid(slot))
                        continue;
                    int chCount = m_pInputDevice->channelsCount().at(slot);
                    if(!IS_CHANNEL_VALID(chCount))
                        continue;
                    if(m_pInputDevice->analog(slot) && m_pInputDevice->input(slot))
                    {
                        k = true;
                        if(m_pInputDevice->request(MODBUS_get_analog_channels(slot, m_pInputDevice->getCommandNamePtr()), &ans) && ans.size() >= (2 + chCount * 2))
                            for(int chan = 0;  chan < chCount;  chan++)
                            {
                                QVariantList list;
                                for(int chan = 0;  chan < chCount;  chan++)
                                    list.append((ushort)(ans.at(2 + chan * 2) << 8 )| (uchar)ans.at(3 + chan * 2));
                                m_pInputDevice->getDataModel(slot)->setData(0, chCount, list, ModuleDataModel::DeviceValueRole);
                            }
                        __precessRequest();
                    }
                }
                if(!k)
                {
                    setrunning(false);
                    QMetaObject::invokeMethod(this, "warning", Q_ARG(QString, "Невозможно провести проверку"), Q_ARG(QString, "Нет модулей аналогового ввода!"));
                }
            }
        }
    }
    m_pInputDevice->setrunning(false);
}

void TestWindows::DialogReplyTime::on_pushButton_stop_clicked()
{
    setrunning(false);
    m_future.waitForFinished();
}

void TestWindows::DialogReplyTime::on_pushButton_start_clicked()
{
    if(m_future.isRunning())
        return;
    m_future = QtConcurrent::run(this, &DialogReplyTime::__functionStart);
}
