﻿#include "dialogcommtest.h"

using namespace TestWindows;

DialogCommTest::DialogCommTest(DeviceList *pDeviceList, QWidget *parent)
    : QDialog(parent), m_pDeviceList(pDeviceList)
{
    setModal(true);
    setWindowTitle("Проверка связи");
    m_pSearchDevice = pDeviceList->getSearchDevice();
    m_pSearchDevice->setrunning(false);//здесь running - показатель процесса поиска.
    setupUi(this);
    setLayout(gridLayout);
    connect(pushButton_refresh, SIGNAL(clicked()), this, SLOT(refreshPortList()));
    connect(comboBox_port, SIGNAL(currentIndexChanged(QString)), m_pSearchDevice, SLOT(setport(QString)));
    connect(m_pSearchDevice, SIGNAL(runningChanged(bool)), pushButton_start, SLOT(setDisabled(bool)));
    connect(m_pSearchDevice, SIGNAL(runningChanged(bool)), pushButton_stop, SLOT(setEnabled(bool)));
    connect(m_pSearchDevice, SIGNAL(runningChanged(bool)), this, SLOT(refreshProgressBarFomat()));
    connect(m_pSearchDevice, SIGNAL(runningChanged(bool)), pushButton_refresh, SLOT(setDisabled(bool)));
    connect(m_pSearchDevice, SIGNAL(runningChanged(bool)), comboBox_port, SLOT(setDisabled(bool)));
    connect(m_pSearchDevice, SIGNAL(runningChanged(bool)), spinBox_speed, SLOT(setDisabled(bool)));

    connect(m_pSearchDevice, SIGNAL(deviceNumChanged(int)), progressBar, SLOT(setValue(int)));
    connect(m_pSearchDevice, SIGNAL(portChanged(QString)), this, SLOT(refreshProgressBarFomat()), Qt::QueuedConnection);
    connect(m_pSearchDevice, SIGNAL(deviceFounded(int,int,int,int,QString)), this, SLOT(onDeviceFounded(int,int,int,int,QString)));
    connect(m_pSearchDevice, SIGNAL(deviceFailed(int,int,int,QString,QString)), this, SLOT(onDeviceFailed(int,int,int,QString,QString)));
    on_spinBox_speed_valueChanged(spinBox_speed->value());
    connect(m_pSearchDevice, SIGNAL(hardwareErrorChanged(QString)), this, SLOT(onError()));
}

DialogCommTest::~DialogCommTest()
{
    on_pushButton_stop_clicked();
    m_pDeviceList->settargetDeviceIndex(DeviceList::All);
}

bool DialogCommTest::beforeShow()
{
    m_pDeviceList->settargetDeviceIndex(-3);
    refreshPortList();
    spinBox_speed->setValue(g_defaultRateIndex);
    if(m_pDeviceList->length())
    {
        const DeviceWithData *pDevice = m_pDeviceList->getDevice(m_pDeviceList->currentIndex());
        spinBox_speed->setValue(pDevice->rateIndex());
        QString port = pDevice->port();
        comboBox_port->setCurrentIndex(comboBox_port->findText(port));
    }
    on_pushButton_start_clicked();

    return true;
}

void DialogCommTest::onError()
{
    if(m_pSearchDevice->hardwareError().isEmpty())
        return;
    QMessageBox msgBox(this);
    msgBox.setMinimumWidth(320);
    msgBox.setWindowTitle("Ошибка!");
    msgBox.setText("Ошибка COM порта!");
    msgBox.setInformativeText(QString("Ошибка работы с портом %1. Продолжение невозможно").arg(m_pSearchDevice->port()));
    msgBox.setDetailedText(m_pSearchDevice->hardwareError());
    msgBox.exec();
}

static const char allPortsText[] = "Все порты";

void DialogCommTest::refreshPortList()
{
    comboBox_port->clear();
    if(g_autoSearchEnabled)
    {
        comboBox_port->addItem(allPortsText);
        comboBox_port->addItems(availablePorts());
    }
    else
        comboBox_port->addItem(g_defaultPort);
}

void DialogCommTest::onDeviceFounded(int bprState, int rateIndex, int deviceNum, int autoSearch, QString port)
{
    Q_UNUSED(bprState)
    Q_UNUSED(rateIndex);
    Q_UNUSED(autoSearch);
    Q_UNUSED(port);
    QString bprStateStr = "БПР: ";
    if(bprState != -1)
    {
        if(bprState & 0x02)
            bprStateStr += QString("работает, ") + QString(bprState & 1 ? "резервный" : "основной") + QString(".");
        else
            bprStateStr += "отключен.";
    }
    listWidget->addItem(QString("Прибор №%1").arg(deviceNum) + "\t" + bprStateStr); //поправлено с учетом ТУ.
}

void DialogCommTest::onDeviceFailed(int rateIndex, int deviceNum, int autoSearch, QString port, QString lastError)
{
    Q_UNUSED(rateIndex);
    Q_UNUSED(autoSearch);
    Q_UNUSED(port);
    listWidget->addItem(QString("По запросу прибору №%1 пришла ошибка:\n%2").arg(deviceNum).arg(lastError));
}

void TestWindows::DialogCommTest::on_pushButton_start_clicked()
{
    m_pDeviceList->clear();
    listWidget->clear();
    if(comboBox_port->currentText() == allPortsText)
        m_pSearchDevice->unsetPort();
    m_pSearchDevice->findAllSlow(checkBox_MODBUS1->isChecked(), checkBox_MODBUS2->isChecked());
}

void DialogCommTest::refreshProgressBarFomat()
{
    QString format;
    if(m_pSearchDevice->running())
        format = QString("Порт: %1, номер прибора %v").arg(m_pSearchDevice->port());
    else
        format = QString("Ожидаю старта");
    progressBar->setFormat(format);
}

void TestWindows::DialogCommTest::on_pushButton_stop_clicked()
{
    m_pSearchDevice->stop();
}

void TestWindows::DialogCommTest::on_spinBox_speed_valueChanged(int arg1)
{
    m_pSearchDevice->setrateIndex(arg1);
    label_realRate->setText(QString::number(m_pSearchDevice->realRate()));
}
