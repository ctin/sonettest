﻿#ifndef DIALOGWATCHAI_H
#define DIALOGWATCHAI_H

#include "dialogallchannels.h"

namespace TestWindows {

class DialogAllChannelsWithFilter : public DialogAllChannels
{
    Q_OBJECT
public:
    explicit DialogAllChannelsWithFilter(DeviceList *pDeviceList, QString filter, QWidget *parent = 0);
    bool beforeShow();
protected:
    QString m_filter;
};

class DialogSelectModule : public DialogAllChannelsWithFilter
{
    Q_OBJECT
public:
    explicit DialogSelectModule(DeviceList *pDeviceList, QString filter, QWidget *parent = 0);
    bool beforeShow();
    static int getSelectedModule(QWidget *parent, DeviceList *pDeviceList, int deviceCurrentID, QString filter);
};
}
#endif // DIALOGWATCHAI_H
