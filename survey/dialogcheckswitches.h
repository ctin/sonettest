﻿#ifndef DIALOGCHECKSWITCHES_H
#define DIALOGCHECKSWITCHES_H

#include "parentwindow.h"

namespace TestWindows {
class DialogCheckSwitches : public ParentWindow
{
    Q_OBJECT
public:
    DialogCheckSwitches(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogCheckSwitches();
    bool beforeShow();
private:
    void __functionStart();
    QLCDNumber *m_pLCDNumberSpeed, *m_pLCDNumberRealSpeed;
    QLCDNumber *m_pLCDNumberAddress10, *m_pLCDNumberAddress1, *m_pLCDNumberAddress;
private slots:
    void displayResult();
};
}

#endif // DIALOGCHECKSWITCHES_H
