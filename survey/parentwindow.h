﻿#ifndef PARENTWINDOW_H
#define PARENTWINDOW_H

#include <QtGui>
#include <QtConcurrent/QtConcurrent>
#include "objects.h"
#include "devicelist.h"
#define START_FUNC  setrunning(true); while(running()) { {    QMutexLocker lock(&m_deviceMutex);

#define END_FUNC    } surveyDelay(20); }

#define DESTRUCT_FUNC_CLASS(class) class :: ~class() {setrunning(false); QMutexLocker lock(&m_deviceMutex); m_future.waitForFinished();}

namespace TestWindows {
class ParentWindow : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(int deviceCurrentID READ deviceCurrentID WRITE setdeviceCurrentID NOTIFY deviceCurrentIDChanged)
    Q_PROPERTY(int slot READ slot WRITE setslot NOTIFY slotChanged)
    Q_PROPERTY(int channel READ channel WRITE setchannel NOTIFY channelChanged)
    Q_PROPERTY(int deviceNum READ deviceNum WRITE setdeviceNum NOTIFY deviceNumChanged)
    Q_PROPERTY(int deviceID READ deviceID WRITE setdeviceID NOTIFY deviceIDChanged)
    Q_PROPERTY(int type READ type WRITE settype NOTIFY typeChanged)
    Q_PROPERTY(int channelsCount READ channelsCount WRITE setchannelsCount NOTIFY channelsCountChanged)
    Q_PROPERTY(int startFlags READ startFlags WRITE setstartFlags NOTIFY startFlagsChanged)
    Q_PROPERTY(bool running READ running WRITE setrunning NOTIFY runningChanged)
public:

    explicit ParentWindow(DeviceList *pDeviceList, QWidget *parent = 0);
    ~ParentWindow();
    virtual bool beforeShow();
    bool m_isOnline; //требуется чтоб был онлайн контроллер
protected:
    enum {
        SurveyNeither = 1 << 0,
        SurveyCurrent = 1 << 1,
        SurveyAll = 1 << 2
    };
    bool request(QByteArray req, ByteArray *ans = 0, DeviceWithData *pDevice = 0);
    bool request(RequestFunc func, DeviceWithData *pDevice = 0);

signals:

protected:
    QMutex m_deviceMutex;
    QFuture<void> m_future;
    void abort();
    const int m_sleepTime;
    void surveyDelay(int delayTime);
private slots:
    void onHardwareError();
protected:
    DeviceList *m_pDeviceList;
    DeviceWithData *m_pInputDevice;

//properties
public:
    int deviceCurrentID() const;
    int channel() const;
    int slot() const;
    int deviceNum() const;
    int deviceID() const;
    int type() const;
    int channelsCount() const;
    int startFlags() const;
    bool running() const;
public slots:
    void setdeviceCurrentID(const int deviceCurrentID);
    void setchannel(const int chan);
    void setslot(const int slot);
    void setdeviceNum(const int deviceNum);
    void setdeviceID(const int deviceID);
    void settype(const int type);
    void setchannelsCount(const int channelsCount);
    void ondeviceCurrentIDChanged();
    void onSlotChanged();
    void setstartFlags(int arg);
    void setrunning(bool arg);
signals:
    void deviceCurrentIDChanged(int deviceCurrentID);
    void slotChanged(int slot);
    void channelChanged(int channel);
    void deviceNumChanged(int deviceNum);
    void deviceIDChanged(int deviceID);
    void typeChanged(int type);
    void channelsCountChanged(int channelsCount);
    void channelChanged(QVariant);
    void startFlagsChanged(int arg);
    void runningChanged(bool arg);
private:
    int m_deviceCurrentID;
    int m_slot;
    int m_deviceNum;
    int m_deviceID;
    int m_type;
    int m_channelsCount;
protected:
    int m_channel;
    int m_lastChannel;
    int m_startFlags;
    volatile bool m_running;
};
}

#endif // PARENTWINDOW_H
