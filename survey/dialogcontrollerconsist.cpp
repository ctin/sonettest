﻿#include "dialogcontrollerconsist.h"
#include <QtConcurrent/QtConcurrent>

using namespace TestWindows;

DialogControllerConsist::DialogControllerConsist(DeviceList *pDeviceList, QWidget *parent)
    : ParentWindow(pDeviceList, parent)
{
    setWindowTitle("Состав контроллера");
    QStringList strList;
    strList << "ID"
            << "Тип модуля"
            << "Каналы"
            << "Обозначение"
            << "КУНИ"
            << "дополнительно"
            << "Калибровка"
            << "Поверка";
    m_pTableWidget = new QTableWidget(this);
    m_pTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_pTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    m_pTableWidget->setAlternatingRowColors(true);
    m_pTableWidget->setRowCount(SLOTS_COUNT);
    m_pTableWidget->setColumnCount(strList.size());
    m_pTableWidget->setEditTriggers(QTableWidget::NoEditTriggers);
    m_pTableWidget->setHorizontalHeaderLabels(strList);
    strList.clear();
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
        strList << QString::number(slot + 1);
    m_pTableWidget->setVerticalHeaderLabels(strList);
    QHBoxLayout *pLayout = new QHBoxLayout(this);
    pLayout->addWidget(m_pTableWidget);
    setstartFlags(SurveyNeither);
    connect(m_pTableWidget->horizontalHeader(), SIGNAL(sectionResized(int,int,int)), m_pTableWidget, SLOT(resizeRowsToContents()));
    connect(m_pTableWidget->verticalHeader(), SIGNAL(sectionResized(int,int,int)), m_pTableWidget, SLOT(resizeColumnsToContents()));

    connect(this, SIGNAL(refresh()), this, SLOT(refreshTableConsist()), Qt::QueuedConnection);
}

 DESTRUCT_FUNC_CLASS(DialogControllerConsist)

void DialogControllerConsist::__functionStart()
{
    START_FUNC
    PortOpener opener(m_pInputDevice);
    bool k = request(&DeviceWithData::refreshConsist, m_pInputDevice) && running();
    if(k)
        k = request(&DeviceWithData::readEEPROMData, m_pInputDevice);
    if(k)
        emit refresh();
    END_FUNC
}

bool DialogControllerConsist::beforeShow()
{
    ParentWindow::beforeShow();
    m_future = QtConcurrent::run(this, &DialogControllerConsist::__functionStart);
    refreshTableConsist();
    return true;
}

void DialogControllerConsist::refreshTableConsist()
{
    m_pTableWidget->clearContents();
    for(int row = m_pTableWidget->rowCount();  row >= 0;  row--)
        for(int col = m_pTableWidget->columnCount() - 1;  col >= 0;  col--)
            m_pTableWidget->setItem(row, col, new QTableWidgetItem);
    for(int slot = 0;  slot < SLOTS_COUNT;  slot++)
    {
        const int type = m_pInputDevice->types().at(slot);
        const int id = m_pInputDevice->deviceIDs().at(slot);
        const int channelsCount = m_pInputDevice->channelsCount().at(slot);
        int step = 0;
        m_pTableWidget->item(slot, step++)->setText(QString::number(id));
        m_pTableWidget->item(slot, step++)->setText(!m_pInputDevice->valid(slot) ? QString::number(type)
            : (QString(type & 0x40 ? "Аналоговый" : "Дискретный") + QString(" ") + QString(type & 0x20 ? "вывод" : "ввод")));
        m_pTableWidget->item(slot, step++)->setText(QString::number(channelsCount));
        m_pTableWidget->item(slot, step++)->setText(g_parameterStorage.getName(id));
        m_pTableWidget->item(slot, step++)->setText(g_parameterStorage.getKYNI(id));
        m_pTableWidget->item(slot, step++)->setText(g_parameterStorage.getComments(id));
        ModuleDataModel *pModel = m_pInputDevice->getDataModel(slot);
        Q_ASSERT(pModel);
        QDate date = pModel->data(QModelIndex(), ModuleDataModel::DeviceCalibrDateRole).toDate();
        QString fio = QString::fromUtf8(pModel->data(QModelIndex(), ModuleDataModel::DeviceCalibrFullNameRole).toByteArray());
        QString text = (!date.isValid() || date.isNull()) ? "отсутствует" : date.toString("yyyy.MM.dd");
        if(!fio.isEmpty() && fio.at(0) != '\0')
        {
            text += "\n\r" + fio;
        }
        m_pTableWidget->item(slot, step++)->setText(text);
        date = pModel->data(QModelIndex(), ModuleDataModel::DeviceVerifyDateRole).toDate();
        //fio = QString::fromUtf8(pModel->data(QModelIndex(), ModuleDataModel::DeviceVerifyFullNameRole).toByteArray());
        text = (!date.isValid() || date.isNull()) ? "отсутствует" : date.toString("yyyy.MM.dd");
        //if(!fio.isEmpty() && fio.at(0) != '\0')
            //text += "\n\r" + fio;
        m_pTableWidget->item(slot, step++)->setText(text);
    }
    m_pTableWidget->resizeRowsToContents();
    m_pTableWidget->resizeColumnsToContents();
    int h = 0, w = 0;
    for(int col = m_pTableWidget->columnCount() - 1;  col >= 0;  col--)
        w += m_pTableWidget->columnWidth(col);
    w += m_pTableWidget->frameWidth() * 2 + m_pTableWidget->verticalHeader()->width();
    w += m_pTableWidget->columnCount() * 4;
    for(int row = m_pTableWidget->rowCount();  row >= 0;  row--)
        h += m_pTableWidget->rowHeight(row);
    h += m_pTableWidget->horizontalHeader()->height();
    h += m_pTableWidget->rowCount() * 4;
    resize(w, h);
}
