﻿#ifndef DIALOGCONTROLLERCONSIST_H
#define DIALOGCONTROLLERCONSIST_H

#include "parentwindow.h"

namespace TestWindows {
class DialogControllerConsist : public ParentWindow
{
    Q_OBJECT
public:
    DialogControllerConsist(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogControllerConsist();
    bool beforeShow();
protected:
    void __functionStart();
public slots:
    void refreshTableConsist();
private:
    QTableWidget *m_pTableWidget;
signals:
    void refresh();
};
}

#endif // DIALOGCONTROLLERCONSIST_H
