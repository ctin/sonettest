﻿#ifndef DialogVerifyInputInput_H
#define DialogVerifyInputInput_H

#include "dialogverifymodule.h"

namespace TestWindows {
class DialogVerifyInput : public DialogVerifyModule
{
    Q_OBJECT
public:
    DialogVerifyInput(const VerifyType type, DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogVerifyInput();
    void afterExec();
protected slots:
    void createReport();
    virtual bool whenProcessCollectedData();
    virtual void whenFinishProcess();
protected:
    virtual void saveVerificationData();
    virtual void whenEnterRegime();
    virtual void whenLeaveRegime();
    virtual void whenProcessNewData();
};
}

#endif // DialogVerifyInputInput_H
