﻿#include "dialogwatchtypes.h"

using namespace TestWindows;

DialogAllChannelsWithFilter::DialogAllChannelsWithFilter(DeviceList *pDeviceList, QString filter, QWidget *parent) :
    DialogAllChannels(pDeviceList, parent), m_filter(filter)
{
    QString text = "Просмотр модулей ";
    if(filter.contains('A'))
        text += "аналогового ";
    else if(filter.contains('D'))
        text += "дискретного ";
    if(filter.contains('I'))
        text += "ввода";
    else if(filter.contains('O'))
        text += "вывода";
    setWindowTitle(text);
}

bool DialogAllChannelsWithFilter::beforeShow()
{
    DialogAllChannels::beforeShow();
    m_pCentralView->rootObject()->setProperty("filter", m_filter);
    return true;
}

DialogSelectModule::DialogSelectModule(DeviceList *pDeviceList, QString filter, QWidget *parent)
    : DialogAllChannelsWithFilter(pDeviceList, filter, parent)
{
    setWindowTitle("Выбор модуля. Укажите модуль");
}

bool DialogSelectModule::beforeShow()
{
    DialogAllChannelsWithFilter::beforeShow();
    connect(m_pCentralView->rootObject(), SIGNAL(selected(int)), this, SLOT(done(int)), Qt::QueuedConnection);
    m_pCentralView->rootObject()->setProperty("selectionMode", true);
    return true;
}

int DialogSelectModule::getSelectedModule(QWidget *parent, DeviceList *pDeviceList, int deviceCurrentID, QString filter)
{
    DialogSelectModule dialog(pDeviceList, filter, parent);
    dialog.setdeviceCurrentID(deviceCurrentID);
    int count = 0, slot;
    DeviceWithData *pDevice = pDeviceList->getDevice(deviceCurrentID);
    int firstValidSlot = -1;
    for(slot = 0;  slot < 8 && count < 2;  slot++) {
        if(pDevice->valid(slot)) {
            if((pDevice->analog(slot) && filter.indexOf('A') != -1)
                    || (!pDevice->analog(slot) && filter.indexOf('D') != -1))
                if((pDevice->input(slot) && filter.indexOf('I') != -1)
                        || (!pDevice->input(slot) && filter.indexOf('O') != -1))
                    if(!count++)
                        firstValidSlot = slot;
        }
    }
    if(!count)
    {
        QMessageBox::warning(qApp->activeModalWidget(), "Нет подходящих модулей", "Подходящие модули отсутствуют!");
        return -1;
    }
    if(count == 1)
        return firstValidSlot;
    dialog.beforeShow();
    int ret = dialog.exec() - 1;
    if(ret < 0)
        QMessageBox::warning(qApp->activeModalWidget(), "Не выбран модуль", "Модуль не выбран, процесс прекращен");
    return ret;
}
