﻿#include "dialogtemperature.h"

using namespace TestWindows;

DialogTemperature::DialogTemperature(DeviceList *pDeviceList, QWidget *parent) :
    ParentWindow(pDeviceList, parent)
{
    setWindowTitle("Проверка температуры");
    m_pLabelInfo = new  QLabel(this);
    setLayout(new QGridLayout());
    layout()->addWidget(m_pLabelInfo);
    setstartFlags(SurveyNeither);
}

DESTRUCT_FUNC_CLASS(DialogTemperature)

void DialogTemperature::__functionStart()
{
    START_FUNC
    PortOpener opener(m_pInputDevice);
    ByteArray ans;
    if(request(MODBUS_get_termo(m_pInputDevice->getCommandNamePtr()), &ans) && running())
    {
        if(ans.size() >= 2)
        {
            ushort val = (ans.at(0) << 8) | (ans.at(1) & 0xff);
            QMetaObject::invokeMethod(this, "refresh", Qt::QueuedConnection, Q_ARG(qreal, (val * 250.0) / 0x400 - 123.5));
        }
    }
    END_FUNC
}

bool DialogTemperature::beforeShow()
{
    if(!m_pInputDevice->isSonet())
    {
        QMessageBox::warning(qApp->activeWindow(), "Ошибочный прибор", "Выберите контроллер сонет");
        return false;
    }
    ParentWindow::beforeShow();
    m_future = QtConcurrent::run(this, &DialogTemperature::__functionStart);
    return true;
}

void DialogTemperature::refresh(qreal temp)
{
    m_pLabelInfo->setText(QString("0x%1 -> %2 °C").arg((unsigned short)(((temp + 123.5) * 0x400) / 250), 0, 16).arg(temp, 0, 'f', 1));
}
