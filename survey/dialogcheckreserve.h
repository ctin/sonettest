﻿#ifndef DIALOGCHECKRESERVE_H
#define DIALOGCHECKRESERVE_H

#include "parentwindow.h"

namespace TestWindows {
class DialogCheckReserve : public ParentWindow
{
    Q_OBJECT
public:
    explicit DialogCheckReserve(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogCheckReserve();
    bool beforeShow();
private:
    QLabel *m_pLabelInfo;
    QRadioButton *m_pRadioButtonBase, *m_pRadioButtonReserve;
    QPushButton *m_pButtonStart;
    DeviceWithData* m_pSecondDevice;
    QTime m_timer;
public slots:
    void switchReserve();
    void switchReserveTwice();
private slots:
    void onSecondHardwareError();
protected:
    void __switchReserve(const bool baseChecked, const bool twice);
    void __switchReserveWrapper(const bool baseChecked, const bool twice);
    bool __sendRequest(DeviceWithData *pDevice, const bool active);
    bool __waitForChange(DeviceWithData *pDevice, int *pTimeToSwitch);
    void __reportError();
};
}

#endif // DIALOGCHECKRESERVE_H
