﻿#ifndef DIALOGALLCHANNELS_H
#define DIALOGALLCHANNELS_H

#include "parentwindow.h"

namespace TestWindows {
class DialogAllChannels : public ParentWindow
{
    Q_OBJECT
public:
    explicit DialogAllChannels(DeviceList *pDeviceList, QWidget *parent = 0);

    bool beforeShow();
protected:
    QDeclarativeView *m_pCentralView;
    void closeEvent(QCloseEvent *);

};
}

#endif // DIALOGALLCHANNELS_H
