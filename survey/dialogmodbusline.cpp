﻿#include "dialogmodbusline.h"

using namespace TestWindows;

DialogMODBUSLine::DialogMODBUSLine(DeviceList *pDeviceList, QWidget *parent)
    : QDialog(parent), m_pDeviceList(pDeviceList)
{
    setModal(true);
    setWindowTitle("Проверка связи");

    m_pSearchDevice = pDeviceList->getSearchDevice();
    m_pSearchDevice->setrunning(false);//здесь running - показатель процесса поиска.
    setupUi(this);
    on_lineEdit_name_textChanged(lineEdit_name->text());
    on_lineEdit_numbersNoRes_textChanged(lineEdit_numbersNoRes->text());
    on_lineEdit_numbersRes_textChanged(lineEdit_numbersRes->text());
    connect(m_pSearchDevice, SIGNAL(runningChanged(bool)), this, SLOT(onSearchDeviceRunningChanged(bool)));

    setLayout(gridLayout);
}

DialogMODBUSLine::~DialogMODBUSLine()
{
    on_pushButton_stop_clicked();
    m_pDeviceList->settargetDeviceIndex(DeviceList::All);
}

bool DialogMODBUSLine::beforeShow()
{
    m_pDeviceList->settargetDeviceIndex(-3);
    pushButton_start->setEnabled(!m_pSearchDevice->running());
    pushButton_stop->setEnabled(m_pSearchDevice->running());

    connect(m_pSearchDevice, SIGNAL(runningChanged(bool)), pushButton_start, SLOT(setDisabled(bool)));
    connect(m_pSearchDevice, SIGNAL(runningChanged(bool)), pushButton_stop, SLOT(setEnabled(bool)));
    connect(m_pSearchDevice, SIGNAL(deviceFounded(int,int,int,int,QString)), this, SLOT(onDeviceFounded(int,int,int,int,QString)));
    connect(m_pSearchDevice, SIGNAL(deviceFailed(int,int,int,QString,QString)), this, SLOT(onDeviceFailed(int,int,int,QString,QString)));
    connect(m_pSearchDevice, SIGNAL(deviceNumChanged(int)), progressBar, SLOT(setValue(int)));
    connect(m_pSearchDevice, SIGNAL(portChanged(QString)), this, SLOT(refreshProgressBarFomat()));
    connect(m_pSearchDevice, SIGNAL(hardwareErrorChanged(QString)), this, SLOT(onError()));

    return true;
}

void TestWindows::DialogMODBUSLine::on_pushButton_stop_clicked()
{
    m_pSearchDevice->stop();
}

void TestWindows::DialogMODBUSLine::on_pushButton_start_clicked()
{    
    //m_pDeviceList->clear();
    textEdit_result->clear();
    m_modbusLineData.clear();
    m_pSearchDevice->findAllSlow(true, true);
}

void DialogMODBUSLine::onDeviceFounded(int bprState, int rateIndex, int deviceNum, int autoSearch, QString port)
{
    Q_UNUSED(rateIndex);
    Q_UNUSED(autoSearch);
    Q_UNUSED(port);
    if(bprState != -1 && bprState & 0x02)
        m_modbusLineData.m_nodesFoundedRes.append(deviceNum);
    else
        m_modbusLineData.m_nodesFoundedNoRes.append(deviceNum);
    refreshText();
}

void DialogMODBUSLine::refreshProgressBarFomat()
{
    QString format;
    if(m_pSearchDevice->running())
        format = QString("Порт: %1, номер прибора %v").arg(m_pSearchDevice->port());
    else
        format = QString("Ожидаю старта");
    progressBar->setFormat(format);
}

void DialogMODBUSLine::onDeviceFailed(int rateIndex, int deviceNum, int autoSearch, QString port, QString lastError)
{
    Q_UNUSED(rateIndex);
    Q_UNUSED(autoSearch);
    Q_UNUSED(port);
    Q_UNUSED(lastError);
    m_modbusLineData.m_nodesErr.append(deviceNum);
    refreshText();
}

void DialogMODBUSLine::refreshText()
{
    QString result;
    result.append(QString("Обнаружено контроллеров в сети: %1\n").arg(m_modbusLineData.m_nodesFoundedRes.size() + m_modbusLineData.m_nodesFoundedNoRes.size()));
    result.append(QString("С резервом: %1\n").arg(m_modbusLineData.m_nodesFoundedRes.size()));
    for(QList<int>::ConstIterator iter = m_modbusLineData.m_nodesFoundedRes.constBegin();  iter != m_modbusLineData.m_nodesFoundedRes.constEnd();  iter++)
        result.append(QString(" ") + QString::number(*iter));
    result.append(QString("Без резерва: %1\n").arg(m_modbusLineData.m_nodesFoundedNoRes.size()));
    for(QList<int>::ConstIterator iter = m_modbusLineData.m_nodesFoundedNoRes.constBegin();  iter != m_modbusLineData.m_nodesFoundedNoRes.constEnd();  iter++)
        result.append(QString(" ") + QString::number(*iter));
    result.append("\n");
    result.append(QString("Ошибок: %1\n").arg(m_modbusLineData.m_nodesErr.size()));
    for(QList<int>::ConstIterator iter = m_modbusLineData.m_nodesErr.constBegin();  iter != m_modbusLineData.m_nodesErr.constEnd();  iter++)
        result.append(QString(" ") + QString::number(*iter));
    textEdit_result->setText(result);

}

void DialogMODBUSLine::onError()
{
    if(m_pSearchDevice->hardwareError().isEmpty())
        return;
    QMessageBox msgBox(this);
    msgBox.setMinimumWidth(320);
    msgBox.setWindowTitle("Ошибка!");
    msgBox.setText("Ошибка COM порта!");
    msgBox.setInformativeText("Ошибка работы с COM портом. Продолжение невозможно");
    msgBox.setDetailedText(m_pSearchDevice->hardwareError());
    msgBox.exec();
}

void TestWindows::DialogMODBUSLine::on_commandLinkButton_report_clicked()
{
    on_pushButton_stop_clicked();
    DialogMODBUSLineReport dialogMODBUSLineReport(&m_modbusLineData, this);
    dialogMODBUSLineReport.exec();
    int pos = 0;
    QRegExp rx("\\d+");
    m_modbusLineData.m_nodesRes.clear();
    QString text = lineEdit_numbersRes->text();
    while ((pos = rx.indexIn(text, pos)) != -1) {
        m_modbusLineData.m_nodesRes.append(rx.cap().toInt());
        pos += rx.matchedLength();
    }
    m_modbusLineData.m_nodesNoRes.clear();
    text = lineEdit_numbersNoRes->text();
    while ((pos = rx.indexIn(text, pos)) != -1) {
        m_modbusLineData.m_nodesNoRes.append(rx.cap().toInt());
        pos += rx.matchedLength();
    }
}

void TestWindows::DialogMODBUSLine::on_lineEdit_name_textChanged(const QString &arg1)
{
    m_modbusLineData.m_name = arg1;
}

void TestWindows::DialogMODBUSLine::on_lineEdit_numbersRes_textChanged(const QString &arg1)
{
    int pos = 0;
    QRegExp rx("\\d+");
    m_modbusLineData.m_nodesRes.clear();
    while ((pos = rx.indexIn(arg1, pos)) != -1) {
        m_modbusLineData.m_nodesRes.append(rx.cap().toInt());
        pos += rx.matchedLength();
    }
}

void TestWindows::DialogMODBUSLine::on_lineEdit_numbersNoRes_textChanged(const QString &arg1)
{
    int pos = 0;
    QRegExp rx("\\d+");
    m_modbusLineData.m_nodesNoRes.clear();
    while ((pos = rx.indexIn(arg1, pos)) != -1) {
        m_modbusLineData.m_nodesNoRes.append(rx.cap().toInt());
        pos += rx.matchedLength();
    }
}

void TestWindows::DialogMODBUSLine::onSearchDeviceRunningChanged(const bool running)
{
    if(running)
        textEdit_result->clear();
    else
        textEdit_result->append("Поиск завершен!");
}

void TestWindows::DialogMODBUSLine::on_pushButton_getFile_clicked()
{
    QString text;
    {
        QSettings settings(qApp->organizationName(), qApp->applicationName());
        settings.beginGroup("DialogModbusLine");
        text = settings.value("FilePath").toString();
        settings.endGroup();
    }
    text = QFileDialog::getOpenFileName(this, tr("Открыть файл"), text, "", 0,QFileDialog::ReadOnly);
    lineEdit_filePath->setText(text);
    {
        QSettings settings(qApp->organizationName(), qApp->applicationName());
        settings.beginGroup("DialogModbusLine");
        settings.setValue("FilePath", text);
        settings.endGroup();
    }
    QSettings settings(text, QSettings::IniFormat, this);
    settings.setIniCodec("CP866");
    settings.beginGroup("Data Manager Options");
    text = settings.value("project").toString();
    text.truncate(text.indexOf('#'));
    lineEdit_name->setText(text.simplified());
    settings.endGroup();
    foreach (QString group, settings.childGroups())
        if(group.contains("card driver"))
        {
            settings.beginGroup(group);
            if(settings.contains("SLAVE_RESERV"))
            {
                lineEdit_numbersRes->setText(settings.value("SLAVE_RESERV").toStringList().join(' '));
            }
            if(settings.contains("SLAVE_ALL"))
            {
                QString textAll;
                foreach(QString str, settings.value("SLAVE_ALL").toStringList())
                {
                    if(!lineEdit_numbersRes->text().contains(str))
                        textAll += str + ' ';
                }

                lineEdit_numbersNoRes->setText(textAll);
            }
            settings.endGroup();
        }
}
