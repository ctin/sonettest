﻿#include "parentwindow.h"
#include "macro.h"

using namespace TestWindows;

ParentWindow::ParentWindow(DeviceList *pDeviceList, QWidget *parent)
    : QDialog(parent), m_sleepTime(10), m_pDeviceList(pDeviceList)
{
    setModal(true);
    m_lastChannel = m_channel = -1;
    m_slot = -1;
    m_deviceNum = -1;
    m_deviceCurrentID = -1;
    m_pInputDevice = 0;
    connect(this, SIGNAL(deviceCurrentIDChanged(int)), this, SLOT(ondeviceCurrentIDChanged()), Qt::DirectConnection);
    m_running = true;
    setstartFlags(SurveyNeither);
    m_isOnline = true;
}

ParentWindow::~ParentWindow()
{

    Sleepy::msleep(20);
    m_pDeviceList->settargetDeviceIndex(DeviceList::All);
    if(m_pInputDevice)
        m_pInputDevice->m_underTests = false;
}

void ParentWindow::ondeviceCurrentIDChanged()
{
    DeviceWithData *pDevice = m_pDeviceList->getDevice(deviceCurrentID());
    m_pInputDevice = pDevice;
    setdeviceNum(pDevice->deviceNum());
    onSlotChanged();
}

void ParentWindow::onSlotChanged()
{
    if(slot() < 0 || slot() > SLOTS_COUNT - 1)
        return;
    Q_ASSERT(m_pInputDevice->deviceIDs().size() >= SLOTS_COUNT);
    setdeviceID(m_pInputDevice->deviceIDs().at(slot()));
    Q_ASSERT(m_pInputDevice->types().size() >= SLOTS_COUNT);
    settype(m_pInputDevice->types().at(slot()));
    Q_ASSERT(m_pInputDevice->channelsCount().size() >= SLOTS_COUNT);
    setchannelsCount(m_pInputDevice->channelsCount().at(slot()) - g_parameterStorage.getDchan(deviceID()));
}

void ParentWindow::surveyDelay(int delayTime)
{
    for(int i = 0;  i < delayTime / m_sleepTime && running();  i++)
        Sleepy::msleep(m_sleepTime);
}

void ParentWindow::onHardwareError()
{
    if(m_pInputDevice->hardwareError().isEmpty())
        return;
    abort();
    QMessageBox::warning(qApp->activeWindow(), "Ошибка порта!", "Произошла ошибка подключения к COM порту! Работа будет остановлена.");
}

void ParentWindow::abort()
{
    m_running = false;
    QMetaObject::invokeMethod(this, "reject", Qt::QueuedConnection);
}

bool ParentWindow::request(QByteArray req, ByteArray *ans, DeviceWithData *pDevice)
{
    if(!m_running)
        return false;
    if(!pDevice)
        pDevice = m_pInputDevice;
    if(pDevice->request(req, ans))
        return true;
    else
    {
        abort();
        return false;
    }
}

bool ParentWindow::request(RequestFunc func, DeviceWithData *pDevice)
{
    if(!m_running)
        return false;
    if(!pDevice)
        pDevice = m_pInputDevice;
    if((pDevice->*(func))())
        return true;
    else
    {
        abort();
        return false;
    }
}

bool ParentWindow::beforeShow()
{
    int pos = DeviceList::Neither;
    if(startFlags() & SurveyAll)
        pos = DeviceList::All;
    else if(startFlags() & SurveyCurrent)
        pos = deviceCurrentID();
    else
        pos = DeviceList::Neither;
    m_pDeviceList->settargetDeviceIndex(pos);
    if(m_pInputDevice)
        connect(m_pInputDevice, SIGNAL(hardwareErrorChanged(QString)), this, SLOT(onHardwareError()));
    if(m_isOnline)
        m_pInputDevice->m_underTests = true;
    return true;
}

int ParentWindow::slot() const
{
    return m_slot;
}

void ParentWindow::setslot(const int slot)
{
    if(m_slot == slot)
        return;
    m_slot = slot;
    onSlotChanged();
    emit slotChanged(slot);
}

PROPERTY_CPP(int, ParentWindow, deviceCurrentID)
PROPERTY_CPP(int, ParentWindow, deviceNum)
PROPERTY_CPP(int, ParentWindow, deviceID)
PROPERTY_CPP(int, ParentWindow, type)
PROPERTY_CPP(int, ParentWindow, channelsCount)
PROPERTY_CPP(int, ParentWindow, startFlags)
PROPERTY_CPP(bool, ParentWindow, running)

int ParentWindow::READ_PROPERTY(channel)

void ParentWindow::setchannel(const int channel)
{
    if(m_channel == channel)
        return;
    m_lastChannel = m_channel;
    m_channel = channel;
    emit channelChanged(channel);
    emit channelChanged(QVariant::fromValue(channel));
}
