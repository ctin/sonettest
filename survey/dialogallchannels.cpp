﻿#include "dialogallchannels.h"
#include "core.h"

using namespace TestWindows;

DialogAllChannels::DialogAllChannels(DeviceList *pDeviceList, QWidget *parent) :
    ParentWindow(pDeviceList, parent)
{
    setWindowTitle("Обзор всех каналов");
    m_pCentralView = new QDeclarativeView(this);
    m_pCentralView->rootContext()->setContextProperty("backgroundColor", "transparent");
    m_pCentralView->setResizeMode(QDeclarativeView::SizeRootObjectToView);
    m_pCentralView->setStyleSheet("background-color: transparent");
    m_pCentralView->setMinimumWidth(800);
    resize(800, 400);
    QGridLayout *pLayout = new QGridLayout(this);
    pLayout->addWidget(m_pCentralView);
    setstartFlags(SurveyAll);
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("DialogAllChannels");
    resize(settings.value("size", QSize(800, 400)).toSize());
    move(settings.value("pos").toPoint());
    settings.endGroup();
}

void DialogAllChannels::closeEvent(QCloseEvent *)
{
    QSettings settings(qApp->organizationName(), qApp->applicationName());
    settings.beginGroup("DialogAllChannels");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
}

bool DialogAllChannels::beforeShow()
{
    ParentWindow::beforeShow();
    m_pCentralView->rootContext()->setContextProperty("core", QVariant::fromValue(Core::instance()));
    m_pCentralView->rootContext()->setContextProperty("deviceList", QVariant::fromValue(m_pDeviceList));
#ifdef FAKE_SONET
    m_pCentralView->rootContext()->setContextProperty("fake", QVariant::fromValue(true));
#else
    m_pCentralView->rootContext()->setContextProperty("fake", QVariant::fromValue(false));
#endif
    m_pCentralView->setSource(QUrl(QString::fromUtf8("qrc:qml/ControllerForSurvey.qml")));
    return true;
}
