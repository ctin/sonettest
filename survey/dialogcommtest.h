﻿#ifndef DIALOGCOMMTEST_H
#define DIALOGCOMMTEST_H

#include "ui_dialogcommtest.h"
#include "parentwindow.h"
#include "searchdevice.h"

namespace TestWindows {
class DialogCommTest : public QDialog, private Ui::DialogCommTest
{
    Q_OBJECT

public:
    explicit DialogCommTest(DeviceList *pDeviceList, QWidget *parent = 0);
    ~DialogCommTest();
    bool beforeShow();
private:
    SearchDevice *m_pSearchDevice;
    DeviceList *m_pDeviceList;
private slots:
    void refreshPortList();
    void on_pushButton_start_clicked();
    void on_pushButton_stop_clicked();
    void onDeviceFounded(int bprState, int rateIndex, int deviceNum, int autoSearch, QString port);
    void onDeviceFailed(int rateIndex, int deviceNum, int autoSearch, QString port, QString lastError);
    void refreshProgressBarFomat();
    void on_spinBox_speed_valueChanged(int arg1);
    void onError();
};
}

#endif // DIALOGCOMMTEST_H
