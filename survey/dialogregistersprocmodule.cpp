﻿#include "dialogregistersprocmodule.h"

using namespace TestWindows;

DialogRegistersProcModule::DialogRegistersProcModule(DeviceList *pDeviceList, QWidget *parent) :
    ParentWindow(pDeviceList, parent)
{
    setWindowTitle("Проверка регистров диагностики");
    QGridLayout *pLayout = new QGridLayout(this);
    QStringList strList;
    strList << "Версия прошивки"
            << "Ошибки CRC"
            << "Зарезервировано"
            << "Перезагрузок"
            << "Ошибок FLASH"
            << "Ошибок UART"
            << "Отсутствующих функций"
            << "Ошибок длины запроса"
            << "Превышения таймаутов"
            << "Ошибок модулей"
            << "Ошибок SPI"
            << "Время таймаута"
            << "Ошибок EEEPROM в регистрах"
            << "Ошибок EEEPROM в таймаутах"
            << "Контрольная сумма: регистры"
            << "Контрольная сумма: таймауты";

    for(int i = 0;  i < strList.size();  i++)
    {
        QLabel *pLabel = new QLabel(strList.at(i));
        pLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
        pLayout->addWidget(pLabel, i, 0);
        QLCDNumber *pLCDNumber = new QLCDNumber(this);
        pLCDNumber->setMinimumHeight(30);
        pLayout->addWidget(pLCDNumber, i, 1);
        m_LCDNumbers.append(pLCDNumber);
    }
    setstartFlags(SurveyNeither);
}

DESTRUCT_FUNC_CLASS(DialogRegistersProcModule)

bool DialogRegistersProcModule::beforeShow()
{
    ParentWindow::beforeShow();
    m_future = QtConcurrent::run(this, &DialogRegistersProcModule::__functionStart);
    return true;
}

void DialogRegistersProcModule::__functionStart()
{
    START_FUNC
    PortOpener opener(m_pInputDevice);
    ByteArray ans;
    float ver;
    if(request(MODBUS_get_version(m_pInputDevice->getCommandNamePtr()), &ans))
    {
        ver = ans.at(0) + ans.at(1) * 1.0 / 10;
        QMetaObject::invokeMethod(this, "displayVer", Qt::QueuedConnection, Q_ARG(float, ver));
    }
    if(request(MODBUS_get_registers(m_pInputDevice->getCommandNamePtr()), &ans) && running())
        QMetaObject::invokeMethod(this, "displayData", Qt::QueuedConnection, Q_ARG(ByteArray, ans));
    END_FUNC
}

void DialogRegistersProcModule::displayVer(float version)
{
    m_LCDNumbers.at(0)->display(version);
}

void DialogRegistersProcModule::displayData(ByteArray ans)
{
    for (int i = 0;  i < 10;  i++)
        m_LCDNumbers.at(i + 1)->display(ans.at(i) & 0xFF);
    m_LCDNumbers.at(11)->display(ushort(ans.at(10) << 8) + (ans.at(11) & 0xFF));
    for (int i = 11;  i < 15;  i++)
        m_LCDNumbers.at(i + 1)->display(ans.at(i + 1) & 0xFF);
}
